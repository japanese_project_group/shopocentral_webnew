// MUI Components
import { Typography } from "@mui/material";

export default function ErrorMessage({
    errors,
    name,
    type,
}: {
    errors: any;
    name: any;
    type: any;
}) {
    // if (!errors[name] || errors[name].type !== type) {
    //     return null;
    // }

    return (
        <Typography
            sx={{ color: "#ff0000", fontSize: "12px", m: 1, ml: 3, opacity: 0.75 }}
        >
            {errors}
        </Typography>
    );
}