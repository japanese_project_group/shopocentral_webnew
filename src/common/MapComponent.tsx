// Third party library
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
// Build in library
import React from "react";
import { LocationInterface } from "src/@types/common";

const google_api_key = process.env.NEXT_PUBLIC_GOOGLE_API_KEY;
const containerStyle = {
  width: "100%",
  height: "300px",
  marginBottom: "20px",
};

interface MapComponentProps {
  location: { lat: number; lng: number };
  zoom: number;
  setLocation: React.Dispatch<React.SetStateAction<LocationInterface>>
}

const MapComponent: React.FC<MapComponentProps> = ({
  location,
  zoom,
  setLocation,
}) => {
  const handleMarkerDragEnd = (e: any) => {
    setLocation({
      lat: e.latLng.lat(),
      lng: e.latLng.lng(),
    });
  };
  return (
    <LoadScript googleMapsApiKey={google_api_key ?? ""}>
      <GoogleMap
        center={location}
        zoom={zoom}
        mapContainerStyle={containerStyle}
        onClick={handleMarkerDragEnd}
      >
        <Marker
          position={location}
          draggable={true}
        />
      </GoogleMap>
    </LoadScript>
  );
};

export default MapComponent;
