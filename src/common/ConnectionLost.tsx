import RefreshIcon from '@mui/icons-material/Refresh';
import { Box, Button, Typography } from "@mui/material";
import Image from "next/image";
import connectionLost from '../../public/connectionLost.png';

const ConnectionLost = () => {

    const handleReload = () => {
        window.location.reload();
    }

    return (
        <Box className={'flex-col-center h-full w-full'} gap={'0.9rem'}>
            <Box height={260} width={260}>
                <Image
                    src={connectionLost}
                    alt="Access Denied"
                    width={260}
                    height={260}
                    layout="responsive"
                />
            </Box>
            <Typography variant="body1" fontSize={'1.1rem'} color="#777777">Connection Lost</Typography>
            <Typography variant="h6" color="#000" fontSize={'1.5rem'} width={280} textAlign={'center'}>Seems like the internet is off!</Typography>
            <Button
                variant="contained"
                color="primary"
                startIcon={<RefreshIcon />}
                onClick={handleReload}
            >
                Reload
            </Button>
        </Box>
    )
}


export default ConnectionLost;