import RefreshIcon from '@mui/icons-material/Refresh';
import { Box, Button, Typography } from "@mui/material";
import Image from "next/image";
import serverDown from '../../public/AccessDenide.png';

const ServerDown = () => {

    const handleReload = () => {
        window.location.reload();
    }

    return (
        <Box className={'flex-col-center h-full w-full'} gap={'0.9rem'}>
            <Box height={260} width={260}>
                <Image
                    src={serverDown}
                    alt="Server Down"
                    width={260}
                    height={260}
                    layout="responsive"
                />
            </Box>
            <Typography variant="body1" fontSize={'1.1rem'} color="#777777">Server Down</Typography>
            <Typography variant="h6" color="#000" fontSize={'1.5rem'} width={280} textAlign={'center'}>Seems like you got issue with server!</Typography>
            <Button
                variant="contained"
                color="primary"
                startIcon={<RefreshIcon />}
                onClick={handleReload}
            >
                Reload
            </Button>
        </Box>
    )
}
export default ServerDown;