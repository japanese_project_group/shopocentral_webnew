// MUI Components
import AddPhotoAlternateOutlinedIcon from "@mui/icons-material/AddPhotoAlternateOutlined";
import { Box, Typography } from "@mui/material";

// const ImageStyle  {
//     maxWidth: "100%",
//     maxHeight: "100";
// border - radius: 8px; /* Add border-radius for rounded corners */
// box - shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
// }

const ImageComponent = (src: any, alt: any) => {
    const height = "100px";
    const width = "100px";
    return (
        <Box
            className="custom-image-container"
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
            height={height}
            width={width}
            bgcolor={"#f0f0f0"}
            borderRadius={"6px"}
        >



            {!src ? (
                <img src={src} alt={alt} className="custom-image" />
            ) : (
                <Box display={"flex"} flexDirection={"column"} alignItems={"center"}>
                    <AddPhotoAlternateOutlinedIcon sx={{ fontSize: "3rem" }} />
                    <Typography sx={{ fontSize: "12px" }}>Add Image</Typography>
                </Box>
            )}
        </Box>
    );
};

export default ImageComponent;
