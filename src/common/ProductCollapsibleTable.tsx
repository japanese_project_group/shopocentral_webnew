import EditNoteTwoToneIcon from '@mui/icons-material/EditNoteTwoTone';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Image from 'next/image';
import * as React from 'react';
import NoImage from './NoImage';

function Row(row: any) {
    const [open, setOpen] = React.useState(false);

    const products = row?.row?.MasterProduct;

    const uniqueVariationTypes = Array.from(
        new Set(
            row?.row?.productVariation.flatMap((data: any) =>
                data.VariationWithProduct.Variations.map((aData: any) =>
                    aData.VariationItem.VariationType.name
                )
            )
        )
    )
    const dateTimeString = products?.MasterOffers?.createdAt;
    const dateOnly = dateTimeString?.slice(0, 10);

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {products.name}
                </TableCell>
                <TableCell align="left">{products?.Brand?.name_en}</TableCell>
                <TableCell align="left">{products?.businessAccessId}</TableCell>
                <TableCell align="left">
                    {products?.MasterOffers?.purchasing_price ?
                        `¥ ${products?.MasterOffers?.purchasing_price}`
                        :
                        "No price found"
                    }
                </TableCell>
                <TableCell align="left">
                    {products?.image?.masterImages[0] ?
                        <Image src={products?.image?.masterImages[0]} alt="Product Image" height={"50px"} width={"50px"} />
                        :
                        <NoImage />
                    }
                </TableCell>
                <TableCell align="left">{row?.row?.status}</TableCell>
                <TableCell align="left">{row?.row?.User?.name}</TableCell>
                <TableCell align="left">{dateOnly}</TableCell>
                <TableCell align="left">
                    <Box display={"flex"} gap={"10px"}>
                        <IconButton>
                            <RemoveRedEyeOutlinedIcon />
                        </IconButton>
                        <IconButton>
                            <EditNoteTwoToneIcon />
                        </IconButton>
                    </Box>
                </TableCell>
            </TableRow>
            <TableRow sx={{ bgcolor: "#fafafa" }}>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        {uniqueVariationTypes.length > 0 ?
                            <Box sx={{ margin: 1 }}>
                                <Typography variant="h6" gutterBottom component="div">
                                    Variation
                                </Typography>
                                <Table size="small" aria-label="purchases">
                                    <TableHead>
                                        <TableRow>
                                            {uniqueVariationTypes.map((variationType: any, index: any) => (
                                                <TableCell key={index} align="left" sx={{ textTransform: "capitalize" }}>
                                                    {variationType}
                                                </TableCell>
                                            ))}
                                            <TableCell>SKU</TableCell>
                                            <TableCell>Product ID</TableCell>
                                            <TableCell align="left">Product Condition</TableCell>
                                            <TableCell align="left">Price</TableCell>
                                            <TableCell align="left">Image</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {row?.row?.productVariation?.map((historyRow: any) => (
                                            <TableRow key={historyRow.id}>
                                                {historyRow?.VariationWithProduct?.Variations?.map((SingleVardata: any) => (
                                                    <TableCell align="left" sx={{ textTransform: "capitalize" }}>
                                                        {SingleVardata?.VariationItem?.name}
                                                    </TableCell>
                                                ))}
                                                <TableCell component="th" scope="row">
                                                    {historyRow.VariationWithProduct.sku}
                                                </TableCell>
                                                <TableCell>{historyRow.VariationWithProduct.product_id}</TableCell>
                                                <TableCell>{historyRow.VariationWithProduct.product_condition}</TableCell>
                                                <TableCell align="left">¥ {historyRow.VariationWithProduct?.price}</TableCell>
                                                <TableCell align="left">
                                                    {historyRow?.VariationWithProduct?.vrImages?.vrImages[0] ?
                                                        <Image src={historyRow?.VariationWithProduct?.vrImages?.vrImages[0]} height={"50px"} width={"50px"} alt='variation image' />
                                                        :
                                                        <NoImage />
                                                    }
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Box>
                            :
                            <Typography variant='body1' display={"flex"} justifyContent={"center"} bgcolor={"#f00000310"} padding={"1rem"}>No Variation founds</Typography>
                        }
                    </Collapse>
                </TableCell>

            </TableRow>
        </React.Fragment>
    );
}


const ProductCollapsibleTable = (myProductData: any) => {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow className="data-grid-header-classname">
                        <TableCell />
                        <TableCell>Name</TableCell>
                        <TableCell>Brand</TableCell>
                        <TableCell>Category</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell>Image</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell>Added By</TableCell>
                        <TableCell>Date</TableCell>
                        <TableCell>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {myProductData?.myProductData.map((row: any) => (
                        <Row key={row.name} row={row} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
export default ProductCollapsibleTable;