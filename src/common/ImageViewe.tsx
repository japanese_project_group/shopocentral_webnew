// Third party library
import Lightbox from "yet-another-react-lightbox";
import Captions from "yet-another-react-lightbox/plugins/captions";
import "yet-another-react-lightbox/plugins/captions.css";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import Video from "yet-another-react-lightbox/plugins/video";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "yet-another-react-lightbox/styles.css";

const ImageViewer = ({
  slideImage,
  openImageModal,
  setOpenImageModal,
}: {
  slideImage: any;
  openImageModal: boolean;
  setOpenImageModal: any;
}) => {
  return (
    <Lightbox
      open={openImageModal}
      close={() => setOpenImageModal(false)}
      slides={slideImage}
      index={0}
      plugins={[Captions, Fullscreen, Slideshow, Thumbnails, Video, Zoom]}
    />
  );
};

export default ImageViewer;
