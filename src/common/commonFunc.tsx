// const [allCountryData, setAllCountryData] = useState<any>();
// const CountryData = () => {
//   useEffect(() => {
//     fetch(
//       "https://valid.layercode.workers.dev/list/countries?format=select&flags=true&value=code"
//     )
//       .then((response) => response.json())
//       .then((data) => {
//         setAllCountryData(data.countries);
//       });
//   }, []);
// }

// export default CountryData;

// function ParentComponent() {
//   const [allCountryData, setAllCountryData] = useState<any>();

//   useEffect(() => {
//     fetch(
//       "https://valid.layercode.workers.dev/list/countries?format=select&flags=true&value=code"
//     )
//       .then((response) => response.json())
//       .then((data) => {
//         setAllCountryData(data.countries);
//       });
//   }, []);

//   // Pass allCountryData as a prop to ChildComponent
//   return <ChildComponent allCountryData={allCountryData} />;
// }

// export default ParentComponent;


export function removeDuplicates(arr: any) {
  return arr?.filter((item: any, index: number) => arr.indexOf(item) === index);
}



export const randomPasswordGenerator = () => {
  var charsCapitalAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var specialCharacter = "!@#$%^&*()";
  var passwordLength = 2;
  let value = "";
  const generator = (chars?: any) => {
    for (var i = 0; i <= passwordLength; i++) {
      var randomNumber = Math.floor(Math.random() * chars.length);
      value += chars.substring(randomNumber, randomNumber + 1);
      return value;
    }
  };
  const password1 = generator(charsCapitalAlphabet);
  const password2 = generator(specialCharacter);
  const finalPassword = `${password1}${Math.random()
    .toString(36)
    .slice(-8)}${password2}`;
  return finalPassword;
};

export const dateSorting = (a: any, b: any) =>
  new Date(a.createdAt) > new Date(b.createdAt)
    ? -1
    : new Date(a.createdAt) < new Date(b.createdAt)
      ? 1
      : 0;

export const optTimer = (counter: any, setCounter: any, delay: number) => {
  const timer: any =
    counter > 0 &&
    setInterval(() => setCounter((counter: number) => (counter -= 1)), delay);
  return () => clearInterval(timer);
};


export const generateRandomNumber = () => {
  let min = 100000;
  let max = 999999;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

