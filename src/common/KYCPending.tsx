import PaidOutlinedIcon from '@mui/icons-material/PaidOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import { Box, Button, Card, Grid, Icon, Typography } from '@mui/material';
import Image from 'next/image';
import { useRouter } from 'next/router';
import jumpingImg from '../../public/jumping.svg';
import masterProductTab from "../../public/masterProducttab.svg";
import workPending from '../../public/workInProgress.png';

import CalendarComponent from '../components/dashboard/Calender';

const KYCPending = ({ profileStatus, allProfileData }: any) => {
    const router = useRouter();

    const handleViewKYC = () => {
        router.push('/kycforms/displayforms/individual');
    }
    const handleSubmitKYC = () => {
        router.push('/kycforms/businesskyc');
    }

    const cardList = [
        { name: 'Business', icon: <TrendingUpIcon /> },
        { name: 'Staff', icon: <PersonOutlineOutlinedIcon /> },
        { name: 'Product', icon: <Image src={masterProductTab} style={{ opacity: "0.6" }} /> },
        { name: 'Reward & Offer', icon: <PaidOutlinedIcon /> },
        { name: 'Shop', icon: <PaidOutlinedIcon /> },
    ];

    {/* {profileStatus === 'FINITIAL' ? (
        <BusinessKycStatusMessage status="FINITIAL" />
      ) : profileStatus === ('PENDING' || 'UNVERIFIED') ? (
        <BusinessKycStatusMessage
          status="PENDING"
          message={allDataProfile?.data?.identityStatus?.[0]?.message}
        />
      ) : (
        profileStatus === 'INITIAL' && <BusinessKycStatusMessage status="INITIAL" />
      )} */}
    console.log("allDataProfile?.data?.identityStatus?.[0]?.message", allProfileData?.data?.identityStatus?.[0]?.message, profileStatus);
    return (
        <Box boxShadow={" -12px -12px 20px 0px #FFFFFFCC"}
            borderRadius={'20px'}
            padding={'20px 2rem 2.5rem 2rem'}
            bgcolor={"#fff"}
        >
            <Grid container spacing={5}>
                <Grid item lg={8} md={8} sm={12} xs={12} spacing={5}>
                    <Box className='flex-center' sx={{ height: '260px', position: 'relative', zIndex: '1' }}>
                        <Box bgcolor={'#eee'} width={'100%'} textAlign={'left'} minWidth={'320px'} padding={'45px 22px 24px'}>
                            <Typography variant="body1" fontWeight={'700'} fontSize={'1.5rem'} textTransform={'capitalize'} color="#000">Hello ! {allProfileData?.data?.user.firstName}</Typography>
                            {(profileStatus === 'FINITIAL') ? (
                                <Typography className='kyc_Message'>You Need a verified Business kyc form to have complete access of this application.</Typography>
                            ) : (profileStatus === 'PENDING' || profileStatus === 'UNVERIFIED') ? (
                                <>
                                    <Typography className='kyc_Message'>Your status of KYC IS still {profileStatus}. {allProfileData?.data?.identityStatus?.[0]?.message}</Typography>
                                    <Button
                                        sx={{ marginTop: '0.5rem' }}
                                        variant="contained"
                                        color="primary"
                                        onClick={handleViewKYC}
                                    >
                                        View KYC
                                    </Button>
                                </>
                            ) : (profileStatus === 'INITIAL' &&
                                <>
                                    <Typography className='kyc_Message' width={400} >Your Business KYC form is in progress for admin review. Please check once in a while</Typography>
                                    <Button
                                        sx={{ marginTop: '0.5rem' }}
                                        variant="contained"
                                        color="primary"
                                        onClick={handleViewKYC}
                                    >
                                        View KYC
                                    </Button>
                                </>
                            )}
                        </Box>
                        <Box height={260} width={260} position={'absolute'} zIndex={'10'} right={'20%'} top={'10%'}>
                            <Image
                                src={workPending}
                                alt="No data found"
                                width={260}
                                height={260}
                                layout="responsive"
                            />
                        </Box>
                    </Box>
                </Grid>

                <Grid item lg={4} md={4} sm={12} xs={12}>
                    <Card sx={{ px: 3, py: 2, mb: 10, boxShadow: "0px 4px 24px 0px #0000000F" }} >
                        <CalendarComponent />
                    </Card>
                </Grid>
            </Grid>
            {/* FINITIAL */}
            {profileStatus === 'FINITIAL' &&
                <Box className='flex-start' margin={'auto'} width={"92%"} sx={{ height: '212px', position: 'relative', zIndex: '1' }}>
                    <Box width={285} height={175} marginRight={'-3%'}>
                        <Image
                            src={jumpingImg}
                            alt="No data found"
                            width={'215px'}
                            height={175}
                            layout="responsive"
                        />
                    </Box>
                    <Box className={'flex-around'} bgcolor={'#1976d21c'} width={'100%'} textAlign={'left'} minWidth={'320px'} padding={'27px'}>
                        <Typography variant="h6" color="#777" fontSize={'1.15rem'} width={320} >You need to complete the KYC to signup for Business Account</Typography>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={handleSubmitKYC}
                        >
                            Proceed
                        </Button>
                    </Box>
                </Box>
            }
            <Box marginTop={'2.5rem'}>
                <Typography variant="body1" fontWeight={'400'} fontSize={'1.9rem'} color="#1A76D2">Our Features</Typography>
                <Box className={'flex-start'} gap={'5%'} marginTop={'2.6rem'}>
                    {cardList.map((item, index) => (
                        <Box key={index} className={'flex-col-center'} borderRadius={'20px'} bgcolor={'#eee'} height={120} width={120} boxShadow={'10px 10px 12px 0px #A6B4C833'}
                        >
                            <Icon className="feature-icon" sx={{ fontSize: '3rem', marginBottom: '10px' }}>{item.icon}</Icon>
                            <Typography variant="h6" color={'#2d2d2d'} width='78px' textAlign={'center'} fontSize={'1rem'}>
                                {item.name}
                            </Typography>
                        </Box>
                    ))}
                </Box>
            </Box>
        </Box>
    )
}

export default KYCPending;