// MUI Components
import { Delete, Edit, Visibility } from '@mui/icons-material';
import { IconButton } from '@mui/material';

interface Props {
  iconType: string;
}

const iconStyle = {
  color: '#BBBBC1',
};

const IconSelector = ({ iconType }: Props) => {
  const getIconType = () => {
    switch (iconType) {
      case 'view':
        return <Visibility sx={iconStyle} />;
      case 'edit':
        return <Edit sx={iconStyle} />;
      case 'delete':
        return <Delete sx={iconStyle} />;
      default:
        return null;
    }
  };

  return <IconButton>{getIconType()}</IconButton>;
};

export default IconSelector;
