// Build in library
import * as React from "react";

// MUI Components
import { createTheme } from "@mui/material/styles";

// Third party library
import { useDispatch } from "react-redux";
import { setTheme } from "../../redux/features/themeSlice";

const ThemeSwitcher = () => {
  const dispatch = useDispatch();
  const [darkMode, setDarkMode] = React.useState<any>("");
  // const color = localStorage.getItem("theme"); //get val from redux
  const color = "dark";
  if (color === null) {
    setDarkMode("light");
  }
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setDarkMode((prevMode: string) =>
          prevMode === "light" ? "dark" : "light"
        );
      },
    }),
    []
  );
  React.useEffect(() => {
    dispatch(setTheme(darkMode));
    // localStorage.setItem("theme", darkMode); //set val to redux
    // const colorTheme = localStorage.getItem("theme");
  }, [darkMode]);

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: darkMode,
        },
      }),
    [darkMode]
  );
};
export default ThemeSwitcher;
