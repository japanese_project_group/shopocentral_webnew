// third party library
import { Typography } from '@mui/material';
import Link from 'next/link';


interface Props {
  text: string;
  isDetail: boolean;
  href: string;
  last: boolean;
}

export default function Crumb({
  text,
  isDetail = false,
  href,
  last = false,
}: Props) {

  // The last crumb which isdetail is rendered as detail
  if (isDetail && last) {
    return (
      <Typography color="text.primary">
        {'current'}
      </Typography>
    );
  }
  if (last && text === 'dashboard') {
    return null;
  }

  // The last crumb is rendered as normal text since we are already on the page
  if (last || text === 'edit') {
    return (
      <Typography color="text.primary">
        {/* {text} */}
      </Typography>
    );
  }

  // All other crumbs will be rendered as links that can be visited
  return (
    <Link color="primary" href={href} passHref>
      <Typography sx={{ cursor: 'pointer' }} color="primary">
        {text}
      </Typography>
    </Link>
  );
}
