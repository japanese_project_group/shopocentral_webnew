// third party library
import { NavigateNext } from '@mui/icons-material';
import { Breadcrumbs } from '@mui/material';
// BUild in library
import { useRouter } from 'next/router';
import React from 'react';
// Components
import Crumb from './Crumb';

export default function NextBreadcrumbs() {
  const router = useRouter();

  const breadcrumbs = React.useMemo(
    function generateBreadcrumbs() {
      const asPathWithoutQuery = router.asPath.split('?')[0];
      const asPathNestedRoutes = asPathWithoutQuery
        .split('/')
        .filter((v) => v.length > 0);

      const crumblist = asPathNestedRoutes.map((subpath, idx) => {
        const href = '/' + asPathNestedRoutes.slice(0, idx + 1).join('/');

        return { href, text: subpath };
      });

      return crumblist;
    },
    [router.asPath]
  );

  return (
    <Breadcrumbs
      aria-label="breadcrumb"
      separator={<NavigateNext fontSize="small" />}
    >
      {/*
        Iterate through the crumbs, and render each individually.
        We "mark" the last crumb to not have a link.
      */}
      {breadcrumbs.map((crumb, idx) => (
        <Crumb
          {...crumb}
          isDetail={/^[a-fA-F0-9]{24}$/.test(crumb?.text)}
          key={idx}
          last={idx === breadcrumbs.length - 1}
        />
      ))}
    </Breadcrumbs>
  );
}
