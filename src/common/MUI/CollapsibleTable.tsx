// Build in library
import * as React from "react";

// MUI Components
import { Box, Button, MenuItem, Select, TextField, Typography } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";

// components
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { productCondition } from "../commonData";

const CollapsibleTable = (colors: any, sizes: any, variationValues: any, sendDataToParent: any) => {
  const [allColors, setAllColors] = React.useState<any>(colors);
  const [variations, setVariations] = React.useState<any>([]);

  React.useEffect(() => {
    setAllColors(colors);
  }, [colors]);

  const initialVariations = {
    isChecked: '',
    color: '',
    size: "",
    sku: "",
    product_id: "",
    product_condition: "",
    quantity: "",
    price: "",
    offer_condition: "",
    sale_price: "",
    variation_image: "",
    sale_start: "",
    sale_end: '',
  };

  const handleVariationAdd = () => {
    const id = variations.length + 1;
    const newVariation = { id, ...initialVariations };
    setVariations([...variations, newVariation]);
  };

  const handleVariationChange = (index: any, field: any, value: any) => {
    const updatedVariations = [...variations];
    updatedVariations[index][field] = value;
    setVariations(updatedVariations);
  };

  const handleDeleteChecked = () => {
    const updatedVariations = variations.filter((variation: any) => !variation.isChecked);
    setVariations(updatedVariations);
  };

  const handleDeleteUnChecked = () => {
    const updatedVariations = variations.filter((variation: any) => variation.isChecked);
    setVariations(updatedVariations);
  }

  const sendDataToParentHandler = () => {

  }

  return (
    <>
      {allColors?.variationValues?.length > 0 &&
        <>
          <Box display={"flex"} gap={"0.5rem"} alignItems={"center"} margin={"2rem 1rem 1rem 0rem"}>
            <Button onClick={sendDataToParentHandler} variant="text" sx={{ boxShadow: "1", bgcolor: "#f1f1f1", color: "#202020", textTransform: "capitalize" }}>Apply Changes</Button>
            <Button onClick={handleDeleteChecked} variant="text" sx={{ boxShadow: "1", bgcolor: "#f1f1f1", color: "#202020", textTransform: "capitalize" }}>Delete Selected</Button>
            <Button onClick={handleDeleteUnChecked} variant="text" sx={{ boxShadow: "1", bgcolor: "#f1f1f1", color: "#202020", textTransform: "capitalize" }}>Undelete Selected</Button>
            <Typography variant="body1" sx={{ color: "#202020", textTransform: "capitalize" }}>{variations?.length} Variations</Typography>
            <Button onClick={() => handleVariationAdd()} variant="contained" color="primary" sx={{ boxShadow: "1", textTransform: "capitalize", marginLeft: "auto" }}>Add Variation</Button>
          </Box>
          <TableContainer component={Paper}>
            <Table
              aria-label="collapsible table"
              sx={{ minWidth: "1470px", overflowX: "scroll" }}
            >
              <TableHead>
                <TableRow>
                  <TableCell />

                  {allColors?.colors?.length > 0 &&
                    <TableCell align="left">Color</TableCell>
                  }
                  {allColors?.variationValues?.some((variation: any) => variation === "size") &&
                    <TableCell align="left">Size</TableCell>
                  }
                  <TableCell align="left">SKU</TableCell>
                  <TableCell align="left">Product ID</TableCell>
                  <TableCell align="left">Condition Type</TableCell>
                  <TableCell align="left">Your Price</TableCell>
                  <TableCell align="left">Quantity</TableCell>
                  {allColors?.colors?.length > 0 &&
                    <TableCell align="left">Image</TableCell>
                  }
                  <TableCell align="left">Offer Condition Note</TableCell>
                  <TableCell align="left">Sale Price</TableCell>
                  <TableCell align="left">Sale Start Date</TableCell>
                  <TableCell align="left">Sale End Date</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {variations?.map((variationData: any, index: any) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      <input
                        type="checkbox"
                        id={index}
                        name={variationData.id.toString()}
                        onChange={(e) => handleVariationChange(index, "isChecked", e.target.checked)}
                      />
                    </TableCell>

                    {allColors?.colors?.length > 0 &&
                      <TableCell component="th" scope="row">
                        <Select
                          labelId={`select-color-${index}`}
                          id={`select-color-${index}`}
                          value={variationData.color}
                          variant={"standard"}
                          onChange={(e) => handleVariationChange(index, "color", e.target.value)}
                          sx={{ minWidth: "110px" }}
                        >
                          {allColors?.colors?.map((selectedColor: any) => (
                            <MenuItem value={selectedColor}>{selectedColor}</MenuItem>
                          ))}
                        </Select>
                      </TableCell>
                    }
                    {allColors?.variationValues?.some((variation: any) => variation === "size") &&
                      <TableCell component="th" scope="row">
                        <TextField
                          variant="standard"
                          value={variationData?.size}
                          onChange={(e) =>
                            handleVariationChange(index, "size", e.target.value)
                          }
                        />
                      </TableCell>
                    }
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.sku}
                        onChange={(e) =>
                          handleVariationChange(index, "sku", e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.product_id}
                        onChange={(e) =>
                          handleVariationChange(index, "product_id", e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Select
                        value={variationData?.product_condition}
                        variant={"standard"}
                        onChange={(e) => handleVariationChange(index, "product_condition", e.target.value)}
                        sx={{ minWidth: "110px" }}
                      >
                        {productCondition?.map((data: any) => (
                          <MenuItem value={data.value}>{data.label}</MenuItem>
                        ))}
                      </Select>
                    </TableCell>
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.price}
                        onChange={(e) =>
                          handleVariationChange(index, "price", e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.quantity}
                        onChange={(e) =>
                          handleVariationChange(index, "quantity", e.target.value)
                        }
                      />
                    </TableCell>
                    {allColors?.colors?.length > 0 &&
                      <TableCell>
                        <Select
                          value={variationData?.variation_image}
                          variant={"standard"}
                          onChange={(e) => handleVariationChange(index, "variation_image", e.target.value)}
                          sx={{ minWidth: "110px" }}
                        >
                          {allColors?.colors?.map((selectedImg: any) => (
                            <MenuItem key={selectedImg} value={selectedImg}>{`${selectedImg} Image`}</MenuItem>
                          ))}
                        </Select>
                      </TableCell>
                    }
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.offerNote}
                        onChange={(e) =>
                          handleVariationChange(index, "offerNote", e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <TextField
                        id=""
                        variant="standard"
                        value={variationData?.salePrice}
                        onChange={(e) =>
                          handleVariationChange(index, "salePrice", e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <LocalizationProvider dateAdapter={AdapterMoment}>
                        <DatePicker
                          value={variationData.sale_start}
                          onChange={(date) => handleVariationChange(index, "sale_start", date)}
                          renderInput={(params: any) => (
                            <TextField
                              size="small"
                              {...params}
                              fullWidth
                            />
                          )}
                        />
                      </LocalizationProvider>
                    </TableCell>
                    <TableCell>
                      <LocalizationProvider dateAdapter={AdapterMoment}>
                        <DatePicker
                          value={variationData.sale_end}
                          onChange={(date) => handleVariationChange(index, "sale_end", date)}
                          renderInput={(params: any) => (
                            <TextField
                              size="small"
                              {...params}
                              fullWidth
                            />
                          )}
                        />
                      </LocalizationProvider>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      }
    </>
  );
}

export default CollapsibleTable;