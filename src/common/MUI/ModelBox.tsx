// Build in library
import * as React from "react";
// MUI Components
import { Stack } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  // width: { lg: "65%", sm: "70%", xs: "90%" },
  bgcolor: "background.paper",
  boxShadow: 24,
  // p: 4,
  borderRadius: 2,
};

export default function ModelBox(props: any) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      {!props?.image?.type?.includes("pdf") && <Button onClick={handleOpen} color='primary' variant='outlined'>
        View Document
      </Button>}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          {/* <Stack direction='row' justifyContent={"right"}>
            <Button onClick={handleClose} color='error'>
              <CloseIcon />
            </Button>
          </Stack> */}
          <Stack direction='column' justifyContent={"center"}>
            <img src={!!props.image?.length ? props?.image : URL.createObjectURL(props.image)} alt={props.alt} className="image-view" />
          </Stack>
        </Box>
      </Modal>
    </>
  );
}
