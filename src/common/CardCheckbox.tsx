import { Box, CardActions, CardContent, Typography } from '@mui/material';
import React, { useState } from 'react';
import { RatingComponent } from './Components';

interface CardCheckboxProps {
    cardData: {
        id: number;
        shopImage: { shopProfileImage: string };
        shopBranch: Array<{
            id: number;
            shopImage: { shopProfileImage: string };
            name_en: string;
            Category: any;
        }>;
        name_en: string;
        Category: any;
        // Other main shop properties...
    };
    checked: boolean;
    onCheckboxChange: (cardId: number) => void;
    onCheckboxBranchesChange: (branchId: number) => void;
}

const CardCheckbox: React.FC<CardCheckboxProps> = ({
    cardData,
    checked,
    onCheckboxChange,
    onCheckboxBranchesChange,
}) => {
    const [checkedItems, setCheckedItems] = useState<any>({});
    const [checkedBranches, setCheckedBranches] = useState<any>({});

    const handleCheckboxChange = (cardId: any) => {
        setCheckedItems((prev: any) => ({
            ...prev,
            [cardId]: !prev[cardId],
        }));
        onCheckboxChange(cardId);
    };

    const handleBranchCheckboxChange = (branchId: any) => {
        setCheckedBranches((prev: any) => ({
            ...prev,
            [branchId]: !prev[branchId],
        }));
        onCheckboxBranchesChange(branchId);
    };

    return (
        <Box display={'flex'} gap={'1rem'} alignItems={"flex-end"}>
            <label style={{ position: 'relative', width: '235px', display: 'block' }}>
                <input
                    type='checkbox'
                    checked={checkedItems[cardData.id] || false}
                    onChange={() => handleCheckboxChange(cardData.id)}
                    style={{ display: 'block', position: 'absolute', top: '29px', right: `10px` }}
                />
                <Box sx={{ marginTop: '2rem', maxWidth: '235px' }} boxShadow={checkedItems[cardData.id] ? '2px 2px 15px #00ff0050' : 1}>
                    <CardContent sx={{ width: '200px' }}>
                        {cardData.shopImage?.shopProfileImage && (
                            <img width={'200px'} height={"230px"} style={{ objectFit: "cover" }} src={cardData.shopImage.shopProfileImage} alt='Profile' />
                        )}
                        <Typography variant='body1' textOverflow={"ellipsis"}>{cardData?.name_en}</Typography>
                        <Box display={"flex"}>
                            {cardData.Category.map((category: any) => (
                                <Typography
                                    variant="h6"
                                    color="text.secondary"
                                    style={{ fontSize: "16px" }}
                                >
                                    {category.category.name_en ? category.category.name_en : "Category Name"}
                                </Typography>
                            ))}
                        </Box>
                        <CardActions style={{ marginTop: "-15px", marginLeft: "5px" }}>
                            <RatingComponent value={3} />
                        </CardActions>
                    </CardContent>
                </Box>
            </label>

            {cardData?.shopBranch &&
                cardData?.shopBranch?.map((data: any) => (
                    <label key={data.id} style={{ position: 'relative', width: '180px', display: 'block' }}>
                        <input
                            type='checkbox'
                            checked={checkedBranches[data.id] || false}
                            onChange={() => handleBranchCheckboxChange(data.id)}
                            style={{ display: 'block', position: 'absolute', top: '29px', right: `10px` }}
                        />

                        <Box sx={{ marginTop: '2rem', maxWidth: '235px' }} boxShadow={checkedItems[data.id] ? '2px 2px 15px #00ff0050' : 1}>
                            <CardContent sx={{ width: '160px', overflow: "hidden" }}>
                                {data.shopImage?.shopProfileImage ? (
                                    <img width={'160px'} height={"180px"} src={data.shopImage.shopProfileImage} alt='Profile' />) : (

                                    <img width={'160px'} height={"180px"} src={"https://img.freepik.com/free-vector/shop-with-sign-we-are-open_52683-38687.jpg"} alt='Profile' />
                                )}
                                <Typography variant='body1' textOverflow={"ellipsis"}>{data?.name_en}</Typography>
                                <Box display={"flex"}>
                                    {data.Category.map((category: any) => (
                                        <Typography
                                            variant="h6"
                                            color="text.secondary"
                                            style={{ fontSize: "16px" }}
                                        >
                                            {category.category.name_en ? category.category.name_en : "Category Name"}
                                        </Typography>
                                    ))}
                                </Box>
                                <CardActions style={{ marginTop: "-15px", marginLeft: "5px" }}>
                                    <RatingComponent value={3} />
                                </CardActions>
                            </CardContent>
                        </Box>
                    </label>
                ))
            }
        </Box >
    );
};

export default CardCheckbox;
