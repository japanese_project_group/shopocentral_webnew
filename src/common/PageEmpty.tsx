import { Box, Typography } from "@mui/material";
import Image from "next/image";
import noDataImg from '../../public/EmptyBox.png';

const PageEmpty = () => {
    return (
        <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'} width={'100%'} height={'100%'} gap={'0.9rem'}>
            <Box height={260} width={260}>
                <Image
                    src={noDataImg}
                    alt="No data found"
                    width={260}
                    height={260}
                    layout="responsive"
                />
            </Box>
            <Typography variant="body1" fontSize={'1.1rem'} color="#777777">No Data Found</Typography>
        </Box>
    )
}

export default PageEmpty;