import {
  Autocomplete,
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Chip,
  FormControl,
  Grid,
  InputAdornment,
  MenuItem,
  Modal,
  Rating,
  Select,
  Stack,
  TextField,
  TextareaAutosize,
  Typography,
} from "@mui/material";
import { useEffect, useMemo, useRef, useState } from "react";

import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { useGetDataFromPostalCodeQuery } from "../../redux/api/user/user";
// import Background from "../../public/background-glass.jpeg"
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Image from "next/image";

import AccountCircleRoundedIcon from "@mui/icons-material/AccountCircleRounded";
import { List, ListItem, ListItemButton } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { ErrorMessage, FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import moment from "moment";
import Router from "next/router";
import { useTranslation } from "react-i18next";
import { io } from "socket.io-client";
import Lightbox from "yet-another-react-lightbox";
import Captions from "yet-another-react-lightbox/plugins/captions";
import "yet-another-react-lightbox/plugins/captions.css";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import Video from "yet-another-react-lightbox/plugins/video";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "yet-another-react-lightbox/styles.css";

interface RatingComponentTypes {
  value: number;
}
interface ButtonComponentTypes {
  variant?: string | any;
  disabled?: true;
  data?: string;
  color?: string | any;
  size?: string | any;
  height?: string;
  fontSize?: string;
  onClickHandle?: any;
}
interface DisplayCardType {
  image?: any;
  name?: string;
  categoryName?: [];
  width?: string | number;
  ratingValue?: number;
  headerVariant?: string | any;
  imageType?: string;
  className?: string;
  price?: string;
  status?: string;
  background?: string;
}
interface InputComponentType {
  touched?: any;
  errors?: any;
  getFieldProps?: any;
  label?: any;
  type?: string;
  touched1?: any;
  touched2?: any;
  errors1?: any;
  errors2?: any;
  values?: any;
  getFieldPropsValue?: string;
  setFieldValue?: any;
  onChange?: void | any;
  placeholder?: string;
  disabled?: boolean;
  options?: any;
  compulsary?: string;
  defaultValueOption?: string;
  displayIcon?: any;
  selectValue?: any;
  showTitle?: any;
  addNumber?: any;
  gender?: string;
  setAddNumber?: any;
  defaultValue?: string;
  setSelectType?: any;
  selectDataType?: string;
  accept?: any;
  maxLength?: number;
  closeDropDown?: boolean;
  style?: React.CSSProperties
}
interface ProfileViewTypes {
  showButton?: boolean;
  roleScreen?: boolean;
  header?: string;
  name?: any;
  familyName?: any;
  firstName?: any;
  middleName?: any;
  occupation?: any;
  country_origin?: any;
  email?: any;
  phoneNumber?: any;
  optionalNumber?: any;
  gender?: any;
  dob?: string;
  profileImage?: string;
  country?: any;
  postalCode?: any;
  prefecture?: any;
  town?: any;
  city?: any;
  frontImage?: string;
  backImage?: string;
  nriImage?: string;
  licenseImage?: string;
  passportImage?: string;
  assignedRole?: string;
  accessGivenTypes?: any;
  setOpenAssignRoleBox?: any;
}

interface ViewImageTypes {
  setOpenModal?: any;
  openModal: boolean;
  displayData?: any;
  server?: boolean;
  children?: any;
}

// function handleFieldChange(event: any, fieldName: string) {
//   setFormState((prev) => {
//     let updatedState = { ...prev };
//     updatedState = {
//       ...updatedState,
//       brandId: event.target?.value,
//     };
//   })
// }

export const RatingComponent = ({ value }: RatingComponentTypes) => {
  return <Rating name="simple-controlled" value={value} />;
};
export const ButtonComponent = ({
  variant,
  disabled,
  data,
  color,
  size,
  height,
  fontSize,
  onClickHandle,
}: ButtonComponentTypes) => {
  return (
    <Button
      variant={variant}
      disabled={disabled}
      color={color}
      size={size}
      style={{ height: height, borderRadius: "9px", fontSize: fontSize }}
      onClick={() => onClickHandle}
    >
      {data}
    </Button>
  );
};

export const DisplayCard = ({
  image,
  name,
  categoryName,
  width,
  ratingValue,
  headerVariant,
  price,
  imageType,
  className,
  status,
  background,
}: DisplayCardType) => {
  return (
    <Card
      className={"shop-body-card " + className ? className : ""}
      style={{ width: width, border: "none", boxShadow: "none" }}
    >
      <CardMedia
        component="img"
        alt="green iguana"
        className={
          imageType == "big"
            ? "shop-body-card-image-big"
            : "shop-body-card-image-small"
        }
        image={
          !!image
            ? image
            : "https://img.freepik.com/free-vector/shop-with-sign-we-are-open_52683-38687.jpg"
        }
        style={{ padding: "10px" }}
      />

      <CardContent style={{ paddingTop: "2px" }}>
        <Chip
          label={status ? status : "status"}
          style={{
            height: "21px",
            borderRadius: "3px",
            backgroundColor: `${background !== "VERIFIED" ? "#ff645c" : "#90dd9a"
              }`,
          }}
        />
        <p className="no-padding-margin">{name ? name : "Name"}</p>

        {categoryName ? (
          <Box display={"flex"} flexWrap="nowrap" overflow={"hidden"} gap="7px">
            {categoryName?.map((data: any, index: any) => (
              <Typography
                key={index}
                variant="h6"
                color="text.secondary"
                style={{ fontSize: "12px", textTransform: "capitalize" }}
              >
                {data?.category?.name_en}
              </Typography>
            ))}
          </Box>
        )
          : ("Category Name")}

        {price &&
          <Typography variant={"body1"} color={"#555"}>{price}</Typography>
        }
      </CardContent>
      <CardActions style={{ marginTop: "-15px", marginLeft: "5px" }}>
        <RatingComponent value={3} />
      </CardActions>
    </Card >
  );
};


export const InputComponent = ({
  type,
  touched,
  errors,
  getFieldProps,
  label,
  touched1,
  touched2,
  errors1,
  errors2,
  values,
  getFieldPropsValue,
  setFieldValue,
  onChange,
  placeholder,
  disabled,
  options,
  compulsary,
  defaultValueOption,
  displayIcon,
  selectValue,
  showTitle,
  addNumber,
  gender,
  setAddNumber,
  defaultValue,
  setSelectType,
  selectDataType,
  accept,
  maxLength,
  style
}: InputComponentType) => {
  const postalRef = useRef<any>();
  const [postalData, setPostalData] = useState<any>([]);
  const [finalPostalCode, setFinalPostalCode] = useState<any>("0");
  const [openPostalCodeSearchModal, setOpenPostalCodeSearchModal] =
    useState<boolean>(false);
  const [postalFrontValue, setPostalFrontValue] = useState<any>();
  const [allCountryData, setAllCountryData] = useState<any>([]);
  const [textAreaValue, setTextAreaValue] = useState<any>();
  const [closeDropDown, setCloseDropDown] = useState<boolean>(false);

  const {
    data: allDataAPI,
    isSuccess: getAddressInfo,
    refetch,
    isLoading: isLoadingPostalData,
  } = useGetDataFromPostalCodeQuery(finalPostalCode);
  useEffect(() => {
    if (finalPostalCode === '0') {
      refetch();
    }
  }, [finalPostalCode, refetch]);

  const moveToNext = (e: any, nextId: any) => {
    document?.getElementById(nextId)?.focus();
  };
  const handleAlpha = (e: any) => {
    ["abcdefghijklmnopqrstuvwxyz"].toString().includes(e.key) &&
      e.preventDefault();
  };
  const handleChangeSelect = (status?: any) => {
    setSelectType(status?.target?.value);
  };
  const handlePostalClick = (val: any) => {
    if (!!val) {
      localStorage.setItem("postalcodeValue", val?.postalcode?.slice(0, 3));
      setFieldValue("postalcode_en", val.postalcode.slice(0, 3));
      setFieldValue("postalcode_end", val.postalcode.slice(3));
      setFieldValue("city_en", val.municipalityname);
      setFieldValue("prefecture_en", val.prefecturename);
      setFieldValue("town_en", val.townname);
    }
  };
  const handlePostalCodeFill = () => {
    const finalValue = `${values.postalcode_en?.slice(0, 3)}${values.postalcode_end
      }`;
    const postalCodeMatches = allDataAPI?.data?.filter(
      (data: any) => data?.postalcode == finalValue
    );
    if (!isEmpty(postalCodeMatches)) {
      setFieldValue("city_en", postalCodeMatches?.[0].municipalityname);
      setFieldValue("prefecture_en", postalCodeMatches?.[0].prefecturename);
      setFieldValue("town_en", postalCodeMatches?.[0].townname);
    }
  };
  const handleResetAllFields = () => {
    setFieldValue("city_en", "");
    setFieldValue("prefecture_en", "");
    setFieldValue("town_en", "");
  };
  const takeNumber = (e: any) => {
    [
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrgstuvwxyz.-+<>@#$%^&*!:|±~?/+`\"`,;()[]_ {/'/, /\\/}=+",
    ]
      .toString()
      .includes(e.key) && e.preventDefault();
  };
  const stylePostalCodeModal = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 650,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
    // height: 600,
  };
  const columns: GridColDef[] = [
    {
      field: "postalcode",
      headerName: "Postal Code",
      width: 100,
    },
    {
      field: "prefecturename",
      headerName: "Prefecture ",
      width: 150,
    },
    {
      field: "municipalityname",
      headerName: "City/Village",
      width: 170,
    },
    {
      field: "townname",
      headerName: "Town/Ward",
      width: 170,
    },
  ];

  useEffect(() => {
    if (type === "country_origin") {
      fetch(
        "https://valid.layercode.workers.dev/list/countries?format=select&flags=true&value=code"
      )
        .then((response) => response.json())
        .then((data) => {
          setAllCountryData(data.countries);
        });
    }
  }, []);

  useMemo(() => {
    setPostalData(allDataAPI?.data);
  }, [allDataAPI]);

  const postalcodeSelectedValue: any = localStorage.getItem("postalcodeValue");


  const addNumberField = (e: any, index: number) => {
    const { name, value } = e.target;
    const number = [...addNumber];
    number[index][name] = value;
    setAddNumber(number);
  };
  const deleteNumber = (index: number) => {
    const number = [...addNumber];
    number?.splice(index, 1);
    setAddNumber(number);
  };
  const addPhoneNumber = () => {
    setAddNumber([...addNumber, { otherNumber: "" }]);
  };

  const handleTextFieldChange = (e: any) => {
    if (e.nativeEvent.inputType === "insertLineBreak") {
      setTextAreaValue((previousData: any) => previousData + "\n" + "*");
    } else {
      setTextAreaValue(e.target.value);
    }
    //  && retur
  };
  return (
    <div style={style}>
      {type === "text" && (
        <>
          {label}
          {!!label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              {!compulsary && "*"}
            </Typography>
          )}
          <TextField
            {...getFieldProps(getFieldPropsValue)}
            size="small"
            fullWidth
            error={Boolean(touched && errors)}
            helperText={Boolean(touched && errors) && String(touched && errors)}
            disabled={disabled}
            placeholder={placeholder}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  {displayIcon ? displayIcon : ""}
                </InputAdornment>
              ),
            }}
            // maxLength={maxLength}
            inputProps={{ maxLength: maxLength }}
          />
        </>
      )}
      {type === "file" && (
        <>
          {label}
          {!!label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              *
            </Typography>
          )}
          <TextField
            {...getFieldProps(getFieldPropsValue)}
            size="small"
            fullWidth
            error={Boolean(touched && errors)}
            helperText={Boolean(touched && errors) && String(touched && errors)}
            type="file"
            onChange={(e) => onChange(e, getFieldPropsValue)}
            inputProps={{ accept }}
          />
        </>
      )}
      {type === "postalCode" && (
        <>
          {label && (
            <Box>
              {!showTitle && "Postal Code"}
              <a
                style={{
                  color: "blue",
                  fontSize: "12px",
                  cursor: "pointer",
                  textDecoration: "underline",
                }}
                onClick={() => {
                  setOpenPostalCodeSearchModal(true);
                  setFinalPostalCode("0");
                }}
              >
                (Don't know postal code?)
              </a>
              <Typography
                component="span"
                color="error"
                sx={{ marginLeft: "5px" }}
              >
                *
              </Typography>
            </Box>
          )}
          <Stack direction="column" spacing={1} alignItems="center">
            <Box
              sx={{
                display: "flex",
                width: "100%",
                justifyContent: "space-between",
              }}
            >
              <Box sx={{ width: "49%" }}>
                <Autocomplete
                  disabled={disabled}
                  // options={postalData}
                  options={[]}
                  onClose={() => closeDropDown}
                  getOptionLabel={(option: any) =>
                    option.postalcode?.slice(0, 3)
                  }
                  renderOption={(props, option: any) => (
                    <li {...props}>{option.postalcode}</li>
                  )}
                  freeSolo
                  inputValue={
                    isEmpty(postalcodeSelectedValue) ? "" : postalcodeSelectedValue
                  }
                  onChange={(event, value) => {
                    handlePostalClick(value);
                    setCloseDropDown(true);
                  }}
                  disableClearable
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      size="small"
                      type="text"
                      {...getFieldProps("postalcode_en")}
                      onKeyUp={(e) => {
                        const codeLength = String(
                          values?.postalcode_en
                        )?.length;
                        codeLength > 0 &&
                          setFinalPostalCode(values.postalcode_en);
                        setCloseDropDown(true);
                        if (codeLength > 3) {
                          setFieldValue(
                            "postalcode_end",
                            values?.postalcode_en?.substring(3, 6)
                          );
                          moveToNext(e, "postalcode_end");
                        } else {
                          handleResetAllFields();
                          setFieldValue("postalcode_end", "");
                          localStorage.setItem(
                            "postalcodeValue",
                            values.postalcode_en?.slice(0, 3)
                          );
                        }
                      }}
                      onKeyDown={handleAlpha}
                      fullWidth
                      error={Boolean(touched1 && errors1)}
                      helperText={
                        Boolean(touched1 && errors1) &&
                        String(touched1 && errors1)
                      }
                    />
                  )}
                />
              </Box>
              <Typography style={{ marginTop: "7px" }}>-</Typography>
              <Box sx={{ width: "49%" }}>
                <TextField
                  size="small"
                  type="text"
                  inputProps={{
                    maxLength: 4,
                    pattern: "[0-9]*",
                  }}
                  disabled={disabled}
                  id="postalcode_end"
                  {...getFieldProps("postalcode_end")}
                  onKeyUp={(e) => {
                    if (values.postalcode_end.length <= 3) {
                      handleResetAllFields();
                    }
                    if (values.postalcode_end.length > 3) {
                      handlePostalCodeFill();
                    }
                  }}
                  onKeyDown={handleAlpha}
                  fullWidth
                  error={Boolean(touched2 && errors2)}
                  helperText={
                    Boolean(touched2 && errors2) && String(touched2 && errors2)
                  }
                />
              </Box>
            </Box>
          </Stack>
        </>
      )}
      {type === "number" && (
        <>
          {label}
          {!!label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              {!compulsary && "*"}
            </Typography>
          )}
          <Stack direction="row" spacing={1}>
            <TextField
              sx={{ width: "30%" }}
              value={"+81"}
              disabled
              size="small"
            />
            <TextField
              size="small"
              sx={{ width: "70%" }}
              variant="outlined"
              {...getFieldProps(getFieldPropsValue)}
              disabled={disabled}
              inputProps={{ maxLength: 11 }}
              onKeyDown={takeNumber}
              placeholder={placeholder}
              error={Boolean(touched && errors)}
              helperText={
                Boolean(touched && errors) && String(touched && errors)
              }
            />
          </Stack>
        </>
      )}
      {type === "time" && (
        <>
          {label}
          {!!label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              *
            </Typography>
          )}
          <TextField
            {...getFieldProps(getFieldPropsValue)}
            size="small"
            fullWidth
            error={Boolean(touched && errors)}
            helperText={Boolean(touched && errors) && String(touched && errors)}
            type="time"
          />
        </>
      )}
      {type === "date" && (
        <>
          {label}
          {label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              *
            </Typography>
          )}
          <LocalizationProvider dateAdapter={AdapterMoment}>
            <DatePicker
              maxDate={
                getFieldPropsValue !== "validDate"
                  ? moment().format("YYYY-MM-DD")
                  : ""
              }
              minDate={
                getFieldPropsValue === "validDate"
                  ? moment().format("YYYY-MM-DD")
                  : ""
              }
              {...getFieldProps(getFieldPropsValue)}
              onChange={(newValue: any) => {
                setFieldValue(
                  getFieldPropsValue,
                  moment(newValue).format("YYYY-MM-DD")
                );
              }}
              inputFormat="YYYY/MM/DD"
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  type="date"
                  size="small"
                  fullWidth
                  error={Boolean(touched && errors)}
                  helperText={
                    Boolean(touched && errors) && String(touched && errors)
                  }
                />
              )}
            />
          </LocalizationProvider>
        </>
      )}
      {type === "option" && (
        <>
          {label}
          {label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              *
            </Typography>
          )}
          {selectDataType === "server" ? (
            <>
              <FormControl fullWidth size="small">
                <Select
                  {...getFieldProps(getFieldPropsValue)}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={selectValue ? selectValue.id : ""}
                  onChange={(e) => {
                    setFieldValue(selectValue.id, e.target.value);
                  }}
                >
                  {options && options?.map((option: any) => (
                    <MenuItem key={option.id} value={option.id}>
                      {option.name_en || option.name || option.label}
                    </MenuItem>
                  ))}
                </Select>
                <Typography color="error" sx={{ fontSize: "12px", margin: "4px 14px 0px" }}>
                  {getFieldPropsValue && (
                    <ErrorMessage name={getFieldPropsValue} component="div" />
                  )}
                </Typography>
              </FormControl>

            </>
          ) : (
            <TextField
              {...getFieldProps(getFieldPropsValue)}
              size="small"
              fullWidth
              select
              disabled={disabled}
              error={Boolean(touched && errors)}
              helperText={Boolean(touched && errors) && String(touched && errors)}
              defaultValue={defaultValueOption}
            >
              {options && options?.map((option: any) => (
                <MenuItem key={option.value || option.id} value={option.value || option.id}>
                  {option.label || option.name_en}
                </MenuItem>
              ))}
            </TextField>
          )}
        </>
      )}

      {type === "options" && (
        <>
          {label}
          {label && (
            <Typography
              component="span"
              color="error"
              sx={{ marginLeft: "5px" }}
            >
              *
            </Typography>
          )}

          <FormControl fullWidth size={"small"}>
            {selectDataType === "server" ? (
              <Select
                {...getFieldProps(getFieldPropsValue)}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectValue?.id || selectValue || ""}
                onChange={(e: any) => {
                  setSelectType(e.target.value);
                }}
              >
                {options && options?.map((option: any) => (
                  <MenuItem key={option?.id} value={option?.id || option?.value}>
                    {option?.name_en || option?.name || option.label}
                  </MenuItem>
                ))}
              </Select>
            ) : ""}
          </FormControl>
        </>
      )
      }

      {
        type === "secondaryPhoneNumber" && (
          <>
            {addNumber?.map((data: any, index: any) => (
              <Grid item xs={12} sm={6} key={index}>
                {!showTitle && "Optional Number"}
                <Stack direction="row" spacing={1}>
                  <div style={{ width: "34%" }}>
                    <TextField
                      value={"+81"}
                      disabled
                      size="small"
                      style={{
                        width: index < addNumber?.length - 1 ? "100%" : "100%",
                      }}
                    />
                  </div>

                  <TextField
                    size="small"
                    sx={{ width: index < addNumber?.length - 1 ? "79%" : "70%" }}
                    variant="outlined"
                    inputProps={{ maxLength: 11, minLength: 10 }}
                    onKeyDown={takeNumber}
                    placeholder="Enter optional number"
                    name="addNumber"
                    defaultValue={data?.addNumber}
                    onChange={(e) => addNumberField(e, index)}
                  />
                  {addNumber?.length - 1 === index && (
                    <Button
                      onClick={() => deleteNumber(index)}
                      variant="outlined"
                    >
                      <DeleteIcon />
                    </Button>
                  )}
                </Stack>
              </Grid>
            ))}
            <Box
              style={{
                width: "100%",
                height: "50px",
                display: "flex",
                alignItems: "flex-end",
                justifyContent: "flex-end",
              }}
            >
              <Button onClick={addPhoneNumber} disabled={addNumber?.length === 3}>
                Add Number
              </Button>
            </Box>
          </>
        )
      }

      {
        type === "country_origin" && (
          <>
            <FormControl fullWidth>
              <Autocomplete
                options={allCountryData}
                sx={{ width: 300 }}
                style={{ width: '100%' }}
                autoHighlight
                clearIcon={false}
                onChange={(event: any, value: any) => {
                  setFieldValue('country_origin', value?.label);
                }}
                value={selectValue}
                renderInput={(params: any) => (
                  <TextField
                    {...params}
                    {...getFieldProps('country_origin')}
                    size="small"
                  />
                )}
              />
            </FormControl>
          </>
        )
      }
      {/* {
        type === "textArea" && (
          <>
            <FormControl fullWidth>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={3}
                style={{ borderRadius: "6px", borderColor: "#cccccc" }}
                value={textAreaValue}
                onChange={(e) => {
                  handleTextFieldChange(e);
                }}
              />
            </FormControl>
          </>
        )
      } */}
      {
        type === "textArea" && (
          <>
            {label && (
              <>
                {label}
                {!!label && (
                  <Typography
                    component="span"
                    color="error"
                    sx={{ marginLeft: "5px" }}
                  >
                    {!compulsary && "*"}
                  </Typography>
                )}
              </>
            )}
            <FormControl fullWidth>
              <TextareaAutosize
                {...getFieldProps(getFieldPropsValue)}
                className="textArea"
                aria-label="minimum height"
                minRows={3}
                style={{ borderRadius: "6px", borderColor: "#cccccc", width: "100%", padding: '8.5px 14px' }}
                placeholder={placeholder}
                disabled={disabled}
              />
            </FormControl>
          </>
        )
      }


      {
        type === "password" && (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {label}
            {!!label && (
              <Typography
                component="span"
                color="error"
                sx={{ marginLeft: "5px" }}
              >
                {!compulsary && "*"}
              </Typography>
            )}
            <TextField
              {...getFieldProps(getFieldPropsValue)}
              size="small"
              fullWidth
              type={type}
              error={Boolean(touched && errors)}
              helperText={Boolean(touched && errors) && String(touched && errors)}
              disabled={disabled}
              placeholder={placeholder}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    {displayIcon ? displayIcon : ""}
                  </InputAdornment>
                ),
              }}
            />
          </div>
        )
      }

      {
        openPostalCodeSearchModal && (
          <Modal
            sx={{
              "& .MuiBox-root": {
                maxHeight: "fit-content"
              }
            }}
            open={openPostalCodeSearchModal}
            onClose={() => {
              setOpenPostalCodeSearchModal(false);
            }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={stylePostalCodeModal}>
              <Typography>Search Postal code from your address</Typography>
              <div
                style={{
                  marginTop: "20px",
                  marginBottom: "20px",
                  display: "flex",
                }}
              >
                <TextField
                  onChange={(e: any) => {
                    e.target.value?.length > 0 &&
                      setFinalPostalCode(e.target.value.toUpperCase());
                    if (e.target.value?.length == 0) {
                      setFinalPostalCode(0);
                    }
                  }}
                  size="small"
                  fullWidth
                  placeholder="Search for your address"
                />
              </div>
              {!!postalData && (
                <Box sx={{ height: 400, width: "100%" }}>
                  Results:
                  <DataGrid
                    rows={postalData}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    disableSelectionOnClick
                    experimentalFeatures={{ newEditingApi: true }}
                    onCellClick={(value) => {
                      handlePostalClick(value?.row);
                      setOpenPostalCodeSearchModal(false);
                    }}
                  />
                </Box>
              )}
            </Box>
          </Modal>
        )
      }
    </div >
  );
};

export const ProfileView = ({
  showButton,
  roleScreen,
  header,
  name,
  familyName,
  firstName,
  middleName,
  occupation,
  country_origin,
  email,
  phoneNumber,
  optionalNumber,
  gender,
  dob,
  profileImage,
  country,
  postalCode,
  prefecture,
  town,
  city,
  frontImage,
  backImage,
  nriImage,
  licenseImage,
  passportImage,
  assignedRole,
  accessGivenTypes,
  setOpenAssignRoleBox,
}: ProfileViewTypes) => {
  const [slideImage, setSlideImage] = useState<any>([]);
  const [displayData, setDisplayData] = useState<any>("");
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openImageModal, setOpenImageModal] = useState<boolean>(false);
  const displayField = (name: string, value: string) => {
    return (
      <div
        className="profile-view-body-field display-flex-sb"
        style={{
          display: "flex",
        }}
      >
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div className="profile-view-fieldName">{name} :</div>
          <div className="profile-view-fieldValue">{value}</div>
        </div>
      </div>
    );
  };
  const displayImage = (image?: any, title?: string) => {
    return (
      <div
        style={{
          paddingBottom: "3%",
          marginRight: "20px",
        }}
      >
        {!!image ? (
          <div
            style={{
              display: "flex",
              height: "50%",
              objectFit: "cover",
              boxShadow: " -2px -2px 23px -10px rgba(0,0,0,0.75)",
            }}
          >
            <Image
              src={image}
              alt="Error displaying"
              width={300}
              height={300}
              objectFit="cover"
              onClick={() => {
                setSlideImage([{ src: image, title: title }]);
                setOpenImageModal(true);
                setDisplayData(image);
              }}
            />
          </div>
        ) : (
          <img
            src={"https://i.stack.imgur.com/l60Hf.png"}
            alt=""
            className="display-default-image"
            style={{
              width: "400px",
              height: "320px",
            }}
          />
        )}
        <p style={{ textAlign: "center" }}>{title}</p>
      </div>
    );
  };
  return (
    <div style={{ marginBottom: "10px" }}>
      <div className="profile-view">
        <div className="profile-view-body display-flex-sb">
          <div className="profile-view-field">
            <div className="profile-view-header">{header}</div>
            {header === "Personal Details" && (
              <>
                {displayField("Family Name", familyName)}
                {displayField("First Name", firstName)}
                {displayField("Middle Name", middleName)}
                {displayField("Occupation", occupation)}
                {displayField(
                  "Date of Birth",
                  moment(dob).format("YYYY/MM/DD")
                )}
                {displayField("country_origin", country_origin)}
                {displayField("Gender", gender)}
                {/* {displayField("Name", name)} */}
                {displayField("Email", email)}
                {displayField("Phone Number", phoneNumber)}
                {optionalNumber?.map((value: number, index: any) => (
                  <div className=" profile-view-body-field display-flex-sb">
                    <div className="profile-view-fieldName">
                      optional Number :
                    </div>
                    <div className="profile-view-fieldValue">{value}</div>
                  </div>
                ))}
              </>
            )}

            {header === "Address Details" && (
              <>
                {displayField("Country", country)}
                {displayField("Postal Code", postalCode)}
                {displayField("Prefecture", prefecture)}
                {displayField("Town", town)}
                {displayField("City", city)}
              </>
            )}
            {header === "Roles And Access" && (
              <>
                <h3 className="no-padding-margin">{"ROLE ASSIGNED"}</h3>
                <div className="profile-view-fieldName">{assignedRole}</div>
                <h4 className="no-padding-margin">{"Access Given"}</h4>
                <h5 className="no-padding-margin">
                  {accessGivenTypes?.map((data: any, index: number) =>
                    accessGivenTypes?.length - 1 === index ? data : `${data},`
                  )}
                </h5>
                <div
                  style={{ margin: "30px 0px 20px 0px" }}
                  className="display-flex-jc-center"
                >
                  <Button
                    variant="contained"
                    onClick={() => {
                      setOpenAssignRoleBox(true);
                    }}
                  >
                    <EditIcon />
                    Edit Permission
                  </Button>
                </div>
              </>
            )}
          </div>
        </div>
        <div className=" myprofieShowDocs">
          {header === "KYC Documents" &&
            frontImage &&
            displayImage(frontImage, "frontimage")}
          {header === "KYC Documents" &&
            backImage &&
            displayImage(backImage, "backImage")}
          {header === "KYC Documents" &&
            passportImage &&
            displayImage(passportImage, "passport")}
          {header === "KYC Documents" &&
            licenseImage &&
            displayImage(licenseImage, "license")}
          {header === "KYC Documents" &&
            nriImage &&
            displayImage(nriImage, "nri")}
          {/* {displayImage(Background)} */}
        </div>
      </div>
      <Lightbox
        // mainsrc
        open={openImageModal}
        close={() => setOpenImageModal(false)}
        slides={slideImage}
        index={0}
        plugins={[Captions, Fullscreen, Slideshow, Thumbnails, Video, Zoom]}
      />
    </div>
  );
};

export const ViewImage = ({
  setOpenModal,
  openModal,
  displayData,
  server,
}: ViewImageTypes) => {
  const viewImageStyle = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    borderRadius: 2,
  };
  function handleClose() {
    setOpenModal(false);
  }
  return (
    <>
      <Modal
        open={openModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={viewImageStyle}>
          {/* <Stack direction='row' justifyContent={"right"}>
                        <Button onClick={handleClose} color='error'>
                            <CloseIcon />
                        </Button>
                    </Stack> */}
          <Stack direction="column" justifyContent={"center"}>
            <img
              className="myImageView"
              src={
                server || !!displayData?.length
                  ? displayData
                  : window.URL.createObjectURL(new Blob(displayData))
              }
            // alt={displayData.alt}
            />
          </Stack>
        </Box>
      </Modal>
    </>
  );
};
export const ModalView = ({
  setOpenModal,
  openModal,
  children,
}: ViewImageTypes) => {
  const viewImageStyle = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: { lg: "65%", sm: "70%", xs: "90%" },
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: 2,
  };
  return (
    <>
      <Modal
        open={openModal}
        onClose={setOpenModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={viewImageStyle}>{children}</Box>
      </Modal>
    </>
  );
};

export const NotificationsList = (props: any) => {
  const [openRejectModal, setOpenRejectModal] = useState<boolean>(false);
  const [userIdRejected, setUserIdRejected] = useState<string>("");
  const [merchantNotificationsData, setMerchantNotificationsData] =
    useState<any>([]);

  const { t } = useTranslation();
  // const [statusUpdate, { data: successData, isSuccess }] = usePostPersonalKycStatusMutation();

  const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 450,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };
  const userId = localStorage.getItem("userIdNotification");

  useEffect(() => {
    const socket = io("http://stageapi.shopo.jp");
    socket
      .emit("user-notification", { userId: userId })
      .on("user-notification", (data: any) => {
        setMerchantNotificationsData(data);
      });
  }, []);
  const handleClickStatus = (data: any, status: string) => {
    if (status === "UNVERIFIED") {
      setOpenRejectModal(true);
      setUserIdRejected(data?.userId);
    } else {
      const values = {
        userID: data?.userId,
        status: status,
        message: "It is successfully verified",
        identityStatusId: props?.data?.user?.identityStatus?.id,
      };
      submitNotificationStatus(values);
    }
  };
  const submitNotificationStatus = (values: any) => {
    // statusUpdate(values)
    //     .unwrap()
    //     .catch((error) => {
    //         const ermsg = t(error?.data?.message);
    //         toast.error(ermsg);
    //     });
  };
  const handleNotificationClick = (data: any) => {
    Router.push({
      pathname: "/verifypersonalkycforms/details",
      query: { id: data?.userId },
    });
  };
  const formik = useFormik({
    initialValues: {
      message: "",
    },
    onSubmit: async (values: any) => {
      let reqData = {
        ...values,
        userID: userIdRejected,
        status: "UNVERIFIED",
        identityStatusId: props?.data?.user?.identityStatus?.[0]?.id,
      };
      submitNotificationStatus(reqData);
      setOpenRejectModal(false);
    },
  });

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  return (
    <>
      <List
        sx={{ width: "100%", bgcolor: "background.paper" }}
        aria-label="contacts"
        key={props?.key}
        className="popover-notification-box"
      >
        <ListItem
          disablePadding
          sx={{ paddingLeft: "4px", paddingRight: "4px" }}
        >
          <ListItemButton>
            <div
              className={
                props?.type !== "seeAll" ? "popover-notification-box-width" : ""
              }
            >
              <div className="display-flex">
                <div style={{ alignItems: "center" }}>
                  {props?.data?.user?.profile?.userImage ?
                    <Avatar
                      src={props?.data?.user?.profile?.userImage}
                    // sx={{ width: 44, height: 44 }}
                    />
                    :
                    <AccountCircleRoundedIcon className="popover-notification-rounded-auto-user" />
                  }
                </div>
                <div style={{ marginLeft: "25px" }}>
                  <div>
                    {/* <span className='popover-box-notification-content' onClick={() => { handleNotificationClick(props?.data) }}>
                                            {props?.data?.body?.split(" ")[0]}
                                        </span>{" "}{props?.data?.body.substr(props?.data?.body.indexOf(" ") + 1)} */}
                    {props?.data?.body}
                  </div>
                  {props?.data?.user?.identityStatus?.[0]?.message && (
                    <div style={{ fontWeight: "bold", fontSize: "15px" }}>
                      Message:{props?.data?.user?.identityStatus?.[0]?.message}
                    </div>
                  )}
                  <div className="popover-box-notification-time">
                    {moment(props?.data?.createdAt).format("MMMM Do")} at{" "}
                    {moment(props?.data?.createdAt).format("h:mm A")}
                  </div>
                </div>
              </div>
            </div>
          </ListItemButton>
        </ListItem>
      </List>
      {openRejectModal && (
        <FormikProvider value={formik}>
          <Modal
            open={openRejectModal}
            onClose={() => setOpenRejectModal(false)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <form onSubmit={handleSubmit}>
              <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Please mention why the form is rejected
                </Typography>
                <TextField
                  {...getFieldProps("message")}
                  fullWidth
                  multiline={true}
                  rows={3}
                ></TextField>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Button
                    disabled={isSubmitting}
                    type="submit"
                    variant="contained"
                    color="primary"
                    sx={{ width: "50%", marginTop: "4%" }}
                  >
                    Submit
                  </Button>

                  <Button
                    disabled={isSubmitting}
                    onClick={() => setOpenRejectModal(false)}
                    variant="contained"
                    color="error"
                    sx={{ width: "50%", marginLeft: "4%", marginTop: "4%" }}
                  >
                    Cancel
                  </Button>
                </Box>
              </Box>
            </form>
          </Modal>
        </FormikProvider>
      )}
    </>
  );
};
