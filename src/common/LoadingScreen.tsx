// MUI Components
import { Box, Typography } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";

export default function CircularIndeterminate() {
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Typography variant='h5'>Data is being sent. Please Wait ... </Typography>
      <CircularProgress />
    </Box>
  );
}
