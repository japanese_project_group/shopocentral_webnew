
export const category = [
  {
    value: "Electronics",
    label: "Electronics",
  },
  {
    value: "Clothings",
    label: "Clothings",
  },
  {
    value: "Groceries",
    label: "Groceries",
  },
];

export const handlingServiceTypeData = [
  {
    value: "Mobiles",
    label: "Mobiles",
  },
  {
    value: "Computers",
    label: "Computers",
  },
  {
    value: "Food",
    label: "Food",
  },
];

export const companyTypeData = [
  {
    value: "Personal",
    label: "Personal",
  },
  {
    value: "Corporate",
    label: "Corporate",
  },
];

export const companyType = [
  {
    value: "PERSONAL",
    label: "PERSONAL",
  },
  {
    value: "CORPORATE",
    label: "CORPORATE",
  },
];

export const enrollmentTypeData = [
  {
    value: "Trainee",
    label: "Trainee",
  },
  {
    value: "Part Time",
    label: "Part Time",
  },
  {
    value: "Freelancer",
    label: "Freelancer",
  },
  {
    value: "Temporary Staff (Haken)",
    label: "Temporary Staff (Haken)",
  },
  {
    value: "Contract Employee",
    label: "Contract Employee",
  },
  {
    value: "Full Time Employee",
    label: "Full Time Employee",
  },
  {
    value: "Permanent Employee",
    label: "Permanent Employee",
  },
  {
    value: "Retired",
    label: "Retired",
  },
];

export const genderTypeData = [
  {
    value: "MALE",
    label: "MALE",
  },
  {
    value: "FEMALE",
    label: "FEMALE",
  },
  {
    value: "OTHER",
    label: "OTHER",
  },
];

export const productCondition = [
  {
    value: "brand_new",
    label: "Brand New",
  },
  {
    value: "like_new",
    label: "Like New",
  },
];

export const status = [
  {
    value: "VISIBLE",
    label: "VISIBLE",
  },
  {
    value: "INVISIBLE",
    label: "INVISIBLE",
  },
];

export const productStatus = [
  {
    value: "PENDING",
    label: "PENDING",
  },
  {
    value: "PROCESSING",
    label: "PROCESSING",
  },
  {
    value: "SHIPPED",
    label: "SHIPPED",
  },
  {
    value: "DELIVERED",
    label: "DELIVERED",
  },
  {
    value: "CANCELED",
    label: "CANCELED",
  },
];

export const manageInventory = [
  {
    value: "PRODUCT_MANAGE",
    label: "PRODUCT MANAGE",
  },
  {
    value: "PRODUCT_UNLIMITED",
    label: "PRODUCT UNLIMITED",
  },
];

export const orderOutOfStock = [
  {
    value: "RECEIVED",
    label: "RECEIVED",
  },
  {
    value: "DONT_RECEIVED",
    label: "DON'T RECEIVED",
  },
];

export const styleForModal = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: "40px",
  borderRadius: "9px",
};

export const taxCategoryData = [
  {
    value: "No Tax (無税):0%",
    label: "No Tax (無税):0%",
  },
  {
    value: "Reduced C-Tax (軽減税):8%",
    label: "Reduced C-Tax (軽減税):8%",
  },
  {
    value: "Standard C-Tax(標準税):10%",
    label: "Standard C-Tax(標準税):10%",
  },
];
