import { TextField } from "@mui/material";
import { useField } from "formik";

export default function DateField(props: any) {
  const { errorText, ...rest } = props;
  const [field, meta] = useField(props);

  return (
    <TextField
      type="date"
      error={meta.touched && meta.error && true}
      {...field}
      {...rest}
    />
  );
}
