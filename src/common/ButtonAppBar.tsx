// MUI Components
import { Switch } from "@mui/material";
import Box from "@mui/material/Box";

const ButtonAppBar = ({ change, checked }: any) => {
  const label = { inputProps: { "aria-label": "Switch demo" } };
  return (
    <Box>
      <Switch
        {...label}
        defaultChecked
        onChange={change}
        checked={checked}
      ></Switch>
    </Box>
  );
};
export default ButtonAppBar;
