// Crypto js
import { getDecryptedData } from "..";

let businessAccessId: any = null;
let businessId: any = null;
let userId: any = null;

if (typeof window !== 'undefined') {
    // for Business Access Id
    const encBusinessAccessId = localStorage.getItem("businessAccessId");
    businessAccessId = encBusinessAccessId ? getDecryptedData(encBusinessAccessId) : null;

    // for Business Id
    const encBusinessId = localStorage.getItem("businessId");
    businessId = encBusinessId ? getDecryptedData(encBusinessId) : null;
    // for user ID
    const encUserId = localStorage.getItem("userIdNotification");
    userId = encUserId ? getDecryptedData(encUserId) : null;
}

export { businessAccessId, businessId, userId };

