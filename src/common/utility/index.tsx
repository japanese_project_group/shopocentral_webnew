// Third party library
import CryptoJS from "crypto-js";
const key: string = "Hello World!";

export const base64Encode = (data: any) => {
  if (typeof data === "string") {
    let encodedWord = CryptoJS.enc.Utf8.parse(data);
    let encoded = CryptoJS.enc.Base64.stringify(encodedWord);
    return encoded;
  }
  const jsonString = JSON.stringify(data);
  const base64String = CryptoJS.enc.Base64.stringify(
    CryptoJS.enc.Utf8.parse(jsonString)
  );
  return base64String;
};

export const generateSignature = (key: any, data: any) => {
  const encryptedKey = key.slice(0, 16);
  const keywordArray = CryptoJS.enc.Utf8.parse(encryptedKey);
  const iv = CryptoJS.enc.Utf8.parse(encryptedKey);
  const dataBytes = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
  const encrypted = CryptoJS.AES.encrypt(dataBytes, keywordArray, {
    iv: iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  });
  const encryptedSignature = encrypted.toString();
  return encryptedSignature;
};

// Encrypt
export const getEncryptedData = (data: any) => {
  const encryptedData: string = CryptoJS.AES.encrypt(data, key).toString();
  return encryptedData;
};


// Decrypt
export const getDecryptedData = (data: any) => {
  try {
    const bytes = CryptoJS.AES.decrypt(data, key);

    // Check if decryption was successful
    if (bytes && bytes.sigBytes > 0) {
      const decryptedData: any = bytes.toString(CryptoJS.enc.Utf8);
      return decryptedData;
    } else {
      console.error('Decryption failed. Bytes:', bytes);
      return null; // or handle the error appropriately
    }
  } catch (error) {
    console.error('Error during decryption:', error);
    return null; // or handle the error appropriately
  }
};

