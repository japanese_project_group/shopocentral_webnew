// Third party library
import * as yup from "yup";
// Regex
import { emailRegex } from "../constant/regex";

const yesterday = new Date(Date.now() - 86400000);

export const validationSchemaAddBranch = yup.object({
  name_en: yup.string().required("Please enter branch name"),
  // openingTime: yup.string().required("Please enter your Opening Time"),
  closingTime: yup
    .string()
    .required("Please enter your Closing and Opening Time"),
  email: yup
    .string()
    .required("Enter branch Email")
    .test(
      "test-name",
      "Please enter valid email address",
      function (value: any) {
        const emailRegexValidation = emailRegex;
        let isValidEmail = emailRegexValidation.test(value);
        if (!isValidEmail) {
          return false;
        }
        return true;
      }
    ),
  phoneNumber: yup
    .number()
    .typeError("Please enter a telephone number")
    .required("Please enter your telephone number"),
  town_en: yup.string().required("Please enter your town"),
  city_en: yup.string().required("Please enter your City"),
  prefecture_en: yup.string().required("Please enter your Prefecture"),
  country_en: yup.string().required("Please enter your country"),
});

export const validationSchemaaRegister = yup.object({
  email: yup
    .string()
    .required("Email is Required")
    .email("email is invalid")
    .matches(
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/m,
      "Enter a valid email"
    ),
  password: yup.string().required("Password is Required"),
});

export const registerCustomerValidation = yup.object().shape({
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
  name: yup
    .string()
    .matches(
      /^[a-zA-Z-' ]+$/,
      "Name can only contain letters, spaces, hyphens, and apostrophes."
    )
    .required("Full name is required."),
  email: yup.string().email().required("Email is Required"),
  roleName: yup.string().required("Role is Required"),
  phoneType: yup.string().required("Mobile Type is Required"),

  phonenumber: yup
    .string()
    .required("required")
    .min(11, "Mobile number must be of 11 digits")
    .max(11, "too long"),
  // phonenumber: yup
  //   .number()
  //   .min(11, "mumber number should be of 11 digits")
  //   // .max(11, "number should be of 11 digits")
  //   .typeError("Phone number must be number")
  //   .required("Phone Number is Required"),
  fcmToken: yup.string().required("Password is Required"),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Confirm Password is Required"),
});

export const validationSchemaAdmino = yup.object({
  email: yup.string().email().required("Please enter a valid Email"),
});

const specialChar = "[$&+,:;=?@#|'<>.-^*()%!/\\/]/\\/";

export const validationSchemaBusiness = yup.object({
  businessEmail: yup
    .string()
    .email()
    .required("Please enter a valid Business Email"),
  katagana_name: yup.string().required("Please enter a Name in katagana"),
  // name: yup.string().required("Please enter a valid Name "),
  name_en: yup.string().required("Please enter English Name"),
  registerTradeName: yup.string().required("please enter a Registered Name"),
  companyType: yup.string().required("Please select type of company"),
  // furigana: yup.string().required("please enter furigana"),
  // englishname: yup.string(),
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
  businessAccessId: yup
    .string()
    .max(8, "Must be of 8 characters")
    .min(8, "Must be of 8 characters")
    .matches(
      /^(?=.*?\d)(?=.*?[a-zA-Z])[\da-zA-Z]+$/,
      "Must be alphaNumeric data"
    )
    .required("Please enter your Shopo Central Business ID"),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Confirm Password is Required"),
  city_en: yup.string().required(" Please enter a City/Village "),
  country_en: yup.string().required("Please enter a valid country"),
  postalcode_en: yup.string().required("Please enter a valid Postal Code"),
  postalcode_end: yup.string().required("Please enter a valid Postal Code"),
  prefecture_en: yup.string().required("Please enter a Prefecture"),
  // district_en: yup.string().required("Please enter a valid District"),
  town_en: yup.string().required(" Please enter a Town/Ward"),
  // state_en: yup.string().required("Please enter a valid State"),
  phoneNumber: yup
    .string()
    // .matches(specialChar, "cannot include special char")
    .min(10, "phone number must be of 10 digits")
    .max(11, "too long")
    .required("Please enter business phone number")
    .typeError("Cannot include char"),

  phoneNumber2: yup
    .string()
    .min(11, "phone number must be of 11 digits")
    .max(11, "too long"),
  house_number: yup
    .string()
    .required("Please enter a Building Name/Room Number"),
});

export const validationSchemaCreateAccount = yup.object({
  // dob: yup.string().required('Please enter your date of birth'),
  dob: yup
    .date()
    .max(new Date(Date.now() - 504921600000), "You must be at least 16 years")
    .required("Required")
    .typeError("please enter valid date"),
  termsAndConditions: yup
    .bool()
    .oneOf([true], "You need to accept the terms and conditions"),
  gender: yup.string().required("Please enter gender information"),
  nationality: yup.string().required("Please enter nationality information"),
});

export const verifyPasswordOtpValidation = yup.object({
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
  bId: yup.string().required("Business ID is Required"),
  email: yup.string().email().required("Email is Required"),
  code: yup.string().required("OTP is Required"),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Confirm Password is Required"),
});

export const validationSchemaForgetPassword = yup.object({
  email: yup.string().required("Please enter a valid Email").email(),
  bId: yup.string().required("Please enter a valid Business ID"),
});

export const validationSchemaLogin = yup.object({
  email: yup
    .string()
    .required("Please enter a valid email or mobile number")
    .matches(
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/m,
      "Enter a valid email"
    ),
  businessAccessId: yup.string().required("Please enter a valid Business Id"),
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
});

export const validationSchemaOtp = yup.object({
  code: yup
    .number()
    .typeError("Please enter a number")
    .required("Please enter a valid otp "),
});

export const validationSchemaBusinessKyc = yup.object({
  corporateNumber: yup.string().required("Corporate Number is Required"),
  // registerTradeName: yup.string().required("Register Trade Name is Required"),
  establishedDate: yup
    .date()
    .max(new Date(Date.now()), "Future date not allowed")
    .required("Established Date is Required")
    .typeError("please enter valid date"),
  // companyType: yup.string().required("Company Type is required"),
});

export const ValidationSchemaPersonalKyc = yup.object({
  dob: yup
    .date()
    .max(new Date(Date.now() - 504921600000), "You must be at least 16 years")
    .required("Required")
    .typeError("please enter valid date"),
  firstName: yup
    .string()
    .matches(/^[a-zA-Z-' ]+$/, "Name can only contain letters")
    .required("Please enter first name")
    .typeError("Input must be a alphabet"),
  middleName: yup
    .string()
    .matches(/^[a-zA-Z-' ]+$/, "Name can only contain letters")
    .typeError("Input must be a alphabet"),
  lastName: yup
    .string()
    .matches(/^[a-zA-Z-' ]+$/, "Family Name only contains alphabate")
    .required("Please enter family name")
    .typeError("Input must be a string"),

  phone: yup.string().matches(/^[0-9]{11}$/, "Phone Number Must be 11 Digits"),

  occupation: yup
    .string()
    .required("Please enter your Occupation")
    .typeError("Input must be a string"),

  country_en: yup
    .string()
    .required("Please enter a country name")
    .typeError("Input must be string"),

  postalcode_en: yup
    .string()
    .required("Please enter a postal code")
    .typeError("Input must be number"),
  // .max(3, "Cannot be more than 3 here"),
  // .min(3, "kamal"),

  postalcode_end: yup
    .number()
    .required("Please enter a postal code")
    .typeError("Input must be number"),

  prefecture_en: yup.string().required("Please enter your prefecture"),
  city_en: yup.string().required("Please enter your City/Village"),
  town_en: yup.string().required("Please enter your town/ward"),
  house_number: yup
    .string()
    // .typeError("Please enter a number")
    .required("Please enter your Building/Room Number"),
  documentType: yup.string().required("Please enter your ID Document Type."),
  idNumber: yup.string().required("Please enter your ID Number"),
  issueDate: yup
    .date()
    .max(new Date(), " Please do not enter future date")
    .required("Please enter the date of issue")
    .typeError("please enter valid date"),
  issueFrom: yup.string().required(" Please enter the Issue Date"),
  issuedBy: yup.string().required("Please enter your Issue By"),
  validDate: yup
    .date()
    .min(new Date(Date.now() - 86400000), "Please do not enter past date")
    .required("Please enter valid date")
    .typeError("please enter valid date"),
  secondaryNumber: yup.number().min(10, "must be atleast 10 digits"),
  // frontImage: yup.string().required("Please Upload Front Image of Document"),
});

export const validationSchemaAddShop = yup.object({
  name_en: yup.string().required("Please enter shop name"),
  postalcode_en: yup
    .number()
    .typeError("Please enter a number")
    .required("Please enter postal code "),
  // category: yup
  //   .string()
  //   .required("Select business Category")
  //   .typeError("Select business Category"),
  // subCategory: yup
  //   .string()
  //   .required("Select business SubCategory")
  //   .typeError("Select business SubCategory"),
  country_en: yup.string().required("Please enter your country"),
  openingTime: yup.string().required("Please enter Opening Time"),
  closingTime: yup.string().required("Please enter Closing Time"),
  town_en: yup.string().required("Please enter town/ward"),
  city_en: yup.string().required("Please enter city/village."),
  prefecture_en: yup.string().required("Please enter Prefecture"),
  // handlingServices: yup.string().required("Please select Handling services"),
  email: yup
    .string()
    .required("Please input email address")
    .test(
      "test-name",
      "Please enter valid email address",
      function (value: any) {
        const emailRegexValidation = emailRegex;
        let isValidEmail = emailRegexValidation.test(value);
        if (!isValidEmail) {
          return false;
        }
        return true;
      }
    ),
  phoneNumber: yup
    .string()
    .min(11, "number must be of 11 digits")
    .max(11, "max number is 11")
    .typeError("Please enter a mobile number")
    .required("Please enter contact number"),

  postalcode_end: yup
    .string()
    .required("Postal Code is Required")
    .max(4, "Must be 4 characters long"),

  house_number: yup
    .string()
    .required("Please enter house number")
    .typeError("Please enter your houseNo"),
});
export const validationSchemeSearchStaff = yup.object({
  email: yup
    .string()
    .required("Please enter email address")
    .test(
      "test-name",
      "Please enter valid email address",
      function (value: any) {
        const emailRegexValidation = emailRegex;
        let isValidEmail = emailRegexValidation.test(value);
        if (!isValidEmail) {
          return false;
        }
        return true;
      }
    ),
  phonenumber: yup
    .string()
    // .matches(specialChar, "cannot include special char")
    .min(10, "phone number must be of 11 digits")
    .max(11, "phone number should not exceed 11 digits")
    .required("Please enter business phone number")
    .typeError("Cannot include char"),
});

export const validationSchemaChangeEmail = yup.object({
  newEmail: yup
    .string()
    .required("field-required")
    .test(
      "test-name",
      "Please enter valid email address",
      function (value: any) {
        const emailRegexValidation = emailRegex;
        let isValidEmail = emailRegexValidation.test(value);
        if (!isValidEmail) {
          return false;
        }
        return true;
      }
    ),
});

export const validationSchemaChangePassword = yup.object({
  currentPassword: yup
    .string()
    .required("You must enter your current password"),
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Confirm Password is Required"),
});

export const validatePasswordChange = yup.object({
  code: yup
    .string()
    .min(6, "code must be atleast 6 digits")
    .required("code is required, for password change"),
  password: yup
    .string()
    .required("Please enter password")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
      "Password needs to contain at least one special characters, one number and one uppercase."
    ),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Confirm Password is Required"),
});

export const validationSchemaAddMasterProduct = yup.object({
  name: yup.string().required("Required"),
  region_origin: yup.string().required("Required"),
  brandId: yup.string().required("Required"),
  // businessType: yup.string().required("Required"),
  keyWordValue: yup.string().required("Required"),
  categoryId: yup.string().required("Required"),
  model_name: yup.string().required("Required"),
  model_no: yup.string().required("Required"),
  // country_en: yup.string().required("Required"),
  // ecCategoryId: yup.string().required("Required"),
  // ecCategoryId: yup.array()
  //   .of(yup.string().required('Required'))
  //   .min(1, 'At least one string is required'),
  // ecCategoryId: yup.array()
  //   .nullable()
  //   .of(yup.string().uuid('Invalid UUID format'))
  //   .min(1, 'At least one string is required'),
  // ecCategoryId: yup.array().required("Required").of(yup.string()),
  multipleCategory: yup.string().required("Required"),
  taxCategory: yup.string().required("Required"),
  manufactureId: yup.string().required("Required"),
  jan_ean_instore_code: yup.string().required("Required"),
});

export const validationSchemaPatchMasterProduct = yup.object({
  name: yup.string().required("Required"),
  region_origin: yup.string().required("Required"),
  brandId: yup.string().required("Required"),
  // businessType: yup.string().required("Required"),
  keyWordValue: yup.string().required("Required"),
  categoryId: yup.string().required("Required"),
  model_name: yup.string().required("Required"),
});


export const validationSchemaAddSubscription = yup.object({
  title_en: yup.string().required("Please enter shop name"),
  price: yup.string().required("Please enter price"),
  description: yup.string().required("Please enter description"),
  // features: yup
  //   .string()
  //   .required("Select features")
  //   .typeError("Select features"),
  expireDays: yup.string().required("Please enter expiry date"),
  // permissions: yup.string().required("Please enter permission"),
  // payment: yup.string().required("Please enter payment"),
  // status: yup.string().required("Please enter payment"),
});

// for FAQS
export const validationSchemaAddFaqs = yup.object({
  question_en: yup.string().required("Please enter shop name"),
  answer_en: yup.string().required("Please enter price"),
  // category: yup.string().required("Please enter description"),
});
