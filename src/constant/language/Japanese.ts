export const japeneseTranslation = {
  translation: {
    testing: "テスト",
    language: "言語",
    conversion: "変換",
    test: "注文注文注文注文注文",
    english: "注文注文",
    wizardTitle1: "SHOPS NEARBY",
    wizardTitle2: "注文",
    wizardTitle3: "MEMBERSHIP",
    wizardTitle4: "REWARD POINTS",
    wizardSubTitle1: "Find the nearest shop in a click.",
    wizardSubTitle2: "Place your order in no time.",
    wizardSubTitle3:
      "Your subscriptions provides you access to exclusive offers and promotions with your favorite shops.",
    wizardSubTitle4: "Get reward poitns as well as discount coupons.",
  },
};
