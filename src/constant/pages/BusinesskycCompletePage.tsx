import {
  Grid,
  Box,
  TextField,
  Button,
  Typography,
  IconButton,
} from "@mui/material";
const BusinesskycCompletePage = () => {
  return (
    <>
      <Typography>Thank you for submitting KYC form!</Typography>
      <Typography>Please check back later for admin verfication</Typography>
    </>
  );
};

export default BusinesskycCompletePage;
