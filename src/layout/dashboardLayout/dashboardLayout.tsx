import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import CategoryOutlinedIcon from '@mui/icons-material/CategoryOutlined';
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import DashboardIcon from "@mui/icons-material/Dashboard";
import EmailIcon from '@mui/icons-material/Email';
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import FactoryOutlinedIcon from '@mui/icons-material/FactoryOutlined';
import GroupIcon from "@mui/icons-material/Group";
import MenuIcon from "@mui/icons-material/Menu";
import QuizOutlinedIcon from "@mui/icons-material/QuizOutlined";
import RadarIcon from '@mui/icons-material/Radar';
import StoreIcon from "@mui/icons-material/Store";
import SubscriptionsIcon from "@mui/icons-material/Subscriptions";
import SubtitlesOutlinedIcon from '@mui/icons-material/SubtitlesOutlined';
import { Collapse } from "@mui/material";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import MuiDrawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { CSSObject, Theme, styled, useTheme } from "@mui/material/styles";
import Image from "next/image";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import withAuthProtected from "../../../hoc/PrivateRoute";
import { RootState } from "../../../redux/store";
import ProfileMenu from "../navbarcomponents/profileMenu";

import masterProductTab from "../../../public/masterProducttab.svg";
import order from "../../../public/order.png";
import shopTab from "../../../public/shoptab.svg";
const drawerWidth = 240;
interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}


const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(16)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(14)} + 1px)`,
  },
});

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

interface Props {
  mainPage: any;
  window?: () => Window;
}
function ResponsiveDrawer(props: any) {
  const { title, expandIcon, collapseIcon, path, subNav, icon } = props;
  const router = useRouter();
  const theme = useTheme();
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const sidebarOpen = localStorage.getItem("sidebarOpen");
  const [open, setOpen] = React.useState(sidebarOpen === "true" ? true : false);
  const [openMasterProductSubDrawer, setOpenMasterProductSubDrawer] = React.useState(false);
  const [openProductSubDrawer, setOpenProductSubDrawer] = React.useState(false);
  const [openShopSubDrawer, setOpenShopSubDrawer] = React.useState(false);
  const [bool, setBool] = useState<any>(false);
  const [expandMore, setExpandMore] = useState<any>(false);

  const userDetail: any = useSelector(
    (state: RootState) => state?.auth?.userDetail
  );

  function handleClick(bool: boolean) {
    setBool(!bool);
    setExpandMore(!expandMore);
  }

  const handleDrawerOpen = () => {
    setOpen(true);
    localStorage.setItem("sidebarOpen", "true");
  };

  const handleDrawerClose = () => {
    setOpen(false);
    localStorage.setItem("sidebarOpen", "false");
  };

  const handleDrawerToggle = () => {
    const isDrawerOpen = localStorage.getItem("sidebarOpen") === "true";
    setOpen(!isDrawerOpen);
    localStorage.setItem("sidebarOpen", String(!isDrawerOpen));
  };

  const drawer = [
    {
      name: "Dashboard",
      icon: <DashboardIcon />,
      route: "/dashboard",
    },
    {
      name: "Shop",
      icon: <StoreIcon />,
      route: "",
      expandIcon: <ExpandMore />,
      collapseIcon: <ExpandLess />,
      setOpen: setOpenShopSubDrawer,
      open: openShopSubDrawer,
      dropDown: [
        {
          name: "Order",
          icon: <img src={order.src} style={{ maxWidth: '24px' }} />,
          route: "/shop/order",
          mainTitle: "Shop",
        },
        {
          name: "Shop",
          icon: <StoreIcon />,
          route: "/shop",
          mainTitle: "Shop",
        },
      ],
    },
    {
      name: "Staff",
      icon: <GroupIcon />,
      route: "/staff",
    },
    {
      name: "Master Product",
      icon: <Image src={masterProductTab} style={{ opacity: "0.6" }} />,
      route: "",
      expandIcon: <ExpandMore />,
      collapseIcon: <ExpandLess />,
      setOpen: setOpenMasterProductSubDrawer,
      open: openMasterProductSubDrawer,
      dropDown: [
        {
          name: "All Products",
          icon: <Image src={shopTab} style={{ opacity: "0.6" }} />,
          route: "/master-product/allproduct",
          mainTitle: "Master Product",
        },
        {
          name: "Brand",
          icon: <AddShoppingCartIcon />,
          route: "/master-product/brand",
          mainTitle: "Master Product",
        },
        {
          name: "Manufacture",
          icon: <FactoryOutlinedIcon />,
          route: "/master-product/manufacture",
          mainTitle: "Master Product",
        },
        {
          name: "Variation",
          icon: <SubtitlesOutlinedIcon />,
          route: "/master-product/variation",
          mainTitle: "Master Product",
        },
        {
          name: "Category",
          icon: <CategoryOutlinedIcon />,
          route: "/master-product/category",
          mainTitle: "Master Product",
        },
        {
          name: "Ec Category",
          icon: <CategoryOutlinedIcon />,
          route: "/master-product/ec-category",
          mainTitle: "Master Product",
        },
      ],
    },
    {
      name: "Product",
      icon: <Image src={masterProductTab} style={{ opacity: "0.6" }} />,
      route: "",
      expandIcon: <ExpandMore />,
      collapseIcon: <ExpandLess />,
      setOpen: setOpenProductSubDrawer,
      open: openProductSubDrawer,
      dropDown: [
        {
          name: "My Products",
          icon: <Image src={shopTab} style={{ opacity: "0.6" }} />,
          route: "/product/myproduct",
          mainTitle: "Master Product",
        },
        {
          name: "Featured Products",
          icon: <AddShoppingCartIcon />,
          route: "/product/featuredProduct",
          mainTitle: "Master Product",
        },
      ],
    },
    {
      name: "Subscription",
      icon: <SubscriptionsIcon />,
      route: "/subscription",
    },
    {
      name: "FAQS",
      icon: <QuizOutlinedIcon />,
      route: "/faqs",
    },
    {
      name: "Shop Radius",
      icon: <RadarIcon />,
      route: "/shopradius",
    },
  ];

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar position="fixed" open={open} >
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <div className="display-flex">
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerToggle}
              edge="start"
              sx={{
                marginRight: 5,
                ...(open && { display: "none" }),
              }}
              className="menu-dashboard"
            >
              <MenuIcon />
            </IconButton>
            {/* <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerToggle}
              edge="start"
              className="menu-dashboard-sm"
            // sx={{
            //   marginRight: 5,
            //   ...(open && { display: "none" }),
            // }}
            >
              <MenuIcon />
            </IconButton> */}
            <Typography variant="h5" noWrap component="div">
              Shopo Central
            </Typography>
          </div>

          <Box sx={{
            display: "flex", alignItems: 'center', gap: '1rem'
          }}>
            <Link href={'/message'} style={{ cursor: 'pointer' }}>
              <EmailIcon sx={{ cursor: 'pointer' }} />
            </Link>
            <ProfileMenu />
          </Box>
        </Toolbar >
      </AppBar >
      <Drawer variant="permanent" open={open} >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {drawer?.map((drawers, index) => (
            <ListItem key={index} disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? "initial" : "center",
                  px: 2.5,
                }}
                onClick={() => {
                  drawers?.dropDown
                    ? drawers.setOpen(!drawers.open)
                    : Router.push(drawers.route);
                }}
                className={
                  router.route !== drawers.route
                    ? "nav-unselected-menu"
                    : "nav-selected-menu"
                }
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: open ? 3 : "auto",
                    justifyContent: "center",
                  }}
                >
                  {drawers?.icon}
                </ListItemIcon>
                <ListItemText
                  primary={drawers?.name}
                  sx={{ opacity: open ? 1 : 0 }}
                />
                {drawers.open ? drawers.expandIcon : drawers.collapseIcon}
              </ListItemButton>
              {drawers.dropDown && (
                <Collapse
                  in={drawers.open}
                  timeout="auto"
                  unmountOnExit
                  className={open ? "collapse-bar-open" : "collapse-bar-close"}
                >
                  <List component="div" disablePadding>
                    {drawers.dropDown.map((item: any, index?: number) => (
                      <Link href={item.route} key={index}>
                        <ListItemButton
                          sx={{ pl: 4, gap: "8px" }}
                          key={item.title}
                          style={{
                            color: router.route === item.path ? "#1a76d2" : "",
                          }}
                        >
                          <ListItemIcon sx={{ minWidth: "30px" }}>{item.icon}</ListItemIcon>
                          <a>
                            <ListItemText primary={item.name} />
                          </a>
                        </ListItemButton>
                      </Link>
                    ))}
                  </List>
                </Collapse>
              )}
            </ListItem>
          ))}
        </List>
      </Drawer>

      <Box
        component="main"
        sx={{
          flexGrow: 1,
          padding: "24px",
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          height: "fit-content",
        }}
      >
        <DrawerHeader />
        {props.mainPage}
      </Box>
    </Box >
  );
}
export default withAuthProtected(ResponsiveDrawer);
