import React from "react";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import StarBorder from "@mui/icons-material/StarBorder";

import DashboardIcon from "@mui/icons-material/Dashboard";
import GroupIcon from "@mui/icons-material/Group";
import StoreIcon from "@mui/icons-material/Store";
import Groups2Icon from "@mui/icons-material/Groups2";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
import ArchitectureIcon from "@mui/icons-material/Architecture";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";
import BusinessCenterIcon from "@mui/icons-material/BusinessCenter";
import Inventory2Icon from "@mui/icons-material/Inventory2";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";

const data = [
  // {
  //   title: "Forms",
  //   icon: <GroupIcon />,
  //   expandIcon: <ExpandLess />,
  //   collapseIcon: <ExpandMore />,
  //   subNav: [
  //     {
  //       title: "Personal Kyc Form",
  //       icon: <StarBorder />,
  //       path: "/kycforms/displayforms/personal/",
  //     },
  //     {
  //       title: "Business Kyc Form",
  //       icon: <StarBorder />,
  //       path: "/kycforms/displayforms/business/",
  //     },
  //   ],
  // },

  // {
  //   title: "Shop",
  //   icon: <StoreIcon />,
  //   expandIcon: <ExpandLess />,
  //   collapseIcon: <ExpandMore />,
  //   subNav: [
  //     {
  //       title: "View Shop Details",
  //       icon: <StarBorder />,
  //       path: "/shop/",
  //     },
  //     {
  //       title: "Add Shop Details",
  //       icon: <StarBorder />,
  //       path: "/shop/shopdetails",
  //     },
  //   ],
  // },
  // {
  //   title: "Branch",
  //   icon: <Groups2Icon />,
  //   expandIcon: <ExpandLess />,
  //   collapseIcon: <ExpandMore />,
  //   subNav: [
  //     {
  //       title: "View Branches",
  //       icon: <StarBorder />,
  //       path: "/branches/viewbranches",
  //     },
  //     {
  //       title: "Add Branches",
  //       icon: <StarBorder />,
  //       path: "/branches/addbranches",
  //     },
  //   ],
  // },
  // {
  //   title: "Staff",
  //   icon: <ArchitectureIcon />,
  //   expandIcon: <ExpandLess />,
  //   collapseIcon: <ExpandMore />,
  //   subNav: [
  //     {
  //       title: "Add Staff",
  //       icon: <StarBorder />,
  //       path: "/staff/addstaff",
  //     },
  //     {
  //       title: "View Staff",
  //       icon: <StarBorder />,
  //       path: "/staff/viewstaff",
  //     },
  //   ],
  // },
  {
    title: "Subscriptions",
    icon: <InsertDriveFileIcon />,
    expandIcon: <ExpandLess />,
    collapseIcon: <ExpandMore />,
    subNav: [
      {
        title: "Subscriptions plans",
        icon: <StarBorder />,
        path: "/dashboard/",
      },
    ],
  },


];
export default data;
