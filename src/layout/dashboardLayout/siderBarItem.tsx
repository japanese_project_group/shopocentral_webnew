import React, { useState } from "react";
import List from "@mui/material/List";
import Link from "next/link";
import Collapse from "@mui/material/Collapse";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import Router from "next/router";
const SidebarItem = (props: any) => {
  const [expandMore, setExpandMore] = useState(false);
  const { title, expandIcon, collapseIcon, path, subNav, icon } = props;
  const [bool, setBool] = useState(false);
  function handleClick(bool: boolean) {
    if (subNav.length > 0) {
      setBool(!bool);
      setExpandMore(!expandMore);
    }
  }
  return (
    <>
      <ListItemButton onClick={() => handleClick(bool)}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={title} sx={{ marginRight: -10 }} />{" "}
        <ListItemIcon sx={{ marginLeft: 10 }}>
          {expandMore ? expandIcon : collapseIcon}
        </ListItemIcon>
      </ListItemButton>

      {subNav?.length > 0 && (
        <Collapse in={bool} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {subNav?.map((item: any) => (
              <Link href={item.path} key={item.title}>
                <a>
                  <ListItemButton sx={{ pl: 4 }}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.title} />
                  </ListItemButton>
                </a>
              </Link>
            ))}
          </List>
        </Collapse>
      )}
    </>
  );
};

export default SidebarItem;
