import * as React from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { useRouter } from "next/router";
import { Box, Typography } from "@mui/material";
import withAuth from "../../../hoc/PublicRoute";

interface Props {
  children: React.ReactNode;
}

const AuthLayout = ({ children }: Props) => {
  const router = useRouter();

  return (
    <Grid container component="main" sx={{ height: "100vh" }}>
      <Grid item xs={12} sm={12} md={7} component={Paper} elevation={6} square>
        <Box
          sx={{
            // position: 'absolute',
            padding: "1rem",
            top: 1,
            left: 2,
            "&:hover": {
              cursor: "pointer",
            },
          }}
          onClick={() => router.push("/")}
        ></Box>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {children}
        </Box>
        <Box position="sticky" sx={{ padding: "1rem" }}>
          <Typography align="center">shop</Typography>
        </Box>
      </Grid>
      <Grid
        item
        md={5}
        sx={{ display: { xs: "none", md: "flex" }, position: "relative" }}
      >
        <div
          style={{
            backgroundImage: "url(/images/auth/authImg.png)",
            backgroundSize: "cover",
            width: "100%",
            height: "100%",
          }}
        />
        <Box
          sx={{
            position: "absolute",
            right: 10,
            top: 10,
          }}
        >
          layout
        </Box>
      </Grid>
    </Grid>
  );
};

export default withAuth(AuthLayout);
