import Styles from "styles/global"

export const AppWrapper = ({ children }: { children: React.ReactNode }) => {
    return (
        <>
            <Styles />
            {typeof window !== 'undefined' && children}
        </>
    )
}