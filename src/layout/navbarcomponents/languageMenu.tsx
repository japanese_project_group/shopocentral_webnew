import * as React from "react";
import Box from "@mui/material/Box";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import "../../constant/language/i18n";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { setLang } from "../../../redux/features/langSlice";
import { useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import Image from "next/image";
import japan from "../../../public/japan.svg";
import nepal from "../../../public/nepal.svg";
import unitedstates from "../../../public/unitedstates.svg";
export default function LanguageMenu() {
  const dispatch = useDispatch();
  const Lang = useSelector((state: RootState) => state?.lang?.lang);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const { t, i18n } = useTranslation();
  const changeLanguageHandler = (value: string) => {
    dispatch(setLang(value));
    i18n.changeLanguage(value);
  };

  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Tooltip title="language settings">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? "language-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "40px",
                maxWidth: "40px",
              }}
            >
              {Lang == "np" ? (
                <>
                  <Image src={nepal} />

                </>
              ) : Lang == "jp" ? (
                <>
                  <Image src={japan} />
                </>
              ) : (
                <>
                  <Image src={unitedstates} />
                </>
              )}
            </Box>
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="language-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={() => changeLanguageHandler("en")}>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box sx={{ width: "100%" }}>
              <Image src={unitedstates} height="30%" />{" "}
            </Box>
            <Box sx={{ width: "100%" }}> {t("English")}</Box>
          </Box>
        </MenuItem>
        <MenuItem onClick={() => changeLanguageHandler("jp")}>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box sx={{ width: "100%" }}>
              <Image src={japan} height="30%" />
            </Box>
            <Box sx={{ width: "100%" }}>{t("Japan")}</Box>
          </Box>
        </MenuItem>
        <MenuItem onClick={() => changeLanguageHandler("np")}>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box sx={{ width: "100%" }}>
              <Image src={nepal} height="40%" />{" "}
            </Box>
            <Box sx={{ width: "100%" }}> {t("Nepal")}</Box>
          </Box>
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
}
