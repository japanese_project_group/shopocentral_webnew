// build in library
import { useState } from "react";

// third party library
import { Button, Typography } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { isEmpty } from "lodash";

// redux
import { useGetAllNotificationDataQuery, usePostPersonalKycStatusMutation, useUpdateReadNotificationDataMutation } from "../../../redux/api/user/user";

// components
import { NotificationsList } from "../../common/Components";

export const NotificationComponent = () => {
  const [merchantNotificationsData, setMerchantNotificationsData] =
    useState<any>([]);
  const { data: AllNotificationData } = useGetAllNotificationDataQuery()
  const [markAll, setMarkAll] = useState<any>(AllNotificationData?.data?.isRead)

  const [updateNotification, { data: UpdateData }] = useUpdateReadNotificationDataMutation();
  const [statusUpdate, { data: successData, isSuccess }] =
    usePostPersonalKycStatusMutation();

  const handleMarkAll = () => {
    setMarkAll(true)
    updateNotification()
  }

  return (
    <div>
      <Typography
        className="popover-notification"
        style={{ paddingLeft: "5%" }}
      >
        <div className="popover-notification-header">
          <div className="popover-notification-header-notification">
            Notifications
          </div>
          <Button onClick={handleMarkAll} variant="text" sx={{ color: "#202020", fontSize: "12px", textTransform: "capitalize" }}>Mark all as read</Button>
        </div>
        {!isEmpty(AllNotificationData) ? (
          AllNotificationData?.allNotification?.map(
            (data: any, key: number) => (
              <NotificationsList data={data} key={key} type="seeAll" />
            )
          )
        ) : (
          <CircularProgress style={{ margin: "20% 50% 0% 40%" }} />
        )}
      </Typography>
    </div>
  );
};
