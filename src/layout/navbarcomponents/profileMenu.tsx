// build in library
import Link from "next/link";
import Router from "next/router";
import React, { useEffect, useState } from "react";

// third party library
import { Settings } from "@mui/icons-material";
import Logout from "@mui/icons-material/Logout";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { Badge } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Popover from "@mui/material/Popover";
import Tooltip from "@mui/material/Tooltip";
import PopupState, { bindPopover, bindTrigger } from "material-ui-popup-state";
import { useDispatch } from "react-redux";
import { io } from "socket.io-client";

// redux
import { useGetAllDataQuery, useGetAllNotificationDataQuery } from "../../../redux/api/user/user";
import { logoutUser } from "../../../redux/features/authSlice";

// components
import { NotificationsList } from "../../common/Components";
import { userId } from "../../common/utility/Constants";

export default function ProfileMenu() {
  const [modalOpen, setModalOpen] = React.useState<boolean>(false);
  const [socketData, setSocketData] = React.useState<any>([]);
  const [notifications, setNotifications] = React.useState<any>([]);
  const [merchantNotificationsData, setMerchantNotificationsData] = React.useState<any>([]);
  const { data: AllNotificationData } = useGetAllNotificationDataQuery()
  const [userProfile, setUserProfile] = useState<any>("");

  //fetch user data
  const { data: userData, isSuccess, refetch } = useGetAllDataQuery();

  // const { profileData } = useSelector((state) => state?.profile);

  useEffect(() => {
    setUserProfile(userData?.data?.profile?.userImage);
    // setUserProfile(profileData);
  }, [userData]);

  const dispatch = useDispatch();
  function handleLogOut() {
    Router.replace("/login");
    localStorage.clear();
    dispatch(logoutUser());
  }
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpen = () => {
    setModalOpen(!modalOpen);
  };


  React.useEffect(() => {
    const socket = io("http://stageapi.shopo.jp");
    socket
      .emit("user-notification", { userId: userId })
      .on("user-notification", (data: any) => {
        setMerchantNotificationsData(data);
      });
  }, []);

  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        {/* <Typography sx={{ minWidth: 100 }}>Contact</Typography>
        <Typography sx={{ minWidth: 100 }}>Profile</Typography> */}
        <PopupState variant="popover" popupId="demo-popup-popover">
          {(popupState) => (
            <div>
              <Badge
                color="secondary"
                badgeContent={AllNotificationData?.totalunReadCount}
                {...bindTrigger(popupState)}
                style={{ cursor: "pointer" }}
              >
                <NotificationsIcon />
              </Badge>
              <Popover
                {...bindPopover(popupState)}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "center",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "center",
                }}
                style={{ marginTop: "13px", width: "437px", minWidth: "360px" }}
                className="popover-box-notification"
              >
                <div className="popover-notification">
                  {AllNotificationData?.allNotification ?
                    <Box className="popover-notification-header" >
                      <div className="popover-notification-header-notification">
                        Notifications
                      </div>
                      <div className="popover-notification-header-seemore">
                        <Link href="/dashboard/notifications">See All</Link>
                      </div>
                    </Box>
                    :
                    <Box padding={'1.5rem'} textAlign={'center'} >
                      No Notification Founds
                    </Box>
                  }
                  {AllNotificationData?.allNotification
                    ?.slice(0, 4)
                    ?.map((data: any, key: number) => (
                      <NotificationsList data={data} key={key} />
                    ))}
                </div>
              </Popover>
            </div>
          )}
        </PopupState>
        <Tooltip title="Account settings">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 4 }}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Avatar sx={{ width: 32, height: 32 }}>
              {userProfile ? (
                <img
                  src={userProfile}
                  alt="profile"
                  style={{
                    objectFit: "cover",
                    height: 32,
                    width: 32,
                    borderRadius: "50%",
                  }}
                />
              ) : (
                userData?.data?.firstName?.charAt(0)?.toUpperCase()
              )}
            </Avatar>
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem
          onClick={() => {
            Router.push("/profile");
          }}
        >
          <Avatar src={userProfile} style={{ objectFit: "contain" }} /> Profile
        </MenuItem>

        <MenuItem
          onClick={() => {
            Router.push("/profile/settings");
          }}
        >
          <ListItemIcon>
            <Settings fontSize="small" />
          </ListItemIcon>
          Settings
        </MenuItem>
        <MenuItem onClick={handleLogOut}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
}
