import {
  Box,
  Button,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Typography
} from "@mui/material";
import { DetailsTopView } from "../../customComponents/detailsTopView";

const subscriptionBox = [
  {
    title: "Basic",
    feature: ["feature", "feature1", "feature2"],
    amount: 3000,
    color: "#FF0000",
  },
  {
    title: "Standard",
    feature: ["feature", "feature1", "feature2"],
    amount: 3000,
    color: "#263238",
  },
  {
    title: "Advanced",
    feature: ["feature", "feature1", "feature2"],
    amount: 3000,
    color: "#0CB108",
  },
];
const SubscriptionPage = () => {
  const handleChangeButton = () => { };

  return (
    <>
      <DetailsTopView
        headerName="Subscriptions"
        handleButtonChange={handleChangeButton}
        buttonText="Add New Subscription"
        searchField={false}
      />
      <Box
        boxShadow={"5"}
        padding={"4rem  1rem 2rem 1rem"}
        sx={{
          borderRadius: "16px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box>
          <Box
            sx={{
              bgcolor: "background.default",
              display: "grid",
              gridTemplateColumns: { md: "1fr 1fr 1fr", sm: "1fr" },
              columnGap: 8,
              rowGap: 4,
            }}
          >
            {subscriptionBox.map((item, index) => (
              <Box
                key={index}
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  boxShadow: "2px 1px 10px #00000025",
                  gap: "30px",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingBottom: "1rem",
                  borderRadius: "20px",
                  width: "210px",
                }}
              >
                <Box
                  sx={{
                    backgroundColor: item.color,
                    color: "#fff",
                    borderRadius: "10px",
                    textAlign: "center",
                    height: "60px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "-15px",
                    width: "100%",
                  }}
                >
                  <Typography variant="h5">{item.title}</Typography>
                </Box>
                <Box sx={{ textAlign: "center" }}>
                  <List>
                    {item.feature.map((singleFeature, index) => (
                      <ListItem disablePadding>
                        <ListItemButton>
                          <ListItemText primary={singleFeature} />
                        </ListItemButton>
                      </ListItem>
                    ))}
                  </List>
                </Box>
                <Typography
                  variant="h6"
                  fontWeight={"700"}
                  sx={{ textAlign: "center" }}
                >
                  ¥ {item.amount}
                </Typography>
                <Button
                  variant="contained"
                  color="primary"
                  style={{ width: "fit-content" }}
                >
                  Buy Now
                </Button>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default SubscriptionPage;
