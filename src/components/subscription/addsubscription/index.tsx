// Build in function
import React, { useEffect, useState } from "react";

// library
import CancelIcon from "@mui/icons-material/Cancel";
import {
  Avatar,
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Modal,
  TextField,
  Typography
} from "@mui/material";
import { GridCheckCircleIcon } from "@mui/x-data-grid";
import { FormikProvider, useFormik } from "formik";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// redux
import {
  usePostAddMerchantSubscriptionDataMutation
} from "../../../../redux/api/user/user";

// components
import { DeleteForeverOutlined } from "@mui/icons-material";
import { InputComponent } from "../../../common/Components";
import { validationSchemaAddSubscription } from "../../../common/validationSchema";
import { DetailsTopView } from "../../customComponents/detailsTopView";

// custom style
const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "none",
  boxShadow: "3px 1px 20px #00000020",
  p: 4,
};

export const AddSubscription = () => {
  const router = useRouter();
  const [
    submitAddSubscription,
    { data: SubscriptionData, isSuccess, isError },
  ] = usePostAddMerchantSubscriptionDataMutation();

  const [keyword, setKeyword] = useState<any>([""]);
  const [error, setError] = useState<any>("");

  // Define a type for checkbox values
  type CheckboxValues = {
    pos: { create: boolean; read: boolean; update: boolean; delete: boolean };
    loyaltyPoints: { create: boolean; read: boolean; update: boolean; delete: boolean };
    offersRewards: { create: boolean; read: boolean; update: boolean; delete: boolean };
  };


  const [checkboxValues, setCheckboxValues] = useState<CheckboxValues>({
    pos: { create: false, read: false, update: false, delete: false },
    loyaltyPoints: { create: false, read: false, update: false, delete: false },
    offersRewards: { create: false, read: false, update: false, delete: false },
  });

  const handleCheckboxChange = (category: keyof CheckboxValues, action: keyof CheckboxValues[keyof CheckboxValues]) => {
    setCheckboxValues((prevValues) => ({
      ...prevValues,
      [category]: {
        ...prevValues[category],
        [action]: !prevValues[category][action],
      },
    }));
  };

  const isCategoryValid = (category: keyof CheckboxValues) => {
    return Object.values(checkboxValues[category]).some((value) => value);
  };


  const addFeatures = () => {
    setKeyword([...keyword, [""]]);
  };

  const onChangeKeyProduct = (e: any, index: any) => {
    const list = [...keyword];
    list[index] = e.target.value;
    setKeyword(list);
  };

  const delteBox = (index: any) => {
    const cloneKeyword = [...keyword];
    cloneKeyword.splice(index, 1);
    setKeyword(cloneKeyword);
  };

  useEffect(() => {
    if (isSuccess && SubscriptionData) {
      toast("Successfully Added!", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      router.push("./");
    }
  }, [isSuccess, SubscriptionData]);

  useEffect(() => {
    if (isError) {
      const parseData: any = error?.data;
      parseData?.message?.map((data1: any) => {
        return toast.error(data1, {
          autoClose: 2500,
          style: {
            fontSize: "14px",
          },
        });
      });
    }
  }, [isError]);

  const formik = useFormik({
    initialValues: {
      title_en: "",
      price: "",
      description: "",
      features: "",
      expireDays: "",
      permissions: "",
      payment: "",
      status: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemaAddSubscription,
    onSubmit: async (values: any) => {
      let body = {
        ...values,
        permissions: [],
        features: keyword,
      };
      submitAddSubscription(body)
        .unwrap()
        .catch((error: any) => setError(error));
      console.log("values:", values);
    },
  });
  const {
    values,
    handleSubmit,
    getFieldProps,
    setFieldValue,
    handleBlur,
    errors,
    touched,
  } = formik;

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    selectDataType?: string
  ) => {
    return (
      <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
        <div>{fieldName}:</div>
        <div style={{ width: "60%" }}>
          <InputComponent
            type={type}
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            errors={errors}
            touched={touched}
            disabled={disabled}
            setFieldValue={setFieldValue}
            values={values}
            // handleBlur={handleBlur}
            // label={type === "postalCode" && "grgrt"}
            // showTitle={
            //   type === "postalCode" ||
            //   (type === "secondaryPhoneNumber" && "grgrt")
            // }
            // addNumber={addNumber}
            // setAddNumber={setAddNumber}
            selectValue={selectValue}
            setSelectType={setSelectType}
            options={options}
            selectDataType={selectDataType}
          />
        </div>
      </div>
    );
  };

  const displayForms = () => {
    return (
      <div>
        <FormikProvider value={values}>
          <form onSubmit={handleSubmit}>
            <div>
              {fieldFormsText(
                "Title",
                "title_en",
                "text",
                errors.title_en,
                touched.title_en
              )}
              {fieldFormsText(
                "Price",
                "price",
                "text",
                errors.price,
                touched.price
              )}
              {fieldFormsText("Description ", "description", "text", errors.description, touched.description)}
              {fieldFormsText(
                "Shop Limit",
                "shopLimit",
                "text",
                errors.shopLimit,
                touched.shopLimit
              )}
              {fieldFormsText(
                "Staff Limit",
                "staffLimit",
                "text",
                errors.staffLimit,
                touched.staffLimit
              )}
              {fieldFormsText("Expiry Period (in days)", "expireDays",
                "text",
                errors.expireDays,
                touched.expireDays
              )}
              <Box
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <Typography>Features</Typography>

                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    width: "60%",
                  }}
                >
                  {keyword?.map((data: any, index: any) => (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        minWidth: "calc(100%)",
                        // width: "100%",
                      }}
                    >
                      <TextField
                        key={index}
                        // label="features"
                        size="small"
                        style={{ marginBottom: "10px", width: "100%" }}
                        onChange={(e) => onChangeKeyProduct(e, index)}
                        name={index}
                      />
                      <IconButton
                        onClick={(index) => delteBox(index)}
                        sx={{
                          borderRadius: "4px",
                          backgroundColor: "#ff000020",
                          marginTop: "-8px",
                        }}
                      >
                        <DeleteForeverOutlined sx={{ color: "#ff0000" }} />
                      </IconButton>
                    </div>
                  ))}

                  <Button
                    onClick={addFeatures}
                    variant="outlined"
                    sx={{
                      marginBottom: "10px",
                      // marginLeft: "10px",
                      fontSize: "14px",
                    }}
                  >
                    Add Features
                  </Button>
                </div>
              </Box>
              <Box display={"flex"} justifyContent={"space-between"}>
                <Typography >Permission</Typography>
                <Box width={"60%"}>
                  <Typography>POS</Typography>
                  <Box display="flex" gap="10px">
                    {/* <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.create} onChange={() => handleCheckboxChange('pos', 'create')} />}
                      label="Create"
                    /> */}
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.create} onChange={() => handleCheckboxChange('pos', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.read} onChange={() => handleCheckboxChange('pos', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.update} onChange={() => handleCheckboxChange('pos', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.delete} onChange={() => handleCheckboxChange('pos', 'delete')} />}
                      label="Delete"
                    />
                    {/* <FormControlLabel control={<Checkbox />} label="Delete" />
                    <FormControlLabel control={<Checkbox />} label="Read" />
                    <FormControlLabel control={<Checkbox />} label="Update" />
                    <FormControlLabel control={<Checkbox />} label="Delete" /> */}
                  </Box>
                  {/* {isCategoryValid('pos') ? null : <div style={{ color: 'red' }}>Select at least one checkbox in POS</div>} */}
                  <Typography marginTop={"1rem"}>Loyalty Points</Typography>
                  <Box display="flex" gap="10px">
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.create} onChange={() => handleCheckboxChange('loyaltyPoints', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.read} onChange={() => handleCheckboxChange('loyaltyPoints', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.update} onChange={() => handleCheckboxChange('loyaltyPoints', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.delete} onChange={() => handleCheckboxChange('loyaltyPoints', 'delete')} />}
                      label="Delete"
                    />
                  </Box>
                  <Typography marginTop={"1rem"}>Offers/Rewards</Typography>
                  <Box display="flex" gap="10px" marginBottom={"1rem"}>
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.create} onChange={() => handleCheckboxChange('offersRewards', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.read} onChange={() => handleCheckboxChange('offersRewards', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.update} onChange={() => handleCheckboxChange('offersRewards', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.delete} onChange={() => handleCheckboxChange('offersRewards', 'delete')} />}
                      label="Delete"
                    />
                  </Box>
                </Box>
              </Box>

              {/* {fieldFormsText(
                "Permission",
                "permissions",
                "checkbox",
                errors.permission,
                touched.permission
              )}{" "} */}
              {/* {fieldFormsText(
                "Payment",
                "payment",
                "text",
                errors.payment,
                touched.payment
              )}
              {fieldFormsText(
                "Status",
                "status",
                "text",
                errors.status,
                touched.status
              )} */}
            </div>
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              <Box sx={{ flex: "1 1 auto" }} />
              <Button
                type="submit"
                variant="contained"
                sx={{
                  "&:hover": {
                    backgroundColor: "#0069d9",
                    borderColor: "#0062cc",
                    boxShadow: "none",
                  }
                }}
              >
                Create Subscription
              </Button>
            </Box>
          </form>
        </FormikProvider>
      </div >
    );
  };

  const [success, setSuccess] = React.useState(true);
  const [open, setOpen] = React.useState(false);
  const openSubscriptionModal = () => setOpen(true);
  const handleClose = () => setOpen(false);

  setTimeout(() => {
    setOpen(false);
  }, 5000);

  const handleChangeButton = () => {
    router.push("./");
  };

  return (
    <Box>
      <DetailsTopView
        headerName="Subscriptions"
        handleButtonChange={handleChangeButton}
        buttonText="Back"
        searchField={false}
        isButton={true}
        isBackBtn={true}
      />
      <Box bgcolor={"#fff"} borderRadius={"1.5rem"} className="form-view-multistep-form box-kyc-multiple-step-form">
        {displayForms()}
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <ListItem sx={style} disableGutters style={{ borderRadius: "1rem" }}>
            {success ? (
              <>
                {" "}
                <ListItemAvatar>
                  <Avatar sx={{ bgcolor: "green" }}>
                    <GridCheckCircleIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Successfully Created" />
              </>
            ) : (
              <>
                {" "}
                <ListItemAvatar>
                  <Avatar sx={{ bgcolor: "red" }}>
                    <CancelIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Sorry, something went wrong.Please try again later" />
              </>
            )}
          </ListItem>
        </Modal>
      </Box>
    </Box>
  );
};
