// Build in function
import { useEffect, useState } from "react";

// library
import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  TextField,
  Typography
} from "@mui/material";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";

// redux
import { useGetMerchantIndividualSubscriptionDataQuery } from "../../../../redux/api/user/user";
import { DetailsTopView } from "../../customComponents/detailsTopView";

// components
// import { PageTopTitle } from "../../../common/pageTopTitle";

export const ViewSubscription = () => {
  const router = useRouter();
  const [ID, setID] = useState<any>();
  const [keyword, setKeyword] = useState<any>([""]);
  const [subscriptionData, setSubscriptionData] = useState<any>();
  const { data: SingleSubscriptionData, refetch } =
    useGetMerchantIndividualSubscriptionDataQuery(ID);

  const onChangeKeyProduct = (e: any, index: any) => {
    const list = [...keyword];
    list[index] = e.target.value;
    setKeyword(list);
  };

  useEffect(() => {
    setID(router.query.id);
  }, [router.query]);

  useEffect(() => {
    if (!isEmpty(SingleSubscriptionData)) {
      setSubscriptionData(SingleSubscriptionData);
    }
  }, []);

  // Define a type for checkbox values
  type CheckboxValues = {
    pos: { create: boolean; read: boolean; update: boolean; delete: boolean };
    loyaltyPoints: { create: boolean; read: boolean; update: boolean; delete: boolean };
    offersRewards: { create: boolean; read: boolean; update: boolean; delete: boolean };
  };


  const [checkboxValues, setCheckboxValues] = useState<CheckboxValues>({
    pos: { create: false, read: false, update: false, delete: false },
    loyaltyPoints: { create: false, read: false, update: false, delete: false },
    offersRewards: { create: false, read: false, update: false, delete: false },
  });

  const handleCheckboxChange = (category: keyof CheckboxValues, action: keyof CheckboxValues[keyof CheckboxValues]) => {
    setCheckboxValues((prevValues) => ({
      ...prevValues,
      [category]: {
        ...prevValues[category],
        [action]: !prevValues[category][action],
      },
    }));
  };

  const isCategoryValid = (category: keyof CheckboxValues) => {
    return Object.values(checkboxValues[category]).some((value) => value);
  };

  interface SingleSubscriptionData {
    fieldName: string;
    getFieldPropsValue: string;
    type?: string;
    errors?: any;
    touched?: any;
    disabled?: boolean;
    options?: any;
    selectValue?: any;
    setSelectType?: any;
    selectDataType?: string;
  }

  const handleCancel = () => {
    router.push("./");
  };

  const handleEdit = (id: any) => {
    router.push({
      pathname: `/subscription/edit`,
      query: { id },
    });
  };

  const subscriptionDetails = (title: string, data?: any, type?: string) => {
    return (
      <div className="display-flex-sb" style={{ padding: "6.5px" }}>
        <div className="shop-view-details-tab-title">{title}</div>
        {type !== "multiple" ? (
          <TextField
            disabled
            size="small"
            className="shop-view-details-tab-data"
            style={{ width: "60%" }}
            value={data}
          ></TextField>
        ) : (
          <div
            style={{
              width: "60%",
              display: "flex",
              flexDirection: "column",
              gap: "10px",
            }}
          >
            {data?.map((sub_data: any, index?: number) => {
              return (
                <TextField size="small" value={sub_data} disabled></TextField>
              );
            })}
          </div>
        )}
      </div>
    );
  };

  return (
    <Box margin={"2% 5%"}>
      <DetailsTopView
        headerName="Subscriptions"
        handleButtonChange={handleCancel}
        buttonText="Back"
        searchField={false}
        isButton={true}
        isBackBtn={true}
      />
      <Box
        sx={{ backgroundColor: "#fff", padding: "2rem 5%" }}
        style={{ height: "100%", overflowY: "auto" }}
        className="box-kyc-multiple-step-form"
      >
        {subscriptionDetails("Title:", SingleSubscriptionData?.title_en)}
        {subscriptionDetails("Price: ", SingleSubscriptionData?.price)}
        {subscriptionDetails(
          "Description: ",
          SingleSubscriptionData?.description
        )}
        {subscriptionDetails(
          "Shop Limit: ",
          SingleSubscriptionData?.shopLimit
        )}
        {subscriptionDetails(
          "Staff Limit: ",
          SingleSubscriptionData?.staffLimit
        )}
        {subscriptionDetails(
          "Features",
          SingleSubscriptionData?.features,
          "multiple"
        )}
        {/* {subscriptionDetails("Status", `${SingleSubscriptionData?.status}`)} */}
        <Box display={"flex"} justifyContent={"space-between"} marginTop={"1rem"}>
          <Typography>Permission</Typography>
          <Box width={"60%"}>
            <Typography>POS</Typography>
            <Box display="flex" gap="10px">
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.pos.create} onChange={() => handleCheckboxChange('pos', 'create')} />}
                label="Create"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.pos.read} onChange={() => handleCheckboxChange('pos', 'read')} />}
                label="Read"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.pos.update} onChange={() => handleCheckboxChange('pos', 'update')} />}
                label="Update"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.pos.delete} onChange={() => handleCheckboxChange('pos', 'delete')} />}
                label="Delete"
              />
            </Box>
            {/* {isCategoryValid('pos') ? null : <div style={{ color: 'red' }}>Select at least one checkbox in POS</div>} */}
            <Typography marginTop={"1rem"}>Loyalty Points</Typography>
            <Box display="flex" gap="10px">
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.loyaltyPoints.create} onChange={() => handleCheckboxChange('loyaltyPoints', 'create')} />}
                label="Create"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.loyaltyPoints.read} onChange={() => handleCheckboxChange('loyaltyPoints', 'read')} />}
                label="Read"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.loyaltyPoints.update} onChange={() => handleCheckboxChange('loyaltyPoints', 'update')} />}
                label="Update"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.loyaltyPoints.delete} onChange={() => handleCheckboxChange('loyaltyPoints', 'delete')} />}
                label="Delete"
              />
            </Box>
            <Typography marginTop={"1rem"}>Offers/Rewards</Typography>
            <Box display="flex" gap="10px" marginBottom={"1rem"}>
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.offersRewards.create} onChange={() => handleCheckboxChange('offersRewards', 'create')} />}
                label="Create"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.offersRewards.read} onChange={() => handleCheckboxChange('offersRewards', 'read')} />}
                label="Read"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.offersRewards.update} onChange={() => handleCheckboxChange('offersRewards', 'update')} />}
                label="Update"
              />
              <FormControlLabel
                control={<Checkbox checked={checkboxValues.offersRewards.delete} onChange={() => handleCheckboxChange('offersRewards', 'delete')} />}
                label="Delete"
              />
            </Box>
          </Box>
        </Box>
        <Box
          display={"flex"}
          gap={"1rem"}
          sx={{ float: "right", paddingTop: "2rem" }}
        >
          <Button
            variant="contained"
            onClick={() => handleEdit(ID)}
            sx={{ padding: "4px 16px" }}
          >
            Edit
          </Button>
          <Button
            variant="outlined"
            onClick={handleCancel}
            sx={{ padding: "4px 16px" }}
          >
            Cancel
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
