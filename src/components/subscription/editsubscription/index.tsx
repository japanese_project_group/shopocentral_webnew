// Build in function
import { useEffect, useState } from "react";

// library
import { Box, Button, Checkbox, FormControlLabel, IconButton, TextField, Typography } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// redux
import {
  useGetMerchantIndividualSubscriptionDataQuery,
  useUpdateMerchantSubscriptionDataMutation,
} from "../../../../redux/api/user/user";

// components
import { Delete } from "@mui/icons-material";
import { InputComponent } from "../../../common/Components";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const EditSubscription = () => {
  const router = useRouter();
  const [ID, setID] = useState<any>();
  const [keyword, setKeyword] = useState<any>();
  const [subscriptionData, setSubscriptionData] = useState<any>();
  const { data: editSubscriptionData, refetch } =
    useGetMerchantIndividualSubscriptionDataQuery(ID);
  const [updateSubscription, { data: SubscriptionData }] =
    useUpdateMerchantSubscriptionDataMutation();

  useEffect(() => {
    setID(router.query.id);
  }, [router.query]);

  useEffect(() => {
    if (!isEmpty(SubscriptionData)) {
      toast.success("Successfully updated data");
      router.push("./");
    }
  }, [SubscriptionData]);

  useEffect(() => {
    refetch();
  }, []);

  // Define a type for checkbox values
  type CheckboxValues = {
    pos: { create: boolean; read: boolean; update: boolean; delete: boolean };
    loyaltyPoints: { create: boolean; read: boolean; update: boolean; delete: boolean };
    offersRewards: { create: boolean; read: boolean; update: boolean; delete: boolean };
  };


  const [checkboxValues, setCheckboxValues] = useState<CheckboxValues>({
    pos: { create: false, read: false, update: false, delete: false },
    loyaltyPoints: { create: false, read: false, update: false, delete: false },
    offersRewards: { create: false, read: false, update: false, delete: false },
  });

  const handleCheckboxChange = (category: keyof CheckboxValues, action: keyof CheckboxValues[keyof CheckboxValues]) => {
    setCheckboxValues((prevValues) => ({
      ...prevValues,
      [category]: {
        ...prevValues[category],
        [action]: !prevValues[category][action],
      },
    }));
  };

  const isCategoryValid = (category: keyof CheckboxValues) => {
    return Object.values(checkboxValues[category]).some((value) => value);
  };

  console.log("editSubscriptionData zdxfsadf", SubscriptionData);
  const addFeatures = () => {
    setKeyword([...keyword, [""]]);
  };

  const onChangeKeyProduct = (e: any, index: any) => {
    const list = [...keyword];
    list[index] = e.target.value;
    setKeyword(list);
  };

  const delteBox = (index: any) => {
    const cloneKeyword = [...keyword];
    cloneKeyword.splice(index, 1);
    setKeyword(cloneKeyword);
  };

  useEffect(() => {
    setKeyword(editSubscriptionData?.features);
  }, [editSubscriptionData]);

  const handleCancel = () => {
    router.push("./subscription/view");
  };

  const handleEdit = () => {
    router.push("./edit");
  };

  const formik = useFormik({
    initialValues: {
      title_en: editSubscriptionData?.title_en,
      price: editSubscriptionData?.price,
      description: editSubscriptionData?.description,
      features: editSubscriptionData?.features,
      expireDays: editSubscriptionData?.expireDays,
      shopLimit: editSubscriptionData?.shopLimit,
      staffLimit: editSubscriptionData?.staffLimit,
      payment: editSubscriptionData?.payment,
      status: editSubscriptionData?.status,
    },
    enableReinitialize: true,
    // validationSchema: validationSchema,
    onSubmit: async (values: any) => {
      let body = {
        ...values,
        features: keyword,
        permissions: [],
      };
      updateSubscription({ id: ID, body })
        .unwrap()
        .catch((error: { data: { message: string } }) => {
          const errmsg = error?.data?.message;
          toast.error(errmsg);
        });
    },
  });
  const {
    values,
    handleSubmit,
    getFieldProps,
    setFieldValue,
    handleBlur,
    errors,
    touched,
  } = formik;

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    selectDataType?: string
  ) => {
    return (
      <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
        <div>{fieldName}:</div>
        <div style={{ width: "60%" }}>
          <InputComponent
            type={type}
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            errors={errors}
            touched={touched}
            disabled={disabled}
            setFieldValue={setFieldValue}
            values={values}
            // handleBlur={handleBlur}
            selectValue={selectValue}
            setSelectType={setSelectType}
            options={options}
          // selectDataType={selectDataType}
          />
        </div>
      </div>
    );
  };

  const handleChangeButton = () => {
    router.push("./");
  };

  return (
    <Box margin={"2% 5%"}>
      <DetailsTopView
        headerName="Subscriptions"
        handleButtonChange={handleChangeButton}
        buttonText="Back"
        searchField={false}
        isButton={true}
        isBackBtn={true}
      />
      <DetailsTopView
        headerName="this is header name"
        handleChangeButton={"this is handle change button name"}
        searchField={"this is search field name"}
        isButton={true}
      />
      <Box bgcolor={"#fff"} borderRadius={"1.5rem"} className="form-view-multistep-form box-kyc-multiple-step-form">
        <FormikProvider value={values}>
          <form onSubmit={handleSubmit}>
            <div>
              {fieldFormsText(
                "Title",
                "title_en",
                "text",
                errors.title_en,
                touched.title_en
              )}
              {fieldFormsText(
                "Price",
                "price",
                "text",
                errors.price,
                touched.price
              )}
              {fieldFormsText("Description ", "description", "text", "", "")}
              {fieldFormsText("Expiry Date", "expireDays", "text", "", "")}
              {fieldFormsText("Shop Limit", "shopLimit", "text", "", "")}
              {fieldFormsText("Staff Limit", "staffLimit", "text", "", "")}
              <Box
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <Typography>Features</Typography>

                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    width: "60%",
                  }}
                >
                  {keyword?.map((data: any, index: any) => (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        minWidth: "100%",
                        //width: "100%",
                      }}
                    >
                      <TextField
                        key={index}
                        size="small"
                        style={{ marginBottom: "10px", width: "100%" }}
                        onChange={(e) => onChangeKeyProduct(e, index)}
                        name={index}
                        value={data}
                      />
                      <IconButton aria-label="Example" color="error" sx={{ background: "#ccc", marginTop: "-8px", marginLeft: "6px" }} >
                        <Delete />
                      </IconButton>
                    </div>
                  ))}

                  <Button
                    onClick={addFeatures}
                    variant="outlined"
                    sx={{
                      marginBottom: "10px",
                      // padding: "10px 0",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    Add Features
                  </Button>
                </div>
              </Box>
              <Box display={"flex"} justifyContent={"space-between"}>
                <Typography >Permission</Typography>
                <Box width={"60%"}>
                  <Typography>POS</Typography>
                  <Box display="flex" gap="10px">
                    {/* <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.create} onChange={() => handleCheckboxChange('pos', 'create')} />}
                      label="Create"
                    /> */}
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.create} onChange={() => handleCheckboxChange('pos', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.read} onChange={() => handleCheckboxChange('pos', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.update} onChange={() => handleCheckboxChange('pos', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.pos.delete} onChange={() => handleCheckboxChange('pos', 'delete')} />}
                      label="Delete"
                    />
                    {/* <FormControlLabel control={<Checkbox />} label="Delete" />
                    <FormControlLabel control={<Checkbox />} label="Read" />
                    <FormControlLabel control={<Checkbox />} label="Update" />
                    <FormControlLabel control={<Checkbox />} label="Delete" /> */}
                  </Box>
                  {/* {isCategoryValid('pos') ? null : <div style={{ color: 'red' }}>Select at least one checkbox in POS</div>} */}
                  <Typography marginTop={"1rem"}>Loyalty Points</Typography>
                  <Box display="flex" gap="10px">
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.create} onChange={() => handleCheckboxChange('loyaltyPoints', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.read} onChange={() => handleCheckboxChange('loyaltyPoints', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.update} onChange={() => handleCheckboxChange('loyaltyPoints', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.loyaltyPoints.delete} onChange={() => handleCheckboxChange('loyaltyPoints', 'delete')} />}
                      label="Delete"
                    />
                  </Box>
                  <Typography marginTop={"1rem"}>Offers/Rewards</Typography>
                  <Box display="flex" gap="10px" marginBottom={"1rem"}>
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.create} onChange={() => handleCheckboxChange('offersRewards', 'create')} />}
                      label="Create"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.read} onChange={() => handleCheckboxChange('offersRewards', 'read')} />}
                      label="Read"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.update} onChange={() => handleCheckboxChange('offersRewards', 'update')} />}
                      label="Update"
                    />
                    <FormControlLabel
                      control={<Checkbox checked={checkboxValues.offersRewards.delete} onChange={() => handleCheckboxChange('offersRewards', 'delete')} />}
                      label="Delete"
                    />
                  </Box>
                </Box>
              </Box>

              {/* {fieldFormsText(
                "Permission",
                "permissions",
                "text",
                errors.permission,
                touched.permission
              )}{" "}
              {fieldFormsText(
                "Payment",
                "payment",
                "text",
                errors.payment,
                touched.payment
              )}
              {fieldFormsText(
                "Status",
                "status",
                "text",
                errors.status,
                touched.status
              )} */}
            </div>
            <Box
              display={"flex"}
              gap={"1rem"}
              sx={{ justifyContent: "end", paddingTop: "2rem" }}
            >
              <Button
                variant="outlined"
                onClick={handleCancel}
                sx={{ padding: "4px 16px" }}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                type="submit"
                sx={{ padding: "4px 16px" }}
              >
                Update
              </Button>
            </Box>
          </form>
        </FormikProvider>
      </Box>
    </Box>
  );
};
