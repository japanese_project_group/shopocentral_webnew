// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// MUI components
import { Box, CircularProgress, Grid, Typography } from "@mui/material";

// assets
import ImageIcon from "../../../public/statistic.svg";

// Redux
import { useGetMerchantSubscriptionDataQuery } from "../../../redux/api/user/user";

// Custom Components
import { DetailsTopView } from "../customComponents/detailsTopView";

const SubscriptionPackages = () => {
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState<any>(null);
  const { data: subData, isSuccess, isLoading, refetch } = useGetMerchantSubscriptionDataQuery();

  const handleExpand = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleDelete = () => {
    handleClose();
  };

  useEffect(() => {
    refetch()
  }, [subData])

  function handleChangeButton() {
    router.push("/subscription/add");
  }

  const handleView = (id: any) => {
    router.push({
      pathname: `/subscription/view`,
      query: { id },
    });
  };

  const filteredAdminSubscription = subData && subData?.filter((item: any) => item.businessAccessId === null);
  const filteredMerchantSubscription = subData && subData.filter((item: any) => item.businessAccessId !== null);

  return (
    <>
      <DetailsTopView
        headerName="Subscriptions"
        handleButtonChange={handleChangeButton}
        buttonText="Add New Subscription"
        searchField={false}
      // isButton={true}
      // isBackBtn={true}
      />

      <Box
        boxShadow={'0px 4px 20px 0px rgba(0, 0, 0, 0.22)'}
        padding={{ lg: '2rem', sm: '1.5rem', xs: '1rem' }}
        sx={{ borderRadius: '16px' }}
      >
        {/* {subData ? (
                    <Box> */}
        {isLoading ? (
          <Box width="100%" display={"flex"} justifyContent={"center"} alignItems={"center"}>
            <CircularProgress />
          </Box>
        ) : (
          filteredAdminSubscription || filteredMerchantSubscription ? (
            <>
              <>
                {filteredAdminSubscription && (
                  <Box
                    sx={{
                      bgcolor: 'transparent',
                      display: 'grid',
                      gridTemplateColumns: { lg: '1fr 1fr 1fr', md: '1fr 1fr', sm: '1fr' },
                      columnGap: 8,
                      rowGap: 4,
                    }}
                  >
                    {filteredAdminSubscription?.map((item: any, index: any) => (
                      <Box
                        key={index}
                        sx={{
                          display: 'flex',
                          padding: 2,
                          boxShadow: '2px 1px 10px #00000025',
                          gap: '10px',
                          borderRadius: '6px',
                          bgcolor: '#fff',
                        }}
                      >
                        <Box
                          sx={{
                            borderRadius: '4px',
                            backgroundColor: '#3FAB2D',
                            padding: 0,
                            minWidth: "80px",
                            color: '#fff',
                            display: 'grid',
                            placeContent: 'center',
                          }}
                        >
                          <Image src={ImageIcon} width={'100%'} />
                        </Box>
                        <Box
                          sx={{
                            width: 'calc(100% - 90px)',
                          }}
                        >
                          <Box
                            sx={{
                              display: 'flex',
                              justifyContent: 'spaceBetween',
                              width: '100%',
                            }}
                          >
                            <Box className='subs__Desc'>
                              <Typography
                                className='subs_text'
                                variant="h6"
                                fontWeight={'600'}
                                sx={{ textTransform: 'capitalize' }}
                              >
                                {item.title_en}
                              </Typography>
                              {/* <Typography
                                className=''
                                sx={{ typography: 'subtitle2', height: '43px', color: "#757575", overflow: 'hidden' }}
                              >
                                {item.description ?? 'This is description'}
                              </Typography> */}
                              <Grid item xs zeroMinWidth>
                                <Typography noWrap> {item.description ?? 'This is description'}</Typography>
                              </Grid>
                            </Box>
                            {/* <IconButton
                        onClick={handleExpand}
                        sx={{ height: "30px", width: "30px", marginLeft: 'auto' }}
                      >
                        <GridMoreVertIcon />
                      </IconButton>
                      <Menu
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                        PaperProps={{
                          sx: {
                            boxShadow: "1px 2px 14px #00000010 !important",
                          },
                        }}
                      >
                        <MenuItem onClick={handleDelete}>Delete</MenuItem>
                      </Menu> */}
                          </Box>
                          <Box
                            display={'flex'}
                            justifyContent={'space-between'}
                            alignItems={'center'}
                          >
                            <Typography variant="body1" sx={{ fontWeight: '500' }}>
                              Yen {item.price}
                            </Typography>
                            {/* <Button
                        onClick={() => handleView(item?.id)}
                        sx={{
                          textTransform: 'UpperCase',
                          typography: 'subtitle2',
                          marginLeft: 'auto',
                          fontWeight: '500',
                        }}
                        style={{ width: 'fit-content', float: 'right' }}
                      >
                        {'Learn More'}
                      </Button> */}
                          </Box>
                        </Box>
                      </Box>
                    ))}
                  </Box>
                )}
              </>
              <>
                {filteredMerchantSubscription && (
                  <>
                    <Typography variant="h5" margin={"2rem 0"}>My Subscription Data</Typography>
                    <Box
                      sx={{
                        bgcolor: 'transparent',
                        display: 'grid',
                        gridTemplateColumns: { lg: '1fr 1fr 1fr', md: '1fr 1fr', sm: '1fr' },
                        columnGap: 8,
                        rowGap: 4,
                      }}
                    >
                      {filteredMerchantSubscription?.map((item: any, index: any) => (
                        <Box
                          key={index}
                          sx={{
                            display: 'flex',
                            padding: 2,
                            boxShadow: '2px 1px 10px #00000025',
                            gap: '10px',
                            borderRadius: '6px',
                            bgcolor: '#fff',
                          }}
                        >
                          <Box
                            sx={{
                              borderRadius: '4px',
                              backgroundColor: '#3FAB2D',
                              padding: 0,
                              minWidth: "80px",
                              color: '#fff',
                              display: 'grid',
                              placeContent: 'center',
                            }}
                          >
                            <Image src={ImageIcon} width={'100%'} />
                          </Box>
                          <Box
                            sx={{
                              width: 'calc(100% - 90px)',
                            }}
                          >
                            <Box
                              sx={{
                                display: 'flex',
                                flexDirection: '',
                                justifyContent: 'spaceBetween',
                                width: '100%',
                              }}
                            >
                              <Box className='subs__Desc'>
                                <Typography
                                  className='subs_text'
                                  variant="h6"
                                  fontWeight={'600'}
                                  sx={{ textTransform: 'capitalize' }}
                                >
                                  {item.title_en}
                                </Typography>
                                <Typography
                                  className=''
                                  sx={{ typography: 'subtitle2', height: '43px', color: "#757575", overflow: 'hidden' }}
                                >
                                  {item.description ?? 'This is description'}
                                </Typography>
                              </Box>
                            </Box>
                            <Box
                              display={'flex'}
                              justifyContent={'space-between'}
                              alignItems={'center'}
                            >
                              <Typography variant="body1" sx={{ fontWeight: '500' }}>
                                Yen {item.price}
                              </Typography>
                              {/* <Button
                        onClick={() => handleView(item?.id)}
                        sx={{
                          textTransform: 'UpperCase',
                          typography: 'subtitle2',
                          marginLeft: 'auto',
                          fontWeight: '500',
                        }}
                        style={{ width: 'fit-content', float: 'right' }}
                      >
                        {'Learn More'}
                      </Button> */}
                            </Box>
                          </Box>
                        </Box>
                      ))}
                    </Box>
                  </>
                )}
              </>
            </>
          ) : (
            <Typography textAlign={'center'} display={"block"} variant="body1">
              No data Found
            </Typography>
          )
        )}
      </Box>
    </>
  );
};

export default SubscriptionPackages;