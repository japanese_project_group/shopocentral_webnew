import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Button, IconButton, InputAdornment } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { usePostChangePasswordMutation } from "../../../redux/api/user/user";
import { InputComponent } from "../../common/Components";
import { validationSchemaChangePassword } from "../../common/validationSchema";
export const SettingPage = ({ }) => {
  const router = useRouter();
  // const [passwordShow, setPasswordShow] = useState<boolean>(false);
  const [resetPassword, { data: passwordResetData }] =
    usePostChangePasswordMutation();
  const { profileData } = useSelector((state: any) => state?.profile);
  const [currentPassword, setCurrentPassword] = useState(false);
  const [newPassword, setNewPassword] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState(false);

  const formik = useFormik({
    initialValues: {
      currentPassword: "",
      password: "",
      password_confirmation: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemaChangePassword,
    onSubmit: async (values) => {
      const body = {
        userCredentialId: profileData?.userOnBusinessKyc?.[0]?.id,
        oldPassword: values.currentPassword,
        newPassword: values.password,
      };
      if (values.password === values.currentPassword) {
        return toast.error("new and old password cannot be same");
      }
      resetPassword(body)
        .unwrap()
        .then((data: any) => {
          router.replace("/dashboard");
        })
        .catch((error: any) => {
          toast.error(error?.data?.message);
        });
    },
  });
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  const textField = (
    labelName: string,
    getFieldPropsValue: string,
    errors?: any,
    touched?: any,
    setPasswordShow?: any,
    passwordShow?: boolean
  ) => {
    return (
      <div
        className="display-flex-sb"
        style={{ width: "70%", marginBottom: "5px" }}
      >
        <div>{labelName}:</div>
        <div style={{ width: "70%" }}>
          <InputComponent
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            type={passwordShow ? "text" : "password"}
            errors={errors}
            touched={touched}
            displayIcon={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={(index: any) =>
                    setPasswordShow((prev: boolean) => !prev)
                  }
                  edge="end"
                >
                  {passwordShow ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
        </div>
      </div>
    );
  };

  useEffect(() => {
    if (!!passwordResetData) {
      toast.success(passwordResetData?.message);
    }
  }, [passwordResetData]);
  return (
    <>
      <div className="change-password-view border-radius">
        <FormikProvider value={formik}>
          <form onSubmit={handleSubmit}>
            <div
              className="display-flex-sb"
              style={{ width: "70%", marginBottom: "5px" }}
            ></div>
            <div
              style={{
                fontWeight: "800",
                fontSize: "29px",
                margin: "10px 0px 5px 0px",
              }}
            >
              Change Password
            </div>
            <div
              className="change-password-field-view"
              style={{ marginTop: "25px" }}
            >
              {textField(
                "Current Password",
                "currentPassword",
                errors.currentPassword,
                touched.currentPassword,
                setCurrentPassword,
                currentPassword
              )}

              {textField(
                "New Password",
                "password",
                errors.password,
                touched.password,
                setNewPassword,
                newPassword
              )}
              {textField(
                "Confirm Password",
                "password_confirmation",
                errors.password_confirmation,
                touched.password_confirmation,
                setConfirmPassword,
                confirmPassword
              )}
            </div>
            <div style={{ marginTop: "10px" }}>
              <Button
                variant="contained"
                style={{ marginRight: "10px" }}
                type="submit"
              >
                Reset Password
              </Button>
              <Button
                variant="outlined"
                onClick={() => {
                  router.replace("/dashboard");
                }}
              >
                Cancel
              </Button>
            </div>
          </form>
        </FormikProvider>
      </div>
    </>
  );
};
