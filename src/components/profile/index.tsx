import {
  Box,
  Button
} from "@mui/material";
import Skeleton from "@mui/material/Skeleton";
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { toast } from "react-toastify";
import Lightbox from "yet-another-react-lightbox";
import Captions from "yet-another-react-lightbox/plugins/captions";
import "yet-another-react-lightbox/plugins/captions.css";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import Video from "yet-another-react-lightbox/plugins/video";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "yet-another-react-lightbox/styles.css";
import {
  useGetAllDataQuery,
  useGetBusinessKycIndividualDataQuery,
  useUpdateBusinessKycMutation,
  useUpdateProfileMutation,
} from "../../../redux/api/user/user";
import { InputComponent } from "../../common/Components";
import { companyType } from "../../common/commonData";
import { businessId } from "../../common/utility/Constants";

const UserProfile = (props: any) => {
  const router = useRouter();
  const ref: any = useRef();
  const [open, setOpen] = React.useState(false);
  const [userID, setVal] = useState<any>();
  const [statusState, setStatus] = useState<any>();
  const [dataLoading, setDataLoading] = useState<any>(false);
  const [formData, setFormData] = useState<any>("");
  const [details, setDetails] = useState<any>([]);
  const [openImageModal, setOpenImageModal] = useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>("");
  const [selectCompanyType, setSelectCompanyType] = useState<any>("");
  const [saveStatus, setSaveStatus] = useState<string>("");
  const [slideImage, setSlideImage] = useState<any>([]);
  const [profileImage, setProfileImage] = useState();
  const [file, setFile] = useState<any>("");
  const [selectProfile, setSelectProfile] = useState<string>("");
  const [profileId, setProfileId] = useState<string>("");

  const { data: allData, refetch } =
    useGetBusinessKycIndividualDataQuery(businessId);
  console.log("all datat aasa", allData)
  const [
    updateProfile,
    { data: profileData, isSuccess: updateSuccess, isLoading: updateLoading },
  ] = useUpdateProfileMutation();

  const {
    data: userData,
    isSuccess,
    refetch: refetchUserData,
  } = useGetAllDataQuery();

  const [
    updateFormBusiness,
    { data: updateSuccessBusinessData, isSuccess: updateBusinessKyc, error },
  ] = useUpdateBusinessKycMutation();

  const changeFile = (e: any) => {
    setFile(e.target.files[0]);
  };

  useEffect(() => {
    const fileReader = new FileReader();
    if (file) {
      fileReader.readAsDataURL(file);
      fileReader.onload = (e: any) => {
        const { result } = e.target;
        if (result) {
          setSelectProfile(result);
        }
      };

      const fd = new FormData();
      fd.append("id", profileId);
      fd.append("userImage", file);
      if (fd) {
        updateProfile({ body: fd })
          .then((data: any) => {
            if (data?.data?.userImage !== "") {
              toast.success("sucessfully updated profile picture");
              refetchUserData();
            } else {
              toast.error("something went wrong");
            }
          })
          .catch((err: any) => {
            toast.error(err);
          });
      }
    }
  }, [file]);

  useEffect(() => {
    setProfileImage(userData?.data?.profile?.userImage);
    setProfileId(userData?.data?.profile?.id);
  }, [userData]);

  const formik = useFormik({
    initialValues: {
      corporateNumber: details?.corporateNumber,
      country_en: details?.country_en,
      //registerd name
      katagana_name: details?.katagana_name,
      name_en: details?.name_en,
      email: details?.user?.email,
      postalcode_en: details?.address?.postalcode_en?.trim()?.slice(0, 3),
      postalcode_end: allData?.address?.postalcode_en.slice(3, 7),
      city_en: details?.address?.city_en,
      prefecture_en: details?.address?.prefecture_en,
      town_en: details?.address?.town_en,
      registered_name: details?.registered_name,
      firstName: details?.user?.firstName,
      middleName: details?.user?.middleName,
      lastName: details?.user?.lastName,
      registerTradeName: details?.registerTradeName,
      house_number: details?.address?.house_number,
      establishedDate: "" || details?.establishedDate,
      addressId: details?.address?.id,
    },
    enableReinitialize: true,
    onSubmit: async (values: any) => {
      let body = {
        ...values,
        status: statusState,
        identityStatusId: details?.identityStatus?.[0]?.id,
        businessKycId: details?.identityStatus?.[0]?.businessKycId,
        postalcode_en: `${values.postalcode_en}${values.postalcode_end}`,
        type: selectCompanyType,
      };
      await updateFormBusiness({ userId: businessId, body: body })
        .then((res: any) => {
          toast.success(res.message);
        })
        .catch((error: any) => {
          toast.error(error.message);
        });
    },
  });

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  const displayInfo = (
    fieldName?: string,
    fieldValue?: string,
    editable?: boolean,
    fieldsValueDisplay?: any,
    type?: string,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    multiple?: boolean,
    ref?: any
  ) => {
    return (
      <Box
        sx={{
          display: "flex",
          width: "100%",
          marginBottom: editable ? "1%" : "15px",
        }}
      >
        <Box sx={{ width: "30%" }}>{fieldName}</Box>
        {!editable ? (
          multiple ? (
            <div>
              {fieldsValueDisplay?.map((data: any, index: number) => (
                <Box key={index} sx={{ width: "70%" }}>
                  {"+81-" + data}
                </Box>
              ))}
            </div>
          ) : (
            <Box sx={{ width: "70%" }}>{fieldsValueDisplay}</Box>
          )
        ) : (
          <div style={{ marginTop: "-4px", width: "60%" }}>
            <InputComponent
              type={type ? type : "text"}
              getFieldProps={getFieldProps}
              getFieldPropsValue={fieldValue}
              options={options}
              selectValue={selectValue}
              setFieldValue={setFieldValue}
              label={type === "postalCode" ? "hh" : ""}
              // showtitle={type === "postalCode" ? "hh" : ""}
              values={values}
            />
          </div>
        )}
      </Box>
    );
  };

  const displayImage = (imageSrc?: any, imageName?: string) => {
    const pdfOrNot = imageSrc?.includes("pdf");

    return (
      <Box
        sx={{
          borderRadius: 2,
        }}
        className="display-images-details"
        style={{ objectFit: "contain" }}
      >
        <Image
          onClick={() => {
            setSlideImage([{ src: imageSrc, title: imageName }]);
            setOpenImageModal(true);
            setDisplayData(imageSrc);
          }}
          src={imageSrc}
          height={400}
          width={600}
          alt={"Front image"}
          style={{ marginTop: "10px", objectFit: "contain" }}
        />

        <span
          className="display-flex-center"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {imageName}
        </span>
        <div style={{ display: "flex", alignItems: "flex-start" }}></div>

        <input
          type="file"
          accept=" .jpeg, .jpg, .png"
          ref={ref}
          style={{ display: "none" }}
          onChange={(e: any) => changeFile(e)}
        />

        {imageName === "Profile" && (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              paddingBottom: "10px",
            }}
          >
            <Button variant="contained" onClick={() => ref?.current?.click()}>
              Update
            </Button>
          </div>
        )}
      </Box>
    );
  };

  useEffect(() => {
    setVal(router.query.id);
  }, [router.query]);

  useEffect(() => {
    refetch();
  }, []);

  const companyTypeData = companyType;

  useEffect(() => {
    if (allData) {
      setStatus(allData?.identityStatus?.[0]?.status);
      setDataLoading(true);
      setDetails(allData);
      setSelectCompanyType(allData?.type?.trim());
      localStorage.setItem(
        "postalcodeValue",
        allData?.address?.postalcode_en?.trim()?.slice(0, 3)
      );
      setFieldValue(
        "postalcode_end",
        allData?.address?.postalcode_en?.trim()?.slice(3, 7)
      );
    }
  }, [allData]);

  useEffect(() => {
    if (!isEmpty(formData)) {
      setStatus(saveStatus);
      setOpen(false);
    }
  }, [formData]);

  useEffect(() => {
    if (openImageModal) {
      const includesOrNot = (image: any, imageName: string) => {
        return image?.[0]?.title === imageName;
      };
      !includesOrNot(slideImage, "Certified corporate Register") &&
        !!details?.certifiedRegistered &&
        !details?.certifiedRegistered?.includes("pdf") &&
        setSlideImage((previousData: any) => [
          ...previousData,
          {
            src: details?.certifiedRegistered,
            title: "Certified corporate Register",
          },
        ]);

      !includesOrNot(slideImage, "Tax Payment Certificate") &&
        !!details?.recieptTaxOffice &&
        !details?.recieptTaxOffice?.includes("pdf") &&
        setSlideImage((previousData: any) => [
          ...previousData,
          { src: details?.recieptTaxOffice, title: "Tax Payment Certificate" },
        ]);

      !includesOrNot(slideImage, "Corporate Seal Certificate") &&
        !!details?.sealedCertificate &&
        !details?.sealedCertificate?.includes("pdf") &&
        setSlideImage((previousData: any) => [
          ...previousData,
          {
            src: details?.sealedCertificate,
            title: "Corporate Seal Certificate",
          },
        ]);

      !includesOrNot(slideImage, "Other Image") &&
        !!details?.others &&
        !details?.others?.includes("pdf") &&
        setSlideImage((previousData: any) => [
          ...previousData,
          { src: details?.others, title: "Other Image" },
        ]);
    }
  }, [openImageModal]);

  return (
    <div>
      <div className="detail-view-kyc">
        <div>
          <FormikProvider value={formik}>
            <form onSubmit={handleSubmit}>
              <div className="kyc-view-box">
                <div className="kyc-view-sub-box">
                  <h4
                    style={{
                      margin: "0px",
                      textDecoration: "underline",
                      marginBottom: "10px",
                      fontSize: "22px",
                    }}
                  >
                    Business Information
                  </h4>
                  {displayInfo("Registered Name:", "registerTradeName", true)}
                  {displayInfo("Name in katagana:", "katagana_name", true)}
                  {displayInfo("English Name:", "name_en", true)}
                  {displayInfo(
                    "Business Email:",
                    "email",
                    false,
                    details?.user?.email
                  )}
                  {displayInfo(
                    "Company Type:",
                    "type",
                    true,
                    "",
                    "options",
                    companyTypeData,
                    selectCompanyType,
                    setSelectCompanyType
                  )}
                  {displayInfo(
                    "Business Phone Number:",
                    "phoneNumber",
                    false,
                    `+81-${details?.user?.phoneNumber?.phoneNumber}`
                  )}
                  {details?.phoneNumber?.secondaryPhoneNumber?.map(
                    (number: any) => (
                      <>
                        {displayInfo(
                          "Optional Number",
                          "secondaryPhoneNumber",
                          false,
                          `+81 -${number}`
                        )}
                      </>
                    )
                  )}
                </div>
              </div>
              <div className="kyc-view-box">
                <div className="kyc-view-sub-box">
                  <h4
                    style={{
                      margin: "0px",
                      textDecoration: "underline",
                      marginBottom: "10px",
                      fontSize: "22px",
                    }}
                  >
                    Business KYC
                  </h4>
                  {displayInfo("Corporate Number:", "corporateNumber", true)}
                  {displayInfo(
                    "Established Date",
                    "establishedDate",
                    true,
                    moment(details?.establishedDate).format("YYYY/MM/DD"),
                    "date"
                  )}
                </div>
              </div>

              <div className="kyc-view-box">
                <div className="kyc-view-sub-box">
                  <h4
                    style={{
                      margin: "0px",
                      textDecoration: "underline",
                      marginBottom: "10px",
                      fontSize: "22px",
                    }}
                  >
                    Address
                  </h4>
                  {displayInfo("Country:", "country_en", false, "Japan")}
                  {displayInfo(
                    "Postal Code:",
                    "postalcode_en",
                    true,
                    "",
                    "postalCode",
                    "",
                    "",
                    ""
                  )}
                  {displayInfo("City/Village:", "city_en", true)}
                  {displayInfo("Prefecture:", "prefecture_en", true)}
                  {displayInfo("Town/Ward:", "town_en", true)}
                  {displayInfo(
                    "Building Name/Room Number:",
                    "house_number",
                    true
                  )}
                </div>
              </div>

              <div className="kyc-view-button">
                <Button
                  variant="contained"
                  style={{ marginRight: "10px" }}
                  type="submit"
                >
                  Update
                </Button>
                <Button
                  variant="outlined"
                  onClick={() => {
                    router.push("/dashboard");
                  }}
                >
                  Cancel
                </Button>
              </div>
            </form>
          </FormikProvider>
        </div>
        <div>
          <div>
            <div className="kyc-view-box">
              <div className="kyc-view-sub-box">
                <h4
                  style={{
                    margin: "0px",
                    textDecoration: "underline",
                    marginBottom: "10px",
                  }}
                >
                  User Profile
                </h4>

                {!updateLoading ? (
                  displayImage(
                    profileImage
                      ? updateSuccess
                        ? selectProfile
                        : profileImage
                      : !profileImage &&
                      "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png?20150327203541",
                    "Profile"
                  )
                ) : (
                  <Skeleton variant="rectangular" width="100%" height={400} />
                )}
              </div>
              <div className="kyc-view-sub-box">
                <h4
                  style={{
                    margin: "0px",
                    textDecoration: "underline",
                    marginBottom: "10px",
                  }}
                >
                  KYC Documents
                </h4>
                <div>
                  {details?.certifiedRegistered &&
                    displayImage(
                      details?.certifiedRegistered,
                      "Certified corporate Register"
                    )}
                  {details?.recieptTaxOffice &&
                    displayImage(
                      details?.recieptTaxOffice,
                      "Tax Payment Certificate"
                    )}
                  {details?.sealedCertificate &&
                    displayImage(
                      details?.sealedCertificate,
                      "Corporate Seal Certificate"
                    )}
                  {details?.others &&
                    displayImage(details?.others, "Other Image")}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {openImageModal && (
        <Lightbox
          open={openImageModal}
          close={() => setOpenImageModal(false)}
          slides={slideImage}
          index={0}
          plugins={[Captions, Fullscreen, Slideshow, Thumbnails, Video, Zoom]}
        />
      )}
    </div>
  );
};
export default UserProfile;
