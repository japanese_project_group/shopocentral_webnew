import { Collapse } from "@material-ui/core";
import { KeyboardArrowDown, KeyboardArrowUp } from "@material-ui/icons";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import DoneIcon from "@mui/icons-material/Done";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import {
  Box,
  Button,
  Divider,
  Grid,
  IconButton,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import moment from "moment";
import Image from "next/image";
import Router, { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useGetBranchDataQuery, usePostBranchDetailsMutation } from "../../../../redux/api/user/user";
import { RootState } from "../../../../redux/store";
import CircularIndeterminate from "../../../common/LoadingScreen";
import { validationSchemaAddBranch } from "../../../common/validationSchema";
import styles from "../../Forms.module.css";
const BranchComponent = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [ValueUpdate, { isSuccess, isLoading, data }] =
    usePostBranchDetailsMutation();
  const { data: allData, refetch: branchRefetch } = useGetBranchDataQuery();
  const { profileData } = useSelector((state: RootState) => state?.profile);
  const [bId, setbId] = useState<any>();
  const [userDetails, setUserDetails] = useState<any>();
  const [signboards, setSignBoards] = useState<any>([]);
  const [flyers, setFlyers] = useState<any>([]);
  const [shopImages, setShopImages] = useState<any>([]);
  const [businessCards, setBusinessCards] = useState<any>([]);
  const [certifiedFiledName, setCertifiedFileName] = useState<string | null>();
  const [shopid, setshopid] = useState<any>();
  const router = useRouter();
  const [isDataSending, setIsdataSending] = useState<boolean>(false);

  useEffect(() => {
    if (isLoading) {
      setIsdataSending(true);
    } else {
      setIsdataSending(false);
    }
  }, [isLoading]);

  useEffect(() => {
    branchRefetch();
  }, [router, allData]);

  useEffect(() => {
    setshopid(router.query.id);
  }, [router.query, allData]);

  function handleShopImagesDelete(val: any) {
    setShopImages([...shopImages.slice(0, val), ...shopImages.slice(val + 1)]);
  }
  function handleBusinessCardsDelete(val: any) {
    setBusinessCards([
      ...businessCards.slice(0, val),
      ...businessCards.slice(val + 1),
    ]);
  }
  function handleSignBoardsDelete(val: any) {
    setSignBoards([...signboards.slice(0, val), ...signboards.slice(val + 1)]);
  }
  function handleFlyersDelete(val: any) {
    setFlyers([...flyers.slice(0, val), ...flyers.slice(val + 1)]);
  }

  useEffect(() => {
    setUserDetails(profileData);
    setbId(profileData?.businessKyc[0]?.id);
  }, [profileData]);
  useEffect(() => {
    if (isSuccess && data) {
      Router.push("/shop");
      toast("Branches Added SuccessFully", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
    }
  }, [isSuccess]);
  function handleJapanaeseForm() {
    setOpen(!open);
  }

  const formik = useFormik({
    initialValues: {
      // ----------------------------------------------------------//
      city_en: "" || allData?.city_en,
      city_jpn: "" || allData?.city_jpn,
      city_np: "" || allData?.city_np,
      country_en: "" || allData?.country_en,
      country_np: "" || allData?.country_np,
      country_jpn: "" || allData?.country_jpn,
      postalcode_en: "" || allData?.postalcode_en,
      postalcode_jpn: "" || allData?.postalcode_jpn,
      postalcode_np: "" || allData?.postalcode_np,
      prefecture_en: "" || allData?.prefecture_en,
      prefecture_jpn: "" || allData?.prefecture_jpn,
      prefecture_np: "" || allData?.prefecture_np,
      district_en: "" || allData?.district_en,
      district_jpn: "" || allData?.district_jpn,
      district_np: "" || allData?.district_np,
      town_en: "" || allData?.town_en,
      town_jpn: "" || allData?.town_jpn,
      town_np: "" || allData?.town_np,
      state_en: "" || allData?.state_en,
      state_np: "" || allData?.state_np,
      state_jpn: "" || allData?.state_jpn,
      name_en: "" || allData?.name_en,
      name_jp: "" || allData?.name_jp,
      name_np: "" || allData?.name_np,
      email: "" || allData?.email,
      website: "" || allData?.website,
      tel: "" || allData?.tel,
      openingTime: "" || allData?.openingTime,
      closingTime: "" || allData?.closingTime,
      latitude: "27.678494",
      longitude: "85.324049",
      businessLicense: "",
      postalcode_end: "",
      // "bId": "152-5684-787s-sadaz45"
    },
    enableReinitialize: true,
    validationSchema: validationSchemaAddBranch,
    onSubmit: async (values: any) => {
      const { postalcode_en, postalcode_end, phone, ...rest } = values;
      const fullpostalCode = ` ${postalcode_en}${postalcode_end}`;
      await submitValues({
        postalcode_en: fullpostalCode,
        ...rest,
      });
    },
  });

  async function submitValues(values: any) {
    values.openingTime = moment(values.openingTime, "hh:mm").format("hh:mm A");
    values.closingTime = moment(values.closingTime, "hh:mm").format("hh:mm A");
    const finalData: any = {
      ...values,
      bId: bId,
      flyers: flyers,
      signBoards: signboards,
      businessCard: businessCards,
      shopsPhotographs: shopImages,
      shopBranchId: shopid,
    };
    let fd = new FormData();
    Object.keys(finalData).map((item: any) => {
      if (
        item === "flyers" ||
        item === "signBoards" ||
        item === "businessCard" ||
        item === "shopsPhotographs"
      ) {
        finalData[item].map((i: any) => {
          fd.append(item, i);
        });
      } else {
        fd.append(item, finalData[item] ? finalData[item] : null);
      }
    });
    let reqData = {
      data: fd,
    };

    ValueUpdate(reqData);
  }
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;
  let imgarr: any = [];
  const onChangeBusinessCards = (e: any) => {
    setBusinessCards([...businessCards, ...e.target.files]);
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setBusinessCards([]);
      }
    });
  };
  const onChangeShopImages = (e: any) => {
    setShopImages([...shopImages, ...e.target.files]);
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setShopImages([]);
      }
    });
  };
  const onChangeSignBoards = (e: any) => {
    setSignBoards([...signboards, ...e.target.files]);
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setSignBoards([]);
      }
    });
  };
  const onChangeFlyers = (e: any) => {
    setFlyers([...flyers, ...e.target.files]);
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setFlyers([]);
      }
    });
  };
  const onChangecertifiedRegistered = (e: any) => {
    setCertifiedFileName(e.target.files[0]?.name);
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        if (file.size > 2000000) {
          toast("Please upload image with size less than 2 MB", {
            type: "error",
          });
        } else {
          setFieldValue("businessLicense", e.target.files[0]);
        }
      };
    }
  };
  const moveToNext = (e: any, nextId: any) => {
    document?.getElementById(nextId)?.focus();
  };
  return (
    <>
      {isDataSending == true ? (
        <>
          <CircularIndeterminate></CircularIndeterminate>
        </>
      ) : (
        <>
          <FormikProvider value={formik}>
            <Stack justifyContent={"center"} direction="row" mt={10}>
              <Stack
                sx={{
                  width: { lg: "60%", md: "90%", sm: "95%", xs: "100%" },
                  borderRadius: 4,
                  padding: 5,
                }}
                className={styles.neomorphismBackground}
              >
                <Typography
                  variant="h4"
                  textAlign={"center"}
                  mb={2}
                  color="primary"
                >
                  Branch Details
                </Typography>
                <form onSubmit={handleSubmit}>
                  <Stack>
                    <Grid container spacing={3}>
                      <Grid item xs={12} sm={6}>
                        Branch Name :
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("name_en")}
                          fullWidth
                          error={Boolean(touched.name_en && errors.name_en)}
                          helperText={
                            Boolean(touched.name_en && errors.name_en) &&
                            String(touched.name_en && errors.name_en)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Box>
                          Postal Code:{" "}
                          <Typography
                            component="span"
                            color="error"
                            sx={{ marginLeft: "5px" }}
                          >
                            *
                          </Typography>
                        </Box>
                        <Stack direction="row" spacing={1}>
                          <Grid item xs={6} sm={6}>
                            <TextField
                              inputProps={{
                                maxLength: 3,
                              }}
                              type="number"
                              {...getFieldProps("postalcode_en")}
                              onKeyUp={(e) => {
                                const codeLength = String(
                                  values.postalcode_en
                                ).length;
                                if (codeLength >= 3) {
                                  moveToNext(e, "postalcode_end");
                                }
                              }}
                              fullWidth
                              error={Boolean(
                                touched.postalcode_en && errors.postalcode_en
                              )}
                              helperText={
                                Boolean(
                                  touched.postalcode_en && errors.postalcode_en
                                ) &&
                                String(
                                  touched.postalcode_en && errors.postalcode_en
                                )
                              }
                            />
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            <TextField
                              type="number"
                              id="postalcode_end"
                              {...getFieldProps("postalcode_end")}
                              fullWidth
                              error={Boolean(
                                touched.postalcode_end && errors.postalcode_end
                              )}
                              helperText={
                                Boolean(
                                  touched.postalcode_end &&
                                  errors.postalcode_end
                                ) &&
                                String(
                                  touched.postalcode_end &&
                                  errors.postalcode_end
                                )
                              }
                            />
                          </Grid>
                        </Stack>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Prefecture
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("prefecture_en")}
                          fullWidth
                          error={Boolean(
                            touched.prefecture_en && errors.prefecture_en
                          )}
                          helperText={
                            Boolean(
                              touched.prefecture_en && errors.prefecture_en
                            ) &&
                            String(
                              touched.prefecture_en && errors.prefecture_en
                            )
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        District
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("district_en")}
                          fullWidth
                          error={Boolean(
                            touched.district_en && errors.district_en
                          )}
                          helperText={
                            Boolean(
                              touched.district_en && errors.district_en
                            ) &&
                            String(touched.district_en && errors.district_en)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Town
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("town_en")}
                          fullWidth
                          error={Boolean(touched.town_en && errors.town_en)}
                          helperText={
                            Boolean(touched.town_en && errors.town_en) &&
                            String(touched.town_en && errors.town_en)
                          }
                        />
                      </Grid>

                      <Grid item xs={12} sm={6}>
                        City
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("city_en")}
                          fullWidth
                          error={Boolean(touched.city_en && errors.city_en)}
                          helperText={
                            Boolean(touched.city_en && errors.city_en) &&
                            String(touched.city_en && errors.city_en)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Country
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("country_en")}
                          fullWidth
                          error={Boolean(
                            touched.country_en && errors.country_en
                          )}
                          helperText={
                            Boolean(touched.country_en && errors.country_en) &&
                            String(touched.country_en && errors.country_en)
                          }
                        />
                      </Grid>

                      <Grid item xs={12} sm={6}>
                        Email:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("email")}
                          fullWidth
                          error={Boolean(touched.email && errors.email)}
                          helperText={
                            Boolean(touched.email && errors.email) &&
                            String(touched.email && errors.email)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        State:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("state_en")}
                          fullWidth
                          error={Boolean(touched.state_en && errors.state_en)}
                          helperText={
                            Boolean(touched.state_en && errors.state_en) &&
                            String(touched.state_en && errors.state_en)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Telephone Number:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("tel")}
                          fullWidth
                          error={Boolean(touched.tel && errors.tel)}
                          helperText={
                            Boolean(touched.tel && errors.tel) &&
                            String(touched.tel && errors.tel)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Website:
                        <TextField {...getFieldProps("website")} fullWidth />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Opening Time:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          type="time"
                          {...getFieldProps("openingTime")}
                          fullWidth
                          error={Boolean(
                            touched.openingTime && errors.openingTime
                          )}
                          helperText={
                            Boolean(
                              touched.openingTime && errors.openingTime
                            ) &&
                            String(touched.openingTime && errors.openingTime)
                          }
                        />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        Closing Time:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          type="time"
                          {...getFieldProps("closingTime")}
                          fullWidth
                          error={Boolean(
                            touched.closingTime && errors.closingTime
                          )}
                          helperText={
                            Boolean(
                              touched.closingTime && errors.closingTime
                            ) &&
                            String(touched.closingTime && errors.closingTime)
                          }
                        />
                      </Grid>
                    </Grid>
                  </Stack>
                  <Stack direction={"column"} spacing={2} mt={10}>
                    <Button
                      onClick={handleJapanaeseForm}
                      variant="outlined"
                      color="primary"
                    // sx={{ width: "70%", marginBottom: "2%" }}
                    >
                      Upload Images
                      {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                    </Button>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                      <Box
                        sx={{
                          width: "100%",
                          display: {
                            md: "flex",
                            sm: "block",
                          },
                          border: "2",
                          marginTop: "1%",
                          justifyContent: "space-between",
                        }}
                      >
                        <Box
                          sx={{
                            width: {
                              md: "50%",
                              sm: "100%",
                            },
                            border: "1px solid ",
                            borderColor: "#808080",
                            height: "150%",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "0.5%",
                            marginBottom: "4%",
                          }}
                        >
                          <IconButton
                            component="label"
                            sx={{ display: "flex", flexDirection: "column" }}
                          >
                            {signboards.length === 0 ? (
                              <>
                                <UploadFileIcon fontSize="large" />
                              </>
                            ) : (
                              <>
                                <DoneIcon fontSize="large" />
                              </>
                            )}
                            <input
                              type="file"
                              hidden
                              accept="image/png, image/jpg, image/jpeg, image/pdf"
                              onChange={onChangeSignBoards}
                              multiple
                            />
                          </IconButton>
                          {signboards.length === 0 ? (
                            <>
                              <Typography
                                sx={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                Please upload signboards
                              </Typography>
                            </>
                          ) : (
                            <>
                              <Box sx={{ width: "100%" }}>
                                {signboards.map((i: any, index: number) => {
                                  return (
                                    <>
                                      <Box
                                        sx={{
                                          width: "100%",
                                          display: "flex",
                                          justifyContent: "space-between",
                                          marginBottom: "2%",
                                        }}
                                        key={index}
                                      >
                                        <Image
                                          src={URL.createObjectURL(i)}
                                          height="100"
                                          width="150"
                                        />
                                        <Typography
                                          noWrap
                                          sx={{
                                            width: "20vw",
                                            marginTop: "5%",
                                          }}
                                        >
                                          {i.name}
                                        </Typography>
                                        <Button
                                          onClick={() =>
                                            handleSignBoardsDelete(index)
                                          }
                                        >
                                          <DeleteForeverIcon color="error" />
                                        </Button>
                                      </Box>
                                      <Divider />
                                    </>
                                  );
                                })}
                              </Box>
                            </>
                          )}
                        </Box>
                        <Box
                          sx={{
                            width: {
                              md: "50%",
                              sm: "100%",
                            },
                            border: "1px solid ",
                            borderColor: "#808080",
                            height: "150%",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "0.5%",
                            marginBottom: "4%",
                          }}
                        >
                          <IconButton
                            component="label"
                            sx={{ display: "flex", flexDirection: "column" }}
                          >
                            {flyers.length === 0 ? (
                              <>
                                <UploadFileIcon fontSize="large" />
                              </>
                            ) : (
                              <>
                                <DoneIcon fontSize="large" />
                              </>
                            )}
                            <input
                              type="file"
                              hidden
                              accept="image/png, image/jpg, image/jpeg"
                              onChange={onChangeFlyers}
                              multiple
                            />
                          </IconButton>
                          {flyers.length === 0 ? (
                            <>
                              <Typography
                                sx={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                Please upload flyers
                              </Typography>
                            </>
                          ) : (
                            <>
                              <Box sx={{ width: "100%" }}>
                                {flyers.map((i: any, index: number) => {
                                  return (
                                    <>
                                      <Box
                                        sx={{
                                          width: "100%",
                                          display: "flex",
                                          justifyContent: "space-between",
                                          marginBottom: "2%",
                                        }}
                                        key={index}
                                      >
                                        <Image
                                          src={URL.createObjectURL(i)}
                                          height="100"
                                          width="150"
                                        />
                                        <Typography
                                          noWrap
                                          sx={{
                                            width: "20vw",
                                            marginTop: "5%",
                                          }}
                                        >
                                          {i.name}
                                        </Typography>
                                        <Button
                                          onClick={() =>
                                            handleFlyersDelete(index)
                                          }
                                        >
                                          <DeleteForeverIcon color="error" />
                                        </Button>
                                      </Box>
                                      <Divider />
                                    </>
                                  );
                                })}
                              </Box>
                            </>
                          )}
                        </Box>
                      </Box>
                      <Box
                        sx={{
                          width: "100%",
                          display: {
                            md: "flex",
                            sm: "block",
                          },
                          border: "2",
                          marginTop: "1%",
                          justifyContent: "space-between",
                        }}
                      >
                        <Box
                          sx={{
                            width: {
                              md: "50%",
                              sm: "100%",
                            },
                            border: "1px solid ",
                            borderColor: "#808080",
                            height: "150%",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "0.5%",
                            marginBottom: "4%",
                          }}
                        >
                          <IconButton
                            component="label"
                            sx={{ display: "flex", flexDirection: "column" }}
                          >
                            {shopImages.length === 0 ? (
                              <>
                                <UploadFileIcon fontSize="large" />
                              </>
                            ) : (
                              <>
                                <DoneIcon fontSize="large" />
                              </>
                            )}
                            <input
                              type="file"
                              hidden
                              accept="image/png, image/jpg, image/jpeg"
                              onChange={onChangeShopImages}
                              multiple
                            />
                          </IconButton>
                          {shopImages.length === 0 ? (
                            <>
                              <Typography
                                sx={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                Please upload pictures of shop
                              </Typography>
                            </>
                          ) : (
                            <>
                              <Box sx={{ width: "100%" }}>
                                {shopImages.map((i: any, index: number) => {
                                  return (
                                    <>
                                      <Box
                                        sx={{
                                          width: "100%",
                                          display: "flex",
                                          justifyContent: "space-between",
                                          marginBottom: "2%",
                                        }}
                                        key={index}
                                      >
                                        <Image
                                          src={URL.createObjectURL(i)}
                                          height="100"
                                          width="150"
                                        />
                                        <Typography
                                          noWrap
                                          sx={{
                                            width: "20vw",
                                            marginTop: "5%",
                                          }}
                                        >
                                          {i.name}
                                        </Typography>
                                        <Button
                                          onClick={() =>
                                            handleShopImagesDelete(index)
                                          }
                                        >
                                          <DeleteForeverIcon color="error" />
                                        </Button>
                                      </Box>
                                      <Divider />
                                    </>
                                  );
                                })}
                              </Box>
                            </>
                          )}
                        </Box>
                        <Box
                          sx={{
                            width: {
                              md: "50%",
                              sm: "100%",
                            },
                            border: "1px solid ",
                            borderColor: "#808080",
                            height: "150%",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "0.5%",
                            marginBottom: "4%",
                          }}
                        >
                          <IconButton
                            component="label"
                            sx={{ display: "flex", flexDirection: "column" }}
                          >
                            {businessCards.length === 0 ? (
                              <>
                                <UploadFileIcon fontSize="large" />
                              </>
                            ) : (
                              <>
                                <DoneIcon fontSize="large" />
                              </>
                            )}
                            <input
                              type="file"
                              hidden
                              accept="image/png, image/jpg, image/jpeg"
                              onChange={onChangeBusinessCards}
                              multiple
                            />
                          </IconButton>
                          {businessCards.length === 0 ? (
                            <>
                              <Typography
                                sx={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                Please upload Business Cards
                              </Typography>
                            </>
                          ) : (
                            <>
                              <Box sx={{ width: "100%" }}>
                                {businessCards.map((i: any, index: number) => {
                                  return (
                                    <>
                                      <Box
                                        sx={{
                                          width: "100%",
                                          display: "flex",
                                          justifyContent: "space-between",
                                          marginBottom: "2%",
                                        }}
                                        key={index}
                                      >
                                        <Image
                                          src={URL.createObjectURL(i)}
                                          height="100"
                                          width="150"
                                        />
                                        <Typography
                                          noWrap
                                          sx={{
                                            width: "20vw",
                                            marginTop: "5%",
                                          }}
                                        >
                                          {i.name}
                                        </Typography>
                                        <Button
                                          onClick={() =>
                                            handleBusinessCardsDelete(index)
                                          }
                                        >
                                          <DeleteForeverIcon color="error" />
                                        </Button>
                                      </Box>
                                      <Divider />
                                    </>
                                  );
                                })}
                              </Box>
                            </>
                          )}
                        </Box>
                      </Box>
                      <Box
                        sx={{
                          width: "99%",
                          border: "1px solid ",
                          borderColor: "#808080",
                          height: {
                            md: "15%",
                            sm: "20%",
                          },
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          margin: "0.5%",
                          marginBottom: "6%",
                        }}
                      >
                        <IconButton
                          component="label"
                          sx={{ display: "flex", flexDirection: "column" }}
                        >
                          {certifiedFiledName == null ? (
                            <>
                              {" "}
                              <UploadFileIcon fontSize="large" />
                            </>
                          ) : (
                            <>
                              <DoneIcon fontSize="large" />
                            </>
                          )}
                          <input
                            type="file"
                            hidden
                            accept="image/png, image/jpg, image/jpeg"
                            onChange={onChangecertifiedRegistered}
                          />
                          {certifiedFiledName == null ? (
                            <>
                              <Typography noWrap>
                                Please upload Business Licence
                              </Typography>
                            </>
                          ) : (
                            <>
                              <Typography noWrap sx={{ width: "20vw" }}>
                                {certifiedFiledName}
                              </Typography>
                            </>
                          )}

                          <Box></Box>
                        </IconButton>
                      </Box>
                    </Collapse>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                    // sx={{ width: "70%", marginTop: "2%" }}
                    >
                      submit
                    </Button>
                  </Stack>
                </form>
              </Stack>
            </Stack>
          </FormikProvider>
        </>
      )}
    </>
  );
};
export default BranchComponent;
