import { Box, Button, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import style from "../Forms.module.css";
import { useVerifyEmailandCodeMutation } from "../../../redux/api/auth/auth";
import { toast } from "react-toastify";
import Router from "next/router";
const OtpVerification = () => {
  const router = useRouter();
  const { id } = router.query;

  const [verifyEmailAndCode, { isSuccess, error, data, isError }] =
    useVerifyEmailandCodeMutation();

  if (id !== undefined) {
    var email = id.toString().split("&&")[2].split("=")[1];
    var code = id.toString().split("&&")[3].split("=")[1];
  }
  function sendData() {
    const data = {
      email: email,
      code: code,
    };
    verifyEmailAndCode(data)
      .unwrap()
      .catch((error) => {
        const ermsg = error?.data?.message;
        toast.error(ermsg);
      });
  }
  useEffect(() => {
    if (data && isSuccess) {
      Router.replace("/createaccountform/");
      toast("OTP successfully verified", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
    }
  }, [data, isSuccess]);
  return (
    <Stack className={style.colorBackground} justifyContent="center">
      <Box height={"100vh"}>
        <Stack
          direction="column"
          alignItems={"center"}
          className={style.glassMorphism}
          width={"30%"}
          m={"auto"}
          sx={{ p: 2, mt: 10 }}
          spacing={2}
        >
          <Typography variant="h5">Verify Your Account</Typography>
          <Box>
            <Button
              variant="contained"
              onClick={(e) => {
                e.preventDefault();
                sendData();
              }}
            >
              Verify
            </Button>
          </Box>
        </Stack>
      </Box>
    </Stack>
  );
};

export default OtpVerification;
