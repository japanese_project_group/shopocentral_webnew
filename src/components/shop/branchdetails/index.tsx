import { IconButton, Paper, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import {
  useGetIndividualBranchQuery,
  useGetIndividualShopDataQuery,
} from "../../../../redux/api/user/user";
import Image from "next/image";
import { isEmpty } from "lodash";
import Link from "next/link";
import { BranchDetailsTab } from "./branchDetailsTab";

export const BranchDetailsPage = () => {
  const router = useRouter();
  const [ID, setID] = useState<any>();
  const [shopData, setShopData] = useState<any>([]);
  const {
    data: branchDetails,
    isSuccess,
    refetch,
  } = useGetIndividualBranchQuery({ id: router.query.id });

  useEffect(() => {
    setID(router.query.id);
  }, [router.query]);

  useEffect(() => {
    if (!isEmpty(branchDetails)) {
      setShopData(branchDetails);
    }
    refetch();
  }, [branchDetails]);

  return (
    <div className="shop-body">
      <div style={{ marginBottom: "38px" }}>
        <Paper
          component="form"
          sx={{
            p: "2px 4px",
            display: "flex",
            alignItems: "center",
            width: 400,
            height: "34px",
          }}
          className="shop-body-search-form"
        >
          <IconButton type="button" sx={{ p: "10px" }} aria-label="search">
            <SearchIcon />
          </IconButton>
          <TextField
            sx={{ ml: 1, flex: 1 }}
            placeholder="Search By shop/branch name"
            inputProps={{ "aria-label": "search google maps" }}
          />
        </Paper>
      </div>
      <div className="display-flex">
        <ArrowBackIcon
          onClick={() => router.back()}
          style={{
            height: "2rem",
            width: "2rem",
            marginTop: "6px",
            cursor: "pointer",
          }}
        />
        <div className="shop-body-search-form-header">View Branch Details</div>
      </div>
      <div className="shop-body-details-image">
        <BranchDetailsTab {...branchDetails?.data} />
      </div>
    </div>
  );
};
