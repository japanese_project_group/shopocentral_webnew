import { Box, Button, Tab, Tabs } from "@mui/material";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import { useState } from "react";
import { ViewImage } from "../../../common/Components";
import Products from "../products";
import { ShopReviewComponent } from "../review";
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}
interface ShopDetailsProps {
  name_en?: string;
  email: string;
  tel: any;
  website: string;
  closingTime: string;
  openingTime: string;
  category?: any;
  subcategory?: any;
  shopImage?: any;
  address?: any;
  phoneNumber?: any;
}

export const BranchDetailsTab = (props: any) => {
  const router = useRouter();

  // const { shopBranch } = props;
  const [value, setValue] = useState<number>(0);
  const [openImageDisplayModal, setOpenImageDisplayModal] =
    useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>("");
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  const shopInfoDetails = (title: string, data?: any, type?: string) => {
    return (
      <div
        className="display-flex-sb"
        style={{ width: "60%", padding: "6.5px" }}
      >
        <div className="shop-view-details-tab-title">{title}</div>
        {type !== "multiple" ? (
          <div className="shop-view-details-tab-data" style={{ width: "20%" }}>
            {data}
          </div>
        ) : (
          <div className="shop-view-details-tab-data" style={{ width: "20%" }}>
            {data?.map((sub_data: any, index?: number) =>
              index === data?.length - 1
                ? sub_data?.category?.name_en || sub_data?.subCategory?.name_en
                : `${sub_data?.category?.name_en ||
                sub_data?.subCategory?.name_en
                },`
            )}
          </div>
        )}
      </div>
    );
  };
  const shopImageDetails = (title?: string, images?: any, type?: string) => {
    return (
      <div
        className="display-flex-sb"
        style={{ width: "60%", padding: "6.5px" }}
      >
        <div className="shop-view-details-tab-title">
          <span>{title}</span>
        </div>
        {type !== "single" ? (
          <div className="shop-view-details-tab-data display-images-shop">
            {images?.slice(0, 2)?.map((image: any, index?: number) => (
              <img
                src={image}
                className="image-view-shop-details"
                onClick={() => {
                  setOpenImageDisplayModal(true);
                  setDisplayData(image);
                }}
              />
            ))}
            {images?.length > 2 && (
              <span
                className="text-blue text-underlined cursor-pointer"
                onClick={() => {
                  setValue(4);
                }}
              >
                View More
              </span>
            )}
          </div>
        ) : (
          <div className="shop-view-details-tab-data">
            <img
              src={images}
              className="image-view-shop-details"
              onClick={() => {
                setOpenImageDisplayModal(true);
                setDisplayData(images);
              }}
            />
          </div>
        )}
      </div>
    );
  };
  const shopImageDetailsGallery = (title?: string, images?: any) => {
    return (
      <div className="display-flex-sb" style={{ padding: "6.5px" }}>
        <div className="shop-view-details-tab-data display-images-shop-gallery-view">
          {images?.map((image: any, index?: number) => (
            <img
              src={image}
              className="image-view-shop-details"
              onClick={() => {
                setOpenImageDisplayModal(true);
                setDisplayData(image);
              }}
            />
          ))}
        </div>
      </div>
    );
  };

  const handleUpdateBranch = () => {
    router.push({
      pathname: "/shop/update-branch",
      query: { branchId: props?.id },
    });
  };

  const shopInfo = () => {
    return (
      <div style={{ height: "100%", overflowY: "auto" }}>
        <Button
          style={{ margin: "20px 30px" }}
          variant="contained"
          onClick={() => handleUpdateBranch()}
        >
          update
        </Button>
        {shopInfoDetails("Branch Name", props?.name_en)}
        {shopInfoDetails("Branch Category", props?.Category, "multiple")}
        {shopInfoDetails("Branch Sub-Category", props?.SubCategory, "multiple")}
        {shopInfoDetails("Branch Email", props?.email)}
        {shopInfoDetails(
          "Branch Telephone Number",
          `${props?.phoneNumber?.phoneType}-${props?.phoneNumber?.phoneNumber}`
        )}
        {shopInfoDetails("Branch Website", props?.website)}
        {shopInfoDetails("Branch opening Time", props?.closingTime)}
        {shopInfoDetails("Branch Closing Time", props?.openingTime)}
        <span
          className="display-flex-jc-center text-blue"
          style={{ marginTop: "10px" }}
        >
          Address
        </span>
        {shopInfoDetails("Country", props?.address?.country_en)}
        {shopInfoDetails("Postal Code", props?.address?.postalcode_en)}
        {shopInfoDetails("Prefecture", props?.address?.prefecture_en)}
        {shopInfoDetails("City/Village", props?.address?.city_en)}
        {shopInfoDetails("Town/Ward", props?.address?.town_en)}
        {shopInfoDetails("House Number", props?.address?.house_number)}
        <span
          className="display-flex-jc-center text-blue"
          style={{ marginTop: "10px" }}
        >
          Branch Profile Picture
        </span>
        {shopImageDetails(
          "Branch Profile Picture",
          props?.shopImage?.shopProfileImage,
          "single"
        )}
        <span className="display-flex-jc-center text-blue">Images</span>
        {!isEmpty(props?.shopImage?.flyers) &&
          shopImageDetails("Flyers Images", props?.shopImage?.flyers)}
        {!isEmpty(props?.shopImage?.businessCard) &&
          shopImageDetails(
            "Business Card Images",
            props?.shopImage?.businessCard
          )}
        {!isEmpty(props?.shopImage?.signBoards) &&
          shopImageDetails("SignBoards Images", props?.shopImage?.signBoards)}
        {!isEmpty(props?.shopImage?.shopsPhotographs) &&
          shopImageDetails(
            "Branch Photographs Images",
            props?.shopImage?.shopsPhotographs
          )}
      </div>
    );
  };

  const shopProduct = () => {
    return (
      <Products shopData={props} />
    )
  }


  const ratingReview = () => {
    return (
      <ShopReviewComponent />
    )
  }

  const galleryView = () => {
    return (
      <div style={{ height: "100%", overflowY: "auto" }}>
        {!isEmpty(props?.shopImage?.shopsPhotographs) &&
          shopImageDetailsGallery(
            "Shops Photographs Images",
            props?.shopImage?.shopsPhotographs
          )}
      </div>
    );
  };

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            {index === 0 && shopInfo()}
            {index === 1 && shopProduct()}
            {index === 2 && ratingReview()}
            {index === 3 && galleryView()}
            {/* {index === 2 && branchView()} */}
          </Box>
        )}
      </div>
    );
  }
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  return (
    <Box sx={{ width: "100%" }} className="shop-view-details-tab">
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          className="shop-view-details-tab-individual"
        >
          <Tab label="Branch Info" {...a11yProps(0)} />
          <Tab label="Product" {...a11yProps(1)} />
          {/* <Tab label="Branches" {...a11yProps(2)} /> */}
          <Tab label="Rating & Reviews" {...a11yProps(3)} />
          <Tab label="Gallery" {...a11yProps(4)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        Branch Info
      </TabPanel>
      <TabPanel value={value} index={1}>
        Product
      </TabPanel>
      <TabPanel value={value} index={2}>
        Rating & Reviews
      </TabPanel>
      <TabPanel value={value} index={3}>
        Gallery
      </TabPanel>
      {openImageDisplayModal && (
        <ViewImage
          displayData={displayData}
          setOpenModal={setOpenImageDisplayModal}
          openModal={openImageDisplayModal}
          server={true}
        />
      )}
    </Box>
  );
};
