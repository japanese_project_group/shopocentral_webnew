import { Box, CircularProgress, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useGetIndividualShopProductsQuery } from "../../../redux/api/user/user";
import ProductCard from "./productCard";

const Products = (shopData: any) => {
    const [shopID, setShopID] = useState<any>("")
    const { data: shopProduct, refetch, isLoading } = useGetIndividualShopProductsQuery(shopID);

    useEffect(() => {
        setShopID(shopData?.shopData?.id || shopData?.data?.data?.id)
    }, [shopData])

    useEffect(() => {
        if (shopID) {
            refetch();
        }
    }, [shopID, refetch]);

    return (
        <Box width="100%">
            {isLoading ? (
                <Box display="flex" justifyContent="center" alignItems="center" height="200px">
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    {shopProduct && shopProduct.length > 0 ? (
                        <Box display="flex" gap="1rem" flexWrap="wrap">
                            {shopProduct.map((product: any) => (
                                <ProductCard
                                    key={product?.product?.id}
                                    width="200px"
                                    headerVariant="h5"
                                    imageType="big"
                                    name={product?.product?.MasterProduct?.name}
                                    image={product?.product?.MasterProduct?.image?.masterImages[0]}
                                    brandName={product?.product?.MasterProduct?.Brand?.name_en}
                                    price={product?.product?.MasterProduct?.MasterOffers?.price_with_tax}
                                    estimatedPrice={product?.product?.MasterProduct?.MasterOffers?.estimated_price}
                                />
                            ))}
                        </Box>
                    ) : (
                        <Typography variant="body1" textAlign="center" mt={3}>
                            No Product found.
                        </Typography>
                    )}
                </>
            )}
        </Box>
    );
}

export default Products;
