import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import SearchIcon from "@mui/icons-material/Search";
import { IconButton, Paper, TextField } from "@mui/material";
import { isEmpty } from "lodash";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useGetIndividualShopDataQuery, useGetShopReviewDataQuery } from "../../../../redux/api/user/user";
import { ViewDetailsTab } from "./viewDetailsTab";

export const ViewDetails = () => {
  const router = useRouter();

  const [ID, setID] = useState<any>();
  const [shopData, setShopData] = useState<any>([]);
  const {
    data: shopDetails,
    isSuccess,
    refetch,
  } = useGetIndividualShopDataQuery(ID);

  const shopId = 'da6bc1bc-4ead-47f5-8ac3-7fd29a4911a4';
  const { data: shopReview, isSuccess: isSuccessReview } =
    useGetShopReviewDataQuery(shopId);
  console.log("shopReview", shopReview, shopDetails);

  useEffect(() => {
    setID(router.query.id);
  }, [router.query]);

  useEffect(() => {
    if (!isEmpty(shopDetails)) {
      setShopData(shopDetails);
    }
  }, [shopDetails]);

  return (
    <div className="shop-body">
      <div style={{ marginBottom: "38px" }}>
        <Paper
          component="form"
          sx={{
            p: "2px 4px",
            display: "flex",
            alignItems: "center",
            width: 400,
            height: "34px",
          }}
          className="shop-body-search-form"
        >
          <IconButton type="button" sx={{ p: "10px" }} aria-label="search">
            <SearchIcon />
          </IconButton>
          <TextField
            sx={{ ml: 1, flex: 1 }}
            placeholder="Search By shop/branch name"
            inputProps={{ "aria-label": "search google maps" }}
          />
        </Paper>
      </div>
      <div className="display-flex">
        <Link href="/shop">
          <ArrowBackIcon
            style={{
              height: "2rem",
              width: "2rem",
              marginTop: "6px",
              cursor: "pointer",
            }}
          />
        </Link>
        <div className="shop-body-search-form-header">View Shop Details</div>
      </div>
      <div className="shop-body-details-image">
        <ViewDetailsTab {...{ ...shopDetails?.data, shopReview }} />
      </div>
    </div>
  );
};
