import { Box, Button, Tab, Tabs } from "@mui/material";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { DisplayCard } from "../../../common/Components";
import ImageViewer from "../../../common/ImageViewe";
import Products from "../products";
import { ShopReviewComponent } from "../review";
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}
interface ShopDetailsProps {
  name_en?: string;
  email: string;
  tel: any;
  website: string;
  closingTime: string;
  openingTime: string;
  Category?: any;
  SubCategory?: any;
  shopImage?: any;
  address?: any;
  phoneNumber?: any;
  identityStatus?: any;
  handlingService?: Array<string>;
  color?: any;
  id?: any;
  shopReview?: any;
}

export const ViewDetailsTab = (props: ShopDetailsProps) => {
  const router = useRouter();
  const { shopBranch }: any = props;
  const { identityStatus } = props;
  const [value, setValue] = useState<number>(0);
  const [openImageDisplayModal, setOpenImageDisplayModal] =
    useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>("");
  const [iAmVerified, setIAmVerified] = useState<boolean>(false);
  const [slideImage, setSlideImage] = useState<any>([]);
  const [openImageModal, setOpenImageModal] = useState<boolean>(false);
  console.log("props>>", props?.shopReview)

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    {
      identityStatus &&
        identityStatus[0]?.status === "VERIFIED" &&
        setIAmVerified(true);
    }
  }, [identityStatus]);
  const shopInfoDetails = (
    title: string,
    data?: any,
    type?: string,
    color?: boolean
  ) => {
    return (
      <div
        className="display-flex-sb"
        style={{ width: "60%", padding: "6.5px" }}
      >
        <div className="shop-view-details-tab-title">{title}:</div>

        {type !== "multiple" ? (
          <div className="shop-view-details-tab-data" style={{ width: "20%" }}>
            {!color && data}
            {color && (
              <div
                style={{
                  width: "20%",
                  height: "20px",
                  backgroundColor: `${data}`,
                }}
              />
            )}
          </div>
        ) : (
          <div
            className="shop-view-details-tab-data"
            style={{
              width: "20%",
              display: "flex",
              flexDirection: "column",
              gap: "2px",
            }}
          >
            {data?.map((sub_data: any, index?: number) =>
              index === data?.length - 1 ? (
                <p style={{ display: "flex", marginTop: "-10px" }}>
                  {sub_data?.category?.name_en ||
                    sub_data?.subCategory?.name_en}
                </p>
              ) : (
                <p style={{ marginTop: "-10px" }}>
                  {sub_data?.category?.name_en ||
                    sub_data?.subCategory?.name_en}
                  ,
                </p>
              )
            )}
          </div>
        )}
      </div>
    );
  };
  const shopImageDetails = (title?: string, images?: any, type?: string) => {
    return (
      <div
        className="display-flex-sb"
        style={{ width: "60%", padding: "6.5px" }}
      >
        <div className="shop-view-details-tab-title ">
          <span>{title}</span>
        </div>
        {type !== "single" ? (
          <div className="shop-view-details-tab-data display-images-shop">
            {images?.slice(0, 2)?.map((image: any, index?: number) => (
              <img
                src={image}
                className="image-view-shop-details"
                // onClick={() => {
                //   setOpenImageDisplayModal(true);
                //   setDisplayData(image);
                // }}
                onClick={() => {
                  setSlideImage([{ src: image, title: title }]);
                  setOpenImageDisplayModal(true);
                  setDisplayData(image);
                }}
              />
            ))}
            {images?.length > 2 && (
              <span
                className="text-blue text-underlined cursor-pointer"
                onClick={() => {
                  setValue(4);
                }}
              >
                View More
              </span>
            )}
          </div>
        ) : (
          <div className="shop-view-details-tab-data">
            <img
              src={images}
              className="image-view-shop-details"
              onClick={() => {
                setSlideImage([{ src: images, title: title }]);
                setOpenImageDisplayModal(true);
                setDisplayData(images);
              }}
            />
          </div>
        )}
      </div>
    );
  };
  const shopImageDetailsGallery = (title?: string, images?: any) => {
    return (
      <div className="display-flex-sb" style={{ padding: "6.5px" }}>
        <div className="shop-view-details-tab-data display-images-shop-gallery-view">
          {images?.map((image: any, index?: number) => (
            <img
              src={image}
              className="image-view-shop-details"
              onClick={() => {
                setSlideImage([{ src: image }]);
                setOpenImageDisplayModal(true);
                setDisplayData(image);
              }}
            />
          ))}
        </div>
      </div>
    );
  };

  const shopInfo = () => {
    return (
      <div style={{ height: "100%", overflowY: "auto" }}>
        <button
          className="shop-branch-status"
          style={{
            backgroundColor: iAmVerified ? "green" : "#ff0000",
          }}
        >
          <p style={{ color: "white", fontWeight: "bold" }}>
            {identityStatus && identityStatus[0]?.status}
          </p>
        </button>
        <div
          style={{
            width: "100%",
            height: "30px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button
            variant="contained"
            style={{ marginLeft: "40%" }}
            onClick={() =>
              router.push({
                pathname: "/shop/update-shop",
                query: {
                  shopId: props?.id,
                },
              })
            }
          >
            Edit
          </Button>
        </div>
        {shopInfoDetails("Shop Name", props?.name_en)}
        {shopInfoDetails("Shop Category", props?.Category, "multiple")}
        {shopInfoDetails("Shop Sub-Category", props?.SubCategory, "multiple")}
        {shopInfoDetails("Email", props?.email)}
        {shopInfoDetails(
          "Phone Number",
          `${props?.phoneNumber?.phoneType}-${props?.phoneNumber?.phoneNumber}`
        )}
        {shopInfoDetails(
          "Website",
          props?.website === "null" ? "" : props?.website
        )}
        {shopInfoDetails("Opening Time", props?.openingTime)}
        {shopInfoDetails("Closing Time", props?.closingTime)}
        {shopInfoDetails(
          "Handling Services",
          props?.handlingService?.map((data: any) => data).toString()
        )}

        {shopInfoDetails(
          "Shop Color",
          props?.color && props?.color[0]?.primaryColor,
          "",
          true
        )}
        <span
          className="display-flex-jc-center text-blue"
          style={{
            marginTop: "10px",
            fontSize: "20px",
            display: "flex",
            alignSelf: "center",
          }}
        >
          Address
        </span>
        {shopInfoDetails("Country", props?.address?.country_en)}
        {shopInfoDetails("Postal Code", props?.address?.postalcode_en)}
        {shopInfoDetails("Prefecture", props?.address?.prefecture_en)}
        {shopInfoDetails("City/Village", props?.address?.city_en)}
        {shopInfoDetails("Town/Ward", props?.address?.town_en)}
        {shopInfoDetails("House Number", props?.address?.house_number)}
        <span
          className="display-flex-jc-center text-blue"
          style={{ marginTop: "10px", fontSize: "20px" }}
        >
          Images
        </span>
        {shopImageDetails(
          "Shop Profile Picture",
          props?.shopImage?.shopProfileImage,
          "single"
        )}
        <span
          className="display-flex-jc-center text-blue"
          style={{ marginTop: "10px", fontSize: "20px" }}
        ></span>
        {!isEmpty(props?.shopImage?.flyers) &&
          shopImageDetails("Flyers Images", props?.shopImage?.flyers)}
        {!isEmpty(props?.shopImage?.businessCard) &&
          shopImageDetails(
            "Business Cards / Home Page",
            props?.shopImage?.businessCard
          )}
        {!isEmpty(props?.shopImage?.signBoards) &&
          shopImageDetails("Signboards", props?.shopImage?.signBoards)}
        {!isEmpty(props?.shopImage?.shopsPhotographs) &&
          shopImageDetails(
            "Shop photographs / office photographs",
            props?.shopImage?.shopsPhotographs
          )}
      </div>
    );
  };
  const galleryView = () => {
    return (
      <div style={{ height: "100%", overflowY: "auto" }}>
        {!isEmpty(props?.shopImage?.shopsPhotographs) &&
          shopImageDetailsGallery(
            "Shop photographs / office photographs",
            props?.shopImage?.shopsPhotographs
          )}
      </div>
    );
  };

  const moveToDetails = (id: any) => {
    router.push({
      pathname: "/shop/branch/details",
      query: { id: id },
    });
  };

  const branchView = () => {
    return (
      <Box display={"flex"} gap="1rem" flexWrap={"wrap"}>
        {shopBranch?.map((data: any) => (
          <div key={data?.id} onClick={() => moveToDetails(data?.id)}>
            <DisplayCard
              width="200px"
              headerVariant="h5"
              imageType="big"
              name={data?.name_en}
              // status={identityStatus?.[0]?.status}
              image={data?.businessLicense}
            />
          </div>
        ))}
      </Box>
    );
  };

  const ratingReview = () => {
    return (
      <ShopReviewComponent data={props.shopReview} />
    )
  }

  const shopProduct = () => {
    return (
      <Products shopData={props} />
    )
  }

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            {index === 0 && shopInfo()}
            {index === 1 && shopProduct()}
            {index === 4 && galleryView()}
            {index === 3 && ratingReview()}
            {index === 2 && branchView()}
          </Box>
        )}
      </div>
    );
  }
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  return (
    <Box sx={{ width: "100%" }} className="shop-view-details-tab">
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          className="shop-view-details-tab-individual"
        >
          <Tab label="Shop Info" {...a11yProps(0)} />
          <Tab label="Product" {...a11yProps(1)} />
          <Tab label="Branches" {...a11yProps(2)} />
          <Tab label="Rating & Reviews" {...a11yProps(3)} />
          <Tab label="Gallery" {...a11yProps(4)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        Shop Info
      </TabPanel>
      <TabPanel value={value} index={1}>
        Product
      </TabPanel>
      <TabPanel value={value} index={2}>
        Branches
      </TabPanel>
      <TabPanel value={value} index={3}>
        Rating & Reviews
      </TabPanel>
      <TabPanel value={value} index={4}>
        Gallery
      </TabPanel>
      {/* {openImageDisplayModal && (
        // <ViewImage
        //   displayData={displayData}
        //   setOpenModal={setOpenImageDisplayModal}
        //   openModal={openImageDisplayModal}
        //   server={true}
        // />
      )} */}
      <ImageViewer
        slideImage={slideImage}
        openImageModal={openImageDisplayModal}
        setOpenImageModal={setOpenImageDisplayModal}
      />
    </Box>
  );
};
