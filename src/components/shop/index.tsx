import PageEmpty from "@/src/common/PageEmpty";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import SearchIcon from "@mui/icons-material/Search";
import {
  Button,
  IconButton,
  Paper,
  Switch,
  TextField
} from "@mui/material";
import Router, { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  useGetAllDataByIdQuery,
  useGetProductReviewDataQuery,
  useGetShopDataQuery
} from "../../../redux/api/user/user";
import { setProfile } from "../../../redux/features/profileSlice";
import { RootState } from "../../../redux/store";
import { BusinessKycStatusMessage } from "../customComponents/businessKycStatusMessage";
import { DetailsTopView } from "../customComponents/detailsTopView";
import { ShopTableView } from "./shopTableView";
import { ViewShopData } from "./viewShopData";
const ShopComponent = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const productId = '8d872d71-4a88-47db-9f0e-25fda448ab99';

  const { data: allData, isSuccess, refetch } = useGetShopDataQuery();
  const businessId = localStorage.getItem("businessAccessId");
  const { data: allDataProfile, isSuccess: isSuccessGetProfile } =
    useGetAllDataByIdQuery(businessId);

  const { data: productReview, isSuccess: isSuccessProductReview } =
    useGetProductReviewDataQuery(productId);

  const { profileData } = useSelector((state: RootState) => state?.profile);

  const profileStatus = allDataProfile?.data?.identityStatus?.[0]?.status;
  const [values, setValues] = useState<any>([]);
  const [tableView, setTableView] = useState<boolean>(false);
  console.log("productReview", productReview);

  const handleNewShop = () => {
    Router.push("/shop/add-shop");
  }

  const changeShopView = (e: any) => {
    setTableView(!tableView);
  };

  useEffect(() => {
    if (allData && isSuccess) {
      setValues(allData?.data);
    }
  }, [isSuccess, allData]);

  useEffect(() => {
    refetch();
  }, []);

  useEffect(() => {
    if (!!allDataProfile?.data) {
      dispatch(setProfile(allDataProfile?.data));
    }
  }, [allDataProfile]);

  return (
    <>
      {profileStatus === "FINITIAL" ? (
        <BusinessKycStatusMessage status="FINITIAL" />
      ) : profileStatus === "INITIAL" ? (
        <BusinessKycStatusMessage status="INITIAL" />
      ) : profileStatus === "PENDING" || profileStatus === "UNVERIFIED" ? (
        <BusinessKycStatusMessage
          status="PENDING"
          message={allDataProfile?.data?.identityStatus?.[0]?.message}
        />
      ) : (
        <>
          {!!allData?.data && values.length <= 0 && (
            <>
              <DetailsTopView
                headerName="SHOPS AND BRANCHES"
                handleButtonChange={handleNewShop}
                buttonText="Add New Shop"
              // searchPlaceholder='Search By Email or phone number'
              // searchField={true}
              />
              <PageEmpty />
              {/* {" "}
              <Paper
                sx={{
                  padding: "2%",
                  marginTop: "2%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {" "}
                <Typography>
                  {" "}
                  No Shop Added. Please Add a new Shop!!!{" "}
                </Typography>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNewShop}
                  sx={{ marginLeft: "5%" }}
                >
                  Add New Shop
                </Button>
              </Paper> */}
            </>
          )}
          {!!allData?.data && values.length > 0 && (
            <div className="shop-body">
              <div style={{ marginBottom: "38px" }} className="display-flex-sb">
                <div>
                  <Paper
                    component="form"
                    sx={{
                      p: "2px 4px",
                      display: "flex",
                      alignItems: "center",
                      width: 400,
                      height: "34px",
                    }}
                    className="shop-body-search-form"
                  >
                    <IconButton
                      type="button"
                      sx={{ p: "10px" }}
                      aria-label="search"
                    >
                      <SearchIcon />
                    </IconButton>
                    <TextField
                      sx={{ ml: 1, flex: 1 }}
                      placeholder="Search By shop/branch name"
                      inputProps={{ "aria-label": "search google maps" }}
                    />
                  </Paper>
                </div>
                <div>
                  <Switch
                    onChange={(e) => {
                      changeShopView(e);
                    }}
                  />
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  marginBottom: "50px",
                }}
              >
                <h1 style={{ lineHeight: "0px" }}>SHOPS AND BRANCHES</h1>
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    className="shop-body-add-button"
                    onClick={handleNewShop}
                  >
                    Add Shop{" "}
                    <AddCircleOutlineIcon className="shop-body-add-icon" />
                  </Button>
                </div>
              </div>
              {!tableView ? (
                <div>
                  {values?.map((data: any) => (
                    <ViewShopData {...data} />
                  ))}
                </div>
              ) : (
                <div>
                  <ShopTableView data={values} />
                </div>
              )}
            </div>
          )}
        </>
      )}
    </>
  );
};
export default ShopComponent;
