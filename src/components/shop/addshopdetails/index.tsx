import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import SearchIcon from "@mui/icons-material/Search";
import {
  Box,
  Button,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { skipToken } from "@reduxjs/toolkit/dist/query";
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import moment from "moment";
import Images from "next/image";
import Router, { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  useGetCategoryQuery,
  useGetIndividualShopDataQuery,
  useGetSubCategoryQuery,
  usePostBranchDetailsMutation,
  usePostShopDetailsMutation,
  useUpdateShopDetailsMerchantMutation,
} from "../../../../redux/api/user/user";
import { RootState } from "../../../../redux/store";
import { InputComponent, ViewImage } from "../../../common/Components";
import CircularIndeterminate from "../../../common/LoadingScreen";
import { handlingServiceTypeData } from "../../../common/commonData";
import { validationSchemaAddShop } from "../../../common/validationSchema";
import { BusinessKycStatusMessage } from "../../customComponents/businessKycStatusMessage";
import { StepForm } from "../stepForm";
import MapComponent from "@/src/common/MapComponent";
import { LocationInterface } from "@/src/@types/common";
// import MapComponent from "@/src/c";

type addNumberType = {
  addNumber: number
}
const ShopDetailsPage = () => {
  const router = useRouter();
  const formType = router.query.addFormType;
  const shopId: any = router.query.shopId;
  const [
    valueAddShop,
    { isSuccess: isSuccessShop, data: dataShop, isLoading, isError, error },
  ] = usePostShopDetailsMutation();
  const [
    valueUpdateBranch,
    {
      isSuccess: isSuccessBranch,
      isLoading: isLoadingBranch,
      data: dataBranch,
    },
  ] = usePostBranchDetailsMutation();

  const profileRef: any = useRef();
  const [
    valueUpdateShop,
    { isSuccess: isSuccessUpdateShop, data: dataUpdateShop },
  ] = useUpdateShopDetailsMerchantMutation();

  const {
    data: shopDetailsData,
    isSuccess: isSuccessShopData,
    refetch,
  } = useGetIndividualShopDataQuery(shopId ?? skipToken);

  const { profileData } = useSelector((state: RootState) => state?.profile);
  const { data: categories, isSuccess: categoryData } = useGetCategoryQuery();

  const [profileDatas, setprofileDatas] = useState<any>();
  const [signboards, setSignBoards] = useState<any>([]);
  const [flyers, setFlyers] = useState<any>([]);
  const [businessLicense, setBusinessLicense] = useState<any>([]);
  const [shopImages, setShopImages] = useState<any>([]);
  const [businessCards, setBusinessCards] = useState<any>([]);
  const [shopProfilePicture, setShopProfilePicture] = useState<any>([]);
  const [isDataSending, setIsdataSending] = useState<boolean>(false);
  const [formState, setFormState] = React.useState({
    subCategory: [],
    category: [],
    handlingServices: [],
  });

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>([]);
  const [addNumber, setAddNumber] = useState<addNumberType[]>([]);
  const [colorPickerValue, setColorPickerValue] = useState<string>('#000000');
  const [shopOnCategoryId, setShopOnCategoryId] = useState<any>([]);
  const [shopOnSubCategoryId, setShopOnSubCategoryId] = useState<any>([]);
  const merchantProfileData: any = useSelector(
    (state: any) => state?.profile?.profileData
  );
  // for map integration
  const [location, setLocation] = React.useState<LocationInterface>({
    lat: 36.2048,
    lng: 138.2529,
  });
  const businessKycDetails = merchantProfileData?.identityStatus?.[0];

  console.log(location, shopDetailsData, 'shopdetails data')


  useEffect(() => {
    if (isLoading) {
      setIsdataSending(true);
    } else {
      setIsdataSending(false);
    }
  }, [isLoading]);

  useEffect(() => {
    setprofileDatas(profileData?.data);
  }, [profileDatas]);

  //get sub category,
  const { data: subcatData, isSuccess } = useGetSubCategoryQuery();

  useEffect(() => {
    if (isSuccessShop && dataShop) {
      Router.push("/shop");
      toast("Shop Added SuccessFully", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
    }
  }, [isSuccessShop, dataShop]);

  useEffect(() => {
    if (isSuccessBranch && dataBranch) {
      Router.push("/shop");
      toast("Branch Added SuccessFully", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
    }
  }, [isSuccessBranch, dataBranch]);
  useEffect(() => {
    if (!!dataUpdateShop) {
      toast.success(dataUpdateShop?.message);
      Router.push("/shop");
    }
  }, [dataUpdateShop]);
  const formik = useFormik({
    initialValues: {
      city_en: "" || shopDetailsData?.data?.address?.city_en,
      city_jpn: "" || shopDetailsData?.data?.address?.city_jpn,
      city_np: "" || shopDetailsData?.data?.address?.city_np,
      country_en: "" || shopDetailsData?.data?.address?.country_en || "Japan",
      country_np: "" || shopDetailsData?.data?.address?.country_np,
      country_jpn: "" || shopDetailsData?.data?.address?.country_jpn,
      postalcode_en:
        "" ||
        (shopDetailsData?.data?.address?.postalcode_en &&
          shopDetailsData?.data?.address?.postalcode_en),
      postalcode_end:
        "" || shopDetailsData?.data?.address?.postalcode_en?.trim().slice(3, 7),
      prefecture_en: "" || shopDetailsData?.data?.address?.prefecture_en,
      prefecture_jpn: "" || shopDetailsData?.data?.address?.prefecture_jpn,
      prefecture_np: "" || shopDetailsData?.data?.address?.prefecture_np,
      district_en: "" || shopDetailsData?.data?.address?.district_en,
      district_jpn: "" || shopDetailsData?.data?.address?.district_jpn,
      district_np: "" || shopDetailsData?.data?.address?.district_np,
      town_en: "" || shopDetailsData?.data?.address?.town_en,
      town_jpn: "" || shopDetailsData?.data?.address?.town_jpn,
      town_np: "" || shopDetailsData?.data?.address?.town_np,
      state_en: "" || shopDetailsData?.data?.address?.state_en,
      state_np: "" || shopDetailsData?.data?.address?.state_np,
      state_jpn: "" || shopDetailsData?.data?.address?.state_jpn,
      name_en: "" || shopDetailsData?.data?.name_en,
      name_jp: "" || shopDetailsData?.data?.name_jp,
      name_np: "" || shopDetailsData?.data?.name_np,
      email: "" || shopDetailsData?.data?.email,
      website: "" || shopDetailsData?.data?.website,
      phoneNumber: "" || shopDetailsData?.data?.phoneNumber?.phoneNumber,
      openingTime: "" || shopDetailsData?.data?.openingTime,
      closingTime: "" || shopDetailsData?.data?.closingTime,
      latitude: location.lat,
      longitude: location.lng,
      businessLicense: "",
      house_number: "" || shopDetailsData?.data?.address?.house_number,
      tel: "",
      phoneType: "+81",
    },
    enableReinitialize: true,
    validationSchema: validationSchemaAddShop,

    onSubmit: async (values: any) => {
      if (
        shopImages?.length < 1 &&
        businessCards?.length < 1 &&
        flyers?.length < 1 &&
        signboards?.length < 1
      ) {
        toast.error("you must upload your document");
        return;
      }
      if (isEmpty(shopProfilePicture)) {
        toast.error("You must upload shop profile picture");
        return;
      }
      const { postalcode_en, postalcode_end, ...rest } = values;
      const fullpostalCode = ` ${postalcode_en}${postalcode_end}`;
      await submitValues({
        postalcode_en: fullpostalCode,
        ...rest,
      });
    },
  });
  let categoryId = [...formState?.category];
  let subcategoryId = [...formState?.subCategory];

  async function submitValues(values: any) {
    values.openingTime = moment(values.openingTime, "hh:mm").format("hh:mm A");
    values.closingTime = moment(values.closingTime, "hh:mm").format("hh:mm A");
    const userId = localStorage.getItem("userIdNotification");
    const finalData: any = {
      ...values,
      flyers: flyers,
      signBoards: signboards,
      businessCard: businessCards,
      shopsPhotographs: shopImages,
      shopBranchId: shopId ? shopId : null,
      primaryColor: colorPickerValue ? colorPickerValue : "#000000",
      shopProfileImage: shopProfilePicture,
    };

    const finalUpdatedata = {
      ...finalData,
      identityStatusId: shopDetailsData?.data?.identityStatus?.[0]?.id,
      addressId: shopDetailsData?.data?.address?.id,
      phoneNumberId: shopDetailsData?.data?.phoneNumber?.id,
      shopImageId: shopDetailsData?.data?.shopImage?.id,
    };
    const finalSubmitData =
      formType === "add-shop" ? finalData : finalUpdatedata;
    let fd = new FormData();
    Object.keys(finalSubmitData).map((item: any) => {
      if (
        item === "flyers" ||
        item === "signBoards" ||
        item === "businessCard" ||
        item === "shopsPhotographs" ||
        item === "shopProfileImage"
      ) {
        finalSubmitData[item].map((i: any) => {
          fd.append(item, i);
        });
      } else {
        fd.append(item, finalSubmitData[item] ? finalSubmitData[item] : null);
      }
    });
    // @ts-ignore
    fd.append(`categoryId[]`, categoryId);
    // @ts-ignore
    fd.append(`subcategoryId[]`, subcategoryId)
    // @ts-ignore
    fd.append(`secondaryPhoneNumber[]`, addNumber?.map((number) => number.addNumber));
    // @ts-ignore
    fd.append(`handlingService[]`, formState?.handlingServices);

    let reqData = {
      data: fd,
      id: shopId,
    };

    if (formType === "add-shop") {
      valueAddShop(reqData)
        .unwrap()
        .catch((error: { data: { message: any } }) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    } else if (formType === "add-branch") {
      valueUpdateBranch(reqData)
        .unwrap()
        .catch((error: { data: { message: any } }) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    } else if (formType === "update-shop") {
      fd.append("shopOnCategoryId[]", shopOnCategoryId);

      fd.append(`shopOnSubCategoryId[]`, shopOnSubCategoryId);
      fd.append("colorId", shopDetailsData?.data?.color[0].id)
      let reqData = {
        data: fd,
        id: shopId,
      };
      valueUpdateShop(reqData)
        .unwrap()
        .catch((error: { data: { message: any } }) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    }
  }

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  let imgarr: any = [];
  const onChangeBusinessCards = (e: any, type?: string) => {
    if (type === "single") {
      // setBusinessCards(e.target.files);
    } else {
      setBusinessCards([...businessCards, ...e.target.files]);
    }
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setBusinessCards([]);
      }
    });
  };
  const onChangeShopImages = (e: any) => {
    if (shopImages?.length >= 9) {
      toast.error("cannot upload more than 10 files");
    }
    setShopImages([...shopImages, ...e.target.files]);
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setShopImages([]);
      }
    });
  };

  const onChangeSignBoards = (e: any, type?: string) => {
    if (type === "single") {
      setSignBoards(e.target.files);
    } else {
      setSignBoards([...signboards, ...e.target.files]);
    }
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setSignBoards([]);
      }
    });
  };


  const onChangeFlyers = (e: any, type?: string) => {
    if (type === "single") {
      setFlyers(e.target.files);
    } else {
      setFlyers([...flyers, ...e.target.files]);
    }
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setFlyers([]);
      }
    });
  };



  const onChangeBusinessLicense = (e: any, type?: string) => {
    if (type === "single") {
      // setFlyers([])
      setBusinessLicense(e.target.files);
    } else {
      setBusinessLicense([...businessLicense, ...e.target.files]);
    }
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setBusinessLicense([]);
      }
    });
  };

  const onChangeProfilePicture = (e: any, type?: string) => {
    if (type === "singleee") {
      setShopProfilePicture(e.target.files);
    } else {
      setShopProfilePicture([...e.target.files]);
    }
    imgarr = [...e.target.files];
    imgarr.map((i: any) => {
      if (i.size / 1024 / 1024 > 2) {
        toast("Please upload image with size less than 2 MB", {
          type: "error",
        });
        setShopProfilePicture([]);
      }
    });
  };
  const handleAddBusinessKyc = () => {
    Router.push("/kycforms/businesskyc");
  };

  function handleFieldChange(event: any) {
    setFormState((formState) => ({
      ...formState,
      [event.target.name]: event.target?.value,
    }));
  }
  const dataform = formState?.category?.map((data: any) => data);
  const filteredSubCateData = subcatData?.filter((data1: any) =>
    dataform?.includes(data1?.category?.id)
  );

  const category = categories?.map((data: any) => ({
    value: data?.id,
    label: data.name_en,
  }));
  const subCategory = filteredSubCateData?.map((data: any) => ({
    value: data?.id,
    label: data?.name_en,
  }));

  const flyersFile = () => {
    document.getElementById("flyersFile")?.click();
  };
  const signboardsFile = () => {
    document.getElementById("signboardsFile")?.click();
  };
  const businessCardsFile = () => {
    document.getElementById("businessCardsFile")?.click();
  };
  const shopImagesFile = () => {
    document.getElementById("shopImagesFile")?.click();
  };
  const profileImage = () => {
    document.getElementById("shopProfileImage")?.click();
  };

  const businessLicenseFile = () => {
    document.getElementById("businessLicenseFile")?.click();
  };

  const displayUploadedImage = (
    image: any,
    file: any,
    onChangeImageUpload: any,
    setImage: any,
    ImageName: string,
    imageValue: string,
    inputFileName: string
  ) => {
    return (
      <div>
        {imageValue !== "shopImages" ? (
          <Images
            onClick={() => {
              setOpenModal(true);
              setDisplayData(image);
            }}
            src={
              image?.[0]?.length > 2
                ? image?.[0]
                : image?.[0]?.length === 1
                  ? image?.[0]?.[0]
                  : URL.createObjectURL(image?.[0])
            }
            height={320}
            width={470}
            style={{ borderRadius: "12px" }}
          />
        ) : (
          <Images
            src={image?.length ? image : URL.createObjectURL(image)}
            height={320}
            width={470}
            style={{
              borderRadius: "12px",
            }}
            onClick={() => {
              setOpenModal(true);
              setDisplayData(image);
            }}
          />
        )}
        <div
          style={{ display: "flex", justifyContent: "flex-start", gap: "10px" }}
        >
          <div style={{ marginTop: "5px" }}>{ImageName}</div>
          <div>
            <Button
              variant="outlined"
              style={{ height: "30px" }}
              onClick={() => {
                setOpenModal(true);
                setDisplayData(image);
              }}
            >
              <RemoveRedEyeIcon />
            </Button>
            <Button
              variant="outlined"
              style={{ height: "30px" }}
              onClick={() => {
                file();
              }}
            >
              <EditIcon />
              <input
                type="file"
                id={inputFileName}
                style={{ display: "none" }}
                onChange={(e) => onChangeImageUpload(e, "single")}
                accept=".jpeg, .png, .jpg, .svg"
              />
            </Button>
            <Button
              variant="outlined"
              style={{ height: "30px" }}
              onClick={() => {
                setFieldValue(imageValue, null);
                setImage([]);
              }}
            >
              <DeleteIcon />
            </Button>
          </div>
        </div>
      </div>
    );
  };
  useEffect(() => {
    if (!!shopDetailsData && formType === "update-shop") {
      setLocation({
        lat: Number(shopDetailsData?.data?.longitude),
        lng: Number(shopDetailsData?.data?.latitude)
      })
      setColorPickerValue(shopDetailsData?.data?.color?.[0]?.primaryColor);
      !!shopDetailsData?.data?.shopImage?.shopProfileImage &&
        setShopProfilePicture([
          shopDetailsData?.data?.shopImage?.shopProfileImage,
        ]);
      !isEmpty(shopDetailsData?.data?.shopImage?.signBoards) &&
        setSignBoards([shopDetailsData?.data?.shopImage?.signBoards]);
      !isEmpty(shopDetailsData?.data?.shopImage?.businessCard) &&
        setBusinessCards([shopDetailsData?.data?.shopImage?.businessCard]);
      !isEmpty(shopDetailsData?.data?.shopImage?.flyers) &&
        setFlyers([shopDetailsData?.data?.shopImage?.flyers]);
      !isEmpty(shopDetailsData?.data?.shopImage?.shopsPhotographs) &&
        setShopImages([shopDetailsData?.data?.shopImage?.shopsPhotographs]);
      const subCategories = shopDetailsData?.data?.SubCategory?.map(
        (data: any) => data?.subCategoryId
      ) || []
      const uniqueSubCategories = [...new Set(subCategories)];
      setFormState((prev: any) => ({
        ...prev,
        category:
          shopDetailsData?.data?.Category?.map(
            (data: any) => data?.categoryId
          ) || [],
        subCategory: uniqueSubCategories,
        handlingServices:
          shopDetailsData?.data?.handlingService?.map((data: any) => data) ||
          [],
      }));
      setShopOnCategoryId(
        shopDetailsData?.data?.Category?.map((data: any) => data?.id)
      );
      setShopOnSubCategoryId(
        shopDetailsData?.data?.SubCategory?.map((data: any) => data?.id)
      );
      setAddNumber(
        shopDetailsData?.data?.phoneNumber?.secondaryPhoneNumber?.map(
          (number: any) => ({ addNumber: number })
        )
      );
      localStorage.setItem(
        "postalcodeValue",
        shopDetailsData?.data?.address?.postalcode_en?.trim()?.slice(0, 3)
      );
      setFieldValue("openingTime", shopDetailsData?.data?.openingTime);
    }
  }, [shopDetailsData]);
  console.log(shopOnCategoryId, shopOnSubCategoryId, addNumber, 'klklk')

  useEffect(() => {
    if (formType === "add-shop") {
      localStorage.setItem(
        "postalcodeValue",
        merchantProfileData?.address?.postalcode_en?.trim().slice(0, 3)
      );
      setFieldValue(
        "postalcode_en",
        merchantProfileData?.address?.postalcode_en?.trim().slice(0, 3)
      );
      setFieldValue(
        "postalcode_end",
        merchantProfileData?.address?.postalcode_en?.trim().slice(3, 7)
      );
      setFieldValue(
        "prefecture_en",
        merchantProfileData?.address?.prefecture_en
      );
      setFieldValue("city_en", merchantProfileData?.address?.city_en);
      setFieldValue("town_en", merchantProfileData?.address?.town_en);
      setFieldValue("house_number", merchantProfileData?.address?.house_number);
    }
  }, [merchantProfileData]);

  return (
    <>
      {businessKycDetails?.status === "FINITIAL" ? (
        <BusinessKycStatusMessage status="FINITIAL" />
      ) : businessKycDetails?.status === "INITIAL" ? (
        <BusinessKycStatusMessage status="INITIAL" />
      ) : businessKycDetails?.status === "PENDING" ||
        businessKycDetails?.status === "UNVERIFIED" ? (
        <BusinessKycStatusMessage
          status="PENDING"
          message={businessKycDetails?.message}
        />
      ) : (
        <>
          {isDataSending == true ? (
            <>
              <CircularIndeterminate></CircularIndeterminate>
            </>
          ) : (
            <>
              {true ? (
                formType === "add-shop" || formType === "update-shop" ? (
                  <FormikProvider value={formik}>
                    <Stack justifyContent={"center"} direction="row" mt={10}>
                      <Stack
                        sx={{
                          width: {
                            lg: "60%",
                            md: "90%",
                            sm: "95%",
                            xs: "100%",
                          },
                          borderRadius: 4,
                          padding: 5,
                          background: "white",
                        }}
                      >
                        <Typography
                          variant="h4"
                          textAlign={"center"}
                          mb={2}
                          color="primary"
                        >
                          Shop Details
                        </Typography>
                        <form onSubmit={handleSubmit}>
                          <Box
                            sx={{
                              border: "2",
                              marginTop: "2%",
                              marginBottom: "5%",
                            }}
                          >
                            <Box sx={{ width: "100%" }}>
                              <Typography
                                variant="h5"
                                sx={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                                color="primary"
                              >
                                Shop Info
                              </Typography>
                            </Box>
                            <InputComponent
                              label="Shop Name"
                              touched={touched.name_en}
                              errors={errors.name_en}
                              getFieldProps={getFieldProps}
                              getFieldPropsValue="name_en"
                              type="text"
                            />
                            {/* </Grid> */}
                            Shop category
                            <Typography
                              component="span"
                              color="error"
                              sx={{ marginLeft: "5px" }}
                            >
                              *
                            </Typography>
                            <TextField
                              {...getFieldProps("category")}
                              size="small"
                              fullWidth
                              select
                              SelectProps={{
                                multiple: true,
                                value: formState?.category,
                                onChange: handleFieldChange,
                              }}
                            >
                              {category?.map((option: any, index?: number) => (
                                <MenuItem key={index} value={option?.value}>
                                  {option.label}
                                </MenuItem>
                              ))}
                            </TextField>
                            {/* </Grid> */}
                            <Grid item xs={12} sm={6}>
                              Shop sub-category
                              <TextField
                                {...getFieldProps("subCategory")}
                                size="small"
                                fullWidth
                                select
                                SelectProps={{
                                  multiple: true,
                                  value: formState?.subCategory,
                                  onChange: handleFieldChange,
                                }}
                              >
                                {subCategory?.map((option: any) => (
                                  <MenuItem
                                    key={option?.id}
                                    id={option?.value}
                                    value={option?.value}
                                  >
                                    {option.label}
                                  </MenuItem>
                                ))}
                                {subCategory?.length < 1 && (
                                  <MenuItem>No Sub Categories</MenuItem>
                                )}
                              </TextField>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Email"
                                touched={touched.email}
                                errors={errors.email}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="email"
                                type="text"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Contact Number"
                                type="number"
                                touched={touched.phoneNumber}
                                errors={errors.phoneNumber}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="phoneNumber"
                                compulsary="false"
                              />
                            </Grid>
                            <InputComponent
                              type="secondaryPhoneNumber"
                              addNumber={addNumber}
                              setAddNumber={setAddNumber}
                            // touched={touched.secondaryNumber}
                            // errors={errors.secondaryNumber}
                            />
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Website"
                                type="text"
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="website"
                                compulsary="false"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}></Grid>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <div style={{ width: "50%" }}>
                                <InputComponent
                                  label="Opening Time"
                                  type="time"
                                  touched={touched.openingTime}
                                  errors={errors.openingTime}
                                  getFieldProps={getFieldProps}
                                  getFieldPropsValue="openingTime"
                                />
                              </div>

                              <div style={{ width: "50%" }}>
                                <InputComponent
                                  label="Closing Time"
                                  type="time"
                                  touched={touched.closingTime}
                                  errors={errors.closingTime}
                                  getFieldProps={getFieldProps}
                                  getFieldPropsValue="closingTime"
                                />
                              </div>
                            </div>
                            <Grid item xs={12} sm={6}>
                              Handling Services
                              <Typography
                                component="span"
                                color="error"
                                sx={{ marginLeft: "5px" }}
                              >
                                *
                              </Typography>
                              <TextField
                                {...getFieldProps("handlingServices")}
                                size="small"
                                fullWidth
                                select
                                SelectProps={{
                                  multiple: true,
                                  value: formState?.handlingServices,
                                  onChange: handleFieldChange,
                                }}
                              >
                                {handlingServiceTypeData?.map((option) => (
                                  <MenuItem
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </MenuItem>
                                ))}
                              </TextField>
                            </Grid>
                            {/* color picker */}
                            <div
                              className="display-flex-sb"
                              style={{
                                marginTop: "12px",
                                marginBottom: "12px",
                              }}
                            >
                              <div>Choose Your Shop Color:</div>
                              <div>
                                <input
                                  type="color"
                                  id="favcolor"
                                  name="favcolor"
                                  value={colorPickerValue}
                                  onChange={(e: any) => {
                                    setColorPickerValue(e.target.value);
                                  }}
                                />
                              </div>
                            </div>
                            <div>
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                  marginTop: "20px",
                                }}
                              >
                                <div>
                                  Choose your shop profile picture
                                  <Typography component="span" color="error">
                                    *
                                  </Typography>
                                </div>

                                <Button
                                  onClick={() => {
                                    profileImage();
                                  }}
                                  variant="contained"
                                  onClickCapture={() =>
                                    profileRef.current.click()
                                  }
                                >
                                  Add Shop Profile Picture
                                </Button>
                                <input
                                  type="file"
                                  ref={profileRef}
                                  style={{ display: "none" }}
                                  onChange={onChangeProfilePicture}
                                  accept=".jpeg, .png, .jpg, .svg"
                                />
                              </div>
                            </div>
                            <div
                              style={{
                                marginBottom: "40px",
                                marginTop: "10px",
                              }}
                              className="display-image-personal-kyc"
                            >
                              <div style={{ width: "50%" }}>
                                {!isEmpty(shopProfilePicture) &&
                                  displayUploadedImage(
                                    shopProfilePicture,
                                    profileImage,
                                    onChangeProfilePicture,
                                    setShopProfilePicture,
                                    "Shop Profile Image",
                                    "shopProfilePicture",
                                    "shopProfileImage"
                                  )}
                              </div>
                            </div>
                            <Box>
                              <Typography variant="body1" marginBottom={"5px"}>
                                Select Location
                              </Typography>
                              <MapComponent
                                location={location}
                                zoom={12}
                                setLocation={setLocation}
                              />
                            </Box>
                            <Box sx={{ width: "100%", marginTop: "20px" }}>
                              <Typography
                                variant="h5"
                                sx={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                                color="primary"
                              >
                                Address
                              </Typography>
                            </Box>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Country"
                                touched={touched.country_en}
                                errors={errors.country_en}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="country_en"
                                type="text"
                                disabled={true}
                              />
                            </Grid>
                            <Grid
                              item
                              xs={12}
                              sm={6}
                              style={{
                                marginTop: "10px",
                                marginBottom: "10px",
                              }}
                            >
                              <InputComponent
                                type="postalCode"
                                touched1={touched.postalcode_en}
                                touched2={touched.postalcode_end}
                                errors1={errors.postalcode_en}
                                errors2={errors.postalcode_end}
                                getFieldProps={getFieldProps}
                                label="Postal Code"
                                values={values}
                                setFieldValue={setFieldValue}
                                getFieldPropsValue="postalcode_en"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Prefecture"
                                touched={touched.prefecture_en}
                                errors={errors.prefecture_en}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="prefecture_en"
                                type="text"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="City/Village"
                                touched={touched.city_en}
                                errors={errors.city_en}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="city_en"
                                type="text"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="Town/Ward"
                                touched={touched.town_en}
                                errors={errors.town_en}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="town_en"
                                type="text"
                              />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <InputComponent
                                label="House Number"
                                type="text"
                                touched={touched.house_number}
                                errors={errors.house_number}
                                getFieldProps={getFieldProps}
                                getFieldPropsValue="house_number"
                              />
                            </Grid>
                          </Box>
                          {/* <Grid item xs={12} sm={6}> */}
                          Upload Documents (png, jpg, jpeg)
                          <Typography component="span" color="error">
                            *
                          </Typography>
                          <FormControl
                            size="small"
                            sx={{ width: "100%", marginBottom: "40px" }}
                          >
                            <TextField
                              placeholder="Select any file to upload"
                              select
                              fullWidth
                              size="small"
                              SelectProps={{ displayEmpty: true }}
                            >
                              {isEmpty(flyers) && (
                                <>
                                  <MenuItem
                                    onClick={() => {
                                      flyersFile();
                                    }}
                                  >
                                    Flyers
                                  </MenuItem>
                                  <input
                                    type="file"
                                    id="flyersFile"
                                    style={{ display: "none" }}
                                    onChange={onChangeFlyers}
                                    accept=".jpeg, .png, .jpg, .svg"
                                    multiple // Allow selecting multiple files
                                  />
                                </>
                              )}

                              {isEmpty(businessLicense) && (
                                <>
                                  <MenuItem
                                    onClick={() => {
                                      businessLicenseFile();
                                    }}
                                  >
                                    businessLicense
                                  </MenuItem>
                                  <input
                                    type="file"
                                    id="businessLicenseFile"
                                    style={{ display: "none" }}
                                    onChange={onChangeBusinessLicense}
                                    accept=".jpeg, .png, .jpg, .svg"
                                  />
                                </>
                              )}
                              {isEmpty(signboards) && (
                                <>
                                  <MenuItem
                                    onClick={() => {
                                      signboardsFile();
                                    }}
                                  >
                                    Signboards
                                  </MenuItem>
                                  <input
                                    type="file"
                                    id="signboardsFile"
                                    style={{ display: "none" }}
                                    onChange={onChangeSignBoards}
                                    accept=".jpeg, .png, .jpg, .svg"
                                  ></input>
                                </>
                              )}
                              {isEmpty(businessCards) && (
                                <>
                                  <MenuItem
                                    onClick={() => {
                                      businessCardsFile();
                                    }}
                                  >
                                    Business cards / Home Page
                                  </MenuItem>
                                  <input
                                    type="file"
                                    id="businessCardsFile"
                                    style={{ display: "none" }}
                                    onChange={onChangeBusinessCards}
                                    accept=".jpeg, .png, .jpg, .svg"
                                  ></input>
                                </>
                              )}
                              {shopImages?.length === 10 ? (
                                ""
                              ) : (
                                <>
                                  <MenuItem
                                    onClick={() => {
                                      shopImagesFile();
                                    }}
                                  >
                                    Shop photographs / office photographs
                                  </MenuItem>
                                  <input
                                    type="file"
                                    id="shopImagesFile"
                                    style={{ display: "none" }}
                                    onChange={onChangeShopImages}
                                    accept=".jpeg, .png, .jpg, .svg"
                                  ></input>
                                </>
                              )}
                            </TextField>
                          </FormControl>
                          <div
                            style={{
                              marginTop: "-30px",
                              marginBottom: "40px",
                              display: "grid",
                              gridTemplateColumns: "auto auto",
                              gap: "5px",
                              width: "50%"
                            }}
                            className="display-image-personal-kyc"
                          >
                            {!isEmpty(flyers) &&
                              displayUploadedImage(
                                flyers,
                                flyersFile,
                                onChangeFlyers,
                                setFlyers,
                                "Flyers Image",
                                "flyers",
                                "flyers"
                              )}

                            {!isEmpty(businessLicense) &&
                              displayUploadedImage(
                                businessLicense,
                                businessLicenseFile,
                                onChangeBusinessLicense,
                                setBusinessLicense,
                                "business license",
                                "business license",
                                "business license"
                              )}

                            {!isEmpty(signboards) &&
                              displayUploadedImage(
                                signboards,
                                signboardsFile,
                                onChangeSignBoards,
                                setSignBoards,
                                "Sign Boards Image",
                                "signboards",
                                "signboardsFile"
                              )}
                            {!isEmpty(businessCards) &&
                              displayUploadedImage(
                                businessCards,
                                businessCardsFile,
                                onChangeBusinessCards,
                                setBusinessCards,
                                "Business Cards Image",
                                "businessCards",
                                "businessCardsFile"
                              )}
                            {!isEmpty(shopImages) &&
                              shopImages?.map((images: any, index: number) =>
                                displayUploadedImage(
                                  images,
                                  shopImagesFile,
                                  onChangeShopImages,
                                  setShopImages,
                                  "Shop Images",
                                  "shopImages",
                                  "shopImagesFile"
                                )
                              )}
                          </div>
                          {/* </Grid> */}
                          <Box
                            sx={{
                              width: "100%",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              mt: 2,
                            }}
                          >
                            <Button
                              type="submit"
                              variant="contained"
                              color="primary"
                              sx={{ width: "100%" }}
                            >
                              {router?.query?.addFormType === "update-shop"
                                ? "update"
                                : "submit"}
                            </Button>
                          </Box>
                        </form>
                      </Stack>
                    </Stack>
                  </FormikProvider>
                ) : (
                  <>
                    {formType === "add-branch" ? (
                      <div className="shop-body">
                        <div style={{ marginBottom: "38px" }}>
                          <div className="shop-body-search-form-header">
                            {router?.query?.addFormType === "update-branch"
                              ? "UPDATE BRANCH"
                              : "ADD BRANCH"}
                          </div>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            marginBottom: "50px",
                          }}
                        ></div>
                        <div className="add-branch-form">
                          <StepForm
                            shopId={shopId}
                            shopDetailsData={shopDetailsData}
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="shop-body">
                        <div style={{ marginBottom: "38px" }}>
                          <Paper
                            component="form"
                            sx={{
                              p: "2px 4px",
                              display: "flex",
                              alignItems: "center",
                              width: 400,
                              height: "34px",
                            }}
                            className="shop-body-search-form"
                          >
                            <IconButton
                              type="button"
                              sx={{ p: "10px" }}
                              aria-label="search"
                            >
                              <SearchIcon />
                            </IconButton>
                            <TextField
                              sx={{ ml: 1, flex: 1 }}
                              placeholder="Category Branch Name"
                              inputProps={{
                                "aria-label": "search google maps",
                              }}
                            />
                          </Paper>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            marginBottom: "50px",
                          }}
                        >
                          <div className="shop-body-search-form-header">
                            {router?.query?.addFormType === "update-branch"
                              ? "UPDATE BRANCH"
                              : "ADD BRANCH"}
                          </div>
                        </div>
                        <div className="add-branch-form">
                          <StepForm
                            shopId={shopId}
                            shopDetailsData={shopDetailsData}
                          />
                        </div>
                      </div>
                    )}
                  </>
                )
              ) : (
                <Paper
                  sx={{
                    padding: "2%",
                    marginTop: "2%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  {" "}
                  <Typography> No Business Kyc Found!!! </Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleAddBusinessKyc}
                    sx={{ marginLeft: "5%" }}
                  >
                    Add Business Kyc
                  </Button>
                </Paper>
              )}
            </>
          )}
          {openModal && (
            <ViewImage
              displayData={displayData?.[0]}
              setOpenModal={setOpenModal}
              openModal={openModal}
            />
          )}
        </>
      )
      }
    </>
  );
};
export default ShopDetailsPage;
