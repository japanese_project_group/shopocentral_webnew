import { InputComponent } from "@/src/common/Components";
import PageEmpty from "@/src/common/PageEmpty";
import ServerDown from "@/src/common/ServerDown";
import { productStatus } from "@/src/common/commonData";
import {
    Box,
    Button,
    CircularProgress,
    MenuItem,
    TextField,
    Typography,
} from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
    useGetOrderByIdQuery,
    useUpdateOrderMutation,
} from "../../../../redux/api/user/user";
import { DetailsTopView } from "../../customComponents/detailsTopView";

interface FormValues {
    productName: string;
    orderType: string;
    quantity: string;
    price: string;
}

const EditOrder = () => {
    const router = useRouter();
    const [Id, setId] = useState<any>(router.query.id);
    const [isSubmittingMaster, setIsSubmittingMaster] = useState<boolean>(false);
    const [formState, setFormState] = React.useState({
        orderStatus: "",
    });

    const [updateOrderProduct, { data: updateBrandData, isSuccess }] = useUpdateOrderMutation();
    const {
        data: getSingleOrderData,
        refetch: SellProductFetch,
        isLoading: GetSingleOrderLoading,
        isError: error,
    } = useGetOrderByIdQuery(router.query.id);

    useEffect(() => {
        if (router.query.id) {
            setId(router.query.id);
        }
    }, [router.query.id]);

    useEffect(() => {
        SellProductFetch();
    }, [getSingleOrderData, SellProductFetch]);

    const handleBackBtn = () => {
        router.push('/shop/order')
    }

    useEffect(() => {
        setFormState((prev) => ({
            ...prev,
            orderStatus: getSingleOrderData?.orderTracker?.orderStatus || "",
        }));
    }, [getSingleOrderData]);

    const handleFieldChange = (event: any, fieldName: string) => {
        setFormState((prev) => ({
            ...prev,
            [fieldName]: event.target.value,
        }));
    };



    const initialValues: FormValues = {
        orderType: getSingleOrderData?.orderType || "",
        productName:
            getSingleOrderData?.orderedItems?.[0]?.product?.MasterProduct?.name || "",
        quantity: getSingleOrderData?.orderedItems?.[0]?.quantity || "",
        price: getSingleOrderData?.orderedItems?.[0]?.price || "",
    };

    const formik = useFormik({
        initialValues,
        enableReinitialize: true,
        onSubmit: async (values: any, { setErrors }) => {
            try {
                const errors = await formik.validateForm();
                if (Object.keys(errors).length === 0) {
                    await submitForm(values);
                } else {
                    setErrors(errors);
                }
            } catch (error) {
                console.error("Error submitting form:", error);
                toast.error("An error occurred while submitting the form.");
            }
        },
    });

    const handleSubmitButton = async (buttonType: string) => {
        try {
            if (isSubmittingMaster) return;
            setIsSubmittingMaster(true);
            // const errors = await formik.validateForm();
            if (buttonType === "submit") {
                await submitForm(formik.values);
            } else if (buttonType === "update") {
                await submitForm(formik.values);
            }
        } catch (error) {
            console.error("Error handling next step:", error);
            toast.error("An error occurred while proceeding to the next step.");
        } finally {
            setIsSubmittingMaster(false);
        }
    };

    const submitForm = async (values: FormValues) => {
        try {
            const fd = new FormData();

            // Append values
            fd.append("orderStatus", formState.orderStatus);
            fd.append("id", Id);

            // Append other values from the form
            Object.entries(values).forEach(([key, value]) => {
                fd.append(key, value);
            });

            const response = await updateOrderProduct(fd);
            if (response?.statusCode === 400 || !response?.data) {
                throw new Error("Bad Request Error");
            }

            // Handling success case
            toast.success(response?.message || response?.data?.message);
            router.push('/shop/order')
            return true;
        } catch (err: any) {
            toast.error(err?.message || "An error occurred !!");
            return false;
        }
    };

    const {
        errors,
        values,
        touched,
        isSubmitting,
        handleSubmit,
        getFieldProps,
        setFieldValue,
    } = formik;

    const fieldFormsText = (
        fieldName: string,
        getFieldPropsValue: string,
        type?: string,
        errors?: any,
        touched?: any,
        disabled?: boolean,
        options?: any,
        selectValue?: any,
        setSelectType?: any,
        selectDataType?: string,
        defaultValueOption?: string
    ) => {
        return (
            <Box display={"flex"} justifyContent={""}>
                <div className="display-flex-sb">
                    <div>{fieldName}:</div>
                    <div
                        style={{
                            width: "60%",
                            gap: "5px",
                        }}
                    >
                        <InputComponent
                            type={type}
                            getFieldProps={getFieldProps}
                            getFieldPropsValue={getFieldPropsValue}
                            errors={errors}
                            touched={touched}
                            disabled={disabled}
                            setFieldValue={setFieldValue}
                            values={values}
                            label={type === "postalCode" && "grgrt"}
                            selectValue={selectValue}
                            setSelectType={setSelectType}
                            options={options}
                            selectDataType={selectDataType}
                        />
                        {errors &&
                            touched &&
                            errors[getFieldPropsValue] &&
                            touched[getFieldPropsValue] && (
                                <div>{errors[getFieldPropsValue]}</div>
                            )}
                    </div>
                </div>
            </Box>
        );
    };

    const displayForms = () => (
        <FormikProvider value={formik}>
            <form onSubmit={handleSubmit} className="form-field-wrapper">
                {fieldFormsText(
                    "Order Type",
                    "orderType",
                    "text",
                    errors.orderType,
                    touched.orderType,
                    true
                )}
                {fieldFormsText(
                    "Product Name",
                    "productName",
                    "text",
                    errors.productName,
                    touched.productName,
                    true
                )}
                {fieldFormsText(
                    "Quantity",
                    "quantity",
                    "text",
                    errors.quantity,
                    touched.quantity,
                    true
                )}
                {fieldFormsText(
                    "Price ",
                    "price",
                    "text",
                    errors.price,
                    touched.price,
                    true
                )}
                <div
                    style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "space-between",
                    }}
                >
                    <Typography sx={{ width: "25%" }} component="span">
                        Order Status
                    </Typography>
                    <TextField
                        sx={{ width: "60%" }}
                        {...getFieldProps("orderStatus")}
                        size="small"
                        fullWidth
                        select
                        name="orderStatus"
                        SelectProps={{
                            multiple: false,
                            value: formState.orderStatus,
                            onChange: (event) => handleFieldChange(event, "orderStatus"),
                        }}
                    >
                        {productStatus?.map((option: any) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                <Box display="flex" marginTop="2rem" gap="1rem" justifyContent="end">
                    {getSingleOrderData ? (
                        <Button
                            variant="contained"
                            onClick={() => handleSubmitButton("update")}
                            disabled={isSubmittingMaster}
                        >
                            Update
                        </Button>
                    ) : (
                        <Button
                            variant="contained"
                            onClick={() => handleSubmitButton("submit")}
                            disabled={isSubmittingMaster}
                        >
                            Submit
                        </Button>
                    )}
                </Box>
            </form>
        </FormikProvider>
    );

    return (
        <Box>
            <DetailsTopView headerName="Edit Order Product" buttonText="Back" isBackBtn={true} handleButtonChange={handleBackBtn} />
            {GetSingleOrderLoading ? (
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    height="200px"
                >
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    {error ? (
                        <ServerDown />
                    ) : (
                        // <ErrorMessage error={error} />
                        <>
                            {getSingleOrderData ? (
                                <div className="form-view-multistep-form box-kyc-multiple-step-form card-wrapper">
                                    {displayForms()}
                                </div>
                            ) : (
                                <PageEmpty />
                            )}
                        </>
                    )}
                </>
            )}
        </Box>
    );
};

export default EditOrder;
