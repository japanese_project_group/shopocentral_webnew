// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from 'react';

// MUI Components
import { Box, Chip, CircularProgress, Typography } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

// third party librray
import moment from "moment";

// assets
import ViewIcon from "../../../../public/viewIcon.svg";

// redux
import { useGetMyOrderQuery } from "../../../../redux/api/user/user";

// custom components
import PageEmpty from "@/src/common/PageEmpty";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const AllOrder = () => {
    const router = useRouter();
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPageSize, setCurrentPageSize] = useState(10);
    const [orderProductData, setOrderProductData] = useState<any>([]);

    // const { data: masterProductListData, isLoading: getLoading, refetch } =
    //     useGetMasterProductDataQuery({ page: currentPage, pageSize: currentPageSize });

    const { data: orderData, refetch: orderRefetch, isLoading: getLoading } = useGetMyOrderQuery();
    console.log("orderData", orderData);

    useEffect(() => {
        orderRefetch();
    }, [orderRefetch]);

    const handleChangeButton = () => {
        router.push("/shop/order/edit");
    }

    const handleEdit = (id: any) => {
        router.push({
            pathname: `/shop/order/edit`,
            query: { id },
        });
    };

    const handleOnCellClick = () => { };
    const columns = [
        {
            field: "S.No",
            headerName: "S.No",
            headerClassName: "data-grid-header-classname",
            renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
            minWidth: 80,
            maxWidth: 100,
        },

        {
            field: "order_type",
            headerName: "Order Type",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.orderType}</div>,
            minWidth: 110,
            flex: 1,
        },
        {
            field: "name",
            headerName: "Product Name",
            minWidth: 180,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.orderedItems?.[0]?.product?.MasterProduct?.name}</div>
        },

        {
            field: "phone",
            headerName: "Phone No.",
            minWidth: 130,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.category ?
                <Chip label={params?.row?.category?.name_en} color="success" />
                : <p>No Category Found</p>
            }
            </div>
        },
        {
            field: "quantity",
            headerName: "Quantity",
            minWidth: 120,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.orderedItems?.[0]?.quantity ?
                <Typography variant="body2">{params?.row?.orderedItems?.[0]?.quantity} {params?.row?.orderedItems?.[0]?.product?.count_type}</Typography>
                : <p>No Quantity Found</p>
            }
            </div>
        },
        {
            field: "Price",
            headerName: "Price",
            minWidth: 120,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.MasterOffers ?
                <Typography variant="body2">{params?.row?.MasterOffers?.estimated_price}</Typography>
                : <p>No status </p>
            }
            </div>
        },
        {
            field: "image",
            headerName: "Image",
            minWidth: 150,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.image?.masterImages[0] ?
                <img height={"60px"} width={"70px"} style={{ objectFit: "contain" }} src={params?.row?.image?.masterImages[0]} alt="product main image" />
                : <p>No Image Found</p>
            }
            </div>
        },
        {
            field: "shipping_address",
            headerName: "Shipping Address",
            minWidth: 200,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.shippingAddress ?
                <Typography variant="body2">{params?.row?.shippingAddress}</Typography>
                : <p>No Address Found</p>
            }
            </div>
        },
        {
            field: "status",
            headerName: "Status",
            minWidth: 140,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.orderTracker?.orderStatus ?
                <Chip
                    label={params?.row?.orderTracker?.orderStatus}
                    sx={{
                        backgroundColor:
                            params?.row?.orderTracker?.orderStatus === 'CANCELED'
                                ? '#fae9e9'
                                : '#EAF5F0',
                    }}
                />
                : <p>No Status</p>
            }
            </div>
        },
        {
            field: "date",
            headerName: "Date",
            minWidth: 100,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) =>
                moment(params.row.createdAt).format("YYYY/MM/DD"),
        },
        {
            field: "remarks",
            headerName: "Remarks",
            minWidth: 150,
            flex: 1,
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => <div>{params?.row?.remarks ?
                <Typography variant="body2">{params?.row?.remarks}</Typography>
                : <p>No Remarks</p>
            }
            </div>
        },
        {
            field: "Action",
            headerName: "Action",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => (
                <>
                    <strong style={{ display: "flex" }}>
                        <div onClick={() => handleEdit(params?.row?.id)}
                            style={{ marginTop: "8px", marginLeft: "16px", cursor: "pointer" }}
                        >
                            <Image src={ViewIcon} style={{ marginLeft: "10px" }} />
                        </div>
                    </strong>
                </>
            ),
            minWidth: 80,
            flex: 1,
        },
    ];
    console.log("masterProductListData", orderProductData);

    useEffect(() => {
        if (orderData) {
            setOrderProductData(orderData);
        }
    }, [orderData]);

    return (
        <>
            <DetailsTopView
                headerName="All Order"
                handleButtonChange={handleChangeButton}
                // buttonText="Add Master Product"
                // searchPlaceholder='Search By Email or phone number'
                isButton={false}
                searchField={true}
            />
            {getLoading ? (
                <Box display="flex" justifyContent="center" alignItems="center" height="200px">
                    <CircularProgress />
                </Box>
            ) : (
                <div className="staff-view-table">
                    {orderProductData && orderProductData?.length > 0 ? (
                        <Box sx={{ height: 600, width: "100%" }}>
                            <DataGrid
                                rows={orderProductData || []}
                                columns={columns}
                                rowCount={orderProductData?.totalCount}
                                pageSize={currentPageSize}
                                page={currentPage - 1}
                                rowsPerPageOptions={[10, 20, 50]}
                                onPageSizeChange={(newPageSize) => {
                                    setCurrentPageSize(newPageSize);
                                    setCurrentPage(1);
                                }}
                                onPageChange={(newPage) => {
                                    setCurrentPage(newPage + 1);
                                }}
                                paginationMode="server"
                                onCellClick={handleOnCellClick}
                                loading={getLoading}
                            />
                        </Box>
                    ) : (<PageEmpty />)}
                </div>
            )}
        </>
    );
};
