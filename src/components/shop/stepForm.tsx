// Build in Library
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

// third party library
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import { toast } from "react-toastify";

// MUI Components
import { Box, Button, MenuItem, TextField, Typography } from "@mui/material";

// Redux
import {
  useGetCategoryQuery,

  useGetIndividualBranchQuery,
  useGetSubCategoryQuery,
  usePostBranchDetailsMutation,
  useUpdateIndividualBranchMutation
} from "../../../redux/api/user/user";

// Components
import { InputComponent } from "../../common/Components";
import MapComponent from "../../common/MapComponent";

// Validation
import { validationSchemaAddBranch } from "../../common/validationSchema";
import { LocationInterface } from "src/@types/common";

export const StepForm = (props: any) => {
  const [shopOnCategoryId, setShopOnCategoryId] = useState<any>([]);
  const [shopOnSubCategoryId, setShopOnSubCategoryId] = useState<any>([]);
  const [updateState, setUpdateState] = useState<boolean>(false);
  const [colorPickerValue, setColorPickerValue] = useState<string>("");
  const [addNumber, setAddNumber] = useState<any>([{ otherNumber: "" }]);
  //images
  const [businessLicenseImage, setBusinessLicenseImage] = useState<any>([]);
  const [signBoardsImage, setSignBoardsImage] = useState<any>([]);
  const [flyersImage, setFlyersImage] = useState<any>([]);
  const [businessCardImage, setBusinessCardImage] = useState<any>([]);
  const [shopsPhotographsImage, setShopsPhotographsImage] = useState<any>([]);
  const [shopProfileImage, setShopProfileImage] = useState<any>([]);
  const [formState, setFormState] = useState({
    subCategory: [],
    category: [],
  });
  const router = useRouter();

  const branchId = router.asPath.split("=");
  const { data: branchDetails, refetch } = useGetIndividualBranchQuery({
    id: branchId[1],
  });

  const [ValueUpdate, { isSuccess, isLoading, data }] =
    usePostBranchDetailsMutation();
  const { data: categories, isSuccess: categoryData } = useGetCategoryQuery();
  const { data: subcatData, isSuccess: isSuccessSubCategory } =
    useGetSubCategoryQuery();

  const [
    UpdateBranch,
  ] = useUpdateIndividualBranchMutation();

  // for map integration
  const [location, setLocation] = React.useState<LocationInterface>({
    lat: 36.2048,
    lng: 138.2529,
  });

  useEffect(() => {
    if (router?.query?.addFormType === "update-branch") {
      setUpdateState(true);
    }
  }, [router?.query?.addFormType, updateState]);

  useEffect(() => {
    if (branchDetails) {
      setFormState({
        category:
          branchDetails?.data?.Category?.map((cat: any) => cat?.category?.id) ||
          [],
        subCategory:
          branchDetails?.data?.SubCategory?.map(
            (subCat: any) => subCat?.subCategory?.id
          ) || [],
      });

      // setSignBoardsImage(branchDetails?.data?.shopImage?.signBoards[0]);
      localStorage.setItem(
        "postalcodeValue",
        branchDetails?.data?.address?.postalcode_en
      );

      setShopOnCategoryId(
        branchDetails?.data?.Category?.map((cat: any) => cat?.id) || []
      );
      setShopOnSubCategoryId(
        branchDetails?.data?.SubCategory?.map((data: any) => data?.id) || []
      );
    }
  }, [branchDetails]);

  function handleFieldChange(event: any) {
    setFormState((formState) => ({
      ...formState,
      [event.target.name]: event.target?.value,
    }));
  }
  const dataform = formState?.category?.map((data: any) => data);
  const filteredSubCateData = subcatData?.filter((data1: any) =>
    dataform?.includes(data1?.category?.id)
  );

  const category = categories?.map((data: any) => ({
    value: data?.id,
    label: data.name_en,
  }));

  const subCategory = filteredSubCateData?.map((data: any) => ({
    value: data?.id,
    label: data?.name_en,
  }));

  // const secondaryPhoneNumber:  branchDetails?.data?.phoneNumber?.phoneNumber;

  const formik = useFormik({
    initialValues: {
      name_en: "" || branchDetails?.data?.name_en,
      shop_en: localStorage.getItem("shopName"),
      businessType: "",
      phoneNumber: "" || branchDetails?.data?.phoneNumber?.phoneNumber,
      email: "" || branchDetails?.data?.email,
      website: "" || branchDetails?.data?.website,
      openingTime: "" || branchDetails?.data?.openingTime,
      closingTime: "" || branchDetails?.data?.closingTime,
      country_en: "Japan",
      city_en:
        "" ||
        props?.shopDetailsData?.data?.address?.city_en ||
        branchDetails?.data?.address?.city_en,
      town_en:
        "" ||
        props?.shopDetailsData?.data?.address?.town_en ||
        branchDetails?.data?.address?.town_en,
      prefecture_en:
        "" ||
        props?.shopDetailsData?.data?.address?.prefecture_en ||
        branchDetails?.data?.address?.prefecture_en,
      category: "",
      subCategory: "",
      phoneType: "+81",
      postalcode_en: "" || props?.shopDetailsData?.data?.address?.postalcode_en,
      postalcode_end:
        "" || props?.shopDetailsData?.data?.address?.postalcode_en?.slice(3, 7),
      latitude: location.lat,
      longitude: location.lng,
    },

    enableReinitialize: true,
    validationSchema: validationSchemaAddBranch,
    onSubmit: async (values: any) => {
      const finalData: any = {
        ...values,
        postalcode_en: `${values.postalcode_en}${values.postalcode_end}`,
        businessLicense: businessLicenseImage,
        shopProfileImage: shopProfileImage,
        flyers: flyersImage,
        signBoards: signBoardsImage,
        businessCard: businessCardImage,
        shopsPhotographs: shopsPhotographsImage,
        shopId: router?.query?.branchId
          ? router?.query?.branchId
          : router.query?.shopId,
        primaryColor: colorPickerValue,
        identityStatusId: branchDetails?.data?.identityStatus[0]?.id,
        addressId: branchDetails?.data?.address?.id,
        phoneNumberId: branchDetails?.data?.phoneNumber?.id,
        shopImageId: branchDetails?.data?.shopImage?.id,
      };
      let categoryId = [...formState?.category];
      let subcategoryId = [...formState?.subCategory];

      let fd = new FormData();
      //   fd.append("categoryId", categoryId);
      //   fd.append("subcategoryId", subcategoryId);
      Object.keys(finalData).map((item: any) => {
        if (
          item === "flyers" ||
          item === "signBoards" ||
          item === "businessCard" ||
          item === "shopsPhotographs" ||
          item === "businessLicense" ||
          item === "shopProfileImage"
        ) {
          finalData[item].map((i: any) => {
            fd.append(item, i);
          });
        } else {
          fd.append(item, finalData[item] ? finalData[item] : null);
        }
      });
      const cat_id = [...categoryId];
      const cat_sub_id = [...subcategoryId];

      for (let i = 0; i < cat_id.length; i++) {
        fd.append(`categoryId[${[i]}]`, cat_id[i]);
      }
      for (let i = 0; i < cat_sub_id.length; i++) {
        fd.append(`subcategoryId[${[i]}]`, cat_sub_id[i]);
      }

      for (let i = 0; i < shopOnCategoryId.length; i++) {
        fd.append(`shopOnCategoryId[${[i]}]`, shopOnCategoryId[i]);
      }

      for (let i = 0; i < shopOnSubCategoryId.length; i++) {
        fd.append(`shopOnSubCategoryId[${[i]}]`, shopOnSubCategoryId[i]);
      }

      for (let i = 0; i < addNumber?.length; i++) {
        fd.append(
          `secondaryPhoneNumber[${[i]}]`,
          addNumber[i]?.addNumber ? addNumber[i]?.addNumber : ""
        );
      }

      let reqData = {
        data: fd,
      };

      {
        updateState
          ? UpdateBranch({ id: branchDetails?.data?.id, body: fd })
            .then((res: any) => {
              toast.success(res?.data?.message);
              router.push("/shop");
              return;
            })
            .catch((error: { data: { message: any } }) => {
              const ermsg = error?.data?.message;
              toast.error(ermsg);
            })
          : ValueUpdate(reqData)
            .unwrap()
            .catch((error: { data: { message: any } }) => {
              const ermsg = error?.data?.message;
              toast.error(ermsg);
            });
      }
    },
  });
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  const onChangeImage = (e: any, fieldValue?: any) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than 2 MB", {
              type: "error",
            });
          } else {
            {
              fieldValue === "shopProfileImage" &&
                setShopProfileImage([e.target.files[0]]);
            }
            {
              fieldValue === "businessLicense" &&
                setBusinessLicenseImage([e.target.files[0]]);
            }
            {
              fieldValue === "signBoards" &&
                setSignBoardsImage([e.target.files[0]]);
            }
            {
              fieldValue === "flyers" && setFlyersImage([e.target.files[0]]);
            }
            {
              fieldValue === "businessCard" &&
                setBusinessCardImage([e.target.files[0]]);
            }
            {
              fieldValue === "shopsPhotographs" &&
                setShopsPhotographsImage([e.target.files[0]]);
            }
          }
        };
      };
    }
  };

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: any,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    setFieldValue?: any,
    values?: any,
    options?: any,
    twoInput?: boolean,
    getFieldProps2?: any,
    getFieldPropsValue2?: string,
    errors1?: string,
    touched2?: any,
    required?: boolean
  ) => {
    return (
      <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
        <div>
          {fieldName}
          {required ? <span style={{ color: "red" }}>*</span> : ""}
        </div>
        <div
          style={{ width: "50%" }}
          className={twoInput ? "display-flex-end" : ""}
        >
          <InputComponent
            type={type}
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            errors={errors}
            touched={touched}
            disabled={disabled}
            setFieldValue={setFieldValue}
            values={values}
            options={options}
            label={type === "postalCode" && "grgrt"}
            showTitle={
              type === "postalCode" ||
              (type === "secondaryPhoneNumber" && "grgrt")
            }
            addNumber={addNumber}
            setAddNumber={setAddNumber}
          />
          {twoInput && (
            <InputComponent
              type={type}
              getFieldProps={getFieldProps2}
              getFieldPropsValue={getFieldPropsValue2}
              errors={errors1}
              touched={touched2}
              disabled={disabled}
            />
          )}
        </div>
      </div>
    );
  };
  const fieldFormsFile = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string
  ) => {
    return (
      <>
        <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
          <div>{fieldName}</div>
          <InputComponent
            type="file"
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            onChange={(e: any) => onChangeImage(e, getFieldPropsValue)}
            defaultValue={signBoardsImage}
            accept=".png, .jpg, .jpeg"
            style={{ width: "50%" }}
          />
        </div>
      </>
    );
  };

  const displayForms = () => {
    return (
      <div style={{ marginTop: "30px", padding: "10px" }}>
        <FormikProvider value={formik}>
          <form onSubmit={handleSubmit}>
            <div
              className="add-branch-form-header"
              style={{ fontSize: "32px" }}
            >
              Add Branch Details
            </div>
            <div>
              <div>
                {fieldFormsText("Shop Name :", "shop_en", "text", "", "", true)}
                {fieldFormsText(
                  "Branch Name :",
                  "name_en",
                  "text",
                  errors.name_en,
                  touched.name_en,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}

                <div
                  className="display-flex-sb"
                  style={{ marginBottom: "5px" }}
                >
                  <div>{"Branch category"}</div>
                  <div style={{ width: "50%" }} className={"display-flex-end"}>
                    <TextField
                      {...getFieldProps("category")}
                      size="small"
                      fullWidth
                      select
                      SelectProps={{
                        multiple: true,
                        value: formState?.category,
                        onChange: handleFieldChange,
                      }}
                    >
                      {category?.map((option: any) => (
                        <MenuItem key={option?.value} value={option?.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </div>
                </div>
                <div
                  className="display-flex-sb"
                  style={{ marginBottom: "5px" }}
                >
                  <div>{"Branch sub-category"}</div>
                  <div style={{ width: "50%" }} className={"display-flex-end"}>
                    <TextField
                      {...getFieldProps("subCategory")}
                      size="small"
                      fullWidth
                      select
                      SelectProps={{
                        multiple: true,
                        value: formState?.subCategory,
                        onChange: handleFieldChange,
                      }}
                    >
                      {subCategory?.map((option: any) => (
                        <MenuItem
                          key={option?.id}
                          id={option?.value}
                          value={option?.value}
                        >
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </div>
                </div>
                <div
                  className="display-flex-sb"
                  style={{ marginTop: "12px", marginBottom: "12px" }}
                >
                  <div>Choose Your Shop Color:</div>
                  <div>
                    <input
                      type="color"
                      id="favcolor"
                      name="favcolor"
                      value={colorPickerValue}
                      onChange={(e: any) => {
                        setColorPickerValue(e.target.value);
                      }}
                    />
                  </div>
                </div>
                {fieldFormsFile(
                  "Select your shop profile",
                  "shopProfileImage"
                )}
                <div style={{ marginBottom: "20px" }}>
                  {fieldFormsText(
                    "Country :",
                    "country_en",
                    "text",
                    errors.country_en,
                    touched.country_en,
                    true
                  )}
                  {fieldFormsText(
                    "Postal Code:",
                    "postalcode_en",
                    "postalCode",
                    "",
                    "",
                    false,
                    setFieldValue,
                    "postalcode_en",
                    "",
                    false,
                    "",
                    "",
                    "",
                    "",
                    true
                  )}
                </div>

                {fieldFormsText(
                  "City/Village :",
                  "city_en",
                  "text",
                  errors.city_en,
                  touched.city_en,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}
                {fieldFormsText(
                  " Town / Ward:",
                  "town_en",
                  "text",
                  errors.town_en,
                  touched.town_en,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}

                {fieldFormsText(
                  "Prefecture:",
                  "prefecture_en",
                  "text",
                  errors.prefecture_en,
                  touched.prefecture_en,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}

                {fieldFormsText(
                  "Contact number :",
                  "phoneNumber",
                  "number",
                  errors.phoneNumber,
                  touched.phoneNumber,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}
                {fieldFormsText(
                  " Optional Number :",
                  "secondaryPhoneNumber",
                  "secondaryPhoneNumber"
                )}
                {fieldFormsText(
                  "Branch Email :",
                  "email",
                  "text",
                  errors.email,
                  touched.email,
                  false,
                  "",
                  "",
                  "",
                  false,
                  "",
                  "",
                  "",
                  "",
                  true
                )}
                {fieldFormsText(
                  "Website :",
                  "website",
                  "text",
                  errors.website,
                  touched.website,
                  false
                )}
                {fieldFormsText(
                  "Opening time & Closing time :",
                  "openingTime",
                  "time",
                  errors.closingTime,
                  touched.closingTime,
                  false,
                  "",
                  "",
                  "",
                  true,
                  getFieldProps,
                  "closingTime",
                  "",
                  "",
                  true
                )}
              </div>
            </div>

            {/* Add GoogleMap Location */}
            <Box>
              <Typography variant="body1" marginBottom={"5px"}>
                Select Location
              </Typography>
              <MapComponent
                location={location}
                zoom={12}
                setLocation={setLocation}
              />
            </Box>

            {fieldFormsFile("Business License", "businessLicense")}
            {fieldFormsFile("Sign Boards", "signBoards")}
            {fieldFormsFile("Flyers", "flyers")}
            {fieldFormsFile("Business cards", "businessCard")}
            {fieldFormsFile("Shop Photographs", "shopsPhotographs")}

            <div style={{ marginTop: "30px" }}>
              <Box sx={{ display: "flex", justifyContent: "end", pt: 2 }}>
                <>
                  {updateState ? (
                    <Button
                      variant="contained"
                      style={{ height: "35px", borderRadius: "9px" }}
                      type="submit"
                    >
                      Update
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      style={{ height: "35px", borderRadius: "9px" }}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                </>
              </Box>
            </div>
          </form>
        </FormikProvider>
      </div>
    );
  };
  useEffect(() => {
    if (!isEmpty(data)) {
      toast.success("Successfully added Branch");
      router.push("/shop");
    }
  }, [data]);

  useEffect(() => {
    if (!!props?.shopDetailsData?.data) {
      setFormState((prev) => ({
        ...prev,
        category:
          props?.shopDetailsData?.data?.Category?.map(
            (data: any) => data?.category?.id
          ) || [],
        subCategory:
          props?.shopDetailsData?.data?.SubCategory?.map(
            (data: any) => data?.subCategoryId
          ) || [],
      }));

      localStorage.setItem(
        "postalcodeValue",
        props?.shopDetailsData?.data?.address?.postalcode_en
          ?.trim()
          ?.slice(0, 3)
      );
      setFieldValue(
        "postalcode_en",
        props?.shopDetailsData?.data?.address?.postalcode_en?.trim().slice(0, 3)
      );
      setFieldValue(
        "postalcode_end",
        props?.shopDetailsData?.data?.address?.postalcode_en?.trim().slice(3, 7)
      );
    }
  }, [props?.shopDetailsData]);

  return <Box className="box-kyc-multiple-step-form">{displayForms()}</Box>;
};
