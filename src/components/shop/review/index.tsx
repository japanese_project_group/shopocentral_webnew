import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import StarIcon from "@mui/icons-material/Star";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Rating,
  Typography,
} from "@mui/material";
import LinearProgress, {
  LinearProgressProps,
} from "@mui/material/LinearProgress";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useGetReviewDataQuery } from "../../../../redux/api/user/user";

export const ShopReviewComponent = ({ data }: any) => {
  const router = useRouter();
  const [value, setValue] = React.useState<number | null>(4);
  const [ID, setID] = useState<any>();
  const [shopData, setShopData] = useState<any>([]);
  const { data: shopDetails, isSuccess, refetch } = useGetReviewDataQuery();

  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const options: Intl.DateTimeFormatOptions = { day: '2-digit', month: 'short', year: 'numeric' };
    return date.toLocaleDateString('en-US', options);
  };

  console.log("shopDetails", shopDetails, data);
  const ratingData = ["1", "2", "3", "4", "5"];

  function LinearProgressWithLabel(
    props: LinearProgressProps & { value: number, ratingCount: string }
  ) {
    return (
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ width: "100%", mr: 1, border: "1px solid #ffba49" }}>
          <LinearProgress variant="determinate" {...props} />
        </Box>
        <Box sx={{ minWidth: 35 }}>
          <Typography variant="body2" color="text.secondary">{props?.ratingCount}</Typography>
        </Box>
      </Box>
    );
  }
  return (
    <>
      {/* <Box className="display-flex" paddingBottom="4rem">
        <Link href="/shop">
          <ArrowBackIcon
            style={{
              height: "2rem",
              width: "2rem",
              marginTop: "6px",
              cursor: "pointer",
            }}
          />
        </Link>
        <div className="shop-body-search-form-header">Review</div>
      </Box> */}
      <Box
        display="flex"
        flexDirection="column"
        gap="1.5rem"
        marginBottom={'1rem'}
      >
        <Box
          display={"flex"}
          justifyContent={"space-around"}
          sx={{ alignItems: "center", padding: "5rem 10%" }}
        >
          <Box>
            <Typography
              variant="h2"
              sx={{ fontWeight: "900", marginButton: "20px" }}
            >
              {data?.avg}
              <StarIcon sx={{ color: "#ffba49", fontSize: "3rem" }} />
            </Typography>
            <Typography variant="subtitle2" fontWeight={"600"}>
              {data?.totalCount} rating & reviews
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column-reverse",
              gap: "24px",
            }}
          >
            {ratingData.map((data, index) => (
              <Box display={"flex"} flexDirection={"row"} gap={"24px"}>
                <Rating
                  name="simple-controlled"
                  value={++index}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
                <LinearProgressWithLabel value={parseInt(data) * 20} ratingCount={data} />
              </Box>
            ))}
          </Box>
        </Box>
        {data.data.map((ratingData: any) => (
          <Card
            sx={{
              boxShadow: "0px 2px 14px 0px #00000054",
            }}
            className="rating_card"
          >
            <CardHeader
              avatar={
                <Avatar src={""} sx={{ bgcolor: "red" }} aria-label="recipe">
                  <AccountCircleOutlinedIcon sx={{ fontSize: "2rem" }} />
                </Avatar>
              }
              action={<Typography variant="subtitle2">{formatDate(ratingData?.updatedAt)}</Typography>}
              title={ratingData?.user_name ?? 'Unknown'}
              subheader={
                <Rating
                  name="simple-controlled"
                  value={ratingData?.stars}
                  // onChange={(event, newValue) => {
                  //   setValue(newValue);
                  // }}
                  sx={{ fontSize: "1rem" }}
                />
              }
            />
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {ratingData?.description_en}
              </Typography>
            </CardContent>
          </Card>
        ))}
      </Box>
    </>
  );
};
