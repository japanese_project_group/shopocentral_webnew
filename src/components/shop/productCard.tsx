import {
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Typography
} from "@mui/material";
import { RatingComponent } from "../../common/Components";

interface ProductCardType {
    image?: any;
    name?: string;
    brandName?: any;
    width?: string | number;
    ratingValue?: number;
    headerVariant?: string | any;
    imageType?: string;
    className?: string;
    price?: string;
    estimatedPrice: string;
    status?: string;
    background?: string;
}

const ProductCard = ({
    image,
    name,
    brandName,
    width,
    ratingValue,
    headerVariant,
    price,
    imageType,
    className,
    estimatedPrice,
    status,
    background,
}: ProductCardType) => {
    return (
        <Card
            className={"shop-body-card " + className ? className : ""}
            style={{ width: width, border: "none", boxShadow: "2px 2px 3px #00000020" }}
        >
            <CardMedia
                component="img"
                alt="green iguana"
                className={
                    imageType == "big"
                        ? "shop-body-card-image-big"
                        : "shop-body-card-image-small"
                }
                image={
                    !!image
                        ? image
                        : "https://img.freepik.com/free-vector/shop-with-sign-we-are-open_52683-38687.jpg"
                }
                style={{ padding: "10px" }}
            />

            <CardContent style={{ paddingTop: "2px" }}>
                <RatingComponent value={3} />
                {/* <Chip
                    label={status ? status : "status"}
                    style={{
                        height: "21px",
                        borderRadius: "3px",
                        backgroundColor: `${background !== "VERIFIED" ? "#ff645c" : "#90dd9a"
                            }`,
                    }}
                /> */}


                {brandName ? (
                    <Typography
                        variant="h6"
                        color="text.secondary"
                        style={{ fontSize: "16px", textTransform: "capitalize" }}
                    >
                        {brandName}
                    </Typography>
                )
                    : ("Brand Name")}
                <p className="no-padding-margin">{name ? name : "Name"}</p>

            </CardContent>
            {price &&
                <CardActions style={{ marginTop: "-15px", marginLeft: "5px", display: "flex", gap: "1rem", padding: "0 12px 12px 12px" }} >
                    {estimatedPrice &&
                        <del style={{ color: "#999" }}>{estimatedPrice}</del>
                    }
                    <Typography variant={"body1"} color={"#555"} fontWeight={"600"}>{price}</Typography>
                </CardActions>
            }
        </Card>
    );
};

export default ProductCard


