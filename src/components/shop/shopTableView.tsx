import {
  Chip,
  Collapse,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useState } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import moment from "moment";
import { useRouter } from "next/router";
import Image from "next/image";
import ViewIcon from "../../../public/viewIcon.svg";
import EditIcon from "../../../public/editIcon.svg";
import { dateSorting } from "../../common/commonFunc";

interface shopTableViewTypes {
  data?: any;
}
export const ShopTableView = ({ data }: shopTableViewTypes) => {
  const router = useRouter();
  function createData(
    shopId: string,
    id: number,
    shopImage: string,
    name: string,
    category: any,
    address: string,
    phoneNumber: any,
    email: string,
    registeredDate?: string,
    handlingService?: any,
    branchData?: any
  ) {
    return {
      shopId,
      id,
      shopImage,
      name,
      category,
      address,
      phoneNumber,
      email,
      registeredDate,
      handlingService,
      branchData,
    };
  }
  function handleViewShop(i: string) {
    router.push({
      pathname: "/shop/details",
      query: { id: i },
    });
  }
  function handleEditShop(i: string) {
    router.push({
      pathname: "/shop/update-shop",
      query: { shopId: i },
    });
  }

  const myData = [...data];
  const rows = myData
    ?.sort(dateSorting)
    ?.map((data: any, index: number) =>
      createData(
        data?.id,
        index + 1,
        data?.shopImage?.shopProfileImage,
        data?.name_en,
        data?.Category,
        `${data?.address?.postalcode_en?.trim()?.slice(0, 7)},${
          data?.address?.town_en
        },${data?.address?.city_en}`,
        data?.phoneNumber,
        data?.email,
        data?.createdAt,
        data?.handlingService,
        data?.shopBranch
      )
    );
  function Row(props: { row: ReturnType<typeof createData> }) {
    const { row } = props;
    const [open, setOpen] = useState<boolean>(false);
    return (
      <>
        <TableRow
          sx={{ "& > *": { borderBottom: "unset" } }}
          className="shop-body-table-css"
        >
          <TableCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {row.id}
          </TableCell>
          <TableCell align="right">
            <img src={row?.shopImage} className="table-image-css" />
          </TableCell>
          <TableCell align="right">{row.name}</TableCell>
          <TableCell align="right">
            <div style={{ alignItems: "center" }}>
              {row.category?.slice(0, 3)?.map((data: any) => (
                <div className="display-flex-jc-center ">
                  <Chip label={data?.category?.name_en} color="success" />
                </div>
              ))}
            </div>
            {row?.category?.length > 3 && (
              <span
                className="see-more-css display-flex-jc-center "
                onClick={() => {
                  handleViewShop(row?.shopId);
                }}
              >
                See More
              </span>
            )}
          </TableCell>
          <TableCell align="right">{row.address}</TableCell>
          <TableCell align="right">{`${row.phoneNumber?.phoneType}${row.phoneNumber?.phoneNumber}`}</TableCell>
          <TableCell align="right">{row.email}</TableCell>
          <TableCell align="right">
            {moment(row.registeredDate).format("YYYY/MM/DD")}
          </TableCell>
          <TableCell align="right">
            <div style={{ alignItems: "center" }}>
              {row.handlingService?.slice(0, 3)?.map((data: any) => (
                <div className="display-flex-jc-center ">
                  <Chip label={data} color="success" />
                </div>
              ))}
            </div>
            {row?.handlingService?.length > 3 && (
              <span
                className="see-more-css display-flex-jc-center "
                onClick={() => {
                  handleViewShop(row?.shopId);
                }}
              >
                See More
              </span>
            )}
          </TableCell>
          <TableCell align="right" sx={{ padding: "12px" }}>
            <div className="display-flex-sb">
              <Image
                src={ViewIcon}
                onClick={() => {
                  handleViewShop(row?.shopId);
                }}
                style={{ marginLeft: "10px" }}
              />
              <Image
                src={EditIcon}
                onClick={() => {
                  handleEditShop(row?.shopId);
                }}
                style={{ marginTop: "20px" }}
                width={20}
              />
            </div>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Branch
                </Typography>
                {row?.branchData?.length > 0 ? (
                  <Table size="small" aria-label="purchases">
                    <TableHead>
                      <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell>Image</TableCell>
                        <TableCell>Branch Name</TableCell>
                        <TableCell>Branch Category</TableCell>
                        <TableCell>Branch Address</TableCell>
                        <TableCell align="right">Phone Number</TableCell>
                        <TableCell align="right">Branch Email</TableCell>
                        <TableCell>Action</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {row?.branchData?.map((branch: any, index: number) => (
                        <TableRow key={index}>
                          <TableCell component="th" scope="row">
                            {index}
                          </TableCell>
                          <TableCell>{"image here"}</TableCell>
                          <TableCell>{branch.name_en}</TableCell>
                          <TableCell align="right">
                            <div style={{ alignItems: "center" }}>
                              {branch.Category?.slice(0, 3)?.map(
                                (data: any) => (
                                  <div className="display-flex-jc-center ">
                                    <Chip
                                      label={data?.category?.name_en}
                                      color="success"
                                    />
                                  </div>
                                )
                              )}
                            </div>
                            {branch?.Category?.length > 3 && (
                              <span
                                className="see-more-css display-flex-jc-center "
                                onClick={() => {
                                  handleViewShop(branch?.shopId);
                                }}
                              >
                                See More
                              </span>
                            )}
                          </TableCell>
                          <TableCell align="right">
                            {branch?.address?.postalcode_en
                              ?.trim()
                              ?.slice(0, 7)}
                            ,{branch?.address?.town_en},
                            {branch?.address?.city_en}
                          </TableCell>
                          <TableCell align="right">
                            {branch.phoneNumber?.phoneType}-
                            {branch.phoneNumber?.phoneNumber}
                          </TableCell>
                          <TableCell align="right">{branch.email}</TableCell>
                          <TableCell align="right" sx={{ padding: "12px" }}>
                            <div className="display-flex-sb">
                              <Image
                                src={ViewIcon}
                                style={{ marginLeft: "10px" }}
                              />
                              <Image
                                src={EditIcon}
                                style={{ marginTop: "20px" }}
                                width={20}
                              />
                            </div>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                ) : (
                  <div>No Branches Added</div>
                )}
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </>
    );
  }
  return (
    <>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell className="shop-table-header-css" />
              <TableCell className="shop-table-header-css">SHOPO ID</TableCell>
              <TableCell align="right" className="shop-table-header-css">
                IMAGE
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                SHOP NAME
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                SHOP CATEGORY
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                ADDRESS
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                PHONE NUMBER
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                EMAIL
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                REGISTERED DATE
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                HANDLING SERVICE
              </TableCell>
              <TableCell align="right" className="shop-table-header-css">
                ACTION
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row: any) => (
              <Row key={row.name} row={row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
