import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {
  Button
} from "@mui/material";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import {
  DisplayCard
} from "../../common/Components";

export const ViewShopData = (props: any) => {
  const router = useRouter();
  function handleViewShop(i: string) {
    router.push({
      pathname: "/shop/details",
      query: { id: i },
    });
  }
  console.log("Props", props);

  const handleAddBranch = () => {
    router.push({
      pathname: "/shop/add-branch",
      query: { shopId: props?.id },
    });
    localStorage.setItem("shopName", props?.name_en);
  };
  const goToBranchDetails = (id: any) => {
    router.push({
      pathname: "/shop/branch/details",
      query: {
        id: id,
      },
    });
  };
  return (
    <div
      style={{ display: "flex", alignItems: "end", marginBottom: "60px", padding: "20px" }}
      className="shop-body-shop"
    >
      <div
        className="shop-body-shop-section"
        onClick={() => {
          handleViewShop(props?.id);
        }}
        style={{
          paddingLeft: "10px",
          paddingRight: "10px",
          boxShadow: "rgba(0, 0, 0, 0.2) 0px 5px 15px",
          cursor: "pointer",
          borderRadius: "7px",
        }}
      >
        <DisplayCard
          width="200px"
          headerVariant="h5"
          imageType="big"
          name={props?.name_en}
          status={props?.identityStatus?.[0]?.status}
          image={props?.shopImage?.shopProfileImage}
          categoryName={props?.Category}
          background={props?.identityStatus[0].status}
        />
      </div>
      <div className="shop-body-branch-section" style={{ marginBottom: "-10px", height: "100%" }}>
        <div
          style={{ marginRight: "5px", display: "flex", justifyContent: "end" }}
        >
          <Button
            variant="contained"
            color="primary"
            className="shop-body-add-button-branch"
            onClick={handleAddBranch}
          // disabled={
          //   props?.identityStatus?.[0]?.status !== "VERIFIED" ? true : false
          // }
          >
            Add Branch{" "}
            <AddCircleOutlineIcon className="shop-body-add-icon-branch" />
          </Button>
        </div>
        <div className="shop-body-branch-section" style={{ display: "flex" }}>
          {!isEmpty(props?.shopBranch)
            ? props?.shopBranch?.map((data: any) => (
              <div
                key={data?.id}
                onClick={() => goToBranchDetails(data?.id)}
                style={{
                  cursor: "pointer",
                  margin: "10px",
                  boxShadow: "2px 3px 15px #00000020",
                  borderRadius: "5px",
                }}

              >
                <DisplayCard
                  headerVariant="p"
                  imageType="small"
                  width="150px"
                  className="display-card-branch"
                  name={data?.name_en}
                  categoryName={data?.Category}
                  status={data?.identityStatus[0]?.status}
                  image={data?.shopImage?.shopProfileImage}
                />
              </div>
            ))
            : ""}
        </div>
      </div>
    </div>
  );
};
