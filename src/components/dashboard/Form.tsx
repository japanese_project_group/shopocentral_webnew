// MUI Components
import { Box, Button, InputLabel, TextField, Typography } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';

// Third party library
import { Formik } from 'formik';

// Build in library
import React from 'react';

const Form = () => {
  function displaydata(values: any) {
  }
  const [age, setAge] = React.useState('');

  const handleChange = (event: SelectChangeEvent) => {
    setAge(event.target.value as string);
  };

  const [value, setValue] = React.useState<Date | null>(null);

  return (
    <>
      <Box
        sx={{
          my: 10,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}
      >
        <Box
          sx={{
            mb: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography
            variant="h3"
            color="primary"
            component="div"
            align="center"
          ></Typography>

          <Typography
            variant="subtitle1"
            sx={{ marginTop: '1rem' }}
            component="div"
            align="center"
          ></Typography>
        </Box>
        <Box
          sx={{
            mt: 1,
            width: { sm: '450px', xs: '100%' },
          }}
        >
          <Formik
            initialValues={{
              email: '',
              age: '',
              date: '',
              file: '',
              startDate: '',
            }}
            onSubmit={async (values) => {
              await displaydata(values);
            }}
          >
            {(props: any) => {
              const {
                values,
                touched,
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue,
                getFieldProps,
              } = props;
              return (
                <form onSubmit={handleSubmit}>
                  <Box mb={4}>
                    <InputLabel shrink htmlFor="bootstrap-input"></InputLabel>
                    email
                    <TextField
                      fullWidth
                      variant="outlined"
                      name="email"
                      id="email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={touched.email && Boolean(errors.email)}
                    />
                  </Box>
                  <Box sx={{ minWidth: 120 }}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label">Age</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="age"
                        name="age"
                        value={values.age}
                        label="Age"
                        onChange={handleChange}
                      >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Box>
                  <Box style={{ margin: '2rem 0 1rem 0' }}></Box>
                  <Box>
                    <input
                      id="file"
                      name="file"
                      type="file"
                      onChange={(e: any) =>
                        setFieldValue('frontpic', e.currentTarget.files[0])
                      }
                    />
                  </Box>
                  <TextField
                    size="small"
                    type="date"
                    {...getFieldProps('startDate')}
                    error={Boolean(touched.startDate && errors.startDate)}
                    helperText={Boolean(touched.startDate && errors.startDate)}
                  />
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    style={{ margin: '2rem 0 1rem 0' }}
                  >
                    <Button
                      type="submit"
                      fullWidth
                      size="large"
                      variant="contained"
                      color="primary"
                    >
                      login
                    </Button>
                  </Box>
                </form>
              );
            }}
          </Formik>
        </Box>
      </Box>
    </>
  );
};

export default Form;
