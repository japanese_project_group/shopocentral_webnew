import CalendarTodayOutlinedIcon from '@mui/icons-material/CalendarTodayOutlined';
import { Avatar, AvatarGroup, Box, Card, CardContent, IconButton, Typography, styled } from '@mui/material';
import cardIlluImg from '../../../public/landingPageGirl.png';
const InfoCard = () => {

    const ContentBox = styled('div')(() => ({
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        gap: '0.8rem'
    }));

    return (
        <Card sx={{ width: "100%", boxShadow: '0px 4px 24px 0px #0000000F' }}>
            <Box sx={{ aspectRatio: '11/5', width: '100%' }}>
                <img
                    src={cardIlluImg.src}
                    alt='image'
                    style={{ width: '100%', height: '100%', objectFit: 'cover' }}
                />
            </Box>
            <CardContent sx={{ diaplay: 'flex !important', flexDirection: 'column', gap: '21px', paddingBottom: '3rem !important' }} >
                <ContentBox>
                    <Box>
                        <Typography color={'#232323'} fontSize={"14px"}>THU</Typography>
                        <Typography color={'#232323'} fontSize={"21px"}>24</Typography>
                    </Box>
                    <div className="verticalLine"></div>
                    <Box display={'flex'} flexDirection={'column'} gap={'10px'}>
                        <Typography color={'#1A76D2'} fontSize={"18px"}>New Package</Typography>
                        <Typography color={'#777'} fontSize={"14px"}>Changes in pricing package</Typography>
                    </Box>
                </ContentBox>
                <ContentBox sx={{ margin: "1.5rem 0" }}>
                    <IconButton sx={{ bgcolor: "#1A76D230", color: "#1A76D2" }}>
                        <CalendarTodayOutlinedIcon />
                    </IconButton>
                    <Box display={'flex'} flexDirection={'column'} gap={'10px'}>
                        <Typography color={'#777'} fontSize={"14px"}>Sat, May 25, 2020</Typography>
                        <Typography color={'#777'} fontSize={"12px"}>10AM to 6 PM</Typography>
                    </Box>
                </ContentBox>
                <AvatarGroup
                    renderSurplus={(surplus) => <span>+42</span>}
                    total={4245}
                >
                    <Avatar alt="Remy Sharp" src="../../../public/user.png" />
                    <Avatar alt="Travis Howard" src="../../../public/user.png" />
                    <Avatar alt="Agnes Walker" src="../../../public/user.png" />
                    <Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg" />
                </AvatarGroup>
            </CardContent>
        </Card>
    );
};

export default InfoCard;