// Build in library
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

// Route
import withAuthProtected from '../../../hoc/PrivateRoute';

// Redux
import { useGetAllDataByIdQuery } from '../../../redux/api/user/user';
import { setProfile } from '../../../redux/features/profileSlice';

// Components
import { Box, Card, CircularProgress, Grid, Typography } from '@mui/material';
import '../../constant/language/i18n';
import CalendarComponent from './Calender';
import DoughnutChart from './DoughnutChart';
import StatCards from './StatCard';
import StatusTable from './StatusTable';

// constant
import KYCPending from '@/src/common/KYCPending';
import { businessAccessId } from '../../common/utility/Constants';
import InfoCard from './InfoCard';

const DashBoardComponent = () => {
  const [isBusinessKyc, setBusinessKyc] = useState(false);

  const dispatch = useDispatch();

  const { data: allDataProfile, isSuccess: isSuccessGetProfile, refetch, isLoading } =
    useGetAllDataByIdQuery(businessAccessId);

  const profileStatus = allDataProfile?.data?.identityStatus?.[0]?.status;

  useEffect(() => {
    refetch();
  }, [profileStatus])

  useEffect(() => {
    if (allDataProfile && isSuccessGetProfile) {
      dispatch(setProfile(allDataProfile?.data));

      if (profileStatus === 'FINITIAL') {
        setBusinessKyc(true);
      }
    }
  }, [allDataProfile]);

  const primary = "#28C76F";
  const lightPrimary = "#28C76F40"
  const mediumPrimary = "#28C76F60"

  console.log("isBusinessKyc", isBusinessKyc);

  return (
    <>
      {isLoading ? (
        <Box display="flex" justifyContent="center" alignItems="center" height="200px">
          <CircularProgress />
        </Box>
      ) : (
        <>
          {profileStatus === 'VERIFIED' ? (
            <Box boxShadow={" -12px -12px 20px 0px #FFFFFFCC"}
              borderRadius={'20px'}
              padding={'20px 2rem 2.5rem 2rem'}
              bgcolor={"#fff"}
            >
              <Grid container spacing={5}>
                <Grid item lg={8} md={8} sm={12} xs={12} spacing={5}>
                  <StatCards />
                  <Card
                    sx={{
                      px: { xs: 2, sm: 4, md: 6 },
                      py: { xs: 2, sm: 3, md: 4 },
                      mb: 6,
                      width: "100%",
                      display: "flex",
                      flexDirection: { xs: "column", lg: "row" },
                      justifyContent: { xs: "center", lg: "space-between" },
                      gap: '1rem',
                      alignItems: 'flex-start',
                      boxShadow: "0px 4px 24px 0px #0000000F",
                    }}
                  >
                    <Box flex="1">
                      <Typography variant="h6" marginBottom={2} color="#232332">
                        Users
                      </Typography>
                      <Typography variant="body2" color="#777">
                        This Month
                      </Typography>
                      <Typography variant="h6" marginBottom={2} color="#232332">
                        2.2K
                      </Typography>
                      <Typography variant="body2" color="#777">
                        <strong style={{ color: "#222222" }}>68.2% </strong>
                        more earning than last month
                      </Typography>
                    </Box>
                    <Box flex="1" minWidth={0} width="100%" maxWidth={300}>
                      <DoughnutChart height={"150px"} color={[primary, mediumPrimary, lightPrimary]} />
                    </Box>
                  </Card>
                  <StatusTable />
                </Grid>

                <Grid item lg={4} md={4} sm={12} xs={12}>
                  <Card sx={{ px: 3, py: 2, mb: 10, boxShadow: "0px 4px 24px 0px #0000000F" }} >
                    <CalendarComponent />
                  </Card>
                  <InfoCard />
                </Grid>
              </Grid>
            </Box>
          ) : (
            <>
              <KYCPending profileStatus={profileStatus} allProfileData={allDataProfile} />
              {/* <CustomizedDialogs /> */}
            </>
          )}
        </>
      )}
    </>
  );
};

export default withAuthProtected(DashBoardComponent);
