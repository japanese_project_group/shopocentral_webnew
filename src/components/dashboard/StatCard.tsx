import { Box, Card, Grid, Icon, Typography, styled } from '@mui/material';
// import { Small } from 'app/components/Typography';
import PaidOutlinedIcon from '@mui/icons-material/PaidOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import Image from 'next/image';
import masterProductTab from "../../../public/masterProducttab.svg";

const StyledCard = styled(Card)(({ theme }) => ({
    padding: '24px !important',
    boxShadow: "0px 4px 24px 0px #0000000F",
    background: "#fff",
    [theme.breakpoints.down('sm')]: { padding: '16px !important' },
}));

const ContentBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    minWidth: `150px`,
    '& small': { color: theme.palette.text.secondary },
    '& .icon': { opacity: 0.6, fontSize: '44px', color: theme.palette.primary.main },
}));

const Heading = styled('h6')(({ theme }) => ({
    margin: 0,
    marginTop: '4px',
    fontSize: '14px',
    fontWeight: '500',
    color: theme.palette.primary.main,
}));

const StatCards = () => {
    const cardList = [
        { name: 'Sales', amount: "230K", icon: <TrendingUpIcon />, bgColor: "#EEEDFD" },
        { name: 'Customers', amount: '8.54K', icon: <PersonOutlineOutlinedIcon />, bgColor: "#E0F9FC" },
        { name: 'Products', amount: '1,432K', icon: <Image src={masterProductTab} style={{ opacity: "0.6" }} />, bgColor: "#EA54551F" },
        { name: 'Revenue', amount: '$9475', icon: <PaidOutlinedIcon />, bgColor: "#e5f8ee" },
    ];

    return (
        <StyledCard sx={{ mb: '24px' }}>
            <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'} marginBottom={'1.8rem'}>
                <Typography variant='h5' fontSize={"18px"} color={"#1A76D2"}>Statistics</Typography>
                <Typography variant='body1' fontSize={"12px"} color={"#777"}>Updated 1 month ago</Typography>
            </Box>
            <Box
                display='flex'
                flexWrap='wrap'
                alignItems='center'
                justifyContent='space-between'
                rowGap={'2rem'}
            >
                {cardList.map((item, index) => (
                    <Grid item xs={6} md={4} key={index} spacing={4}>
                        <ContentBox>
                            <Box style={{ backgroundColor: item.bgColor, display: "flex", justifyContent: 'center', alignItems: "center", borderRadius: '50%', height: "48px", width: "48px", paddingBottom: "5px" }}>
                                <Icon className="">{item.icon}</Icon>
                            </Box>
                            <Box ml="12px">
                                <Typography fontSize={"18px"} color={"#232323"}>{item.amount}</Typography>
                                <Typography fontSize={"12px"} color={"#777"} >{item.name}</Typography>
                            </Box>
                        </ContentBox>
                    </Grid>
                ))}
            </Box>
        </StyledCard >
    );
};

export default StatCards;