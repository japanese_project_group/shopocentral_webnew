
import {
    Avatar, Box, Card,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography,
    styled
} from '@mui/material';

const Paragraph = styled('span')(() => ({
    fontSize: '0.8rem',
    fontWeight: '400',
    textTransform: 'capitalize',
}));


const ProductTable = styled(Table)(() => ({
    minWidth: 400,
    whiteSpace: 'pre',
    '& small': {
        width: 50,
        height: 15,
        borderRadius: 500,
        boxShadow: '0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24)',
    },
    '& td': { borderBottom: 'none' },
    '& td:first-of-type': { paddingLeft: '16px !important' },
}));


const StatusTable = () => {
    return (
        <Card elevation={3} sx={{ pt: '20px', mb: 3, boxShadow: '0px 4px 24px 0px #0000000F' }}>
            <Box overflow="auto">
                <ProductTable>
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{ px: 3 }} colSpan={6}>
                                <Typography fontSize={'16px'} color={'#1A76D2'}>KYC REQUEST</Typography>
                            </TableCell>
                            <TableCell colSpan={2}>
                                <Box display={'flex'} alignItems={'center'}>
                                    <div className="smallCircle"></div>
                                    <Typography fontSize={'16px'} color={'#777'}>Pending</Typography>
                                </Box>
                            </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {productList.map((product, index) => (
                            <TableRow className='userStatusTable' key={index} hover>
                                <TableCell colSpan={6} align="left" sx={{ px: 0, textTransform: 'capitalize' }}>
                                    <Box display="flex" alignItems="center">
                                        <Avatar src={product.imgUrl} />
                                        <Paragraph sx={{ m: 0, ml: 6, color: "#000" }}>{product.name}</Paragraph>
                                    </Box>
                                </TableCell>

                                <TableCell align="left" colSpan={2} sx={{ px: 0, textTransform: 'capitalize' }}>
                                    <Paragraph sx={{ m: 0, color: "#777" }}>{product.status}</Paragraph>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </ProductTable>
            </Box>
        </Card>
    );
};

const productList = [
    {
        imgUrl: '../../../public/user.png',
        name: 'John Doe',
        status: '2 Days Ago',
    },
    {
        imgUrl: '../../../public/user.png',
        name: 'John Doe',
        status: '2 Days Ago',
    },
    {
        imgUrl: '../../../public/user.png',
        name: 'John Doe ',
        status: '2 Days Ago',
    },
    {
        imgUrl: '/assets/images/products/iphone-1.jpg',
        name: 'John Doe ',
        status: '2 Days Ago',
    },
    {
        imgUrl: '/assets/images/products/iphone-1.jpg',
        name: 'John Doe',
        status: '2 Days Ago',
    },
];

export default StatusTable;
