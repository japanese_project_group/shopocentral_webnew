import PageEmpty from "@/src/common/PageEmpty";
import { styleForModal } from "@/src/common/commonData";
import { businessId } from "@/src/common/utility/Constants";
import { Box, CircularProgress } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useGetHotDealProductQuery, useGetMyProductsQuery, useGetProductInventoryManagementQuery, usePostHotDealProductMutation, useUpdateHotDealProductMutation } from "../../../../redux/api/user/user";
import ProductCollapsibleTable from "../../../common/ProductCollapsibleTable";
import { DetailsTopView } from "../../customComponents/detailsTopView";

const FeaturedProduct = () => {
    const router = useRouter();
    const [submitType, setSubmitType] = useState<string>("Submit");
    const [brandDescriptionValue, setBrandDescriptionValue] = useState<string>("");
    const [brandValue, setBrandValue] = useState<string>("");
    const [productData, setProductData] = useState<any[]>()
    const [brandId, setBrandId] = useState<string>("");
    // const { data: myProductData, isLoading, refetch } = useGetMyProductsQuery();
    const { data: InventoryMgmtData } = useGetProductInventoryManagementQuery();
    const [AddBrand, { data: addBrandData }] = usePostHotDealProductMutation();
    const [UpdateBrand, { data: updateBrandData }] = useUpdateHotDealProductMutation();
    const { data: myProductData, refetch, isSuccess, isLoading } = useGetHotDealProductQuery(businessId);
    const { data: myProduct } = useGetMyProductsQuery();

    console.log("InventoryMgmtData", InventoryMgmtData, myProduct);
    const modalStyle = styleForModal;

    useEffect(() => {
        if (myProductData) {
            setProductData(myProductData)
        }
        refetch();
    }, [myProductData, refetch])

    // useEffect(() => {
    //     refetch({ page: currentPage, pageSize: currentPageSize });
    //   }, [currentPage, currentPageSize, refetch]);


    const handleEditBrand = () => {
        router.push("/product/featuredProduct/add");
    };


    console.log("masterProductListData", myProductData, productData);



    return (
        <Box sx={{ padding: '2% 5%' }}>
            <DetailsTopView
                headerName="Featured Products"
                handleButtonChange={handleEditBrand}
                buttonText="Add Featured Product"
                searchPlaceholder='Search by Product Name'
                searchField={true}
            />
            {isLoading ? (
                <Box display="flex" justifyContent="center" alignItems="center" height="200px">
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    {myProductData && myProductData.length > 0 ? (
                        <Box display="flex" gap="1rem" flexWrap="wrap">
                            <ProductCollapsibleTable myProductData={myProductData} />
                        </Box>
                    ) : (
                        <PageEmpty />
                    )}
                </>

            )}
        </Box>
        // <Box width="100%">
        //     {isLoading ? (
        //         <Box display="flex" justifyContent="center" alignItems="center" height="200px">
        //             <CircularProgress />
        //         </Box>
        //     ) : (
        //         <>
        //             {myProductData && myProductData.length > 0 ? (
        //                 <Box display="flex" gap="1rem" flexWrap="wrap">
        //                     {myProductData.map((product: any) => (
        //                         <ProductCard
        //                             key={product?.product?.id}
        //                             width="200px"
        //                             headerVariant="h5"
        //                             imageType="big"
        //                             name={product?.MasterProduct?.name}
        //                             image={product?.MasterProduct?.image?.masterImages[0]}
        //                             brandName={product?.MasterProduct?.Brand?.name_en}
        //                             price={product?.MasterProduct?.MasterOffers?.price_with_tax}
        //                             estimatedPrice={product?.MasterProduct?.MasterOffers?.estimated_price}
        //                         />
        //                     ))}
        //                 </Box>
        //             ) : (
        //                 <Typography variant="body1" textAlign="center" mt={3}>
        //                     No data found.
        //                 </Typography>
        //             )}
        //         </>
        //     )}
        // </Box>
    );
};

export default FeaturedProduct;