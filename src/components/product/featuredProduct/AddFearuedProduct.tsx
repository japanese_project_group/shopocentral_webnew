import { InputComponent } from "@/src/common/Components";
import PageEmpty from "@/src/common/PageEmpty";
import { businessId } from "@/src/common/utility/Constants";
import {
    Box,
    Button,
    CircularProgress,
    MenuItem,
    TextField,
    Typography,
} from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
    useGetHotDealProductQuery,
    useGetMyProductsQuery,
    usePostHotDealProductMutation,
    useUpdateHotDealProductMutation,
} from "../../../../redux/api/user/user";
import { DetailsTopView } from "../../customComponents/detailsTopView";

interface FormValues {
    expiryDate: string;
    advertisementPay: number;
    feature_category: string;
    isFavourite: boolean;
    status: string;
    productId: string;
    reason: string;
}

const AddFeaturedProduct = () => {
    const router = useRouter();
    const [submitType, setSubmitType] = useState<string>("Submit");
    const [brandDescriptionValue, setBrandDescriptionValue] = useState<string>(
        ""
    );
    const [brandValue, setBrandValue] = useState<string>("");
    const [brandId, setBrandId] = useState<string>("");
    const [isSubmittingMaster, setIsSubmittingMaster] = useState<boolean>(
        false
    );
    const [formState, setFormState] = React.useState({
        productId: "",
        feature_category: "",
    });

    const [AddFeaturedProduct, { data: addBrandData }] =
        usePostHotDealProductMutation();
    const [UpdateFeatueredProduct, { data: updateBrandData }] =
        useUpdateHotDealProductMutation();
    const { data: myProductData, refetch, isSuccess, isLoading } =
        useGetHotDealProductQuery(businessId);
    const { data: myProduct } = useGetMyProductsQuery();

    useEffect(() => {
        setFormState((prev) => ({
            ...prev,
            productId: myProduct?.masterProductId || "",
            feature_category: myProduct?.brandId || "",
        }));
    }, [myProduct]);

    const handleFieldChange = (event: any, fieldName: string) => {
        setFormState((prev) => ({
            ...prev,
            [fieldName]: event.target.value,
        }));
    };

    const submitForm = async (values: FormValues) => {
        try {
            const fd = new FormData();

            // Append values
            fd.append("productId", formState.productId);
            fd.append("feature_category", formState.feature_category);

            // Append other values from the form
            Object.entries(values).forEach(([key, value]) => {
                fd.append(key, value);
            });

            const response = await AddFeaturedProduct(fd);

            if (response?.statusCode === 400 || !response?.data) {
                throw new Error("Bad Request Error");
            }

            // Handling success case
            toast.success(response?.message || response?.data?.message);
            return true;
        } catch (err: any) {
            toast.error(err?.message || "An error occurred !!");
            return false;
        }
    };

    const initialValues: FormValues = {
        expiryDate: "",
        advertisementPay: 0,
        feature_category: "",
        isFavourite: true,
        status: "",
        productId: "",
        reason: "",
    };

    const formik = useFormik({
        initialValues,
        enableReinitialize: true,
        onSubmit: async (values: any, { setErrors }) => {
            try {
                const errors = await formik.validateForm();
                if (Object.keys(errors).length === 0) {
                    await submitForm(values);
                } else {
                    setErrors(errors);
                }
            } catch (error) {
                console.error("Error submitting form:", error);
                toast.error("An error occurred while submitting the form.");
            }
        },
    });

    const handleSubmitButton = async (buttonType: string) => {
        try {
            if (isSubmittingMaster) return;
            setIsSubmittingMaster(true);
            const errors = await formik.validateForm();
            if (buttonType === "submit") {
                await submitForm(formik.values);
            } else if (buttonType === "patch") {
                // Logic for patching form
            }
        } catch (error) {
            console.error("Error handling next step:", error);
            toast.error("An error occurred while proceeding to the next step.");
        } finally {
            setIsSubmittingMaster(false);
        }
    };

    const {
        errors,
        values,
        touched,
        isSubmitting,
        handleSubmit,
        getFieldProps,
        setFieldValue,
    } = formik;

    const fieldFormsText = (
        fieldName: string,
        getFieldPropsValue: string,
        type?: string,
        errors?: any,
        touched?: any,
        disabled?: boolean,
        options?: any,
        selectValue?: any,
        setSelectType?: any,
        selectDataType?: string,
        defaultValueOption?: string
    ) => {
        return (
            <Box display={"flex"} justifyContent={""}>
                <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
                    <div>{fieldName}:</div>
                    <div
                        style={{
                            width: "50%",
                            gap: "5px"
                        }}
                    >
                        <InputComponent
                            type={type}
                            getFieldProps={getFieldProps}
                            getFieldPropsValue={getFieldPropsValue}
                            errors={errors}
                            touched={touched}
                            disabled={disabled}
                            setFieldValue={setFieldValue}
                            values={values}
                            label={type === "postalCode" && "grgrt"}
                            selectValue={selectValue}
                            setSelectType={setSelectType}
                            options={options}
                            selectDataType={selectDataType}
                        />
                        {errors && touched && errors[getFieldPropsValue] && touched[getFieldPropsValue] && (
                            <div>{errors[getFieldPropsValue]}</div>
                        )}
                    </div>
                </div>
            </Box>
        );
    };

    const displayForms = () => (
        <FormikProvider value={formik}>
            <form onSubmit={handleSubmit}>
                <div>
                    <div style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginBottom: "5px",
                    }}>
                        <Typography sx={{ width: '25%' }} component="span">Select Product</Typography>
                        <TextField
                            sx={{ width: '50%' }}
                            {...getFieldProps("productId")}
                            size="small"
                            fullWidth
                            select
                            name="productId"
                            SelectProps={{
                                multiple: false,
                                value: formState.productId,
                                onChange: (event) => handleFieldChange(event, "productId"),
                            }}
                        >
                            {myProduct?.map((option: any) => (
                                <MenuItem
                                    key={option.masterProductId}
                                    value={option.masterProductId}
                                >
                                    {option.MasterProduct.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    <div style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginBottom: "5px",
                    }}>
                        <Typography sx={{ width: '25%' }} component="span">Featuring Category</Typography>
                        <TextField
                            sx={{ width: '50%' }}
                            {...getFieldProps("feature_category")}
                            size="small"
                            fullWidth
                            select
                            name="feature_category"
                            SelectProps={{
                                multiple: false,
                                value: formState.feature_category,
                                onChange: (event) =>
                                    handleFieldChange(event, "feature_category"),
                            }}
                        >
                            {myProduct?.map((option: any) => (
                                <MenuItem key={option.id} value={option.id}>
                                    {option.name_en}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    {fieldFormsText("Duration (in days)", "expiryDate", "text", errors.expiryDate,
                        touched.expiryDate)}
                    {fieldFormsText("Offer Price ", "offer_price", "text", errors.offer_price,
                        touched.offer_price)}
                    {fieldFormsText("Remarks/Comment ", "reason", "text", errors.reason,
                        touched.reason)}
                </div>
                <Box
                    display="flex"
                    marginTop="2rem"
                    gap="1rem"
                    justifyContent="end"
                >
                    {updateBrandData?.length > 0 ? (
                        <Button
                            variant="contained"
                            onClick={() => handleSubmitButton("update")}
                            disabled={isSubmittingMaster}
                        >
                            Update
                        </Button>
                    ) : (
                        <Button
                            variant="contained"
                            onClick={() => handleSubmitButton("submit")}
                            disabled={isSubmittingMaster}
                        >
                            Submit
                        </Button>
                    )}
                </Box>
            </form>
        </FormikProvider>
    );

    return (
        <Box>
            {updateBrandData?.length > 0 ? (
                <DetailsTopView
                    headerName="Edit Featured Products"
                    isButton={false}
                />
            ) : (
                <DetailsTopView
                    headerName="Add Featured Products"
                    isButton={false}
                />
            )}

            {isLoading ? (
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    height="200px"
                >
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    {myProduct?.length > 0 ? (
                        <div className="form-view-multistep-form box-kyc-multiple-step-form">
                            {displayForms()}
                        </div>
                    ) : (
                        <PageEmpty />
                    )}
                </>
            )}
        </Box>
    );
};

export default AddFeaturedProduct;
