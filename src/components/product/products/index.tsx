import PageEmpty from "@/src/common/PageEmpty";
import ServerDown from "@/src/common/ServerDown";
import { Box, CircularProgress } from "@mui/material";
import { useEffect, useState } from "react";
import { useGetMyProductsQuery, useGetProductInventoryManagementQuery } from "../../../../redux/api/user/user";
import ProductCollapsibleTable from "../../../common/ProductCollapsibleTable";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const MyProduct = () => {
    const [productData, setProductData] = useState<any[]>()
    const { data: myProductData, isLoading, isError, refetch } = useGetMyProductsQuery();
    const { data: InventoryMgmtData } = useGetProductInventoryManagementQuery();

    console.log("InventoryMgmtData", InventoryMgmtData);

    useEffect(() => {
        if (myProductData) {
            setProductData(myProductData)
        }
        refetch();
    }, [myProductData, refetch])

    console.log("masterProductListData", myProductData, productData);

    return (
        <Box sx={{ padding: '2% 5%' }}>
            <DetailsTopView
                headerName="My Products"
                isButton={false}
                searchField={false}
            />
            {isLoading ? (
                <Box display="flex" justifyContent="center" alignItems="center" height="200px">
                    <CircularProgress />
                </Box>
            ) : (
                <>
                    {isError ?
                        <ServerDown />
                        :
                        <>
                            {myProductData && myProductData.length > 0 ? (
                                <Box display="flex" gap="1rem" flexWrap="wrap">
                                    <ProductCollapsibleTable myProductData={myProductData} />
                                </Box>
                            ) : (
                                <PageEmpty />
                            )}
                        </>
                    }
                </>
            )}
        </Box>
        // <Box width="100%">
        //     {isLoading ? (
        //         <Box display="flex" justifyContent="center" alignItems="center" height="200px">
        //             <CircularProgress />
        //         </Box>
        //     ) : (
        //         <>
        //             {myProductData && myProductData.length > 0 ? (
        //                 <Box display="flex" gap="1rem" flexWrap="wrap">
        //                     {myProductData.map((product: any) => (
        //                         <ProductCard
        //                             key={product?.product?.id}
        //                             width="200px"
        //                             headerVariant="h5"
        //                             imageType="big"
        //                             name={product?.MasterProduct?.name}
        //                             image={product?.MasterProduct?.image?.masterImages[0]}
        //                             brandName={product?.MasterProduct?.Brand?.name_en}
        //                             price={product?.MasterProduct?.MasterOffers?.price_with_tax}
        //                             estimatedPrice={product?.MasterProduct?.MasterOffers?.estimated_price}
        //                         />
        //                     ))}
        //                 </Box>
        //             ) : (
        //                 <Typography variant="body1" textAlign="center" mt={3}>
        //                     No data found.
        //                 </Typography>
        //             )}
        //         </>
        //     )}
        // </Box>
    );
};
