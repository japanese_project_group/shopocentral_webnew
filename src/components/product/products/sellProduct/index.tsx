// Build in librry
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useState } from 'react';

// MUI Components
import { Close } from "@mui/icons-material";
import {
    Box,
    Button,
    Chip,
    CircularProgress, Grid,
    MenuItem,
    Paper,
    Select,
    SelectChangeEvent,
    Step,
    StepLabel,
    Stepper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from "@mui/material";


// Third party library
import { Field, Formik } from "formik";
import { toast } from "react-toastify";
import * as Yup from 'yup';

// redux
import {
    useGetShopDataQuery,
    useGetSingleMasterProductQuery,
    useGetSubCategoryQuery,
    useGetVariationDataQuery,
    usePostProductInventoryManagementMutation,
    usePostProductMutation
} from "../../../../../redux/api/user/user";

// custom components
import CardCheckbox from "../../../../common/CardCheckbox";
import { manageInventory, orderOutOfStock, productCondition, status } from "../../../../common/commonData";
import { generateRandomNumber } from "../../../../common/commonFunc";
import { businessId } from "../../../../common/utility/Constants";
import { DetailsTopView } from "../../../customComponents/detailsTopView";

// type define
type CheckedBranches = { [key: string]: number[] };
type CheckedShops = { [key: string]: boolean };

const SellThisProduct = () => {
    const router = useRouter();
    const [activeStep, setActiveStep] = useState<number>(0);
    const [allShop, setAllShop] = useState<any[]>([]);
    const [checkedShops, setCheckedShops] = useState<CheckedShops>({});
    const [checkedBranches, setCheckedBranches] = useState<CheckedBranches>({});
    const [masterProductID, setMasterProductID] = useState<string | undefined>();
    const [conditionValue, setConditionValue] = useState<string>("");
    const [quantityValue, setQuantityValue] = useState<string>("");
    const [countType, setCountType] = useState<string>("");
    const [skipped, setSkipped] = useState<Set<number>>(new Set<number>());
    const [variationsData, setVariationsData] = useState<any[]>([]);
    const [commonVariationsId, setCommonVariationsId] = useState<string[]>([]);
    const [rand, setRand] = useState<number | undefined>();
    const [formState, setFormState] = useState<{ subCategory: any[]; category: any[] }>({ subCategory: [], category: [] });
    const [allvariationItems, setAllvarioationItems] = useState<any[]>([]);
    const [catchData, setCatchData] = useState<any>();
    const [productTagValues, setProductTagValues] = useState<any[]>([]);
    const [productTagValue, setProductTagValue] = useState<string>('');
    const [singleProductData, setSingleProductData] = useState<any[]>([]);
    // const [productId, setProductId] = useState<string | undefined>();
    const [formValues, setFormValues] = useState<any[]>([]);

    const { data: allData, isSuccess, refetch } = useGetShopDataQuery();
    const { data: getSingleProductData, refetch: SellProductFetch } = useGetSingleMasterProductQuery(router.query.id);
    const [PostProduct, { data: addPostProductData, refetch: postProductRefetch, isLoading }] = usePostProductMutation();
    const { data: subcatData, isSuccess: isSuccessSubCategory } = useGetSubCategoryQuery();
    const { data: getVariationData, refetch: refetchUserData } = useGetVariationDataQuery(businessId);
    const [PostProductInventoryManagement, { isLoading: postInventoryLoading }] = usePostProductInventoryManagementMutation();

    const steps = ['Sell product', 'Inventory management'];

    useEffect(() => {
        if (postProductRefetch) {
            postProductRefetch();
        }
    }, [postProductRefetch])

    useEffect(() => {
        if (getSingleProductData) {
            setCatchData(getSingleProductData)
        }
        SellProductFetch();
    }, [getSingleProductData])

    useEffect(() => {
        if (catchData?.productVariation) {
            setFormValues(catchData.productVariation.map((_: any, index: any) => initialVariations(index)));
        }
    }, [catchData]);

    const initialVariations = (index: string) => ({
        your_price: catchData?.productVariation?.[index]?.price || '',
        variationWithProductId: catchData?.productVariation?.[index]?.id || '',
        variations: catchData?.productVariation?.[index] || [],
        sku: catchData?.productVariation?.[index]?.sku || '',
        vrImages: catchData?.productVariation?.[index]?.vrImages || [],
        quantity: '',
        status: "",
        product_condition: catchData?.productVariation?.[index]?.product_condition || '',
        manage_inventory: "",
        order_out_of_stock: "",
    });


    const finalFornValues = formValues.map((item: any) => {
        const { vrImages, variations, sku, product_condition, ...rest } = item;
        return rest;
    });

    // const handleSubmitInventoryMgmt = async () => {
    //     try {
    //         let body = [
    //             ...finalFornValues,
    //         ]

    //         const response = await PostProductInventoryManagement(body);
    //         console.log("response", response);
    //         if (response) {
    //             toast.success("Successfully created");
    //             router.push("/master-product/allproduct");
    //         } else if (response.error) {
    //             toast.error("Internal Server Error");
    //         }
    //     } catch (error) {
    //         toast.error('An error occurred while creating the variation. Please try again.');
    //     }
    // };


    useEffect(() => {
        if (allData && isSuccess) {
            setAllShop(allData?.data);
        }
    }, [isSuccess, allData]);

    useEffect(() => {
        setRand(generateRandomNumber());
    }, []);

    const isStepSkipped = (step: number) => {
        return skipped.has(step);
    };

    // Convert quantity values to numbers and sum them
    const totalQuantity = formValues.reduce((sum: any, variation: any) => {
        const quantitySum = parseInt(variation.quantity, 10) || 0; // Convert to integer, default to 0 if NaN
        return sum + quantitySum;
    }, 0);

    const handleChange = (event: SelectChangeEvent<typeof variationsData>) => {
        const {
            target: { value },
        } = event;
        setVariationsData(
            typeof value === "string" ? value.split(",") : value
        );
    };

    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    useEffect(() => {
        setMasterProductID(router.query.id as string);
    }, [router.query.id])


    const handleReset = () => {
        setActiveStep(0);
    };

    const handleChangeButton = () => {
        router.push("/master-product/allproduct");
    };

    // common variations name idArray
    React.useEffect(() => {
        const result: string[] = [];
        if (variationsData && getVariationData) {
            variationsData.forEach(variationName => {
                const matchingVariations = getVariationData.filter((variation: any) => variation.name === variationName);
                const matchingIDs = matchingVariations.map((variation: any) => variation.id);
                result.push(...matchingIDs);
            });

            const combinedResult = ([] as string[]).concat(...result);
            setCommonVariationsId(combinedResult);
        }
    }, [variationsData, getVariationData]);

    // group related varaitions
    const groupedData = allvariationItems?.reduce((acc: any, item: any) => {
        if (item) {
            const { variationTypeId, ...rest } = item;
            if (!acc[variationTypeId]) {
                acc[variationTypeId] = [];
            }
            acc[variationTypeId].push(rest);
        }
        return acc;
    }, {});

    const dataform = formState?.category?.map((data: any) => data);
    const filteredSubCateData = subcatData?.filter((data1: any) =>
        dataform?.includes(data1?.category?.id)
    );


    const handleCheckboxChange = (cardId: string) => {
        setCheckedShops((prevCheckedShops) => {
            const newCheckedShops = { ...prevCheckedShops };
            newCheckedShops[cardId] = !prevCheckedShops[cardId];
            return newCheckedShops;
        });
    };
    const handleCheckboxBranchesChange = (branchId: any, shopId: any) => {
        setCheckedBranches((prevCheckedBranches) => {
            const newCheckedBranches = { ...prevCheckedBranches };

            if (!newCheckedBranches[shopId]) {
                newCheckedBranches[shopId] = [];
            }

            if (!newCheckedBranches[shopId].includes(branchId)) {
                newCheckedBranches[shopId].push(branchId);
            } else {
                newCheckedBranches[shopId] = newCheckedBranches[shopId].filter((id) => id !== branchId);
            }

            return newCheckedBranches;
        });
    };

    const checkedShopsArray = Object.keys(checkedShops).filter((shopId) => checkedShops[shopId]);
    const checkedBranchesArray = Object.values(checkedBranches).flat();

    const handleVariationChange = (index: number, field: string, value: any) => {
        setFormValues((prevFormValues: any) => {
            const updatedFormValues = [...prevFormValues];
            updatedFormValues[index] = {
                ...updatedFormValues[index],
                [field]: value,
            };
            return updatedFormValues;
        });
    };

    const uniqueVariationTypes = Array.from(new Set(getSingleProductData?.productVariation.flatMap((data: any) => data?.Variations?.map((SingleVardata: any) => SingleVardata?.VariationItem?.VariationType?.name))));

    const handleInputChange = (e: any) => {
        setProductTagValue(e.target.value);
    };

    const handleClick = () => {
        if (productTagValue.trim() !== '') {
            setProductTagValues((prevKeyWordsValues: any) => [...prevKeyWordsValues, productTagValue.trim()]);
            setProductTagValue('');
        }
    };

    const deleteSingleKeyword = (index: any) => {
        setProductTagValues((prevKeyWordsValues: any) => prevKeyWordsValues.filter((_: any, i: any) => i !== index));
    };


    const initialValues = {
        ...getSingleProductData,
        productName: getSingleProductData?.name || '',
        model_name: getSingleProductData?.model_name || '',
        yourPrice: {
            ...getSingleProductData?.MasterOffers,
            price_with_tax: getSingleProductData?.MasterOffers?.price_with_tax || '',
        },
        condition: conditionValue,
        unit_count: totalQuantity,
        count_type: countType,
        product_tag: productTagValues,
        status: getSingleProductData?.status || '',
    };

    const handleSubmitProduct = async () => {
        try {
            // Submit product data
            const productBody = {
                ...initialValues,
                shopIds: checkedShopsArray,
                shopBranchIds: checkedBranchesArray,
                masterProductId: masterProductID,
                cach_copy_en: 'none',
                status: 'VISIBLE',
            };
            const productResponse = await PostProduct(productBody).unwrap();

            // If product submission is successful
            if (productResponse) {
                const productId = productResponse.data.id;

                // Map form values and add productId to each item
                const finalFornValuesWithProductId = finalFornValues.map((item: any) => ({
                    ...item,
                    productId: productId
                }));

                const inventoryResponse = await PostProductInventoryManagement(finalFornValuesWithProductId);

                // If both API calls are successful, show success message and proceed
                toast.success('Product added successfully');
                router.push("/master-product/allproduct");
            } else {
                throw new Error("Failed to submit product data");
            }
        } catch (error) {
            console.error("Error:", error);
            toast.error('An error occurred while adding the product. Please try again.');
        }
    };



    const schema = Yup.object({
        productName: Yup.string().required('Product Name is required'),
        amazonSalesRank: Yup.string().required('Amazon Sales Rank is required'),
        yourPrice: Yup.number().min(0, 'Price must be non-negative').required('Price is required'),
        maxRetailPrice: Yup.number().min(0, 'Price must be non-negative').required('Price is required'),
        condition: Yup.string().oneOf(['New', 'Used']).required('Condition is required'),
        unit_count: Yup.number().min(1, 'Quantity must be at least 1').required('Quantity is required'),
        fulfillmentChannel: Yup.string().oneOf(['FBM', 'FBA']).required('Fulfillment channel is required'),
    });

    const displayForms = (activeStep: number) => {
        return (
            <>
                {/*For Vital Info */}
                {activeStep === 0 && (
                    <div>
                        <Box sx={{ padding: '2% 5%' }}>
                            <DetailsTopView
                                headerName="Seller Information"
                                handleButtonChange={handleChangeButton}
                                isBackBtn={true}
                                buttonText="Back"
                                searchField={false}
                            />
                            <hr style={{ margin: '1rem 0 2rem 0' }} />
                            {getSingleProductData && (
                                <Formik
                                    initialValues={initialValues}
                                    validationSchema={schema}
                                    onSubmit={(values, actions) => {
                                        console.log(values);
                                        actions.setSubmitting(false);
                                    }}
                                >
                                    {({ values, errors, touched, handleChange, handleSubmit }) => (
                                        <form onSubmit={handleSubmit}  >
                                            <Grid container spacing={3} style={{
                                                padding: "0 12px"
                                            }}>
                                                <Grid xs={12} sm={12}>
                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Product Name</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Field
                                                                as={TextField}
                                                                variant="outlined"
                                                                size="small"
                                                                name="productName"
                                                                value={values.productName}
                                                                disabled
                                                                fullWidth
                                                                margin="normal"
                                                            />
                                                        </Grid>
                                                    </Box>
                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Model Name</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Field
                                                                as={TextField}
                                                                variant="outlined"
                                                                size="small"
                                                                name="model_name"
                                                                value={values.model_name}
                                                                onChange={handleChange}
                                                                disabled
                                                                error={touched.model_name && !!errors.model_name}
                                                                helperText={touched.model_name && errors.model_name}
                                                                fullWidth
                                                                margin="normal"
                                                            />
                                                        </Grid>
                                                    </Box>
                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Quantity</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Field
                                                                as={TextField}
                                                                variant="outlined"
                                                                size="small"
                                                                name="unit_count"
                                                                value={totalQuantity}
                                                                onChange={(e: any) => {
                                                                    setQuantityValue(e.target.value);
                                                                    handleChange(e);
                                                                }}
                                                                disabled
                                                                error={touched.unit_count && !!errors.unit_count}
                                                                helperText={touched.unit_count && errors.unit_count}
                                                                fullWidth
                                                                margin="normal"
                                                            />
                                                        </Grid>
                                                    </Box>
                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Your Price</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Field
                                                                as={TextField}
                                                                variant="outlined"
                                                                size="small"
                                                                name="yourPrice"
                                                                value={values?.yourPrice.price_with_tax}
                                                                onChange={handleChange}
                                                                error={touched.yourPrice && !!errors.yourPrice}
                                                                helperText={touched.yourPrice && errors.yourPrice}
                                                                fullWidth
                                                                margin="normal"
                                                            />
                                                        </Grid>
                                                    </Box>

                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Count Type</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Field
                                                                as={TextField}
                                                                variant="outlined"
                                                                size="small"
                                                                name="count_type"
                                                                value={values.count_type}
                                                                onChange={(e: any) => {
                                                                    setCountType(e.target.value);
                                                                    handleChange(e);
                                                                }}
                                                                error={touched.count_type && !!errors.count_type}
                                                                helperText={touched.count_type && errors.count_type}
                                                                fullWidth
                                                                margin="normal"
                                                            />
                                                        </Grid>
                                                    </Box>

                                                    <Box display={"flex"} alignItems={"center"}>
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Condition</Typography>
                                                        </Grid>
                                                        <Grid xs={8}>
                                                            <Select
                                                                fullWidth
                                                                labelId="condition"
                                                                id="condition"
                                                                value={conditionValue}
                                                                style={{ height: "42px" }}
                                                                name="condition"
                                                                variant='outlined'
                                                                onChange={(e) => {
                                                                    setConditionValue(e.target.value);
                                                                    handleChange(e);
                                                                }}
                                                            >
                                                                <MenuItem value={"New"} sx={{ width: "100%" }} >New</MenuItem>
                                                                <MenuItem value={"Used"} sx={{ width: "100%" }}>Used</MenuItem>
                                                            </Select>
                                                        </Grid>
                                                    </Box>
                                                    <Box display={"flex"} alignItems={"start"} marginTop="10px">
                                                        <Grid xs={4}>
                                                            <Typography variant="body1">Product Tags</Typography>
                                                        </Grid>
                                                        <Grid xs={8} style={{ gap: "5px" }}>
                                                            <TextField
                                                                variant="outlined"
                                                                size="small"
                                                                sx={{ marginBottom: '10px', width: 'calc(100% - 74px)' }}
                                                                value={productTagValue}
                                                                onChange={handleInputChange}
                                                            />
                                                            <Button
                                                                variant="outlined"
                                                                sx={{ height: '40px', marginLeft: "10px" }}
                                                                disabled={!productTagValue.trim()}
                                                                onClick={handleClick}
                                                            >
                                                                Add
                                                            </Button>

                                                            <Box display={'flex'} justifyContent={'flex-start'} gap={'6px'}>
                                                                {productTagValues?.map((value: any, index: any) => (
                                                                    <Box position={'relative'} key={index}>
                                                                        <Chip label={value} variant="outlined" />
                                                                        <Close
                                                                            onClick={() => deleteSingleKeyword(index)}
                                                                            sx={{
                                                                                cursor: 'pointer',
                                                                                fontSize: '13px',
                                                                                position: 'absolute',
                                                                                bgcolor: '#eee',
                                                                                borderRadius: '50%',
                                                                                padding: '1px',
                                                                                right: '-3px',
                                                                            }}
                                                                        />
                                                                    </Box>
                                                                ))}
                                                            </Box>
                                                        </Grid>
                                                    </Box>
                                                </Grid>
                                            </Grid>
                                            <div style={{ display: "flex", flexDirection: "column" }}>
                                                {getSingleProductData?.productVariation?.length > 0 ?
                                                    <>
                                                        <Box display={"flex"} gap={"0.5rem"} alignItems={"center"} margin={"2rem 1rem 1rem 0rem"}>
                                                            <Typography variant="body1" sx={{ color: "#202020", textTransform: "capitalize" }}>{getSingleProductData?.productVariation?.length} Variations</Typography>
                                                        </Box>
                                                        <TableContainer component={Paper}>
                                                            <Table
                                                                aria-label="collapsible table"
                                                                sx={{ minWidth: "1000px", overflowX: "scroll" }}
                                                            >
                                                                <TableHead>
                                                                    <TableRow>
                                                                        {uniqueVariationTypes.map((variationType: any, index: any) => (
                                                                            <TableCell key={index} align="left" sx={{ textTransform: "capitalize" }}>
                                                                                {variationType}
                                                                            </TableCell>
                                                                        ))}
                                                                        <TableCell align="left">SKU</TableCell>
                                                                        <TableCell align="left">Condition Type</TableCell>
                                                                        <TableCell align="left">Your Price</TableCell>
                                                                        <TableCell align="left">Quantity</TableCell>
                                                                        <TableCell align="left">Status</TableCell>
                                                                        <TableCell align="left">Manage Inventory</TableCell>
                                                                        <TableCell align="left">Order Out of Stock</TableCell>
                                                                        <TableCell align="left">Image</TableCell>
                                                                    </TableRow>
                                                                </TableHead>
                                                                <TableBody>
                                                                    {formValues && formValues?.map((variationData: any, index: any) => (
                                                                        <TableRow key={index}>
                                                                            {console.log("singleVar Data", variationData)}
                                                                            {variationData?.variations?.Variations?.map((SingleVardata: any) => (
                                                                                <TableCell align="left" sx={{ textTransform: "capitalize" }}>

                                                                                    <TextField
                                                                                        id=""
                                                                                        variant="standard"
                                                                                        value={SingleVardata?.VariationItem?.name}
                                                                                        disabled
                                                                                    />
                                                                                </TableCell>
                                                                            ))}
                                                                            <TableCell>
                                                                                <TextField
                                                                                    id=""
                                                                                    variant="standard"
                                                                                    value={variationData?.sku}
                                                                                    disabled
                                                                                    onChange={(e) =>
                                                                                        handleVariationChange(index, "sku", e.target.value)
                                                                                    }
                                                                                />
                                                                            </TableCell>
                                                                            <TableCell component="th" scope="row">
                                                                                <Select
                                                                                    value={variationData?.product_condition}
                                                                                    variant={"standard"}
                                                                                    disabled
                                                                                    onChange={(e) => handleVariationChange(index, "product_condition", e.target.value)}
                                                                                    sx={{ minWidth: "110px" }}
                                                                                >
                                                                                    {productCondition?.map((data: any) => (
                                                                                        <MenuItem value={data.value}>{data.label}</MenuItem>
                                                                                    ))}
                                                                                </Select>
                                                                            </TableCell>
                                                                            <TableCell>
                                                                                <TextField
                                                                                    id=""
                                                                                    variant="standard"
                                                                                    value={variationData?.your_price}
                                                                                    onChange={(e) => handleVariationChange(index, 'your_price', e.target.value)}
                                                                                />
                                                                            </TableCell>
                                                                            <TableCell>
                                                                                <TextField
                                                                                    id=""
                                                                                    variant="standard"
                                                                                    value={variationData?.quantity}
                                                                                    onChange={(e) =>
                                                                                        handleVariationChange(index, "quantity", e.target.value)
                                                                                    }
                                                                                />
                                                                            </TableCell>
                                                                            <TableCell component="th" scope="row">
                                                                                <Select
                                                                                    value={variationData?.status}
                                                                                    variant={"standard"}
                                                                                    onChange={(e) => handleVariationChange(index, "status", e.target.value)}
                                                                                    sx={{ minWidth: "110px" }}
                                                                                >
                                                                                    {status?.map((data: any) => (
                                                                                        <MenuItem value={data.value}>{data.label}</MenuItem>
                                                                                    ))}
                                                                                </Select>
                                                                            </TableCell>
                                                                            <TableCell component="th" scope="row">
                                                                                <Select
                                                                                    value={variationData?.manage_inventory}
                                                                                    variant={"standard"}
                                                                                    onChange={(e) => handleVariationChange(index, "manage_inventory", e.target.value)}
                                                                                    sx={{ minWidth: "110px" }}
                                                                                >
                                                                                    {manageInventory?.map((data: any) => (
                                                                                        <MenuItem value={data.value}>{data.label}</MenuItem>
                                                                                    ))}
                                                                                </Select>
                                                                            </TableCell>
                                                                            <TableCell component="th" scope="row">
                                                                                <Select
                                                                                    value={variationData?.order_out_of_stock}
                                                                                    variant={"standard"}
                                                                                    onChange={(e) => handleVariationChange(index, "order_out_of_stock", e.target.value)}
                                                                                    sx={{ minWidth: "110px" }}
                                                                                >
                                                                                    {orderOutOfStock?.map((data: any) => (
                                                                                        <MenuItem value={data.value}>{data.label}</MenuItem>
                                                                                    ))}
                                                                                </Select>
                                                                            </TableCell>
                                                                            <TableCell>
                                                                                <Image src={variationData?.vrImages?.vrImages[0]} height={"60"} width={"60"} alt={"Variation Image"} />
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    ))}

                                                                </TableBody >
                                                            </Table>
                                                        </TableContainer>
                                                    </>
                                                    :
                                                    "Woops! No data found"
                                                }
                                            </div>
                                            <Box>
                                                <Typography variant="h5" marginTop="1rem" fontWeight={"600"}>Shop and Branches</Typography>
                                                {allShop.length > 0 &&
                                                    allShop?.map((card: any) => (
                                                        <CardCheckbox
                                                            key={card.id}
                                                            cardData={card}
                                                            checked={checkedShops[card.id] || false}
                                                            onCheckboxChange={() => handleCheckboxChange(card.id)}
                                                            onCheckboxBranchesChange={(branchId: any) =>
                                                                handleCheckboxBranchesChange(branchId, card.id)
                                                            }
                                                        />
                                                    ))}
                                            </Box>
                                            <Box display="flex" justifyContent="end" marginTop="2rem">
                                                <Button disabled={isLoading} variant="contained" onClick={handleSubmitProduct} color='primary' type="submit">
                                                    {isLoading ? <CircularProgress size={"20px"} /> : "Submit"}
                                                </Button>
                                            </Box>
                                        </form>
                                    )}
                                </Formik>
                            )}
                        </Box>
                    </div>

                )
                }
                {/* For Inventory Management */}
                {activeStep === 1 && (
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        {getSingleProductData?.productVariation?.length > 0 ?
                            <>
                                <Box display={"flex"} gap={"0.5rem"} alignItems={"center"} margin={"2rem 1rem 1rem 0rem"}>
                                    <Typography variant="body1" sx={{ color: "#202020", textTransform: "capitalize" }}>{getSingleProductData?.productVariation?.length} Variations</Typography>
                                </Box>
                                <TableContainer component={Paper}>
                                    <Table
                                        aria-label="collapsible table"
                                        sx={{ minWidth: "1000px", overflowX: "scroll" }}
                                    >
                                        <TableHead>
                                            <TableRow>
                                                {uniqueVariationTypes.map((variationType: any, index: any) => (
                                                    <TableCell key={index} align="left" sx={{ textTransform: "capitalize" }}>
                                                        {variationType}
                                                    </TableCell>
                                                ))}
                                                <TableCell align="left">SKU</TableCell>
                                                <TableCell align="left">Condition Type</TableCell>
                                                <TableCell align="left">Your Price</TableCell>
                                                <TableCell align="left">Quantity</TableCell>
                                                <TableCell align="left">Status</TableCell>
                                                <TableCell align="left">Manage Inventory</TableCell>
                                                <TableCell align="left">Order Out of Stock</TableCell>
                                                <TableCell align="left">Image</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {formValues && formValues?.map((variationData: any, index: any) => (
                                                <TableRow key={index}>
                                                    {console.log("singleVar Data", variationData)}
                                                    {variationData?.variations?.Variations?.map((SingleVardata: any) => (
                                                        <TableCell align="left" sx={{ textTransform: "capitalize" }}>

                                                            <TextField
                                                                id=""
                                                                variant="standard"
                                                                value={SingleVardata?.VariationItem?.name}
                                                                disabled
                                                            />
                                                        </TableCell>
                                                    ))}
                                                    <TableCell>
                                                        <TextField
                                                            id=""
                                                            variant="standard"
                                                            value={variationData?.sku}
                                                            disabled
                                                            onChange={(e) =>
                                                                handleVariationChange(index, "sku", e.target.value)
                                                            }
                                                        />
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        <Select
                                                            value={variationData?.product_condition}
                                                            variant={"standard"}
                                                            disabled
                                                            onChange={(e) => handleVariationChange(index, "product_condition", e.target.value)}
                                                            sx={{ minWidth: "110px" }}
                                                        >
                                                            {productCondition?.map((data: any) => (
                                                                <MenuItem value={data.value}>{data.label}</MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell>
                                                        <TextField
                                                            id=""
                                                            variant="standard"
                                                            value={variationData?.your_price}
                                                            onChange={(e) => handleVariationChange(index, 'your_price', e.target.value)}
                                                        />
                                                    </TableCell>
                                                    <TableCell>
                                                        <TextField
                                                            id=""
                                                            variant="standard"
                                                            value={variationData?.value}
                                                            onChange={(e) =>
                                                                handleVariationChange(index, "value", e.target.value)
                                                            }
                                                        />
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        <Select
                                                            value={variationData?.status}
                                                            variant={"standard"}
                                                            onChange={(e) => handleVariationChange(index, "status", e.target.value)}
                                                            sx={{ minWidth: "110px" }}
                                                        >
                                                            {status?.map((data: any) => (
                                                                <MenuItem value={data.value}>{data.label}</MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        <Select
                                                            value={variationData?.manage_inventory}
                                                            variant={"standard"}
                                                            onChange={(e) => handleVariationChange(index, "manage_inventory", e.target.value)}
                                                            sx={{ minWidth: "110px" }}
                                                        >
                                                            {manageInventory?.map((data: any) => (
                                                                <MenuItem value={data.value}>{data.label}</MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        <Select
                                                            value={variationData?.order_out_of_stock}
                                                            variant={"standard"}
                                                            onChange={(e) => handleVariationChange(index, "order_out_of_stock", e.target.value)}
                                                            sx={{ minWidth: "110px" }}
                                                        >
                                                            {orderOutOfStock?.map((data: any) => (
                                                                <MenuItem value={data.value}>{data.label}</MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell>
                                                        <Image src={variationData?.vrImages?.vrImages[0]} height={"60"} width={"60"} alt={"Variation Image"} />
                                                    </TableCell>
                                                </TableRow>
                                            ))}

                                        </TableBody >
                                    </Table>
                                </TableContainer>
                            </>
                            :
                            "Woops! No data found"
                        }
                    </div>
                )
                }

                <div style={{ marginTop: "30px" }}>
                    <Box sx={{ display: "flex", justifyContent: "end", pt: 2 }}>
                        {activeStep === steps.length - 1 ? (
                            <Button variant="contained" sx={{ display: "block", height: "40px" }} color="primary">
                                Finish
                            </Button>
                        ) : (
                            ""
                        )}
                    </Box>
                </div >

                <Box sx={{ display: "flex", justifyContent: "end", pt: 2 }}>
                    {activeStep !== 0 && (
                        <div
                            className={
                                activeStep === steps.length - 1 ? "finish-line-css" : ""
                            }
                        >
                            <Button
                                variant="contained"
                                color="inherit"
                                style={{
                                    height: "35px",
                                    borderRadius: "9px",
                                    marginRight: "20px",
                                }}
                                onClick={handleBack}
                            >
                                Back
                            </Button>
                        </div>
                    )}
                    {activeStep === steps.length - 1 ? (
                        ""
                    ) : (
                        <>
                            {activeStep !== 0 &&
                                <Button
                                    variant="contained"
                                    style={{ height: "35px", borderRadius: "9px" }}
                                    onClick={handleNext}
                                >
                                    Next
                                </Button>
                            }
                        </>
                    )}
                </Box>
            </>
        );
    };
    return (
        <Box>
            <DetailsTopView
                headerName="Sell This Product"
                handleButtonChange={handleChangeButton}
                isBackBtn={true}
                buttonText="Back"
                searchField={false}
            />
            <Stepper activeStep={activeStep} className="box-kyc-multiple-step-form">
                {steps.map((label, index) => {
                    const stepProps: { completed?: boolean } = {};
                    const labelProps: {
                        optional?: React.ReactNode;
                    } = {};
                    if (isStepSkipped(index)) {
                        stepProps.completed = false;
                    }
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            {activeStep === steps.length ? (
                <>
                    <Typography sx={{ mt: 2, mb: 1 }}>
                        All steps completed - you&apos;re finished
                    </Typography>
                    <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                        <Box sx={{ flex: "1 1 auto" }} />
                        <Button onClick={handleReset}>Reset</Button>
                    </Box>
                </>
            ) : (
                <>
                    <div className="form-view-multistep-form box-kyc-multiple-step-form">
                        {displayForms(activeStep)}
                    </div>
                </>
            )}
        </Box>
    );
}

export default SellThisProduct;