import React, { useState } from 'react';

type ColorImageMap = { [color: string]: File[] };

const VariationImage: React.FC = () => {
  const initialColorImages: ColorImageMap = {
    red: [],
    blue: [],
    green: [],
    // Add more colors as needed
  };

  const [selectedColor, setSelectedColor] = useState<string>('');
  const [newImage, setNewImage] = useState<File | null>(null);
  const [colorImages, setColorImages] = useState<ColorImageMap>(initialColorImages);

  const handleColorChange = (color: string) => {
    setSelectedColor(color);
  };

  const handleImageChange = () => {
    if (selectedColor && newImage) {
      const updatedImages = [...colorImages[selectedColor], newImage];
      setColorImages((prevColorImages) => ({
        ...prevColorImages,
        [selectedColor]: updatedImages,
      }));
      setNewImage(null); // Clear the input field after adding an image
    }
  };

  return (
    <div>
      {/* Dropdown to select a color */}
      <select onChange={(e) => handleColorChange(e.target.value)}>
        <option value="" disabled selected>
          Select a Color
        </option>
        {Object.keys(initialColorImages).map((color) => (
          <option key={color} value={color}>
            {color}
          </option>
        ))}
      </select>

      {/* Input field for adding images */}
      <input
        type="file"
        accept="image/*"
        onChange={(e) => setNewImage(e.target.files?.[0] || null)}
      />

      {/* Button to add image to the selected color */}
      <button onClick={handleImageChange}>Add to Selected Color</button>

      {/* Render UI elements using colorImages arrays */}
      {selectedColor && (
        <ImageGallery color={selectedColor} images={colorImages[selectedColor]} />
      )}
    </div>
  );
};

interface ImageGalleryProps {
  color: string;
  images: File[];
}

const ImageGallery: React.FC<ImageGalleryProps> = ({ color, images }) => {
  return (
    <div>
      <h2>{`${color} Images`}</h2>
      {images.map((image, index) => (
        <img key={index} src={URL.createObjectURL(image)} alt={`Image ${index + 1}`} />
      ))}
    </div>
  );
};

export default VariationImage;
