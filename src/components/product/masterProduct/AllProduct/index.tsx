// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from 'react';

// MUI Components
import { Box, Button, Chip, CircularProgress, IconButton, Typography } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

// third party librray
import moment from "moment";

// assets
import ViewIcon from "../../../../../public/viewIcon.svg";

// redux
import { useGetMasterProductDataQuery } from "../../../../../redux/api/user/user";

// custom components
import PageEmpty from "@/src/common/PageEmpty";
import { DetailsTopView } from "../../../customComponents/detailsTopView";

export const AllProduct = () => {
  const router = useRouter();
  const [currentPage, setCurrentPage] = useState(1);
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const [masterProductData, setMasterProductData] = useState<any>([]);
  const [filteredData, setFilteredData] = useState('');

  const { data: masterProductListData, isLoading: getLoading, refetch } =
    useGetMasterProductDataQuery({ page: currentPage, pageSize: currentPageSize, search: filteredData });

  useEffect(() => {
    refetch({ page: currentPage, pageSize: currentPageSize, search: filteredData });
  }, [currentPage, currentPageSize, filteredData, refetch]);

  const handleDataFiltered = (filteredResults: any) => {
    setFilteredData(filteredResults);
  };

  console.log("filteredData", filteredData, masterProductData, masterProductListData)

  const handleChangeButton = () => {
    router.push("/master-product/allproduct/add");
  }

  const handleSellProduct = (id: any) => {
    router.push({
      pathname: `/master-product/myproduct/sellProduct`,
      query: { id },
    });
  };

  const handleEdit = (id: any) => {
    router.push({
      pathname: `/master-product/allproduct/add`,
      query: { id },
    });
  };

  const handleOnCellClick = () => { };
  const columns = [
    {
      field: "S.No",
      headerName: "S.No",
      headerClassName: "data-grid-header-classname",
      renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
      flex: 1,
      minWidth: 80,
      maxWidth: 120,
    },

    {
      field: "name",
      headerName: "Name",
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.name}</div>,
      flex: 1,
      minWidth: 200,
    },
    {
      field: "brand",
      headerName: "Brand",
      minWidth: 150,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.Brand?.name_en}</div>
    },

    {
      field: "category",
      headerName: "Category",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.category ?
        <Chip label={params?.row?.category?.name_en} color="success" />
        : <p>No Category Found</p>
      }
      </div>
    },
    {
      field: "Price",
      headerName: "Price",
      minWidth: 150,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.MasterOffers ?
        <Typography variant="body2">{params?.row?.MasterOffers?.estimated_price}</Typography>
        : <p>No status </p>
      }
      </div>
    },
    {
      field: "image",
      headerName: "Image",
      minWidth: 180,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.image?.masterImages[0] ?
        <img height={"60px"} width={"70px"} style={{ objectFit: "contain" }} src={params?.row?.image?.masterImages[0]} alt="product main image" />
        : <p>No Image Found</p>
      }
      </div>
    },
    {
      field: "AddedBy",
      headerName: "Added By",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => <div>{params?.row?.User ?
        <Typography variant="body2">{params?.row?.User?.name}</Typography>
        : <p>No User Found</p>
      }
      </div>
    },
    {
      field: "date",
      headerName: "Date",
      minWidth: 180,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) =>
        moment(params?.row?.createdAt).format("YYYY-MM-DD"),
    },
    {
      field: "Action",
      headerName: "Action",
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <>
          <strong style={{ display: "flex" }}>
            <IconButton onClick={() => handleEdit(params?.row?.id)}
              style={{ marginTop: "8px", marginLeft: "16px", cursor: "pointer" }}
            >
              <Image src={ViewIcon} style={{ marginLeft: "10px" }} />
            </IconButton>
          </strong>
          <Button variant="outlined" sx={{ float: "right", marginLeft: "10px" }} onClick={() => handleSellProduct(params?.row?.id)}>
            Sell Product
          </Button>
        </>
      ),
      minWidth: 200,
      flex: 1,
    },
  ];

  useEffect(() => {
    if (masterProductListData) {
      setMasterProductData(masterProductListData);
    }
    refetch();
  }, [masterProductListData, refetch]);
  return (
    <>
      <DetailsTopView
        headerName="Master Product"
        handleButtonChange={handleChangeButton}
        buttonText="Add Master Product"
        searchPlaceholder='Search by Name or SKU'
        searchField={true}
        onDataFiltered={handleDataFiltered}
      />
      {getLoading ? (
        <Box display="flex" justifyContent="center" alignItems="center" height="200px">
          <CircularProgress />
        </Box>
      ) : (
        <div className="staff-view-table">
          {masterProductData && masterProductData?.data?.length > 0 ? (
            <Box sx={{ height: 600, width: "100%" }}>
              <DataGrid
                rows={masterProductData?.data || []}
                columns={columns}
                rowCount={masterProductData?.totalCount}
                pageSize={currentPageSize}
                page={currentPage - 1}
                rowsPerPageOptions={[10, 20, 50]}
                onPageSizeChange={(newPageSize) => {
                  setCurrentPageSize(newPageSize);
                  setCurrentPage(1);
                }}
                onPageChange={(newPage) => {
                  setCurrentPage(newPage + 1);
                }}
                paginationMode="server"
                onCellClick={handleOnCellClick}
                loading={getLoading}
              />
            </Box>
          ) : (<PageEmpty />)}
        </div>
      )}
    </>
  );
};
