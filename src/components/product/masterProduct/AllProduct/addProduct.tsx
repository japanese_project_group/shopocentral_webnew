// Build in library
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";

// MUI Components
import { IconButton } from "@material-ui/core";
import { Close } from "@mui/icons-material";
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import {
  Box,
  Button,
  Checkbox,
  Chip,
  CircularProgress,
  FormControl,
  FormControlLabel,
  InputAdornment,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
  Step,
  StepLabel,
  Stepper,
  TextField,
  Typography
} from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";

// Third party library
import { FormikProvider, useFormik } from "formik";
import moment from "moment";
import { toast } from "react-toastify";

// Redux
import {
  useGetBrandDataQuery,
  useGetManufactureDataQuery,
  useGetMasterCategoryDataQuery,
  useGetMasterECCategoryDataQuery,
  useGetSellingPriceCategoryQuery,
  useGetSingleMasterProductQuery,
  useGetTaxCategoryDataQuery,
  useGetVariationDataQuery,
  useGetVariationImagesQuery,
  useGetVariationItemQuery,
  usePatchUpdateVariationWithProductMutation,
  usePostMasterProductDataMutation,
  usePostMasterProductImageMutation,
  usePostVariationImagesMutation,
  usePostVariationTypeDataMutation,
  usePostVariationWithProductMutation,
  useUpdateAddonMasterProductMutation,
  useUpdateMasterProductMutation,
  useUpdateVariationImagesMutation
} from "../../../../../redux/api/user/user";

// Components
import { InputComponent } from "../../../../common/Components";
import { productCondition } from "../../../../common/commonData";
import { generateRandomNumber } from "../../../../common/commonFunc";
import { DetailsTopView } from "../../../customComponents/detailsTopView";
import { ImageAddViewBoxType } from "../../../customComponents/imageAddViewBoxType";

// validation
import Image from "next/image";
import { businessId } from "../../../../common/utility/Constants";

interface FormValues {
  purchasing_price: string;
  price_with_tax: string;
  gross_margin_percentage: string;
  estimated_price: string;
  expected_price: string;
  margin_price: string;
  sellingPriceCategoryId: string;
  taxCategoryId: string;
  name: string;
  model_no: string;
  model_name: string;
  jan_ean_instore_code: string;
  region_origin: string;
  country_origin: string;
  keyword: string[];
  ecCategoryId: string[];
  shopCategoryId: string[];
  product_code: string;
  product_id: string;
  description: string;
  features: string[];
  legal_disclaimer_note: string;
  variation: Record<string, unknown>;
  manufactureId: string;
  brandId: string;
  categoryId: string;
}

export const AddProduct = () => {
  const ref = useRef();
  const router = useRouter();
  const [activeStep, setActiveStep] = useState<number>(0);
  const [skipped, setSkipped] = useState<Set<number>>(new Set<number>());
  const [addNumber, setAddNumber] = useState<{ otherNumber: string }[]>([{ otherNumber: "" }]);
  const [isButtonVisibleMap, setIsButtonVisibleMap] = useState<{ [key: string]: boolean }>({});
  const [masterImage, setMasterImage] = useState<string[]>([]);
  const [ecSiteImage, setEcSiteImage] = useState<string[]>([]);
  const [variationTypeValue, setVariationTypeValue] = useState<string[]>([]);
  const [selectedIndex, setSelectedIndex] = useState<number>();
  const [keyWordsValues, setKeyWordsValues] = useState<string[]>([]);
  const [features, setFeatures] = useState<any[]>([""]);
  const [colorsValues, setColorsValues] = useState<string[]>([]);
  const [rand, setRand] = useState<number | undefined>();
  const [isImage, setIsImage] = useState<any>({});
  const [inputValue, setInputValue] = useState<string>("");
  const [selectTaxCategoryType, setSelectTaxCategoryType] = useState<string>("");
  const [selectSellingPriceCategory, setSelectSellingPriceCategory] = useState<string>("");
  const [variationsData, setVariationsData] = React.useState<any[]>([]);
  const [catchData, setCatchData] = useState<any>();
  const [masterProductID, setMasterProductID] = useState<string>("");
  const [ID, setID] = useState<string | undefined>(masterProductID);
  const [allvariationItems, setAllvarioationItems] = useState<any[]>([]);
  const [inputValues, setInputValues] = useState<{ [key: string]: any }>({});
  const [commonVariationsId, setCommonVariationsId] = useState<string[]>([]);
  const [finalVariations, setFinalVariations] = React.useState<any[]>([]);
  const [allVariationImages, setAllVariationsImages] = useState<string[]>([]);
  const [checkedItems, setCheckedItems] = useState<string[]>([]);
  const [selectedIds, setSelectedIds] = useState<string[]>([]);
  const [selectedItemIds, setSelectedItemIds] = useState<any[]>([]);
  const [selectedFiles, setSelectedFiles] = useState<{ [key: string]: File | null }>({});
  const [isSubmittingMaster, setIsSubmittingMaster] = useState<boolean>(false);
  const [nationalityValue, setNationalityValue] = useState<any>();
  const [imageSubmittedMap, setImageSubmittedMap] = useState<{ [key: string]: boolean }>({});
  const [grossMarginPercentage, setGrossMarginPercentage] = useState<string>("30");

  const { data: getVariationData, refetch: refetchUserData } = useGetVariationDataQuery(businessId);
  const { data: manufactureDetailsData } = useGetManufactureDataQuery(businessId);
  const { data: brandDetailsData } = useGetBrandDataQuery(businessId);
  const { data: categoryDetailsData } = useGetMasterCategoryDataQuery(businessId);
  const { data: ecCategoryDetails, refetch: refetchEcCategory } = useGetMasterECCategoryDataQuery();
  const [postVariationWithData, { data: variationWithData, isError: postVariationError, isLoading: variationLoading }] = usePostVariationWithProductMutation();
  const [postVariationImage, { isSuccess, }] = usePostVariationImagesMutation();
  const [postVariationType, { data: variationPostData }] = usePostVariationTypeDataMutation();
  const [submitMasterProduct, { data: postMasterProductData }] = usePostMasterProductDataMutation();
  const [updateVariationImagesMutation, { isLoading: variationImageLoading }] = useUpdateVariationImagesMutation();
  const [postMasterImage, { isSuccess: updateSuccess, isLoading: updateLoading }] = usePostMasterProductImageMutation();
  const [addonMasterProduct] = useUpdateAddonMasterProductMutation();
  const [updateMasterProduct] = useUpdateMasterProductMutation();
  const [updateVariationWithProduct] = usePatchUpdateVariationWithProductMutation();
  const { data: VarImagesData, refetch } = useGetVariationImagesQuery(masterProductID);
  const { data: VarItemData, refetch: varItemRefetch } = useGetVariationItemQuery(masterProductID);
  const { data: getSellingPriceCategory, refetch: sellingPriceRefetch } = useGetSellingPriceCategoryQuery();
  const { data: getCategoryData, refetch: categoryRefetch } = useGetTaxCategoryDataQuery();
  const { data: getSingleProductData, refetch: SellProductFetch, isLoading: GetSingleMasterProductLoading } = useGetSingleMasterProductQuery(router.query.id);
  const [formState, setFormState] = React.useState({
    ecCategoryId: [],
    categoryId: '',
    brandId: '',
    manufactureId: '',
    sellingPriceCategoryId: '',
    taxCategoryId: '',
  });

  console.log("keyWordsValues", keyWordsValues);
  console.log("formState", formState);

  useEffect(() => {
    setNationalityValue(values?.country_origin);
  });

  useEffect(() => {
    if (getSingleProductData?.productVariation) {
      setFinalVariations(getSingleProductData.productVariation.map((_: any, index: any) => initialVariations(index)));
    }
  }, [getSingleProductData]);

  useEffect(() => {
    if (masterProductID) {
      varItemRefetch();
    }
  }, [masterProductID, varItemRefetch]);

  useMemo(() => {
    if (!!VarItemData) {
      setAllvarioationItems(VarItemData)
    }
  }, [VarItemData])

  useMemo(() => {
    if (!!getSellingPriceCategory) {
      setSelectSellingPriceCategory(getSellingPriceCategory)
    }
  }, [getSellingPriceCategory])

  const varItemRefetchCallback = useCallback(() => {
    varItemRefetch();
  }, [varItemRefetch]);

  useEffect(() => {
    if (!isEmpty(VarItemData)) {
      setAllvarioationItems(VarItemData);
      varItemRefetchCallback();
    }
  }, [VarItemData, varItemRefetchCallback]);


  const formatString = (str: string) => {
    const words = str.split(' ');
    const formattedWords = words.map((word) => word.charAt(0).toUpperCase() + word.slice(1));
    const formattedStr = formattedWords.join(' ');
    return formattedStr;
  };

  // Function to set visibility for a specific variation
  const setButtonVisibility = (variationName: string, isVisible: boolean): void => {
    setIsButtonVisibleMap((prevMap: Record<string, boolean>) => ({
      ...prevMap,
      [variationName]: isVisible,
    }));
  };

  // comon variations name idArray
  useEffect(() => {
    const result: string[] = [];
    if (variationsData && getVariationData) {
      variationsData.forEach(variationName => {
        const matchingVariations = getVariationData.filter((variation: any) => variation.name === variationName);
        const matchingIDs = matchingVariations.map((variation: any) => variation.id);
        result.push(...matchingIDs);
      });

      const combinedResult = ([] as string[]).concat(...result);
      setCommonVariationsId(combinedResult);
    }
  }, [variationsData, getVariationData]);

  // generiting Variations ID Arrays
  useEffect(() => {
    if (variationsData && selectedIds) {
      const resultsID: string[][] = [];
      variationsData.forEach(variationName => {
        const matchingVariations = selectedIds.map((variation: any) => variation[variationName] || '');
        resultsID.push(matchingVariations);
      });

      const updatedSelectedItems = resultsID[0] ? resultsID[0].map((_, i) => resultsID.map(arr => arr[i])) : [];
      setSelectedItemIds(updatedSelectedItems);
    }
  }, [variationsData, selectedIds]);

  // Sample state to manage input values
  const handleInputChange = (variationName: any, value: any) => {
    setInputValues((prevInputValues: any) => ({
      ...prevInputValues,
      [variationName]: value,
    }));
  };

  useEffect(() => {
    if (Array.isArray(getSingleProductData?.keyword)) {
      setKeyWordsValues(getSingleProductData?.keyword);
    }
  }, [getSingleProductData?.keyword]);

  const steps = [
    "Vital Info",
    "SKU/Inventory",
    "Variations",
    "Image",
    "Offer",
    "Description",
  ];

  const variationItemIdArray = getSingleProductData?.productVariation.map((item: any) =>
    item.id
  );

  let initialVariations: any;

  if (getSingleProductData) {
    initialVariations = (index: string) => ({
      isChecked: '',
      sku: getSingleProductData?.productVariation?.[index]?.sku || '',
      product_id: getSingleProductData?.productVariation?.[index]?.product_id || '',
      product_condition: getSingleProductData?.productVariation?.[index]?.product_condition || '',
      price: getSingleProductData?.productVariation?.[index]?.price || '',
      group_value: 'redSano',
      offer_condition: getSingleProductData?.productVariation?.[index]?.offer_condition || '',
      sale_price: getSingleProductData?.productVariation?.[index]?.sale_price || '',
      vrImagesId: getSingleProductData?.productVariation?.[index]?.vrImages?.vrImages[0] || '',
      sale_start: getSingleProductData?.productVariation?.[index]?.sale_start || '',
      sale_end: getSingleProductData?.productVariation?.[index]?.sale_end || '',
      variations: getSingleProductData?.productVariation?.[index]?.Variations || [],
      masterProductId: masterProductID,
      id: variationItemIdArray[index],
    });
  } else {
    initialVariations = () => ({
      isChecked: '',
      sku: '',
      product_id: '',
      product_condition: '',
      price: '',
      group_value: 'redSano',
      offer_condition: '',
      sale_price: '',
      vrImagesId: '',
      sale_start: '',
      sale_end: '',
      variations: [],
      masterProductId: masterProductID,
    });
  }

  const finalVariationData = finalVariations.map((item: any) => {
    const { isChecked, ...rest } = item;
    return rest;
  });

  const getFinalVariationData = finalVariationData.map((item: any) => {
    const newItem = { ...item, masterProductId: masterProductID, group_value: 'redSano' };
    return newItem;
  });

  const addVariationDataHandler = async () => {
    try {
      const response = await postVariationWithData(getFinalVariationData);
      toast.success(response.data.message);
      handleNext();
    } catch (error: any) {
      toast.error(error.data.error);
    }
  };

  const updateVariationDataHandler = async () => {
    try {
      const response = await updateVariationWithProduct(finalVariationData);
      toast.success(response.data.message);
    } catch (error: any) {
      toast.error(error.data.error);
    }
  };

  // variation post Error
  useEffect(() => {
    if (postVariationError) {
      toast.error("Something Wrong !!");
    }
  }, [variationWithData, postVariationError])

  const handleChangeButton = () => {
    router.push("/master-product/allproduct");
  };

  useEffect(() => {
    setMasterProductID(catchData?.data?.id || catchData?.data?.data?.id)
  }, [catchData])


  // const addVariationImageType = async (color: any) => {
  const addVariationImageType = async (color: any) => {
    let data = {
      vrName: `${color}_image`,
      masterProductId: masterProductID,
    };

    try {
      const res = await postVariationImage({ data: data });
      toast.success(res.message);
      refetch();

      setAllVariationsImages((prevItems: any) => {
        return [...prevItems, res.newItem];
      });

      toast.success("Success!");

      if (!isEmpty(colorsValues)) {
        setID(masterProductID);
      }
      setButtonVisibility(color, true);
    } catch (error) {
      toast.success("Success!");
    }
  };

  const isStepSkipped = (step: number) => {
    return skipped.has(step);
  };

  useEffect(() => {
    setRand(generateRandomNumber());
  }, []);

  const handleNext = () => {
    let newSkipped: any = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };


  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleClick = (varId: any) => {
    setInputValue("Submitting...");
    const inputElement = document.getElementById(`myInput-${varId}`) as HTMLInputElement | null;
    const value = inputElement?.value || null;

    let body = { name: value, variationTypeId: varId, masterProductId: masterProductID };

    postVariationType(body)
      .unwrap()
      .then((data: any) => {
        setInputValue("");
        varItemRefetch();

        setAllvarioationItems((prevItems: any) => {
          return [...prevItems, data.newItem];
        });

        setInputValues((prevInputValues: any) => ({
          ...prevInputValues,
          [varId]: '',
        }));

        toast.success("Success!");
      })
      .catch((error: any) => {
        toast.error("Something went wrong!!");
        setInputValue("Error!");
      });
  };

  // group related varaitions
  const groupedData = allvariationItems?.reduce((acc: any, item: any) => {
    if (item) {
      const { variationTypeId, ...rest } = item;
      if (!acc[variationTypeId]) {
        acc[variationTypeId] = [];
      }
      acc[variationTypeId].push(rest);
    }
    return acc;
  }, {});

  // Add features
  const addFeature = () => {
    setFeatures([...features, [""]]);
  };

  // const onChangeFeature = (e: any, index: any) => {
  //   const list = [...features];
  //   list[index] = e.target.value;
  //   setFeatures(list);
  // };

  const onChangeFeature = (e: any, index: any) => {
    // Ensure that e.target exists and has a value property
    if (e.target && typeof e.target.value !== 'undefined') {
      const list = [...features];
      list[index] = e.target.value;
      setFeatures(list);
    } else {
      console.error('Invalid event object or target value');
    }
  };

  const deleteFeature = (index: any) => {
    const cloneFeature = [...features];
    cloneFeature.splice(index, 1);
    setFeatures(cloneFeature);
  };

  useEffect(() => {
    if (getSingleProductData?.features) {
      setFeatures(getSingleProductData?.features)
    }
  }, [getSingleProductData?.features])

  // for deleting single color
  const deleteSingleColor = (index: number) => {
    const filterUnclickedColor = colorsValues?.filter(
      (data: any, i: number) => i !== index
    );
    setColorsValues(filterUnclickedColor);
  };

  React.useEffect(() => {
    setColorsValues(VarItemData?.name);
  }, [VarItemData]);

  const handleChange = (event: SelectChangeEvent<typeof variationsData>) => {
    const {
      target: { value },
    } = event;
    setVariationsData(
      typeof value === "string" ? value.split(",") : value
    );
  };

  const handleVariationAdd = () => {
    const id = finalVariations.length + 1;
    const newVariation = { id, ...initialVariations };
    setFinalVariations([...finalVariations, newVariation]);
  };

  const handleVariationChange = (index: any, field: any, value: any) => {
    setFinalVariations((prevVariations: any) => {
      const updatedVariations = [...prevVariations];
      updatedVariations[index] = {
        ...updatedVariations[index],
        [field]: value,
      };

      if (!getSingleProductData) {
        (updatedVariations[index].variations = selectedItemIds[index])
      }
      return updatedVariations;
    });
    handleVariationChangeID(index, field, value);
  };

  const handleVariationChangeID = (index: any, field: any, value: any) => {
    setSelectedIds((prevSelectedIds: any) => {
      const updatedId = [...prevSelectedIds];
      updatedId[index] = {
        ...updatedId[index],
        [field]: value,
      };
      return updatedId;
    });
  };

  const uniqueVariationTypes = Array.from(new Set(getSingleProductData?.productVariation.flatMap((data: any) => data?.Variations?.map((SingleVardata: any) => SingleVardata?.VariationItem?.VariationType?.name))));

  //checkbox function in variation
  const handleCheckboxChange = (name: any) => {
    if (checkedItems.includes(name)) {
      setCheckedItems(checkedItems.filter((item: any) => item !== name));
    } else {
      setCheckedItems([...checkedItems, name]);
    }
  };

  // checked item deleted varaiation
  const handleDeleteChecked = () => {
    const updatedVariations = finalVariations.filter((variation: any) => !variation.isChecked);
    setFinalVariations(updatedVariations);
  };

  // unchecked item deleted varaiation
  const handleDeleteUnChecked = () => {
    const updatedVariations = finalVariations.filter((variation: any) => variation.isChecked);
    setFinalVariations(updatedVariations);
  }

  // submit master image to APIS
  const handleSubmitMasterImage = async () => {
    try {
      const formData = new FormData();

      // Append masterImages array
      masterImage.forEach((file: any, index: any) => {
        formData.append('masterImages', file);
      });

      // Append ecSiteImages array
      ecSiteImage.forEach((file: any, index: any) => {
        formData.append('ecSiteImages', file);
      });

      const response = await postMasterImage({ body: formData, ID: masterProductID });
      handleNext();
      if (response.status === 'success') {
        toast.success('Master image updated successfully');
      } else {
        toast.success('Master image updated successfully');
      }
    } catch (error) {
      toast.error('An error occurred while updating master image');
    }
  };


  // chose variation images
  const changeFile = (e: any, id: string) => {
    const file = e.target.files[0];

    if (file.size > 2 * 1024 * 1024) {
      toast.error('Please select an image with a maximum size of 2MB.');
      return;
    }

    // Update selectedFiles state with the selected file
    setSelectedFiles((prevFiles) => ({ ...prevFiles, [id]: file }));

    // Read the selected file as a data URL and display the image
    const reader = new FileReader();
    reader.onload = (event) => {
      const imgElement = document.getElementById(`image-${id}`) as HTMLImageElement | null;
      if (imgElement && event.target && event.target.result) {
        imgElement.src = event.target.result.toString();
        setIsImage((prevIsImage: any) => ({ ...prevIsImage, [id]: imgElement }));
      }
    };
    reader.readAsDataURL(file);

  };

  const handleSubmitProduct = (id: string) => {
    const file = selectedFiles[id];
    if (!file) {
      toast.error('Please select a file.');
      return;
    }

    const fd = new FormData();
    fd.append('vrImages', file);

    updateVariationImagesMutation({ ID: id, data: fd })
      .then((data: any) => {
        if (data?.status === 200) {
          toast.success('Successfully updated!!');
          refetchUserData();
          setImageSubmittedMap(prevState => ({
            ...prevState,
            [id]: true // Set imageSubmitted to true after successful submission for this id
          }));
        } else {
          toast.success('Successfully updated!!');
          setImageSubmittedMap(prevState => ({
            ...prevState,
            [id]: true // Set imageSubmitted to true after successful submission for this id
          }));
        }
      })
      .catch((err: any) => {
        toast.error(err);
      });
  };

  useEffect(() => {
    setFormState((prev) => ({
      ...prev,
      categoryId: getSingleProductData?.category?.id || prev.categoryId,
      ecCategoryId:
        getSingleProductData?.ecCategory?.map((data: any) => data?.id) || [],
      brandId: getSingleProductData?.brandId || prev.brandId,
      manufactureId: getSingleProductData?.Manufacture[0]?.id || prev.manufactureId,
      taxCategoryId: getSingleProductData?.MasterOffers?.taxCategoryId || prev.taxCategoryId,
      sellingPriceCategoryId: getSingleProductData?.MasterOffers?.sellingPriceCategoryId || prev.sellingPriceCategoryId,
    }));
  }, [getSingleProductData]);

  function handleFieldChange(event: any, fieldName: string) {
    setFormState((prev) => {
      let updatedState = { ...prev };

      if (fieldName === "brandId") {
        updatedState = {
          ...updatedState,
          brandId: event.target?.value,
        };
      } else if (fieldName === "manufactureId") {
        updatedState = {
          ...updatedState,
          manufactureId: event.target?.value,
        };
      }
      if (fieldName === "categoryId") {
        updatedState = {
          ...updatedState,
          categoryId: event.target?.value,
        };
      } else if (fieldName === "ecCategoryId") {
        updatedState = {
          ...updatedState,
          ecCategoryId: event.target?.value,
        };
      } else if (fieldName === "taxCategoryId") {
        updatedState = {
          ...updatedState,
          taxCategoryId: event.target?.value,
        };
      } else if (fieldName === "sellingPriceCategoryId") {
        updatedState = {
          ...updatedState,
          sellingPriceCategoryId: event.target?.value,
        };
      }
      return updatedState;
    });
  }

  console.log("getSingleProductData", getSingleProductData);

  const initialValues = {
    name: getSingleProductData?.name || "",
    shop_en: localStorage.getItem("shopName"),
    jan_ean_instore_code: getSingleProductData?.jan_ean_instore_code || "",
    model_no: getSingleProductData?.model_no || "",
    model_name: getSingleProductData?.model_name || "",
    country_origin: getSingleProductData?.country_origin || "",
    region_origin: getSingleProductData?.region_origin || "",
    uniqueProductId: rand,
    businessType: getSingleProductData?.businessType || "",
    subCategory: getSingleProductData?.subCategory || "",
    purchasing_price: getSingleProductData?.MasterOffers?.purchasing_price || "",
    gross_margin_percentage_pre: getSingleProductData?.MasterOffers?.purchasing_price || grossMarginPercentage,
    gross_margin_percentage: getSingleProductData?.MasterOffers?.gross_margin_percentage || "",
    estimated_price: getSingleProductData?.MasterOffers?.estimated_price || "",
    expected_price: getSingleProductData?.MasterOffers?.expected_price || "",
    margin_price: getSingleProductData?.MasterOffers?.margin_price || "",
    price_with_tax: getSingleProductData?.MasterOffers?.price_with_tax || "",
    legal_disclaimer_note: getSingleProductData?.legal_disclaimer_note || "",
    description: getSingleProductData?.description || "",
    sku: getSingleProductData?.sku || "",
    // sellingPriceCategoryId: getSingleProductData?.MasterOffers?.sellingPriceCategoryId || "",
    product_id: rand,
    product_code: rand,
  };

  const formik = useFormik({
    initialValues,
    enableReinitialize: true,
    // validationSchema: validationSchemaAddMasterProduct,
    onSubmit: async (values: any, { setErrors }) => {
      try {
        const errors = await formik.validateForm();
        if (Object.keys(errors).length === 0) {
          await submitForm(values);
        } else {
          setErrors(errors);
        }
      } catch (error) {
        toast.error('An error occurred while submitting the form.');
      }
    },
  });

  // Submit master product main
  const handleSubmitButton = async (buttonType: string) => {
    try {
      if (isSubmittingMaster) return;
      setIsSubmittingMaster(true);
      const errors = await formik.validateForm(); // Yup validation check
      if (buttonType === 'submit') {
        const submitted = await submitForm(formik.values);
        if (submitted) {
          handleNext();
        } else {
        }
      } else if (buttonType === 'patch') {
        await patchForm(formik.values);
      }
      // Rest of your code...
    } catch (error) {
      console.error('Error handling next step:', error);
      toast.error('An error occurred while proceeding to the next step.');
    } finally {
      setIsSubmittingMaster(false);
    }
  };


  // Submit master product
  const submitForm = async (values: FormValues) => {
    try {
      const fd = new FormData();

      // Append values
      fd.append("categoryId", formState.categoryId);
      fd.append("manufactureId", formState.manufactureId);
      fd.append("brandId", formState.brandId);
      fd.append("keyword[]", JSON.stringify(keyWordsValues));
      fd.append("taxCategoryId", formState.taxCategoryId);
      fd.append("sellingPriceCategoryId", formState.sellingPriceCategoryId);


      formState.ecCategoryId.forEach((value: any) => {
        fd.append(`ecCategoryId[]`, value);
      });

      // Append other values from the form
      Object.entries(values).forEach(([key, value]) => {
        fd.append(key, value);
      });

      const response = await submitMasterProduct(fd);

      if (response?.statusCode === 400 || !response?.data) {
        throw new Error('Bad Request Error');
      }

      // Handling success case
      setCatchData(response);
      toast.success(response?.message || response?.data?.message);
      return true;
    } catch (err: any) {
      toast.error(err?.message || 'An error occurred !!');
      return false;
    }
  };

  // Addon Master product
  const patchForm = async (values: any) => {
    try {
      let body;
      if (getSingleProductData) {
        body = {
          ...values,
          manufactureId: formState?.manufactureId,
          categoryId: formState?.categoryId,
          brandId: formState?.brandId,
          ecCategoryId: formState?.ecCategoryId,
          taxCategoryId: formState?.taxCategoryId,
          sellingPriceCategoryId: formState?.sellingPriceCategoryId,
          features: features,
          keyword: keyWordsValues,
        };
      } else {
        body = {
          ...values,
          product_code: rand,
          product_id: rand,
          brandId: catchData?.data?.data?.brandId,
          manufactureId: catchData?.data?.data?.manufactureId,
          categoryId: catchData?.data?.data?.categoryId,
          ecCategoryId: catchData?.data?.data?.ecCategoryId,
          taxCategoryId: formState?.taxCategoryId,
          sellingPriceCategoryId: formState?.sellingPriceCategoryId,
          keyword: keyWordsValues,
          name: catchData?.data?.data?.name,
          model_no: catchData?.data?.data?.model_no,
          region_origin: catchData?.data?.data?.region_origin,
          features: features,
        };
      };

      let response;

      if (getSingleProductData) {
        response = await updateMasterProduct({ body: body, ID: getSingleProductData?.id });
      } else {
        response = await addonMasterProduct({ body: body, ID: masterProductID });
      }

      toast.success(response?.message || response?.data?.message);
      router.push("/master-product/allproduct");
    } catch (error) {
      console.error('Error PATCHing form:', error);
    }
  };

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    selectDataType?: string,
    defaultValueOption?: string
  ) => {
    return (
      <Box display={"flex"} justifyContent={""}>
        <Box className="display-flex-sb">
          <Box>{fieldName}:</Box>
          <Box
            style={{
              width: "50%",
              display:
                getFieldPropsValue === "gross_margin_percentage" ||
                  getFieldPropsValue === "keyword"
                  ? "flex"
                  : "",
              gap: "5px"
            }}
            className=""
          >
            <InputComponent
              type={type}
              getFieldProps={getFieldProps}
              getFieldPropsValue={getFieldPropsValue}
              errors={errors}
              touched={touched}
              disabled={disabled}
              setFieldValue={setFieldValue}
              values={values}
              label={type === "postalCode" && "grgrt"}
              showTitle={
                type === "postalCode" ||
                (type === "secondaryPhoneNumber" && "grgrt")
              }
              addNumber={addNumber}
              setAddNumber={setAddNumber}
              selectValue={selectValue}
              setSelectType={setSelectType}
              options={options}
              selectDataType={selectDataType}
            />
            {errors && touched && errors[getFieldPropsValue] && touched[getFieldPropsValue] && (
              <Box>{errors[getFieldPropsValue]}</Box>
            )}


            {getFieldPropsValue === "gross_margin_percentage" && (
              <TextField
                name="gross_margin_percentage_pre"
                size="small"
                InputProps={{
                  endAdornment: <InputAdornment position="end">%</InputAdornment>,
                }}
                onChange={(e) => setGrossMarginPercentage(e.target.value)}
                value={grossMarginPercentage}
                variant="outlined"
              />
            )}

            {/* keyword values */}
            {getFieldPropsValue === 'keyword' && (
              <Button
                variant="outlined"
                onClick={() => {
                  if (values.keyword.trim() !== '') {
                    setKeyWordsValues((previousValues: any) => [
                      ...(previousValues || []),
                      values.keyword.trim(),
                    ]);

                    // Reset the keyword field after the values have been updated
                    setFieldValue('keyword', '');
                  }
                }}
              // disabled={!getSingleProductData && !values?.keyword?.trim()} // Disable if keyword is empty
              >
                Add
              </Button>
            )}
          </Box>

        </Box>
      </Box>
    );
  };

  const displayForms = (activeStep: number) => {
    return (
      <Box>
        <FormikProvider value={formik}>
          <form onSubmit={handleSubmit}>
            {activeStep === 0 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 1 : Vital Info
              </Box>
            )}

            {activeStep === 1 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 2 : SKU/Inventory
              </Box>
            )}
            {activeStep === 2 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 3 : Variations
              </Box>
            )}
            {activeStep === 3 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 4 : Image
              </Box>
            )}
            {activeStep === 4 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 5 : Offer
              </Box>
            )}
            {activeStep === 5 && (
              <Box className="master-product-form-header display-flex-jc-center ">
                Step 6 : Description
              </Box>
            )}

            {/*For Vital Info */}
            {activeStep === 0 && (
              <Box>
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    gap: "14px",
                  }}
                >
                  {fieldFormsText("Name ", "name", "text", errors.name,
                    touched.name)}
                  <Box
                    className="form-select-wrapper"
                  >
                    <Box style={{ width: '25%' }}>
                      <Typography component="span">
                        Brand Name
                      </Typography>
                    </Box>
                    <Box style={{ width: '50%' }}>
                      <TextField
                        {...getFieldProps('brandId')}
                        size="small"
                        fullWidth
                        select
                        name="brandId"
                        SelectProps={{
                          multiple: false,
                          value: formState?.brandId,
                          onChange: (event) => handleFieldChange(event, 'brandId'),
                        }}
                      >
                        {brandDetailsData?.map((option: any) => (
                          <MenuItem key={option?.id} value={option?.id}>
                            {option?.name_en}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  </Box>
                  <Box
                    className="form-select-wrapper"
                  >
                    <Box style={{ width: '25%' }}>
                      <Typography component="span" >
                        Manufacture Name
                      </Typography>
                    </Box>
                    <Box style={{ width: '50%' }}>
                      <TextField
                        {...getFieldProps('manufactureId')}
                        size="small"
                        fullWidth
                        select
                        name="manufactureId"
                        SelectProps={{
                          multiple: false,
                          value: formState?.manufactureId,
                          onChange: (event) => handleFieldChange(event, 'manufactureId'),
                        }}
                      >
                        {manufactureDetailsData?.map((option: any) => (
                          <MenuItem key={option?.id} value={option?.id}>
                            {option?.name_en}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  </Box>
                  {fieldFormsText("Part/Modal No ", "model_no", "text", errors.model_no, touched.model_no)}
                  {fieldFormsText("Modal Name", "model_name", "text", errors.model_name, touched.model_name)}
                  {fieldFormsText("JAN/EAN/Instore Code", "jan_ean_instore_code", "text", errors.jan_ean_instore_code, touched.jan_ean_instore_code,)}
                  {fieldFormsText(
                    "Country Of Origin",
                    "country_origin",
                    "country_origin",
                    errors.country_origin,
                    touched.country_origin,
                    false,
                    '',
                    nationalityValue,
                    setNationalityValue,
                  )}

                  {fieldFormsText(
                    "Region Of Origin",
                    "region_origin",
                    "text",
                    errors.region_origin,
                    touched.region_origin
                  )}
                  <Box
                    className="form-select-wrapper"
                  >
                    <Box style={{ width: '25%' }}>
                      <Typography component="span" >
                        Category
                      </Typography>
                    </Box>
                    <Box style={{ width: '50%' }}>
                      <TextField
                        {...getFieldProps('categoryId')}
                        size="small"
                        fullWidth
                        select
                        name="categoryId"
                        SelectProps={{
                          multiple: false,
                          value: formState?.categoryId,
                          onChange: (event) => handleFieldChange(event, 'categoryId'),
                        }}
                      >
                        {categoryDetailsData?.map((option: any) => (
                          <MenuItem key={option?.id} value={option?.id}>
                            {option?.name_en}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  </Box>
                  <Box
                    className="form-select-wrapper"
                  >
                    <Box style={{ width: '25%' }}>
                      <Typography component="span" >
                        Master Product EC Category
                      </Typography>
                    </Box>
                    <Box style={{ width: '50%' }}>
                      <TextField
                        {...getFieldProps('ecCategoryId')}
                        size="small"
                        fullWidth
                        select
                        name="ecCategoryId"
                        SelectProps={{
                          multiple: true,
                          value: formState?.ecCategoryId,
                          onChange: (event) => handleFieldChange(event, 'ecCategoryId'),
                        }}
                      >
                        {ecCategoryDetails?.map((option: any) => (
                          <MenuItem key={option?.id} value={option?.id}>
                            {option?.name_en}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  </Box>

                  {/* {fieldFormsText(
                    "Tax Category",
                    "taxCategoryId",
                    "options",
                    errors.taxCategoryId,
                    touched.taxCategoryId,
                    false,
                    getCategoryData,
                    selectTaxCategoryType,
                    setSelectTaxCategoryType,
                    "server"
                  )} */}

                  {fieldFormsText("Keyword", "keyword", "text", errors.keyword, touched.keyword)}
                  <Box width={"50%"} display={"flex"} justifyContent={"flex-start"} sx={{ marginBottom: "1rem", marginLeft: "50%" }}>
                    {keyWordsValues?.length > 0 &&
                      keyWordsValues?.map((value: any, index: number) => (
                        <Chip
                          key={index}
                          label={value}
                          variant="outlined"
                          style={{ margin: '0px 5px' }}
                        />
                      ))}
                  </Box>
                </Box>
                <Box display={"flex"} marginTop={"2rem"} gap={"1rem"} justifyContent={"end"}>
                  {getSingleProductData ?
                    <Button variant="contained" onClick={handleNext}>
                      Next
                    </Button>
                    :
                    <Button variant="contained" onClick={() => handleSubmitButton('submit')} disabled={isSubmittingMaster}>
                      Submit & Next
                    </Button>
                  }
                </Box>
              </Box>

            )}

            {/* For SKU/Inventory */}
            {activeStep === 1 && (
              <Box sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                gap: "14px",
              }}>
                {fieldFormsText(
                  "Product Id ",
                  `uniqueProductId`,
                  "text",
                  "",
                  "",
                  true,
                  "",
                  "",
                  "",
                  ""
                )}
                {fieldFormsText(
                  "Product Code ",
                  "product_code",
                  "text",
                  "",
                  ""
                )}
                {fieldFormsText("SKU", "SKU", "text", "", "")}
              </Box>
            )}

            {/* For Variations */}
            {activeStep === 2 && (
              <Box style={{ display: "flex", flexDirection: "column" }}>
                <Box display='flex' justifyContent={"space-between"}>
                  <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <FormControl sx={{ m: 1, width: 300 }}>
                      <InputLabel id="demo-multiple-checkbox-label">
                        Variation
                      </InputLabel>
                      <Select
                        labelId="demo-multiple-checkbox-label"
                        id="demo-multiple-checkbox"
                        multiple
                        value={variationsData}
                        onChange={handleChange}
                        input={<OutlinedInput label="Variation" />}
                        renderValue={(selected) => selected.join(", ")}
                      >
                        {variationTypeValue?.map(
                          (inputValue: any, index: number) => (
                            <MenuItem
                              key={inputValue?.getVariationData}
                              value={inputValue?.getVariationData}
                            >
                              <Checkbox
                                checked={
                                  variationsData.indexOf(inputValue?.getVariationData) > -1
                                }
                              />
                              <ListItemText primary={inputValue?.getVariationData} />
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </FormControl>
                    <>
                      {variationsData &&
                        <Box marginTop="12px">
                          {getVariationData.map((varData: any) => {
                            const varDataId = varData?.id;
                            const matchingVariation = variationsData.find((variation) => variation === varData.name);
                            const matchedData = groupedData[varDataId];

                            return matchingVariation ? (
                              <Box key={varData.id} marginBottom={"12px"}>
                                {varData.name && (
                                  <>
                                    <Box width={"100%"} marginBottom={"0px"}>
                                      <TextField
                                        variant="outlined"
                                        label={varData.name}
                                        size="small"
                                        id={`myInput-${varData.id}`}
                                        sx={{ marginBottom: "10px", width: "calc(100% - 64px)" }}
                                        value={inputValues[varData.id] || ''}
                                        onChange={(e: any) => handleInputChange(varData.id, e.target.value)}
                                      />
                                      <Button variant='outlined' sx={{ height: '40px' }} disabled={!inputValues[varData.id] || inputValues[varData.id].trim() === ''} onClick={() => handleClick(varData.id)}>
                                        Add
                                      </Button>
                                    </Box>
                                    <Box display={'flex'} justifyContent={'flex-start'} gap={'6px'}>
                                      {matchedData?.map((item: any, index: any) => (
                                        <Box position={'relative'} key={index}>
                                          <Chip label={item.name} variant="outlined" />
                                          <Close
                                            onClick={() => deleteSingleColor(index)}
                                            sx={{
                                              cursor: 'pointer',
                                              fontSize: '13px',
                                              position: 'absolute',
                                              bgcolor: '#eee',
                                              borderRadius: '50%',
                                              padding: '1px',
                                              right: '-3px',
                                            }}
                                          />
                                        </Box>
                                      ))}
                                    </Box>
                                  </>
                                )}
                              </Box>
                            ) : null;
                          })
                          }
                        </Box>
                      }
                    </>
                  </Box>
                  <Box width={"50%"} display={"flex"} flexDirection={"column"}>
                    {variationsData && getVariationData.map((varData: any, index: any) => {
                      const varDataId = varData?.id;
                      const matchingVariation = variationsData.find((variation) => variation === varData.name);
                      const matchedData = groupedData[varDataId];
                      return matchingVariation ? (
                        <Box>
                          {varData.name && (
                            <Box key={index}>
                              <FormControlLabel
                                control={
                                  checkedItems &&
                                  <Checkbox
                                    checked={checkedItems.includes(varData.name)}
                                    onChange={() => handleCheckboxChange(varData.name)}
                                  />
                                }
                                label={`Is ${varData.name} contain image`}
                              />
                              {checkedItems && checkedItems.includes(varData.name) && matchedData?.map((item: any, index: any) => (
                                <Box key={index} className="imageBox" sx={{ display: "flex", gap: "10px", minWidth: "300px", alignItems: "center" }}>
                                  <Typography textTransform={"capitalize"}>{`${item.name} Variation Image`} </Typography>
                                  <Button
                                    onClick={() => addVariationImageType(item.name)}
                                    style={{ display: isButtonVisibleMap[item.name] ? 'none' : 'block' }}
                                  >
                                    Add Image variation
                                  </Button>
                                </Box>
                              ))}
                            </Box>
                          )}
                        </Box>
                      ) : null
                    })}
                  </Box>
                </Box>
                {(variationsData?.length > 0 || getSingleProductData) &&
                  <>
                    <Box display={"flex"} gap={"0.5rem"} alignItems={"center"} margin={"2rem 1rem 1rem 0rem"}>
                      <Button onClick={handleDeleteChecked} variant="text" sx={{ boxShadow: "1", bgcolor: "#f1f1f1", color: "#202020", textTransform: "capitalize" }}>Delete Selected</Button>
                      <Button onClick={handleDeleteUnChecked} variant="text" sx={{ boxShadow: "1", bgcolor: "#f1f1f1", color: "#202020", textTransform: "capitalize" }}>Undelete Selected</Button>
                      <Typography variant="body1" sx={{ color: "#202020", textTransform: "capitalize" }}>{finalVariations?.length} Variations</Typography>
                      {!getSingleProductData ?
                        <Button onClick={() => handleVariationAdd()} variant="contained" color="primary" sx={{ boxShadow: "1", textTransform: "capitalize", marginLeft: "auto" }}>Add Variation</Button>
                        : null
                      }
                    </Box>
                    <TableContainer component={Paper}>
                      <Table
                        aria-label="collapsible table"
                        sx={{ minWidth: "1470px", overflowX: "scroll" }}
                      >
                        <TableHead>
                          <TableRow className={'table-head'}>
                            <TableCell />
                            {getSingleProductData ?
                              uniqueVariationTypes.map((singleVarName: any) => (
                                <TableCell align="left">{singleVarName}</TableCell>
                              )) :
                              <>
                                {getVariationData.map((varData: any) => {
                                  const matchingVariation = variationsData.find((variation) => variation === varData.name);
                                  return matchingVariation ? (
                                    varData.name &&
                                    <TableCell align="left">{varData.name}</TableCell>
                                  ) : null
                                })}
                              </>
                            }
                            <TableCell align="left">SKU</TableCell>
                            <TableCell align="left">Product ID</TableCell>
                            <TableCell align="left">Condition Type</TableCell>
                            <TableCell align="left">Your Price</TableCell>
                            <TableCell align="left">Image</TableCell>
                            <TableCell align="left">Offer Condition Note</TableCell>
                            <TableCell align="left">Sale Price</TableCell>
                            <TableCell align="left">Sale Start Date</TableCell>
                            <TableCell align="left">Sale End Date</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>

                          {getSingleProductData ? (
                            getSingleProductData?.productVariation ? (
                              finalVariations.map((variationData: any, index: any) => (
                                <TableRow key={index}>
                                  <TableCell component="th" scope="row">
                                    <input
                                      type="checkbox"
                                      id={index}
                                      name={variationData.id}
                                      onChange={(e) => handleVariationChange(index, "isChecked", e.target.checked)}
                                    />
                                  </TableCell>
                                  {getSingleProductData ?
                                    <>
                                      {variationData?.variations?.map((item: any, index: number) => (
                                        <TableCell>
                                          <TextField
                                            id=""
                                            variant="standard"
                                            value={item?.VariationItem?.name}
                                            disabled
                                            onChange={(e) =>
                                              handleVariationChange(index, item?.VariationItem?.name, e.target.value)
                                            }
                                          />
                                        </TableCell>
                                      ))}
                                    </>
                                    :
                                    <>
                                      {getVariationData.map((varData: any) => {
                                        const varDataId = varData?.id;
                                        const matchingVariation = variationsData.find((variation) => variation === varData.name);
                                        const matchedData = groupedData[varDataId];
                                        const collectedIds: string[] = [];
                                        // Collect item.id values in the collectedIds array
                                        matchedData?.forEach((item: any) => {
                                          collectedIds.push(item.id);
                                        });

                                        return matchingVariation ? (
                                          <TableCell component="th" scope="row">
                                            {varData.name && (
                                              <Select
                                                labelId={`${varData.name}`}
                                                id={varData.name}
                                                // value={variationData.varData.name}
                                                variant={"standard"}
                                                onChange={(e) => {
                                                  handleVariationChangeID(index, varData.name, e.target.value)
                                                }}
                                                sx={{ minWidth: "110px" }}
                                              >
                                                {matchedData?.map((item: any) => (
                                                  <MenuItem key={item.id} value={item.id}>
                                                    {item.name}
                                                  </MenuItem>
                                                ))}
                                              </Select>
                                            )}
                                          </TableCell>
                                        ) : null
                                      })}
                                    </>
                                  }


                                  <TableCell>
                                    <TextField
                                      id=""
                                      variant="standard"
                                      value={variationData?.sku}
                                      onChange={(e) =>
                                        handleVariationChange(index, "sku", e.target.value)
                                      }
                                    />
                                  </TableCell>
                                  <TableCell>
                                    <TextField
                                      id=""
                                      variant="standard"
                                      value={variationData?.product_id}
                                      onChange={(e) =>
                                        handleVariationChange(index, "product_id", e.target.value)
                                      }
                                    />
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                    <Select
                                      value={variationData?.product_condition}
                                      variant={"standard"}
                                      onChange={(e) => handleVariationChange(index, "product_condition", e.target.value)}
                                      sx={{ minWidth: "110px" }}
                                    >
                                      {productCondition?.map((data: any) => (
                                        <MenuItem value={data.value}>{data.label}</MenuItem>
                                      ))}
                                    </Select>
                                  </TableCell>
                                  <TableCell>
                                    <TextField
                                      id=""
                                      variant="standard"
                                      value={variationData?.price}
                                      onChange={(e) =>
                                        handleVariationChange(index, "price", e.target.value)
                                      }
                                    />
                                  </TableCell>
                                  {getSingleProductData ?
                                    <>
                                      <TableCell>
                                        <Image src={variationData?.vrImagesId} alt="variation image" height={50} width={50} />
                                      </TableCell>
                                    </> :
                                    <TableCell>
                                      <Select
                                        value={variationData?.vrImagesId}
                                        variant={"standard"}
                                        onChange={(e) => handleVariationChange(index, "vrImagesId", e.target.value)}
                                        sx={{ minWidth: "110px" }}
                                      >
                                        {VarImagesData?.map((data: any) => (
                                          <MenuItem value={data?.id}>{data?.vrName}</MenuItem>
                                        ))}
                                      </Select>
                                    </TableCell>
                                  }
                                  <TableCell>
                                    <TextField
                                      id=""
                                      variant="standard"
                                      value={variationData?.offer_condition}
                                      onChange={(e) =>
                                        handleVariationChange(index, "offer_condition", e.target.value)
                                      }
                                    />
                                  </TableCell>
                                  <TableCell>
                                    <TextField
                                      id=""
                                      variant="standard"
                                      value={variationData?.sale_price}
                                      onChange={(e) =>
                                        handleVariationChange(index, "sale_price", e.target.value)
                                      }
                                    />
                                  </TableCell>
                                  <TableCell className="varDate">
                                    <LocalizationProvider dateAdapter={AdapterMoment}>
                                      <DatePicker
                                        value={variationData.sale_start}
                                        onChange={(dateData: any) => {
                                          const formattedDate = dateData ? moment(dateData).format("YYYY-MM-DD") : null;
                                          handleVariationChange(index, "sale_start", formattedDate);
                                        }}
                                        inputFormat="YYYY-MM-DD"
                                        renderInput={(params: any) => (
                                          <TextField
                                            size="small"
                                            {...params}
                                            fullWidth
                                          // error={Boolean(
                                          //   touched.sale_start && errors.sale_start
                                          // )}
                                          // helperText={
                                          //   Boolean(
                                          //     touched.sale_start && errors.sale_start
                                          //   ) &&
                                          //   String(
                                          //     touched.sale_start && errors.sale_start
                                          //   )
                                          // }
                                          />
                                        )}
                                      />
                                    </LocalizationProvider>
                                  </TableCell>
                                  <TableCell className="varDate">
                                    <LocalizationProvider dateAdapter={AdapterMoment}>
                                      <DatePicker
                                        value={variationData.sale_end}
                                        onChange={(dateData: any) => {
                                          const formattedDate = dateData ? moment(dateData).format("YYYY-MM-DD") : null;
                                          handleVariationChange(index, "sale_end", formattedDate);
                                        }}
                                        inputFormat="YYYY-MM-DD"
                                        renderInput={(params) => (
                                          <TextField
                                            size="small"
                                            {...params}
                                            fullWidth
                                          // error={Boolean(
                                          //   touched.sale_end && errors.sale_end
                                          // )}
                                          // helperText={
                                          //   Boolean(
                                          //     touched.sale_end && errors.sale_end
                                          //   ) &&
                                          //   String(
                                          //     touched.sale_end && errors.sale_end
                                          //   )
                                          // }
                                          />
                                        )}
                                      />
                                    </LocalizationProvider>
                                  </TableCell>
                                </TableRow>
                              ))
                            ) : "No data found"
                          ) : (
                            finalVariations?.map((variationData: any, index: any) => (
                              <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                  <input
                                    type="checkbox"
                                    id={index}
                                    name={variationData.id.toString()}
                                    onChange={(e) => handleVariationChange(index, "isChecked", e.target.checked)}
                                  />
                                </TableCell>
                                {getSingleProductData ?
                                  <>
                                    {variationData?.Variations.map((item: any, index: number) => (
                                      <TableCell>
                                        <TextField

                                          id=""
                                          variant="standard"
                                          value={item.VariationItem.name}
                                        // onChange={(e) =>
                                        //   handleVariationChange(index, "sku", e.target.value)
                                        // }
                                        />
                                      </TableCell>
                                    ))}
                                  </>
                                  :
                                  <>
                                    {getVariationData.map((varData: any) => {
                                      const varDataId = varData?.id;
                                      const matchingVariation = variationsData.find((variation) => variation === varData.name);
                                      const matchedData = groupedData[varDataId];
                                      const collectedIds: string[] = [];

                                      // Collect item.id values in the collectedIds array
                                      matchedData?.forEach((item: any) => {
                                        collectedIds.push(item.id);
                                      });

                                      return matchingVariation ? (
                                        <TableCell component="th" scope="row">
                                          {varData.name && (
                                            <Select
                                              labelId={`${varData.name}`}
                                              id={varData.name}
                                              // value={variationData.varData.name}
                                              variant={"standard"}
                                              onChange={(e) => {
                                                handleVariationChangeID(index, varData.name, e.target.value)
                                              }}
                                              sx={{ minWidth: "110px" }}
                                            >
                                              {matchedData?.map((item: any) => (
                                                <MenuItem key={item.id} value={item.id}>
                                                  {item.name}
                                                </MenuItem>
                                              ))}
                                            </Select>
                                          )}
                                        </TableCell>
                                      ) : null
                                    })}
                                  </>
                                }


                                <TableCell>
                                  <TextField
                                    id=""
                                    variant="standard"
                                    value={variationData?.sku}
                                    onChange={(e) =>
                                      handleVariationChange(index, "sku", e.target.value)
                                    }
                                  />
                                </TableCell>
                                <TableCell>
                                  <TextField
                                    id=""
                                    variant="standard"
                                    value={variationData?.product_id}
                                    onChange={(e) =>
                                      handleVariationChange(index, "product_id", e.target.value)
                                    }
                                  />
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  <Select
                                    value={variationData?.product_condition}
                                    variant={"standard"}
                                    onChange={(e) => handleVariationChange(index, "product_condition", e.target.value)}
                                    sx={{ minWidth: "110px" }}
                                  >
                                    {productCondition?.map((data: any) => (
                                      <MenuItem value={data.value}>{data.label}</MenuItem>
                                    ))}
                                  </Select>
                                </TableCell>
                                <TableCell>
                                  <TextField
                                    id=""
                                    variant="standard"
                                    value={variationData?.price}
                                    onChange={(e) =>
                                      handleVariationChange(index, "price", e.target.value)
                                    }
                                  />
                                </TableCell>
                                {getSingleProductData ?
                                  <>
                                    <TableCell>
                                      <Image src={variationData?.vrImages?.vrImages[0]} alt="variation image" height={50} width={50} />
                                    </TableCell>
                                  </> :
                                  <TableCell>
                                    <Select
                                      value={variationData?.vrImagesId}
                                      variant={"standard"}
                                      onChange={(e) => handleVariationChange(index, "vrImagesId", e.target.value)}
                                      sx={{ minWidth: "110px" }}
                                    >
                                      {VarImagesData?.map((data: any) => (
                                        <MenuItem value={data?.id}>{data?.vrName}</MenuItem>
                                      ))}
                                    </Select>
                                  </TableCell>
                                }
                                <TableCell>
                                  <TextField
                                    id=""
                                    variant="standard"
                                    value={variationData?.offer_condition}
                                    onChange={(e) =>
                                      handleVariationChange(index, "offer_condition", e.target.value)
                                    }
                                  />
                                </TableCell>
                                <TableCell>
                                  <TextField
                                    id=""
                                    variant="standard"
                                    value={variationData?.sale_price}
                                    onChange={(e) =>
                                      handleVariationChange(index, "sale_price", e.target.value)
                                    }
                                  />
                                </TableCell>
                                <TableCell className="varDate">
                                  <LocalizationProvider dateAdapter={AdapterMoment}>
                                    <DatePicker
                                      value={variationData.sale_start}
                                      onChange={(date: any) => {
                                        const formattedDate = date ? moment(date).format("YYYY-MM-DD") : null;
                                        handleVariationChange(index, "sale_start", formattedDate);
                                      }}
                                      inputFormat="YYYY-MM-DD"
                                      renderInput={(params: any) => (
                                        <TextField
                                          size="small"
                                          {...params}
                                          fullWidth
                                        // error={Boolean(
                                        //   touched.sale_start && errors.sale_start
                                        // )}
                                        // helperText={
                                        //   Boolean(
                                        //     touched.sale_start && errors.sale_start
                                        //   ) &&
                                        //   String(
                                        //     touched.sale_start && errors.sale_start
                                        //   )
                                        // }
                                        />
                                      )}
                                    />
                                  </LocalizationProvider>
                                </TableCell>
                                <TableCell className="varDate">
                                  <LocalizationProvider dateAdapter={AdapterMoment}>
                                    <DatePicker
                                      value={variationData.sale_end}
                                      onChange={(date: any) => {
                                        const formattedDate = date ? moment(date).format("YYYY-MM-DD") : null;
                                        handleVariationChange(index, "sale_end", formattedDate);
                                      }}
                                      inputFormat="YYYY-MM-DD"
                                      renderInput={(params) => (
                                        <TextField
                                          size="small"
                                          {...params}
                                          fullWidth
                                        // error={Boolean(
                                        //   touched.sale_end && errors.sale_end
                                        // )}
                                        // helperText={
                                        //   Boolean(
                                        //     touched.sale_end && errors.sale_end
                                        //   ) &&
                                        //   String(
                                        //     touched.sale_end && errors.sale_end
                                        //   )
                                        // }
                                        />
                                      )}
                                    />
                                  </LocalizationProvider>
                                </TableCell>
                              </TableRow>
                            ))
                          )}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </>
                }
                <Box width={"100%"}>
                  <Box display={"flex"} marginTop={"2rem"} gap={"1rem"} justifyContent={"end"}>
                    <Button variant="contained" color="inherit" onClick={handleBack}>
                      Back
                    </Button>

                    {getSingleProductData ?
                      <>
                        <Button variant="contained" color="success" onClick={updateVariationDataHandler}>
                          Update
                        </Button>
                        <Button onClick={handleNext} variant="contained">
                          Next
                        </Button>
                      </>
                      :
                      <Button variant="contained" onClick={addVariationDataHandler}>
                        Submit & Next
                      </Button>
                    }
                  </Box>
                </Box>
              </Box>
            )}

            {/* For Image */}
            {
              activeStep === 3 && (
                <Box>
                  <Box>
                    {/* "Master Images" Section */}
                    <Box className="display-flex-sb" style={{ marginBottom: "14px" }}>
                      <Box>{"Master Images"}</Box>
                      <Box style={{ width: "50%" }}>
                        <ImageAddViewBoxType
                          setStoreImage={setMasterImage}
                          storeImage={getSingleProductData?.image ? getSingleProductData?.image?.masterImages : masterImage}
                          imageLimit={1}
                          imageName="masterImage"
                          maxImageSize={1024 * 1024 * 2} // Set your max image size in bytes
                          allowedFileTypes={[".jpeg", ".png", ".jpg", ".webp"]} // Set your allowed file types
                        />
                      </Box>
                    </Box>

                    {/* "Ec-Site Images" Section */}
                    <Box className="display-flex-sb" style={{ marginBottom: "14px" }}>
                      <Box>{"Ec-Site Images"}</Box>
                      <Box style={{ width: "50%" }}>
                        <ImageAddViewBoxType
                          setStoreImage={setEcSiteImage}
                          storeImage={getSingleProductData?.image ? getSingleProductData?.image?.ecSiteImages : ecSiteImage}
                          imageLimit={5}
                          imageName="ecSiteImage"
                          maxImageSize={1024 * 1024 * 2} // Set your max image size in bytes
                          allowedFileTypes={[".jpeg", ".png", ".jpg", ".webp"]} // Set your allowed file types
                        />
                      </Box>
                    </Box>
                  </Box>
                  {VarImagesData && VarImagesData?.length > 0 &&
                    <>

                      <Typography textTransform={"capitalize"} display={"inline-block"} variant="h5">Variation Image</Typography>
                      {VarImagesData?.map((data: any) => (
                        <Box key={data?.vrName} className={'flex-between'} sx={{ display: "flex", alignItems: "center", marginBottom: "12px" }}>
                          <Typography textTransform={"capitalize"} display={"inline-block"}>{formatString(data?.vrName)} </Typography>
                          <Box className={'flex-between variation-img-input'} alignItems={'end'}>

                            {/* {isImage[data?.id.toString()] && ( */}

                            {selectedFiles[data?.id] && (
                              <img
                                id={`image-${data?.id}`}
                                alt={`${data?.vrName}`}
                                style={{ height: '50px', width: "80px", objectFit: "cover", borderRadius: "4px" }}
                              />
                            )}
                            <p></p>
                            <Box>
                              <Button
                                variant={'contained'}
                                sx={{ background: '#000', color: '#fff', marginRight: '10px' }}
                                onClick={() => {
                                  // setVarImgId(data?.id)
                                  const fileInput = document.getElementById(`${data?.id}`);
                                  if (fileInput) {
                                    fileInput.click();
                                  }
                                }}
                              >
                                Upload File <AddIcon />
                              </Button>
                              {console.log("data?.id", data?.id, VarImagesData)}
                              <input
                                type="file"
                                id={`${data?.id}`}
                                style={{ display: 'none' }}
                                onChange={(e: any) => changeFile(e, `${data?.id}`)}
                                accept="image/jpeg, image/png, image/gif, image/webp"
                              />
                              {data?.id &&
                                <>
                                  <Button
                                    variant="outlined"
                                    color="primary"
                                    size="small"
                                    onClick={() => handleSubmitProduct(data?.id)}
                                    disabled={imageSubmittedMap[data?.id]}
                                  >
                                    Submit
                                  </Button>
                                </>
                              }
                            </Box>
                          </Box>
                        </Box>
                      ))}
                    </>
                  }
                  {/* Submit Button */}
                  <Box width={"100%"}>
                    <Box sx={{ float: "right" }} marginTop={"1rem"} display={"flex"} gap={"12px"}>
                      <Button variant="contained" color="inherit" onClick={handleBack}>
                        Back
                      </Button>
                      {getSingleProductData &&
                        <Button variant="contained" color="success" onClick={handleSubmitMasterImage}>
                          Update
                        </Button>
                      }
                      {getSingleProductData ?
                        <Button variant="contained" onClick={handleNext}>
                          Next
                        </Button>
                        :
                        <Button variant="contained" sx={{ float: "right" }} onClick={handleSubmitMasterImage}>
                          Submit & Next
                        </Button>
                      }
                    </Box>
                  </Box>
                </Box>
              )
            }

            {/* For  Offer*/}
            {activeStep === 4 && (
              <Box sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                gap: "14px",
              }}>
                {fieldFormsText(
                  "Purchasing Price",
                  "purchasing_price",
                  "text",
                  "",
                  "",
                )}
                <Box
                  className="form-select-wrapper"
                >
                  <Box style={{ width: '25%' }}>
                    <Typography component="span" >
                      Tax Category
                    </Typography>
                  </Box>
                  <Box style={{ width: '50%' }}>
                    <TextField
                      {...getFieldProps('taxCategoryId')}
                      size="small"
                      fullWidth
                      select
                      name="taxCategoryId"
                      SelectProps={{
                        multiple: false,
                        value: formState?.taxCategoryId,
                        onChange: (event) => handleFieldChange(event, 'taxCategoryId'),
                      }}
                    >
                      {getCategoryData?.map((option: any) => (
                        <MenuItem key={option?.id} value={option?.id} onClick={() => setSelectTaxCategoryType(option?.percentage)}>
                          {option?.name_en}:{option?.percentage}%
                        </MenuItem>
                      ))}
                    </TextField>
                  </Box>
                </Box>
                {fieldFormsText(
                  "Price with Tax",
                  "price_with_tax",
                  "text",
                  "",
                  "",
                  true,
                )}

                {/* {fieldFormsText(
                  "Selling Price Category ",
                  "sellingPriceCategoryId",
                  "options",
                  "",
                  "",
                  false,
                  getSellingPriceCategory,
                  selectSellingPriceCategory,
                  setSelectSellingPriceCategory,
                  "server"
                )} */}
                <Box
                  className="form-select-wrapper"
                >
                  <Box style={{ width: '25%' }}>
                    <Typography component="span" >
                      Selling Price Category
                    </Typography>
                  </Box>
                  <Box style={{ width: '50%' }}>
                    <TextField
                      {...getFieldProps('sellingPriceCategoryId')}
                      size="small"
                      fullWidth
                      select
                      name="sellingPriceCategoryId"
                      SelectProps={{
                        multiple: false,
                        value: formState?.sellingPriceCategoryId,
                        onChange: (event) => handleFieldChange(event, 'sellingPriceCategoryId'),
                      }}
                    >
                      {getSellingPriceCategory?.map((option: any) => (
                        <MenuItem key={option?.id} value={option?.id}>
                          {option?.name}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Box>
                </Box>
                {fieldFormsText(
                  "Gross Margin Percentage",
                  "gross_margin_percentage",
                  "text",
                  "",
                  "",
                  true,
                )}
                {fieldFormsText(
                  "Estimated Price",
                  "estimated_price",
                  "text",
                  "",
                  "",
                  true,
                )}
                {fieldFormsText(
                  "Expected Price",
                  "expected_price",
                  "text",
                  "",
                  ""
                )}
                {fieldFormsText(
                  "Margin Price",
                  "margin_price",
                  "text",
                  "",
                  "",
                  true,
                )}
                {fieldFormsText(
                  "Previous Expected Price",
                  "previous_expected_price",
                  "text",
                  "",
                  ""
                )}
              </Box>
            )}

            {/* Fro Description */}
            {
              activeStep === 5 && (
                <Box sx={{
                  width: '100%',
                  display: 'flex',
                  flexDirection: 'column',
                  gap: "14px",
                }}>
                  {fieldFormsText(
                    "Product Description",
                    "description",
                    "text",
                    "",
                    ""
                  )}

                  <Box style={{ display: "flex", justifyContent: "space-between" }}>
                    <Typography>Key Product Features</Typography>
                    <Box style={{ display: "flex", flexDirection: "column", width: "50%" }}>
                      {features?.map((feature, index: any) => (
                        <Box key={index} style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                          <TextField
                            size="small"
                            value={feature}
                            style={{ marginBottom: "10px", width: "calc(100% - 56px)" }}
                            onChange={(e) => onChangeFeature(e, index)}
                            name={index}
                          />
                          <IconButton onClick={() => deleteFeature(index)}>
                            <DeleteIcon color="error" sx={{ fontSize: "2rem" }} />
                          </IconButton>
                        </Box>
                      ))}

                      <button
                        type="button"
                        onClick={addFeature}
                        style={{
                          marginBottom: "10px",
                          padding: "10px 0",
                          backgroundColor: "rgb(223 241 255)",
                          fontSize: "16px",
                          border: "none"
                        }}
                      >
                        Add Feature
                      </button>
                    </Box>
                  </Box>
                  {fieldFormsText(
                    "Legal Disclaimer Note",
                    "legal_disclaimer_note",
                    "textArea",
                    "",
                    ""
                  )}
                </Box>
              )
            }

            <Box style={{ marginTop: "30px" }}>
              <Box sx={{ display: "flex", justifyContent: "end", pt: 2 }}>
                {activeStep === steps.length - 1 ? (
                  <button type="button" className="blueButton" onClick={() => handleSubmitButton('patch')}>
                    Finish
                  </button>
                ) : (
                  ""
                )}
              </Box>
            </Box>
          </form >
        </FormikProvider >
        <Box sx={{ display: "flex", justifyContent: "end", pt: 2 }}>
          {activeStep !== 0 && activeStep !== 2 && activeStep !== 3 && (
            <Box
              className={
                activeStep === steps.length - 1 ? "finish-line-css" : ""
              }
            >
              <Button
                variant="contained"
                color="inherit"
                style={{
                  height: "35px",
                  borderRadius: "9px",
                  marginRight: "20px",
                }}
                onClick={handleBack}
              >
                Back
              </Button>
            </Box>
          )}
          {activeStep === steps.length - 1 ? (
            ""
          ) : (
            <>
              {activeStep !== 0 && activeStep !== 2 && activeStep !== 3 &&
                <Button
                  variant="contained"
                  style={{ height: "35px", borderRadius: "9px" }}
                  onClick={handleNext}
                >
                  Next
                </Button>
              }
            </>
          )}
        </Box>
      </Box >
    );
  };

  useEffect(() => {
    if (getVariationData) {
      getVariationData?.map((data: any) => {
        setVariationTypeValue((previousValue: any) => [
          ...previousValue,
          { getVariationData: data?.name, inputValue: "" },
        ]);
      });
    }
  }, [getVariationData]);

  useEffect(() => {
    if (selectTaxCategoryType && values.purchasing_price) {
      const percentage = parseFloat(selectTaxCategoryType);
      const value = (percentage / 100) * Number(values.purchasing_price) + Number(values.purchasing_price);
      setFieldValue("price_with_tax", value.toFixed(2));
    }
  }, [selectTaxCategoryType, values.purchasing_price]);


  // useEffect(() => {
  //   // if (selectTaxCategoryType === taxCategoryData?.[0]?.value) {
  //     const value = (selectTaxCategoryType / 100) * Number(values.purchasing_price);
  //   setFieldValue("price_with_tax", value.toFixed(2));


  //   // } else if (selectTaxCategoryType === taxCategoryData?.[1]?.value) {
  //   //   const value =
  //   //     (8 / 100) * Number(values.purchasing_price) +
  //   //     Number(values.purchasing_price);
  //   //   setFieldValue("price_with_tax", value.toFixed(2));
  //   // } else {
  //   //   (selectTaxCategoryType === taxCategoryData?.[2]?.value)
  //   //   const value =
  //   //     (10 / 100) * Number(values.purchasing_price) +
  //   //     Number(values.purchasing_price);
  //   //   setFieldValue("price_with_tax", value.toFixed(2));
  //   // }
  // }, [selectTaxCategoryType])

  // useEffect(() => {
  //   const gross_margin_percentage_value =
  //     (30 / 100) * Number(values.purchasing_price);
  //   setFieldValue("gross_margin_percentage", gross_margin_percentage_value.toFixed(2));

  //   const estimated_price_value =
  //     Number(values.purchasing_price) *
  //     (100 / (100 - Number(values.gross_margin_percentage_pre)));
  //   setFieldValue("estimated_price", estimated_price_value.toFixed(2));
  // }, [values.gross_margin_percentage_pre, values.purchasing_price]);

  useEffect(() => {

    // Ensure that gross_margin_percentage_pre is a valid number before using it in calculations
    const gross_margin_percentage_pre = Number(values.gross_margin_percentage_pre);
    console.log("gross_margin_percentage_pre", gross_margin_percentage_pre)
    const gross_margin_percentage_value =
      (gross_margin_percentage_pre / 100) * Number(values.purchasing_price);
    setFieldValue("gross_margin_percentage", gross_margin_percentage_value.toFixed(2));


    if (!isNaN(gross_margin_percentage_pre)) {
      const estimated_price_value =
        Number(values.purchasing_price) *
        (100 / (100 - gross_margin_percentage_pre));
      setFieldValue("estimated_price", estimated_price_value.toFixed(2));
    }
  }, [values.gross_margin_percentage_pre, values.purchasing_price]);


  useEffect(() => {
    if (values?.expected_price?.length > 0) {
      setFieldValue(
        "margin_price",
        (Number(values.expected_price) - Number(values.purchasing_price)).toFixed(2)
      );
    }
  }, [values.expected_price]);

  useEffect(() => {
    if (variationPostData) {
      setVariationTypeValue(
        variationTypeValue?.map((inputvalue: any, index: number) =>
          selectedIndex === index
            ? {
              ...inputvalue,
              inputValue: [...inputvalue.inputValue, variationPostData?.name],
              inputId: [variationPostData?.id],
              variationTypeValue: "",
            }
            : { ...inputvalue }
        )
      );
    }
  }, [variationTypeValue]);

  return (
    <Box>
      <DetailsTopView
        headerName="Master Product"
        handleButtonChange={handleChangeButton}
        isBackBtn={true}
        buttonText="Back"
        searchField={false}
      />
      {GetSingleMasterProductLoading ?
        <Box display="flex" justifyContent="center" alignItems="center" height="200px">
          <CircularProgress />
        </Box>
        :
        <>
          <Stepper activeStep={activeStep} className="box-kyc-multiple-step-form">
            {steps.map((label, index) => {
              const stepProps: { completed?: boolean } = {};
              const labelProps: {
                optional?: React.ReactNode;
              } = {};
              if (isStepSkipped(index)) {
                stepProps.completed = false;
              }
              return (
                <Step key={label} {...stepProps}>
                  <StepLabel {...labelProps}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
          {activeStep === steps.length ? (
            <>
              <Typography sx={{ mt: 2, mb: 1 }}>
                All steps completed - you&apos;re finished
              </Typography>
              <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                <Box sx={{ flex: "1 1 auto" }} />
                <Button onClick={handleReset}>Reset</Button>
              </Box>
            </>
          ) : (
            <>
              <Box className="form-view-multistep-form box-kyc-multiple-step-form card-wrapper">
                {displayForms(activeStep)}
              </Box>
            </>
          )}
        </>
      }
    </Box>
  );
}