// build in library
import Image from "next/image";
import { useEffect, useState } from "react";

// MUI Components
import { Box, Button, Checkbox, FormControlLabel, FormGroup, Modal, TextField } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

// Third party library
import moment from "moment";
import { toast } from "react-toastify";

// Assets
import EditIcon from "../../../../public/editIcon.svg";
import ViewIcon from "../../../../public/viewIcon.svg";

// Redux
import { useGetMasterCategoryDataQuery, useGetVariationDataQuery, usePatchUpdateVariationMutation, usePostAddVariationMutation } from "../../../../redux/api/user/user";

// custom components
import { styleForModal } from "../../../common/commonData";
import { businessId } from "../../../common/utility/Constants";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const AddVariation = () => {
    const modalStyle = styleForModal;
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPageSize, setCurrentPageSize] = useState(10);
    const [submitType, setSubmitType] = useState<string>("Submit")
    const [variationData, setVariationData] = useState<any>([]);
    const [openAddBrandModel, setOpenAddBrandModel] = useState<boolean>(false);
    const [manufacturerValue, setManufacturerValue] = useState<string>("");
    const [manufacturerId, setManufacturerId] = useState<string>("")
    const [selectedCategoryIds, setSelectedCategoryIds] = useState<number[]>([]);

    const [AddVariation, { data: AddVariationData }] = usePostAddVariationMutation();
    const [UpdateVariation, { data: updateVariationData }] = usePatchUpdateVariationMutation();
    const { data: categoryDetailsData } = useGetMasterCategoryDataQuery(businessId);
    const { data: variationDetails, refetch, isSuccess } = useGetVariationDataQuery({ id: businessId, page: currentPage, pageSize: currentPageSize });
    console.log("variationDetails", variationDetails);

    useEffect(() => {
        refetch({ page: currentPage, pageSize: currentPageSize });
    }, [currentPage, currentPageSize, refetch]);

    function handleChangeButton() {
        setSubmitType("Submit")
        setOpenAddBrandModel(true)
    }
    const handleOnCellClick = () => {

    }

    const handleCheckboxChange = (categoryId: number) => {
        const isSelected = selectedCategoryIds.includes(categoryId);
        if (isSelected) {
            setSelectedCategoryIds((prevIds) => prevIds.filter((id) => id !== categoryId));
        } else {
            setSelectedCategoryIds((prevIds) => [...prevIds, categoryId]);
        }
    };

    const handleSubmitAddManufacture = () => {
        if (submitType === "Submit") {
            let body = { name: manufacturerValue, businessKycId: businessId, categoryId: selectedCategoryIds }
            AddVariation(body)
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.data?.error)
                })
        } else {
            let body = { name: manufacturerValue };
            UpdateVariation({ body, manufacturerId })
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.data?.error)
                })
        }
    }

    const handleEditBrand = (values: any) => {
        setOpenAddBrandModel(true);
        setSubmitType("Update");
        setManufacturerValue(values.row?.name_en);
        setManufacturerId(values?.row?.id)
    }


    const columns = [
        {
            field: 'S.No',
            headerName: 'S.No',
            headerClassName: "data-grid-header-classname",
            minWidth: 100,
            maxWidth: 200,
            flex: 1,
            renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
        },
        {
            field: 'name',
            headerName: 'Variation Name',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'date',
            headerName: 'Date',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
            renderCell: (params: any) => (
                <div>{moment(params.createdAt).format("YYYY-MM-DD")}</div>
            )
        },
        {
            field: "Action",
            headerName: "Action",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => (
                <strong style={{ display: "flex", justifyContent: "space-between" }}>
                    {/* <div style={{ marginTop: "8px", marginLeft: "16px", cursor: "pointer" }}> */}
                    <Image src={ViewIcon} style={{ cursor: "pointer" }} />
                    <Image src={EditIcon} style={{ marginLeft: "10px", cursor: "pointer" }} onClick={() => { handleEditBrand(params) }} />
                    {/* </div> */}
                </strong>
            ),
            minWidth: 150,
            flex: 1,
        }
    ]
    useEffect(() => {
        if (variationDetails) {
            setVariationData(variationDetails)
        }
    }, [variationDetails])
    useEffect(() => {
        if (AddVariationData) {
            toast.success("Successfully created variation");
            setOpenAddBrandModel(false);
            refetch()
        }
    }, [AddVariationData])
    useEffect(() => {
        if (updateVariationData) {
            toast.success("Successfully updated variation");
            setOpenAddBrandModel(false);
            refetch()
        }
    }, [updateVariationData])

    return (
        <>
            <DetailsTopView headerName="Variation" handleButtonChange={handleChangeButton} buttonText="Add Variation" searchField={false} />
            <div className="staff-view-table">
                <Box sx={{ height: 600, width: '100%' }}>
                    <DataGrid
                        rows={variationData?.data || []}
                        columns={columns}
                        rowCount={variationData?.totalCount}
                        pageSize={currentPageSize}
                        page={currentPage - 1}
                        onPageSizeChange={(newPageSize) => {
                            setCurrentPageSize(newPageSize);
                            setCurrentPage(1);
                        }}
                        onPageChange={(newPage) => {
                            setCurrentPage(newPage + 1);
                        }}
                        rowsPerPageOptions={[5, 10, 15]}
                        paginationMode="server"
                        onCellClick={handleOnCellClick}
                        loading={isSuccess ? false : true}
                    />
                </Box>
            </div>
            {openAddBrandModel &&
                <Modal
                    open={openAddBrandModel}
                    onClose={() => setOpenAddBrandModel(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div>
                            <h3 className="no-padding-margin modal-field-title">Variation Name</h3>
                            <FormGroup>
                                {categoryDetailsData &&
                                    categoryDetailsData.map((categoryData: any) => (
                                        <FormControlLabel
                                            key={categoryData.id}
                                            control={
                                                <Checkbox
                                                    checked={selectedCategoryIds.includes(categoryData.id)}
                                                    onChange={() => handleCheckboxChange(categoryData.id)}
                                                />
                                            }
                                            label={categoryData.name_en}
                                        />
                                    ))}
                            </FormGroup>
                            <TextField type="text" size="small" fullWidth value={manufacturerValue} onChange={(e: any) => { setManufacturerValue(e.target.value) }} />
                        </div>
                        <div style={{ marginTop: "20px" }} className="display-flex-end">
                            <Button variant="outlined" onClick={() => { handleSubmitAddManufacture() }}>{submitType}</Button>
                        </div>
                    </Box>
                </Modal>
            }
        </>
    )
}