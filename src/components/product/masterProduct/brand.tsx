import {
  Box,
  Button,
  FormControl,
  Modal,
  TextField,
  TextareaAutosize
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { useFormik } from "formik";
import moment from "moment";
import Image from "next/image";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import * as yup from "yup";
import EditIcon from "../../../../public/editIcon.svg";
import ViewIcon from "../../../../public/viewIcon.svg";
import {
  useGetBrandDataLimitQuery,
  usePatchUpdateBrandMutation,
  usePostAddBrandMutation
} from "../../../../redux/api/user/user";
import { styleForModal } from "../../../common/commonData";
import { businessId, userId } from "../../../common/utility/Constants";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const AddBrand = () => {
  const [submitType, setSubmitType] = useState<string>("Submit");
  const [brandData, setBrandData] = useState<any>([]);
  const [openAddBrandModel, setOpenAddBrandModel] = useState<boolean>(false);
  const [brandDescriptionValue, setBrandDescriptionValue] = useState<string>("");
  const [brandValue, setBrandValue] = useState<string>("");
  const [brandId, setBrandId] = useState<string>("");
  const [currentPage, setCurrentPage] = useState(1);
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const [AddBrand, { data: addBrandData }] = usePostAddBrandMutation();
  const [UpdateBrand, { data: updateBrandData }] = usePatchUpdateBrandMutation();
  const { data: brandDetails, refetch, isSuccess, isLoading } = useGetBrandDataLimitQuery({ id: businessId, page: currentPage, pageSize: currentPageSize });


  function handleChangeButton() {
    setSubmitType("Submit");
    setOpenAddBrandModel(true);
  }
  const validationSchema = yup.object({
    brandValue: yup.string().required("Brand is required"),
    brandDescriptionValue: yup.string().required("Brand Description is required"),
  });

  const formik = useFormik({
    initialValues: {
      brandValue: "",
      brandDescriptionValue: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (submitType === "Submit") {
        let body = {
          name_en: values.brandValue, // Accessing brandValue from formik.values
          description: values.brandDescriptionValue, // Accessing brandDescriptionValue from formik.values
          businessKycId: businessId,
          createdBy: userId,
        };
        AddBrand(body)
          .unwrap()
          .then(() => {
            setOpenAddBrandModel(false);
            refetch();
          })
          .catch((error: any) => {
            toast.error(error?.data?.error || error?.data?.message);
          });
      } else {
        let body = {
          name_en: values.brandValue, // Accessing brandValue from formik.values
          description: values.brandDescriptionValue, // Accessing brandDescriptionValue from formik.values
        };
        UpdateBrand({ body, brandId })
          .unwrap()
          .catch((error: any) => {
            toast.error(error?.data.message);
          });
      }
    },
  });

  const handleOnCellClick = () => { };
  // const handleSubmitAddBrand = () => {
  //   if (submitType === "Submit") {
  //     let body = {
  //       name_en: brandValue,
  //       description: brandDescriptionValue,
  //       businessKycId: businessId,
  //       createdBy: duserId,
  //     };
  //     AddBrand(body)
  //       .unwrap()
  //       .then(() => {
  //         setOpenAddBrandModel(false);
  //         refetch();
  //       })
  //       .catch((error: any) => {
  //         toast.error(error?.data?.error || error?.data?.message);
  //       });
  //   } else {
  //     let body = { name_en: brandValue, description: brandDescriptionValue };
  //     UpdateBrand({ body, brandId })
  //       .unwrap()
  //       .catch((error: any) => {
  //         toast.error(error?.data.message);
  //       });
  //   }
  // };

  const handleEditBrand = (values: any) => {
    console.log("values", values)
    setOpenAddBrandModel(true);
    setSubmitType("Update");
    setBrandValue(values.row?.name_en);
    setBrandDescriptionValue(values.row?.description);
    setBrandId(values?.row?.id);
  };

  const modalStyle = styleForModal;
  const columns = [
    {
      field: "S.No",
      headerName: "S.No",
      headerClassName: "data-grid-header-classname",
      minWidth: 100,
      flex: 1,
      renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
    },
    {
      field: "name_en",
      headerName: "Brand Name",
      headerClassName: "data-grid-header-classname",
      minWidth: 300,
      flex: 1,
    },
    {
      field: "date",
      headerName: "Date",
      headerClassName: "data-grid-header-classname",
      minWidth: 300,
      flex: 1,
      renderCell: (params: any) => (
        <div>{moment(params.createdAt).format("YYYY-MM-DD")}</div>
      ),
    },
    {
      field: "description",
      headerName: "Description",
      headerClassName: "data-grid-header-classname",
      minWidth: 400,
      flex: 1,
    },
    {
      field: "Action",
      headerName: "Action",
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <strong style={{ display: "flex", justifyContent: "space-between" }}>
          {/* <div style={{ marginTop: "8px", marginLeft: "16px", cursor: "pointer" }}> */}
          <Image src={ViewIcon} style={{ cursor: "pointer" }} />
          <Image
            src={EditIcon}
            style={{ marginLeft: "10px", cursor: "pointer" }}
            onClick={() => {
              handleEditBrand(params);
            }}
          />
          {/* </div> */}
        </strong>
      ),
      minWidth: 300,
      flex: 1,
    },
  ];
  useEffect(() => {
    refetch({ page: currentPage, pageSize: currentPageSize });
  }, [currentPage, currentPageSize, refetch]);

  useEffect(() => {
    if (brandDetails) {
      setBrandData(brandDetails);
    }
  }, [brandDetails]);
  useEffect(() => {
    if (addBrandData) {
      toast.success("Successfully created brand");
      setOpenAddBrandModel(false);
      refetch();
    }
  }, [addBrandData]);
  useEffect(() => {
    if (updateBrandData) {
      toast.success("Successfully updated brand");
      setOpenAddBrandModel(false);
      refetch();
    }
  }, [updateBrandData]);

  return (
    <>
      <DetailsTopView
        headerName="Brand"
        handleButtonChange={handleChangeButton}
        buttonText="Add Brand"
        searchField={false}
      />
      <div className="staff-view-table">
        <Box sx={{ height: 600, width: "100%" }}>
          <DataGrid
            rows={brandData?.data || []}
            columns={columns}
            rowCount={brandData?.totalCount}
            pageSize={currentPageSize}
            page={currentPage - 1}
            onPageSizeChange={(newPageSize) => {
              setCurrentPageSize(newPageSize);
              setCurrentPage(1);
            }}
            onPageChange={(newPage) => {
              setCurrentPage(newPage + 1);
            }}
            rowsPerPageOptions={[5, 10, 15]}
            paginationMode="server"
            onCellClick={handleOnCellClick}
            loading={isSuccess ? false : true}
          />
        </Box>
      </div>
      {openAddBrandModel && (
        <Modal
          open={openAddBrandModel}
          onClose={() => setOpenAddBrandModel(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={modalStyle}>
            <div style={{ marginBottom: "20px" }}>
              <h3 className="no-padding-margin modal-field-title">
                Brand Name
              </h3>
              <TextField
                type="text"
                size="small"
                fullWidth
                name="brandValue"
                value={formik.values.brandValue}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              {formik.touched.brandValue && formik.errors.brandValue && (
                <div style={{ color: "red" }}>{formik.errors.brandValue}</div>
              )}
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h3 className="no-padding-margin modal-field-title">
                Brand Description
              </h3>
              <FormControl fullWidth>
                <TextareaAutosize
                  minRows={3}
                  name="brandDescriptionValue"
                  value={formik.values.brandDescriptionValue}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  style={{ borderRadius: "6px", borderColor: "#cccccc", padding: "8px" }}
                />
                {formik.touched.brandDescriptionValue && formik.errors.brandDescriptionValue && (
                  <div style={{ color: "red" }}>{formik.errors.brandDescriptionValue}</div>
                )}
              </FormControl>
            </div>
            <Box className="display-flex-end" gap={3}>
              <Button
                variant="outlined"
                onClick={() => setOpenAddBrandModel(false)}
                style={{ color: "#007bff" }}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                onClick={(e: any) => {
                  e.preventDefault();
                  formik.handleSubmit(e);
                }}
              // style={{ backgroundColor: "#007bff", color: "#ffffff", marginRight: "10px" }}
              >
                {submitType}
              </Button>
            </Box>
          </Box>
        </Modal>
      )}
    </>
  );
};
