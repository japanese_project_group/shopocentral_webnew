// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// MUI components
import { Box, Button, Modal, TextField } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

// third party library
import moment from "moment";
import { toast } from "react-toastify";

// assets
import EditIcon from "../../../../public/editIcon.svg";
import ViewIcon from "../../../../public/viewIcon.svg";

// Redux
import { useGetManufactureDataListQuery, usePatchUpdateManufactureMutation, usePostAddManufactureMutation } from "../../../../redux/api/user/user";

// custom components
import { styleForModal } from "../../../common/commonData";
import { businessId, userId } from "../../../common/utility/Constants";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const AddManufacture = () => {
    const router = useRouter();
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPageSize, setCurrentPageSize] = useState(10);
    const [submitType, setSubmitType] = useState<string>("Submit")
    const [brandData, setBrandData] = useState<any>([]);
    const [openAddBrandModel, setOpenAddBrandModel] = useState<boolean>(false);
    const [manufacturerValue, setManufacturerValue] = useState<string>("");
    const [manufacturerId, setManufacturerId] = useState<string>("")
    const [AddBrand, { data: addBrandData }] = usePostAddManufactureMutation();
    const [UpdateBrand, { data: updateBrandData }] = usePatchUpdateManufactureMutation();
    const { data: brandDetails, refetch, isSuccess } = useGetManufactureDataListQuery({ id: businessId, page: currentPage, pageSize: currentPageSize });

    useEffect(() => {
        refetch({ page: currentPage, pageSize: currentPageSize });
    }, [currentPage, currentPageSize, refetch]);

    function handleChangeButton() {
        setSubmitType("Submit")
        setOpenAddBrandModel(true)
    }
    const handleOnCellClick = () => {

    }
    const handleSubmitAddManufacture = () => {
        if (submitType === "Submit") {
            let body = { name_en: manufacturerValue, businessKycId: businessId, createdBy: userId }
            AddBrand(body)
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.data.error)
                })
        } else {
            let body = { name_en: manufacturerValue };
            UpdateBrand({ body, manufacturerId })
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.data.error)
                })
        }
    }

    const handleEditBrand = (values: any) => {
        setOpenAddBrandModel(true);
        setSubmitType("Update");
        setManufacturerValue(values.row?.name_en);
        setManufacturerId(values?.row?.id)
    }
    const modalStyle = styleForModal;
    const columns = [
        {
            field: 'S.No',
            headerName: 'S.No',
            headerClassName: "data-grid-header-classname",
            minWidth: 100,
            maxWidth: 200,
            flex: 1,
            renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
        },
        {
            field: 'name_en',
            headerName: 'Manufacture Name',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'date',
            headerName: 'Date',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
            renderCell: (params: any) => (
                <div>{moment(params.createdAt).format("YYYY-MM-DD")}</div>
            )
        },
        {
            field: "Action",
            headerName: "Action",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => (
                <strong style={{ display: "flex", justifyContent: "space-between" }}>
                    <Image src={ViewIcon} style={{ cursor: "pointer" }} />
                    <Image src={EditIcon} style={{ marginLeft: "10px", cursor: "pointer" }} onClick={() => { handleEditBrand(params) }} />
                </strong>
            ),
            minWidth: 150,
            flex: 1,
        }
    ]
    useEffect(() => {
        if (brandDetails) {
            setBrandData(brandDetails)
        }
        refetch();
    }, [brandDetails, businessId])

    console.log("manufacture", brandDetails);

    useEffect(() => {
        if (addBrandData) {
            toast.success("Successfully created brand");
            setOpenAddBrandModel(false);
            refetch()
        }
    }, [addBrandData])
    useEffect(() => {
        if (updateBrandData) {
            toast.success("Successfully updated brand");
            setOpenAddBrandModel(false);
            refetch()
        }
    }, [updateBrandData])

    return (
        <>
            <DetailsTopView headerName="Manufacture" handleButtonChange={handleChangeButton} buttonText="Add Manufacture" searchField={false} />
            <div className="staff-view-table">
                <Box sx={{ height: 600, width: '100%' }}>
                    <DataGrid
                        rows={brandData?.data || []}
                        columns={columns}
                        rowCount={brandData?.totalCount}
                        pageSize={currentPageSize}
                        page={currentPage - 1}
                        onPageSizeChange={(newPageSize) => {
                            setCurrentPageSize(newPageSize);
                            setCurrentPage(1);
                        }}
                        onPageChange={(newPage) => {
                            setCurrentPage(newPage + 1);
                        }}
                        rowsPerPageOptions={[5, 10, 15]}
                        paginationMode="server"
                        onCellClick={handleOnCellClick}
                        loading={isSuccess ? false : true}
                    />
                </Box>
            </div>
            {openAddBrandModel &&
                <Modal
                    open={openAddBrandModel}
                    onClose={() => setOpenAddBrandModel(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div>
                            <h3 className="no-padding-margin modal-field-title">Manufacture Name</h3>
                            <TextField type="text" size="small" fullWidth value={manufacturerValue} onChange={(e: any) => { setManufacturerValue(e.target.value) }} />
                        </div>
                        <div style={{ marginTop: "20px" }} className="display-flex-end">
                            <Button variant="outlined" onClick={() => { handleSubmitAddManufacture() }}>{submitType}</Button>
                        </div>
                    </Box>
                </Modal>
            }
        </>
    )
}