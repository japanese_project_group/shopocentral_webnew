// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// third party library
import moment from "moment";
import { toast } from "react-toastify";

// MUI Components
import { Box, Button, Modal, TextField } from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";

// assets
import EditIcon from "../../../../public/editIcon.svg";
import ViewIcon from "../../../../public/viewIcon.svg";

// components
import { styleForModal } from "../../../common/commonData";
import { DetailsTopView } from "../../customComponents/detailsTopView";

// redux
import { businessId } from "src/common/utility/Constants";
import { useGetMasterCategoryDataLimitQuery, usePatchUpdateVariationMutation, usePostAddMasterCategoryMutation } from "../../../../redux/api/user/user";

export const AddCategory = () => {
    const router = useRouter();
    const routerPath = router.query.category;
    const [submitType, setSubmitType] = useState<string>("Submit")
    const [categoryData, setCategoryData] = useState<any>([]);
    const [openAddCategoryModel, setOpenAddCategoryModel] = useState<boolean>(false);
    const [categoryValue, setCategoryValue] = useState<string>("");
    const [categoryId, setCategoryId] = useState<string>("")
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPageSize, setCurrentPageSize] = useState(10);

    const [AddCategory, { data: addCategoryData }] = usePostAddMasterCategoryMutation();
    const [UpdateCategory, { data: updateCategoryData }] = usePatchUpdateVariationMutation();

    const { data: categoryDetails, isSuccess, refetch } = useGetMasterCategoryDataLimitQuery({ id: businessId, page: currentPage, pageSize: currentPageSize });


    useEffect(() => {
        refetch({ page: currentPage, pageSize: currentPageSize });
    }, [currentPage, currentPageSize, refetch]);

    console.log("categoryData", categoryData, categoryDetails)

    function handleChangeButton() {
        setSubmitType("Submit")
        setOpenAddCategoryModel(true)
    }
    const handleOnCellClick = () => {

    }
    const handleSubmitAddCategory = () => {
        if (submitType === "Submit") {
            let body = {
                name_en: categoryValue,
                businessKycId: businessId
            }
            AddCategory(body)
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.message?.[0])
                })
        } else {
            let body = { name: categoryValue };
            UpdateCategory({ body, categoryId })
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.message?.[0])
                })
        }
    }

    const handleEditCategory = (values: any) => {
        setOpenAddCategoryModel(true);
        setSubmitType("Update");
        setCategoryValue(values.row?.name_en);
        setCategoryId(values?.row?.id)
    }
    const modalStyle = styleForModal;

    useEffect(() => {
        if (categoryDetails) {
            setCategoryData(categoryDetails);
        }
    }, [categoryDetails]);


    useEffect(() => {
        if (addCategoryData) {
            toast.success("Successfully created Category");
            setOpenAddCategoryModel(false);
            refetch()
        }
    }, [addCategoryData])

    useEffect(() => {
        if (updateCategoryData) {
            toast.success("Successfully updated category");
            setOpenAddCategoryModel(false);
            refetch()
        }
    }, [updateCategoryData])

    const columns: GridColDef[] = [
        {
            field: 'S.No',
            headerName: 'S.No',
            headerClassName: "data-grid-header-classname",
            minWidth: 100,
            flex: 1,
            maxWidth: 150,
            renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
        },
        {
            field: 'name_en',
            headerName: 'Category Name',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'date',
            headerName: 'Date',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
            renderCell: (params: any) => (
                <div>{moment(params.createdAt).format("YYYY-MM-DD")}</div>
            )
        },
        {
            field: "Action",
            headerName: "Action",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => (
                <strong style={{ display: "flex", justifyContent: "space-between" }}>
                    <Image src={ViewIcon} style={{ cursor: "pointer" }} />
                    <Image src={EditIcon} style={{ marginLeft: "10px", cursor: "pointer" }} onClick={() => { handleEditCategory(params) }} />
                </strong>
            ),
            minWidth: 150,
            flex: 1,
        }
    ]

    return (
        <>
            <DetailsTopView headerName={routerPath === "ec-category" ? "Ec Category" : "Category"} handleButtonChange={handleChangeButton} buttonText={routerPath === "ec-category" ? "Add Ec Category" : "Add Category"} searchField={false} />
            <div className="staff-view-table">
                <Box sx={{ height: 600, width: '100%' }}>
                    <DataGrid
                        rows={categoryData?.data || []}
                        columns={columns}
                        rowCount={categoryData?.totalCount}
                        pageSize={currentPageSize}
                        page={currentPage - 1}
                        onPageSizeChange={(newPageSize) => {
                            setCurrentPageSize(newPageSize);
                            setCurrentPage(1);
                        }}
                        onPageChange={(newPage) => {
                            setCurrentPage(newPage + 1);
                        }}
                        rowsPerPageOptions={[5, 10, 15]}
                        paginationMode="server"
                        onCellClick={handleOnCellClick}
                        loading={isSuccess ? false : true}
                    />
                </Box>
            </div>
            {openAddCategoryModel &&
                <Modal
                    open={openAddCategoryModel}
                    onClose={() => setOpenAddCategoryModel(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div>
                            <h3 className="no-padding-margin modal-field-title">{"Category Name"}</h3>
                            <TextField type="text" size="small" fullWidth value={categoryValue} onChange={(e: any) => { setCategoryValue(e.target.value) }} />
                        </div>
                        <div style={{ marginTop: "20px" }} className="display-flex-end">
                            <Button variant="outlined" onClick={() => { handleSubmitAddCategory() }}>{submitType}</Button>
                        </div>
                    </Box>
                </Modal>
            }
        </>
    )
}