// build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// third party library
import { isEmpty } from "lodash";
import moment from "moment";
import { toast } from "react-toastify";

// MUI Components
import { Box, Button, Modal, TextField } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

// assets
import EditIcon from "../../../../public/editIcon.svg";
import ViewIcon from "../../../../public/viewIcon.svg";

// components
import { styleForModal } from "../../../common/commonData";
import { DetailsTopView } from "../../customComponents/detailsTopView";

// redux
import { useGetMasterECCategoryDataListQuery, usePatchUpdateVariationMutation, usePostAddMasterECCategoryMutation } from "../../../../redux/api/user/user";

export const AddEcCategory = () => {
    const router = useRouter();
    const [submitType, setSubmitType] = useState<string>("Submit")
    const [ecCategoryData, setEcCategoryData] = useState<any>([]);
    const [openAddEcCategoryModel, setOpenAddEcCategoryModel] = useState<boolean>(false);
    const [ecCategoryValue, setEcCategoryValue] = useState<string>("");
    const [ecCategoryId, setEcCategoryId] = useState<string>("")
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPageSize, setCurrentPageSize] = useState(10);

    const [AddEcCategory, { data: addEcCategoryData }] = usePostAddMasterECCategoryMutation();
    const [UpdateEcCategory, { data: updateEcCategoryData }] = usePatchUpdateVariationMutation();
    const { data: ecCategoryDetails, isSuccess, refetch } = useGetMasterECCategoryDataListQuery({ page: currentPage, pageSize: currentPageSize });

    useEffect(() => {
        refetch({ page: currentPage, pageSize: currentPageSize });
    }, [currentPage, currentPageSize, refetch]);

    function handleChangeButton() {
        setSubmitType("Submit")
        setOpenAddEcCategoryModel(true)
    }
    const handleOnCellClick = () => {

    }
    const handleSubmitAddEcCategory = () => {
        if (submitType === "Submit") {
            let body = { name_en: ecCategoryValue }
            AddEcCategory(body)
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.message?.[0])
                })
        } else {
            let body = { name: ecCategoryValue };
            UpdateEcCategory({ body, ecCategoryId })
                .unwrap()
                .catch((error: any) => {
                    toast.error(error?.message?.[0])
                })
        }
    }

    const handleEditCategory = (values: any) => {
        setOpenAddEcCategoryModel(true);
        setSubmitType("Update");
        setEcCategoryValue(values.row?.name_en);
        setEcCategoryId(values?.row?.id)
    }
    const modalStyle = styleForModal;

    useEffect(() => {
        if (!isEmpty(ecCategoryDetails)) {
            setEcCategoryData(ecCategoryDetails)
        }
    }, [ecCategoryDetails])


    useEffect(() => {
        if (addEcCategoryData) {
            toast.success("Successfully created Ec-Category");
            setOpenAddEcCategoryModel(false);
            refetch()
        }
    }, [addEcCategoryData])

    useEffect(() => {
        if (updateEcCategoryData) {
            toast.success("Successfully updated Ec-Category");
            setOpenAddEcCategoryModel(false);
            refetch()
        }
    }, [updateEcCategoryData])

    const columns = [
        {
            field: 'S.No',
            headerName: 'S.No',
            headerClassName: "data-grid-header-classname",
            minWidth: 100,
            flex: 1,
            maxWidth: 200,
            renderCell: (index: any) => index?.api.getRowIndex(index?.row?.id) + 1,
        },
        {
            field: 'name_en',
            headerName: 'Category Name',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'date',
            headerName: 'Date',
            headerClassName: "data-grid-header-classname",
            minWidth: 200,
            flex: 1,
            renderCell: (params: any) => (
                <div>{moment(params.createdAt).format("YYYY-MM-DD")}</div>
            )
        },
        {
            field: "Action",
            headerName: "Action",
            headerClassName: "data-grid-header-classname",
            renderCell: (params: any) => (
                <strong style={{ display: "flex", justifyContent: "space-between" }}>
                    <Image src={ViewIcon} style={{ cursor: "pointer" }} />
                    <Image src={EditIcon} style={{ marginLeft: "10px", cursor: "pointer" }} onClick={() => { handleEditCategory(params) }} />
                </strong>
            ),
            minWidth: 150,
            flex: 1,
        }
    ]

    return (
        <>
            <DetailsTopView headerName={"Ec Category"} handleButtonChange={handleChangeButton} buttonText={"ec-category"} searchField={false} />
            <div className="staff-view-table">
                <Box sx={{ height: 600, width: '100%' }}>
                    <DataGrid
                        rows={ecCategoryData?.data || []}
                        columns={columns}
                        rowCount={ecCategoryData?.totalCount}
                        pageSize={currentPageSize}
                        page={currentPage - 1}
                        onPageSizeChange={(newPageSize) => {
                            setCurrentPageSize(newPageSize);
                            setCurrentPage(1);
                        }}
                        onPageChange={(newPage) => {
                            setCurrentPage(newPage + 1);
                        }}
                        rowsPerPageOptions={[5, 10, 15]}
                        paginationMode="server"
                        onCellClick={handleOnCellClick}
                        loading={isSuccess ? false : true}
                    />
                </Box>
            </div>
            {openAddEcCategoryModel &&
                <Modal
                    open={openAddEcCategoryModel}
                    onClose={() => setOpenAddEcCategoryModel(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={modalStyle}>
                        <div>
                            <h3 className="no-padding-margin modal-field-title">{"Category Name"}</h3>
                            <TextField type="text" size="small" fullWidth value={ecCategoryValue} onChange={(e: any) => { setEcCategoryValue(e.target.value) }} />
                        </div>
                        <div style={{ marginTop: "20px" }} className="display-flex-end">
                            <Button variant="outlined" onClick={() => { handleSubmitAddEcCategory() }}>{submitType}</Button>
                        </div>
                    </Box>
                </Modal>
            }
        </>
    )
}