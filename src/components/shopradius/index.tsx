// import { StaffTableView } from "./staffTableView"

import { Box, Button, TextField, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { DetailsTopView } from "../customComponents/detailsTopView";

export const ShopRadiusComponent = () => {
    const router = useRouter();
    // const [openSearchAddStaff, setOpenSearchStaff] = useState<boolean>(false)
    function handleChangeButton() {
        router.push("/");
    }
    return (
        <div>
            <DetailsTopView
                headerName="Shop Radius"
                isButton={false}
            // handleButtonChange={handleChangeButton}
            />
            <Box padding={"4rem 2rem"} bgcolor={"#fff"} borderRadius={"2rem"} boxShadow={2} >
                <Box className="display-flex" sx={{ gap: "1rem" }} justifyContent={"center"} alignItems={"center"}>
                    <Box display={"flex"} alignItems={"center"}>
                        <Typography fontWeight={"600"} className="no-padding-margin">
                            Set Nearby Shop Radius
                        </Typography>
                        <TextField
                            // key={index}
                            size="small"
                            style={{ marginLeft: '10px' }}
                            name="nearbyShop"
                        // value={""}
                        // onChange={ }
                        />   <span style={{ marginLeft: "10px", fontStyle: "italic" }}>
                            KM
                        </span>
                    </Box>
                    <Button
                        variant="contained"
                        color="primary"
                        style={{ height: '40px', marginLeft: '5px', width: "200px" }}
                    >
                        Submit
                    </Button>
                </Box>
            </Box>
        </div >
    )
}