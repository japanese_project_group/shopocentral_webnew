// MUI
import { ArrowCircleLeftOutlined } from "@mui/icons-material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import SearchIcon from "@mui/icons-material/Search";
import {
    Box,
    Button,
    IconButton,
    Paper,
    TextField,
    Typography,
} from "@mui/material";
import { useState } from "react";

export const DetailsTopView = ({
    headerName,
    handleButtonChange,
    buttonText,
    searchField,
    isButton,
    isBackBtn,
    searchPlaceholder,
    onDataFiltered,
}: any) => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleChange = (event: any) => {
        setSearchQuery(event.target.value);
    };

    const handleSearch = () => {
        onDataFiltered(searchQuery);
    };

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
            onDataFiltered(searchQuery);
        }
    };

    return (
        <>
            <Box sx={{ width: "100% !important", marginBottom: "2rem" }}>
                <Box
                    sx={{ marginTop: "20px", flexWrap: "wrap", alignItems: "end" }}
                    className={searchField ? "display-flex-sb" : "display-flex-sb"}

                >
                    <Box display={"flex"} gap={"1rem"} flexDirection={'column'}>
                        <Typography
                            variant="h3"
                            width={"fit-content"}
                            className="top-view-header font-weight-bold"
                        >
                            {headerName}
                        </Typography>
                        {searchField && (
                            <div>
                                <Paper
                                    component="form"
                                    sx={{
                                        p: "2px 4px",
                                        display: "flex",
                                        alignItems: "center",
                                        width: 400,
                                        height: "42px",
                                    }}
                                    className="search-form-circle"
                                >
                                    <IconButton
                                        onClick={handleSearch}
                                        type="button"
                                        sx={{ p: "10px" }}
                                        aria-label="search"
                                    >
                                        <SearchIcon />
                                    </IconButton>
                                    <TextField
                                        sx={{ ml: 1, flex: 1 }}
                                        placeholder={searchPlaceholder ? searchPlaceholder : 'Search By Name'}
                                        inputProps={{ "aria-label": "search google maps" }}
                                        value={searchQuery}
                                        onChange={handleChange}
                                        onKeyDown={handleKeyPress}
                                    />
                                </Paper>
                            </div>
                        )}
                    </Box>
                    <Box display={"flex"} gap={"1rem"}>


                        {isButton !== false ? (
                            <div>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    sx={{ borderRadius: "9px", width: "max-content" }}
                                    onClick={() => handleButtonChange()}

                                >
                                    {isBackBtn ? (
                                        <ArrowCircleLeftOutlined className="shop-body-add-icon" />
                                    ) : (
                                        <AddCircleOutlineIcon className="shop-body-add-icon" />
                                    )}
                                    {buttonText}
                                </Button>
                            </div>
                        ) : (
                            ""
                        )}
                    </Box>
                </Box>
            </Box>
        </>
    );
};