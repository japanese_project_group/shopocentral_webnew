export const DisplayPdf = ({ file, style }: any) => {
  return (
    <>
      <iframe
        src={`http://docs.google.com/viewer?url=${file}&embedded=true`}
        width="100%"
        height={style ? style : "100%"}
      />
    </>
  );
};
