// MUI
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { Button } from "@mui/material";
// Third party library
import { toast } from "react-toastify";

export const ImageAddViewBoxType = ({
    index,
    setStoreImage,
    storeImage,
    imageLimit,
    imageName,
    maxImageSize,
    allowedFileTypes,
    componentStyle,
}: any) => {
    const onChangeImage = (e: any, fieldValue?: any) => {
        let reader = new FileReader();
        let file = e.target.files[0];
        if (file) {
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = (event: any) => {
                var image = new Image();
                image.src = event.target.result;
                image.onload = function () {
                    const imageWidth = parseInt(`${image.width}`);
                    const imageHeight = parseInt(`${image.height}`);
                    if (imageWidth > 8100 || imageHeight > 8100) {
                        toast("Image height and width too big", {
                            type: "error",
                        });
                    } else if (file.size > maxImageSize) {
                        toast(`Please upload image with size less than ${maxImageSize / (1024 * 1024)} MB`, {
                            type: "error",
                        });
                    } else {
                        setStoreImage((previousImage: any) => [...previousImage, e.target.files[0]]);
                    }
                };
            };
        }
    };

    const onClickUploadButton = () => {
        if (storeImage?.length < imageLimit) {
            document.getElementById(imageName)?.click();
        } else {
            toast.error(`You cannot upload more than ${imageLimit} images`);
        }
    };

    const handleClickCloseIcon = (image: any) => {
        const filterUnclickedImage = storeImage?.filter((data: any) => data !== image);
        setStoreImage(filterUnclickedImage);
    };

    return (
        <div className="image-add-view-box" style={{ padding: "10px", ...componentStyle }}>
            <div className="image-add-view-box-view">
                <div className="image-add-view-box-images">
                    {storeImage?.length > 0 &&
                        storeImage?.map((image: any, index: number) => (
                            <div key={index} style={{ height: "50px", position: "relative" }} className="image-add-view-box-individual-image">
                                {/* {console.log("type>>", typeof image)} */}
                                {typeof image === "string" ?
                                    <>
                                        {console.log("type>>", typeof image)}
                                        <img src={image} width={"100%"} height={"100%"} alt={`Image ${index + 1}`} />
                                    </>
                                    :
                                    <img src={URL.createObjectURL(image)} width={"100%"} height={"100%"} alt={`Image ${index + 1}`} />
                                }
                                <CloseIcon className="image-add-view-box-images-closeicon" onClick={() => handleClickCloseIcon(image)} />
                            </div>
                        ))}
                </div>
            </div>
            <div className="image-add-view-box-add display-flex-end">
                <Button variant="outlined" style={{ background: "black", color: "#ffffff" }} onClick={onClickUploadButton}>
                    Upload File <AddIcon />
                </Button>
                <input
                    type="file"
                    id={imageName}
                    style={{ display: "none" }}
                    onChange={onChangeImage}
                    accept={allowedFileTypes.join(", ")}
                    multiple
                ></input>
            </div>
        </div>
    );
};
