// MUI
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { Box, Button } from "@mui/material";


export const HeaderWithButton = ({ headerName, buttonText }: any) => {
    return (
        <>
            <Box sx={{ width: '100% !important' }}>
                <div style={{ marginTop: "30px" }}>
                    <div className="display-flex-sb">
                        {/* <div> */}
                        <div className="top-view-header font-weight-bold">{headerName}</div>
                        <Button
                            variant='contained'
                            color='primary'
                            style={{ borderRadius: "9px", height: "40px" }}
                        // onClick={() => handleButtonChange()}
                        >
                            <AddCircleOutlineIcon className="shop-body-add-icon" /> {buttonText}
                        </Button>
                        {/* </div> */}
                    </div>
                </div>
            </Box>
        </>
    )
}