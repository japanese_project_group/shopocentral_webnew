// MUI
import { Paper, Typography } from "@mui/material"

// Build in library
import Link from "next/link"

interface KYCStatusMessageTypes {
    status: string,
    message?: string,
}
export const BusinessKycStatusMessage = ({ status, message }: KYCStatusMessageTypes) => {
    return (
        <>
            {status === "INITIAL" &&
                <Paper
                    sx={{
                        width: "100%",
                        padding: "2%",
                        margin: "2%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Typography gutterBottom variant='h5'>
                        Your Business KYC form is in progress for admin review. Please
                        Click Here to {""}
                        <Link href='kycforms/displayforms/individual' passHref>
                            <span
                                style={{
                                    color: "blue",
                                    cursor: "pointer",
                                }}
                            >
                                {""}View your Business KYC form
                            </span>
                        </Link>
                    </Typography>
                </Paper>
            }
            {status === "FINITIAL" &&
                <Paper
                    sx={{
                        width: "100%",
                        padding: "2%",
                        margin: "2%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Typography gutterBottom variant='h5'>
                        You Need a verified Business kyc form to have complete access of
                        this application. Please Click Here to {""}
                        <Link href='/kycforms/businesskyc' passHref>
                            <span
                                style={{
                                    color: "blue",
                                    cursor: "pointer",
                                }}
                            >
                                {""}Submit your Business KYC form
                            </span>
                        </Link>
                    </Typography>
                </Paper>
            }
            {status === ("PENDING" || "UNVERIFIED") &&
                <Paper
                    sx={{
                        width: "100%",
                        padding: "2%",
                        margin: "2%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <Typography gutterBottom variant='h5'>
                        Your Business KYC form has been in {status} state for the reason of{""},
                        <br />
                        "{message}"
                        <br />
                        Please Click Here to {""}
                        <Link href='kycforms/displayforms/individual' passHref>
                            <span
                                style={{
                                    color: "blue",
                                    cursor: "pointer",
                                }}
                            >
                                {""}Re-submit your Business KYC form
                            </span>
                        </Link>
                    </Typography>
                </Paper>
            }
        </>
    )
}