// MUI components
import { Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";

interface KYCStatusMessageTypes {
  status: string;
  message?: string;
}

export const PersonalKycStatusMessage = ({
  status,
  message,
}: KYCStatusMessageTypes) => {
  return (
    <>
      {status === "UNVERIFIED" ||  "PENDING" && (
        <Box
          sx={{
            width: "60%",
            margin: "auto",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Paper
            sx={{
              width: "80%",
              padding: "2%",
              margin: "2%",
            }}
          >
            {status === "UNVERIFIED" && (
              <Box>
                <Typography gutterBottom variant="h5">
                  Your Personal KYC form has been in {status} state for the
                  reason of,
                </Typography>
                <Typography
                  variant="h5"
                  style={{ fontWeight: "1000", fontSize: "29px" }}
                >
                  {message}
                </Typography>
                <Typography gutterBottom variant="h5">
                  Please Re-submit your Personal KYC form using the Form below:
                </Typography>
              </Box>
            )}
            {status === "PENDING" && (
              <Box>
                <Typography gutterBottom variant="h5">
                  Your personal KYC is in pending state.
                </Typography>
              </Box>
            )}
          </Paper>
        </Box>
      )}
    </>
  );
};
