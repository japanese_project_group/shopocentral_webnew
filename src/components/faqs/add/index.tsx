// Build in library
import { useEffect, useState } from "react";

// library
import { Box, Button } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// redux
import {
  useGetManufactureDataQuery,
  usePostAddFaqsDataMutation,
} from "../../../../redux/api/user/user";

// components
import { InputComponent } from "../../../common/Components";
import { businessId } from "../../../common/utility/Constants";
import { validationSchemaAddFaqs } from "../../../common/validationSchema";
import { DetailsTopView } from "../../customComponents/detailsTopView";

export const AddFaqsComponent = () => {
  const router = useRouter();
  const { data: manufactureDetailsData } = useGetManufactureDataQuery(businessId);
  const [submitAddSubscription, { data: FAQSData, isSuccess, isError }] =
    usePostAddFaqsDataMutation();
  const [error, setError] = useState<any>("");
  const [manufactureDetailsSelectData, setManufactureDetailsSelectData] =
    useState<any>([]);

  useEffect(() => {
    if (isSuccess && FAQSData) {
      toast("Successfully Added!", {
        autoClose: 2500,
        type: "success",
      });
      router.push("./");
    }
  }, [isSuccess, FAQSData]);

  useEffect(() => {
    if (isError) {
      const parseData: any = error?.data;
      parseData?.message?.map((data1: any) => {
        return toast.error(data1, {
          autoClose: 2500,
          style: {
            fontSize: "14px",
          },
        });
      });
    }
  }, [isError]);

  const formik = useFormik({
    initialValues: {
      category: "",
      question_en: "",
      answer_en: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemaAddFaqs,
    onSubmit: async (values: any) => {
      let body = {
        ...values,
        question_jpn: "",
        answer_jpn: "",
        question_np: "",
        answer_np: "",
        shopBranchId: "",
      };
      submitAddSubscription(body)
        .unwrap()
        .catch((error: any) => setError(error));
    },
  });
  const {
    values,
    handleSubmit,
    getFieldProps,
    setFieldValue,
    handleBlur,
    errors,
    touched,
  } = formik;

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    selectDataType?: string
  ) => {
    return (
      <div className="display-flex-sb" style={{ marginBottom: "1rem" }}>
        <div>{fieldName}:</div>
        <div style={{ width: "60%" }}>
          <InputComponent
            type={type}
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            errors={errors}
            touched={touched}
            disabled={disabled}
            setFieldValue={setFieldValue}
            values={values}
            // handleBlur={handleBlur}
            selectValue={selectValue}
            setSelectType={setSelectType}
            options={options}
            selectDataType={selectDataType}
          />
        </div>
      </div>
    );
  };

  const displayForms = () => {
    return (
      <>
        <FormikProvider value={values}>
          <form onSubmit={handleSubmit}>
            <div>
              {fieldFormsText(
                "Category",
                "category",
                "options",
                "",
                "",
                false,
                manufactureDetailsData,
                manufactureDetailsSelectData,
                setManufactureDetailsSelectData,
                "server"
              )}
              {fieldFormsText(
                "Questions",
                "question_en",
                "text",
                errors.question_en,
                touched.question_en
              )}
              {fieldFormsText(
                "Answer",
                "answer_en",
                "text",
                errors.answer_en,
                touched.answer_en
              )}
            </div>
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              <Box sx={{ flex: "1 1 auto" }} />
              <Button
                type="submit"
                sx={{
                  backgroundColor: "#000",
                  color: "#fff",
                  "&:hover": {
                    backgroundColor: "#0069d9",
                    borderColor: "#0062cc",
                    boxShadow: "none",
                  },
                }}
              >
                Submit
              </Button>
            </Box>
          </form>
        </FormikProvider>
      </>
    );
  };

  const handleBackButton = () => {
    router.push("./");
  };

  return (
    <Box>
      <DetailsTopView
        headerName="Add new FAQs"
        handleButtonChange={handleBackButton}
        buttonText="Back"
        searchField={false}
        isButton={true}
        isBackBtn={true}
      />
      <div className="form-view-multistep-form box-kyc-multiple-step-form">
        {displayForms()}
      </div>
    </Box>
  );
};
