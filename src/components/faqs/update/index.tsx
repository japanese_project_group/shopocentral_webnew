// Build in function
import { useEffect, useState } from "react";

// library
import { Box, Button } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// redux
import {
  useGetIndividualFaqsDataQuery,
  useUpdateFaqsDataMutation,
} from "../../../../redux/api/user/user";

// components
import { InputComponent } from "../../../common/Components";
import { DetailsTopView } from "../../customComponents/detailsTopView";

// validation

export const UpdateFaqs = () => {
  const router = useRouter();
  const [ID, setID] = useState<any>();
  const [faqslectData, setFaqslectData] = useState<any>([]);
  const { data: editFaqsData, refetch } = useGetIndividualFaqsDataQuery(ID);
  const [updateFaqsData, { data: FaqsData }] = useUpdateFaqsDataMutation();
  console.log("first", editFaqsData);

  useEffect(() => {
    setID(router.query.id);
  }, [router.query]);

  useEffect(() => {
    if (!isEmpty(FaqsData)) {
      toast.success("Successfully updated data");
      router.push("./");
    }
  }, [FaqsData]);

  useEffect(() => {
    refetch();
  }, []);

  const handleChangeButton = () => {
    router.push("./");
  };

  const faqsOptionData = [
    {
      value: "business",
      label: "Business",
    },
    {
      value: "price",
      label: "Price",
    },
  ];

  const handleCancel = () => {
    router.push("./faqs");
  };

  const formik = useFormik({
    initialValues: {
      title_en: editFaqsData?.title_en || '',
      category: editFaqsData?.category || '',
      description: editFaqsData?.description || '',
    },
    enableReinitialize: true,
    // validationSchema: validationSchemaAddSubscription,
    onSubmit: async (values: any) => {
      let body = {
        ...values,
        shopBranchId: "12345",
      };
      updateFaqsData({ id: ID, body })
        .unwrap()
        .catch((error: { data: { message: string } }) => {
          const errmsg = error?.data?.message;
          toast.error(errmsg);
        });
    },
  });
  const {
    values,
    handleSubmit,
    getFieldProps,
    setFieldValue,
    handleBlur,
    errors,
    touched,
  } = formik;

  const fieldFormsText = (
    fieldName: string,
    getFieldPropsValue: string,
    type?: string,
    errors?: any,
    touched?: any,
    disabled?: boolean,
    options?: any,
    selectValue?: any,
    setSelectType?: any,
    selectDataType?: string
  ) => {
    return (
      <div className="display-flex-sb" style={{ marginBottom: "5px" }}>
        <div>{fieldName}:</div>
        <div style={{ width: "60%" }}>
          <InputComponent
            type={type}
            getFieldProps={getFieldProps}
            getFieldPropsValue={getFieldPropsValue}
            errors={errors}
            touched={touched}
            disabled={disabled}
            setFieldValue={setFieldValue}
            values={values}
            // handleBlur={handleBlur}
            selectValue={selectValue}
            setSelectType={setSelectType}
            options={options}
            selectDataType={selectDataType}
          />
        </div>
      </div>
    );
  };

  return (
    <Box>
      <DetailsTopView
        headerName="Update FAQS"
        handleButtonChange={handleChangeButton}
        buttonText="Back"
        searchField={false}
        isButton={true}
        isBackBtn={true}
      />
      <div className="form-view-multistep-form box-kyc-multiple-step-form">
        <div>
          <FormikProvider value={values}>
            <form onSubmit={handleSubmit}>
              <Box paddingBottom="2rem">
                {fieldFormsText(
                  "Category",
                  "category",
                  "options",
                  "",
                  "",
                  false,
                  faqsOptionData,
                  faqslectData,
                  setFaqslectData,
                  "server"
                )}
                {fieldFormsText(
                  "Questions",
                  "question_en",
                  "text",
                  errors.question_en,
                  touched.question_en
                )}
                {fieldFormsText(
                  "Answer",
                  "answer_en",
                  "text",
                  errors.answer_en,
                  touched.answer_en
                )}
              </Box>
              <Box display={"flex"} gap={"1rem"} sx={{ justifyContent: "end" }}>
                <Button
                  variant="contained"
                  type="submit"
                  sx={{ padding: "4px 16px" }}
                >
                  Update
                </Button>
                <Button
                  variant="outlined"
                  onClick={handleCancel}
                  sx={{ padding: "4px 16px" }}
                >
                  Cancel
                </Button>
              </Box>
            </form>
          </FormikProvider>
        </div>
      </div>
    </Box>
  );
};
