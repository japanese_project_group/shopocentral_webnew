// build in library
import { useRouter } from "next/router";
import React, { useEffect } from "react";

// third party library
import ExpandCircleDownOutlinedIcon from "@mui/icons-material/ExpandCircleDownOutlined";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  CircularProgress,
  Typography,
} from "@mui/material";

// components
import { DetailsTopView } from "../customComponents/detailsTopView";

// redux
import NoDataFound from "@/src/common/NoDataFound";
import { useGetFaqsDataQuery } from "../../../redux/api/user/user";

const FAQSComponent = () => {
  const router = useRouter();
  const [expanded, setExpanded] = React.useState<any>(0);
  const [priceExpanded, setPriceExpanded] = React.useState<any>(0);

  const { data: faqsData, isSuccess, isLoading: getLoading, refetch } = useGetFaqsDataQuery();

  useEffect(() => {
    refetch()
  }, [faqsData])

  const handleChange =
    (panel: any) => (event: React.SyntheticEvent, newExpanded: any) => {
      setExpanded(newExpanded ? panel : false);
    };

  const handleChangePrice =
    (panel: any) => (event: React.SyntheticEvent, newExpanded: any) => {
      setPriceExpanded(newExpanded ? panel : false);
    };


  function handleChangeButton() {
    router.push("/faqs/add");
  }

  const handleUpdate = (id: any) => {
    router.push({
      pathname: `/faqs/update`,
      query: { id },
    });
  };
  const filteredFaqsData = faqsData?.filter((item: any) => item.category === item.category);
  return (
    <>
      <DetailsTopView
        headerName="FAQS"
        handleButtonChange={handleChangeButton}
        buttonText="Add"
        searchField={false}
      />
      {getLoading ? (
        <Box display="flex" justifyContent="center" alignItems="center" height="200px">
          <CircularProgress />
        </Box>
      ) : (
        <Box className={'card-wrapper'}>
          {filteredFaqsData && filteredFaqsData?.length > 0 ? (
            <>
              {filteredFaqsData?.map((item: any, index: any) => (
                <Box className={'faq-wrapper'} key={item.id}>
                  <Typography variant="h6" sx={{ color: "#1a76d2", marginBottom: "1rem" }}>
                    {item.category}
                  </Typography>
                  <Accordion
                    expanded={expanded === index}
                    onChange={handleChange(index)}
                    sx={{ borderRadius: "1rem !important", margin: "1rem 0", padding: "0 24px" }}
                  >
                    <AccordionSummary
                      expandIcon={<ExpandCircleDownOutlinedIcon className={'faq-title'} onClick={() => handleUpdate(item.id)} />}
                      aria-controls="panel1d-content"
                      id="panel1d-header"
                    >
                      <Typography className={'faq-title'}>{item.question_en}</Typography>
                    </AccordionSummary>
                    <AccordionDetails sx={{ paddingBottom: '1rem' }}>
                      <Typography className={'faq-desc'}>{item.answer_en}</Typography>
                    </AccordionDetails>
                  </Accordion>
                </Box>
              ))}
            </>) : (
            <NoDataFound />
          )}
        </Box>
      )}
    </>
  );
};
export default FAQSComponent;
