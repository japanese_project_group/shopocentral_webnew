// Build in library
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Box, Button, Paper, Typography } from "@mui/material";

// Third party library
import { Client, HydrationProvider } from "react-hydration-provider";
import { useSelector } from "react-redux";

// Redux
import { useGetShopDataQuery } from "../../../../redux/api/user/user";
import { RootState } from "../../../../redux/store";

const ViewIndividualBusiness = () => {
    const { data: allData, isSuccess, refetch } = useGetShopDataQuery();
    const { lang: language } = useSelector((state: RootState) => state?.lang);
    const [values, setValues] = useState<any>();
    function handleEdit() {
        Router.push("/shop/updateshop");
    }
    function handleAdd() {
        Router.push("/shop/shopdetails");
    }

    // useEffect(() => {
    //   if (allData == null) {
    //     Router.push("/shop/shopdetails");
    //   }
    // }, [allData, isSuccess])

    useEffect(() => {
        if (allData && isSuccess) {
            setValues(allData[0]);
        }
    }, [isSuccess, allData]);

    useEffect(() => {
        refetch();
    }, []);
    return (
        <HydrationProvider>
            <Client>
                <Box>
                    <Box sx={{
                        display: {
                            md: "flex",
                            sx: "block"
                        }, justifyContent: "space-between",
                        marginTop: "2%",
                        marginBottom: "2%"
                    }}>
                        <Box sx={{
                            height: "30%", width: {
                                md: "48%",
                                sx: "100%"
                            },
                        }}>
                            <Paper><Typography sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>staff detail</Typography></Paper>

                        </Box>
                        <Box sx={{
                            height: "30%", width: {
                                md: "48%",
                                sx: "100%"
                            },
                        }}>
                            <Paper><Typography sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>business detail</Typography></Paper>
                        </Box>
                    </Box>
                    <Box sx={{
                        display: {
                            md: "flex",
                            sx: "block"
                        }, justifyContent: "space-between",
                        marginTop: "2%",
                        marginBottom: "2%"
                    }}>
                        <Box sx={{
                            height: "30%", width: {
                                md: "48%",
                                sx: "100%"
                            },
                        }}>
                            <Paper><Typography sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>staff detail</Typography></Paper>
                        </Box>
                        <Box sx={{
                            height: "30%", width: {
                                md: "48%",
                                sx: "100%"
                            },
                        }}>
                            <Paper><Typography sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>business detail</Typography></Paper>
                        </Box>
                    </Box>
                    <Box
                        sx={{
                            display: "flex",
                            justifyContent: "space-between",
                            marginBottom: "2%",
                        }}
                    >
                        <Box><Typography>List of Shops are :-</Typography></Box>
                        <Box>
                            <Button variant="contained" color="primary" onClick={handleAdd}>
                                Add New Shop
                            </Button>
                        </Box>
                    </Box>
                    <Paper sx={{ padding: "2%" }}>
                        {/* {values != null ? (
          <> */}
                        <>
                            {language === "en" ? (
                                <>
                                    <Box sx={{
                                        display: {
                                            md: "flex",
                                            sx: "block"
                                        }, justifyContent: "space-between"
                                    }}>
                                        <Box
                                            sx={{
                                                display: {
                                                    md: "flex",
                                                    sx: "block"
                                                },
                                                width: "100%",
                                            }}
                                        >
                                            <Box sx={{ marginRight: "30%" }}>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Name: {values?.name_en}
                                                    </Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Subscription package:
                                                        {values?.subscriptionPackage}
                                                    </Typography>
                                                </Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Registered Trade:
                        </Typography>
                        <Typography>{values?.name_en}</Typography>
                      </Box> */}
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Company Name:
                                                        {values?.companyName_en}
                                                    </Typography>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Building Number:
                                                        {values?.address?.building_en}
                                                    </Typography>
                                                </Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Corporate Number:
                        </Typography>
                        <Typography>{values?.corporateNumber}</Typography>
                      </Box> */}
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Established Date:
                        </Typography>
                        <Typography>{values?.establishedDate}</Typography>
                      </Box> */}
                                                <Box>
                                                    {/* <Typography sx={{ marginBottom: "10%" }}>
                          Opening Time:
                        </Typography> */}
                                                    {/* s */}
                                                </Box>
                                            </Box>
                                            <Box>
                                                <Box>
                                                    {/* <Typography sx={{ marginBottom: "10%" }}>
                          Closing Time:
                        </Typography> */}
                                                    {/* <Typography>{values?.closingTime}</Typography> */}
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        City:
                                                        {values?.address?.city_en}
                                                    </Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Country:
                                                        {values?.address?.country_en}
                                                    </Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Postal Code:
                                                        {values?.address?.postalcode}
                                                    </Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Prefecture:
                                                        {values?.address?.prefecture_en}
                                                    </Typography>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Town:
                                                        {values?.address?.town_en}
                                                    </Typography>
                                                </Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Building Number:
                        </Typography>
                        <Typography>{values?.building_en}</Typography>s
                      </Box>
                      <Box>

                        <Typography sx={{ marginBottom: "10%" }}>
                          Town:
                        </Typography>
                        <Typography>{values?.town_en}</Typography>
                      </Box> */}
                                            </Box>
                                        </Box>
                                        <Box sx={{
                                            display: "flex", flexDirection: {
                                                md: "column",
                                                sx: "row"
                                            }, justifyContent: "space-around"
                                        }}>
                                            <EditIcon onClick={handleEdit} />
                                            <VisibilityIcon />
                                            <DeleteForeverIcon />
                                        </Box>
                                    </Box>
                                </>
                            ) : language === "jp" ? (
                                <>
                                    <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                                        <Box
                                            sx={{
                                                display: "flex",
                                                width: "100%",
                                            }}
                                        >
                                            <Box sx={{ marginRight: "30%" }}>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Name:{values?.name_jpn}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Postal Code:{values?.address?.postalcode}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        City: {values?.address?.city_jpn}{" "}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Prefecture:{values?.address?.prefecture_jpn}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Subscription package:{values?.subscriptionPackage}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>

                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Company Name: {values?.companyName_jpn}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Corporate Number:{values?.corporateNumber}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Established Date: {values?.establishedDate}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Opening Time:{values?.openingTime}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                            </Box>
                                            <Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Closing Time:{values?.closingTime}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        City: {values?.address?.city_jpn}{" "}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Country:{values?.address?.country_jpn}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>

                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Building Number: {values?.address?.building_jpn}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Town: {values?.address?.town_jpn}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Registered Trade:{values?.registeredTrade_jpn}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                            </Box>
                                        </Box>
                                        <Box sx={{
                                            display: "flex", flexDirection: {
                                                md: "column",
                                                sx: "row"
                                            }, justifyContent: "space-around"
                                        }}>
                                            <EditIcon />
                                            <VisibilityIcon />
                                            <DeleteForeverIcon />
                                        </Box>
                                    </Box>
                                </>
                            ) : (
                                <>
                                    <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                                        <Box
                                            sx={{
                                                display: "flex",
                                                width: "100%",
                                            }}
                                        >
                                            <Box sx={{ marginRight: "30%" }}>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Name:{values?.name_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Postal Code:{values?.address?.postalcode}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Subscription package:{values?.subscriptionPackage}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        City:{values?.address?.city_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>

                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Registered Trade:{values?.registeredTrade_np}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}

                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Established Date: {values?.establishedDate}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Opening Time:{values?.openingTime}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}
                                            </Box>
                                            <Box>
                                                {/* <Box>
                        <Typography sx={{ marginBottom: "10%" }}>
                          Closing Time:{values?.closingTime}
                        </Typography>
                        <Typography></Typography>
                      </Box> */}

                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Country:{values?.address?.country_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Company Name: {values?.companyName_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>

                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Prefecture:{values?.address?.prefecture_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                                <Box>
                                                    <Typography sx={{ marginBottom: "10%" }}>
                                                        Town: {values?.address?.town_np}
                                                    </Typography>
                                                    <Typography></Typography>
                                                </Box>
                                            </Box>
                                        </Box>
                                        <Box sx={{
                                            display: "flex", flexDirection: {
                                                md: "column",
                                                sx: "row"
                                            }, justifyContent: "space-around"
                                        }}>
                                            <EditIcon />
                                            <VisibilityIcon />
                                            <DeleteForeverIcon />
                                        </Box>
                                    </Box>
                                </>
                            )}
                        </>

                    </Paper>
                </Box>
            </Client>
        </HydrationProvider >
    );
};
export default ViewIndividualBusiness;
