// Build in library
import Router, { useRouter } from "next/router";
import { useState } from "react";

// MUI Components
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { Box, Button, Paper, Typography } from "@mui/material";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const ViewBusiness = () => {
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  function handleClick(val: any) {
    router.push({
      pathname: "/business/addbusiness/",
      query: { id: val },
    });
  }
  const handleView = () => {
    Router.push("/business/viewbusiness/");
  };

  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "2%",
        }}
      >
        <Box></Box>
        <Box>
          <Button variant="contained" color="primary" onClick={handleOpen}>
            Add New Business
          </Button>
        </Box>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Please Choose a verified business KYC for new Business:
            </Typography>
            {
              <ul>
                <li>
                  <Button onClick={() => handleClick(1)}>
                    1.verified business one
                  </Button>
                </li>
                <li>
                  <Button onClick={() => handleClick(2)}>
                    2.verified business two
                  </Button>
                </li>
                <li>
                  <Button onClick={() => handleClick(3)}>
                    3.verified business three
                  </Button>
                </li>
                <li>
                  <Button onClick={() => handleClick(4)}>
                    4.verified business four
                  </Button>
                </li>
                <li>
                  <Button onClick={() => handleClick(5)}>
                    5.verified business five
                  </Button>
                </li>
                <li>
                  <Button onClick={() => handleClick(6)}>
                    6.verified business six
                  </Button>
                </li>
              </ul>
            }
          </Box>
        </Modal>
      </Box>
      <Box sx={{ width: "100%", margin: "2%" }}>
        <Paper
          sx={{
            padding: "2%",
            display: {
              md: "flex",
              sx: "block",
            },
            justifyContent: "space-between",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              margin: "1%",
            }}
          >
            Name of Business: Business KYC 1{" "}
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              margin: "1%",
            }}
          >
            Verification Status: unverified
          </Box>
          <Box
            sx={{
              "@media(minWidth: 900px)": {
                minWidth: "90%",
                width: "100%",
              },
              minWidth: "25%",
              display: "flex",
              flexDirection: {
                md: "row",
                sx: "row",
              },
              justifyContent: "space-around",
              margin: "1%",
            }}
          >
            <EditIcon />
            <VisibilityIcon
              onClick={handleView}
              sx={{
                "&:hover": {
                  backgroundColor: "$labelcolor",
                  cursor: "pointer",
                },
              }}
            />
            <DeleteForeverIcon />
          </Box>
        </Paper>
      </Box>
    </>
  );
};
export default ViewBusiness;
