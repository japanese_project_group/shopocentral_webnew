// Build in library
import Images from "next/image";
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import {
  Box,
  Button,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";

// Third party library
import { Form, FormikProvider, useFormik } from "formik";
import moment from "moment";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

// Redux
import {
  useGetAllDataByIdQuery,
  usePostBusinessKycRequestMutation
} from "../../../../redux/api/user/user";

// style
import styles from "../../../../src/components/Forms.module.css";

// component
import { InputComponent, ViewImage } from "../../../common/Components";
import { businessAccessId, businessId, userId } from "../../../common/utility/Constants";
import { validationSchemaBusinessKyc } from "../../../common/validationSchema";

const BusinessKycForm = () => {
  const dispatch = useDispatch();
  const [ValueUpdate, { isSuccess, data, isLoading }] =
    usePostBusinessKycRequestMutation();
  const { data: allData, isSuccess: isSuccessGetProfile } =
    useGetAllDataByIdQuery(businessAccessId);
  const [corporateRegisterImage, setCorporateRegisterImage] = useState<any>();
  const [sealedCertificateImage, setSealedCertificateImage] = useState<any>();
  const [taxPaymentImage, setTaxPaymentImage] = useState<any>();
  const [otherImage, setOtherImage] = useState<any>();
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>([]);

  const [documentID, setDocumentID] = useState("");

  useEffect(() => {
    if (isSuccess && data) {
      localStorage.removeItem("userId");
      localStorage.removeItem("Id");
      localStorage.removeItem("email");
      localStorage.removeItem("verificationEmail");
      localStorage.removeItem("password");
      toast("Business KYC submitted", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      Router.push("/kycforms/formsubmission/business");
    }
  }, [isSuccess, data]);

  const formik = useFormik({
    initialValues: {
      corporateNumber: "",
      registerTradeName: "" || allData?.data?.registerTradeName,
      establishedDate: "" || allData?.data?.establishedDate,
      companyType: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemaBusinessKyc,
    onSubmit: async (values: any) => {
      if (
        !corporateRegisterImage &&
        !sealedCertificateImage &&
        !taxPaymentImage &&
        !otherImage
      ) {
        toast.error("You must upload documents");
      } else {
        let body = { ...values };
        await submitValues(body);
      }
    },
  });
  async function submitValues(val: any) {
    const finalData: any = {
      ...val,
      certifiedRegistered: val.certifiedRegistered,
      sealedCertificate: val.sealedCertificate,
      recieptTaxOffice: val.recieptTaxOffice,
      others: val.others,
      userId: userId,
      documentType: documentID,
    };
    let fd = new FormData();
    Object.keys(finalData).map((item: any) => {
      fd.append(item, finalData[item] ? finalData[item] : null);
    });
    let reqData = {
      data: fd,
      id: businessId,
    };
    ValueUpdate(reqData);
  }
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  const onChangeImageUpload = (e: any, type: string) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file.type === "application/pdf") {
      if (type === "corporateRegisterImage") {
        setFieldValue("certifiedRegistered", e.target.files[0]);
        setCorporateRegisterImage(e.target.files[0]);
      } else if (type === "sealedCertificateImage") {
        setFieldValue("sealedCertificate", e.target.files[0]);
        setSealedCertificateImage(e.target.files[0]);
      } else if (type === "taxPaymentImage") {
        setFieldValue("recieptTaxOffice", e.target.files[0]);
        setTaxPaymentImage(e.target.files[0]);
      } else {
        setFieldValue("others", e.target.files[0]);
        setOtherImage(e.target.files[0]);
      }
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            //file.size / 1024 / 1024 > 2
            toast("Please upload image with size less than 2 MB", {
              type: "error",
            });
          } else {
            if (type === "corporateRegisterImage") {
              setFieldValue("certifiedRegistered", e.target.files[0]);
              setCorporateRegisterImage(e.target.files[0]);
            } else if (type === "sealedCertificateImage") {
              setFieldValue("sealedCertificate", e.target.files[0]);
              setSealedCertificateImage(e.target.files[0]);
            } else if (type === "taxPaymentImage") {
              setFieldValue("recieptTaxOffice", e.target.files[0]);
              setTaxPaymentImage(e.target.files[0]);
            } else {
              setFieldValue("others", e.target.files[0]);
              setOtherImage(e.target.files[0]);
            }
          }
        };
      };
    }
  };

  const corporateRegisterFile = () => {
    document.getElementById("corporateRegisterFile")?.click();
  };
  const sealedCertificateFile = () => {
    document.getElementById("sealedCertificateFile")?.click();
  };
  const taxPaymentFile = () => {
    document.getElementById("taxPaymentFile")?.click();
  };
  const otherFile = () => {
    document.getElementById("otherFile")?.click();
  };

  const displayUploadedImage = (
    image: any,
    file: any,
    onChangeImageUpload: any,
    setImage: any,
    ImageName: string,
    imageValue: string,
    inputFileName: string
  ) => {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          justifyContent: "flex-start",
        }}
      >
        <div
          style={{
            marginBottom: "50px",
            width: "100%",
            padding: "10px",
          }}
        >
          {image.type === "application/pdf" ? (
            //   <iframe
            //   src={`http://docs.google.com/viewer?url=${imageItem}&embedded=true`}
            //   height="320px"
            //   width="100%"
            //   // style={{ minHeight: "400px" }}
            // />
            <iframe
              src={URL.createObjectURL(image)}
              height="320px"
              width="100%"
            />
          ) : (
            <Images
              onClick={() => {
                setOpenModal(true);
                setDisplayData(image);
              }}
              src={URL.createObjectURL(image)}
              height={320}
              width={470}
              style={{ borderRadius: "12px", width: "100%" }}
            />
          )}
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              gap: "10px",
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            <div style={{ marginTop: "5px" }}>{ImageName}</div>
            <div>
              {image.type !== "application/pdf" && (
                <Button
                  variant="outlined"
                  style={{ height: "30px" }}
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(image);
                  }}
                >
                  <RemoveRedEyeIcon />
                </Button>
              )}

              <Button
                variant="outlined"
                style={{ height: "30px" }}
                onClick={() => {
                  file();
                }}
              >
                <EditIcon />
                <input
                  type="file"
                  id={inputFileName}
                  style={{ display: "none" }}
                  onChange={(e) => onChangeImageUpload(e, imageValue)}
                ></input>
              </Button>

              <Button
                variant="outlined"
                style={{ height: "30px" }}
                onClick={() => {
                  setFieldValue(imageValue, null);
                  setImage(null);
                }}
              >
                <DeleteIcon />
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      <FormikProvider value={formik}>
        <Box style={{ width: "100%" }}>
          <Stack>
            <Form
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                padding: 30,
              }}
            >
              <Stack
                className={styles.glassMorphism}
                spacing={2}
                width={{
                  xl: "100%",
                  lg: "100%",
                  md: "100%",
                  sm: "100%",
                  xs: "100%",
                }}
                sx={{ p: 5 }}
                justifyContent="center"
              >
                <Typography
                  variant="h4"
                  color="primary"
                  component="div"
                  align="center"
                >
                  Business KYC Form
                </Typography>
                <Typography align="center" variant="h5">
                  Please Enter Your Kyc Form
                </Typography>
                <Box>
                  <InputComponent
                    label="Corporate Number:"
                    type="text"
                    getFieldProps={getFieldProps}
                    getFieldPropsValue="corporateNumber"
                    errors={errors.corporateNumber}
                    touched={touched.corporateNumber}
                    placeholder="Please enter corporate Number"
                  />
                </Box>

                <Box>
                  <InputComponent
                    label="Registered Trade Name:"
                    type="text"
                    getFieldProps={getFieldProps}
                    getFieldPropsValue="registerTradeName"
                    errors={errors.registerTradeName}
                    touched={touched.registerTradeName}
                    placeholder="Please enter Register Trade Name"
                    disabled={true}
                  />
                </Box>

                <Box>
                  <Typography>
                    {" "}
                    Established Date:{" "}
                    <Typography component="span" color="error">
                      *
                    </Typography>
                  </Typography>
                  <LocalizationProvider dateAdapter={AdapterMoment}>
                    <DesktopDatePicker
                      inputFormat="YYYY-MM-DD"
                      maxDate={moment().format("YYYY-MM-DD")}
                      {...getFieldProps("establishedDate")}
                      onChange={(newValue) => {
                        setFieldValue(
                          "establishedDate",
                          moment(newValue).format("YYYY-MM-DD")
                        );
                      }}
                      renderInput={(params) => (
                        <TextField
                          size="small"
                          {...params}
                          fullWidth
                          error={Boolean(
                            touched.establishedDate && errors.establishedDate
                          )}
                          helperText={
                            Boolean(
                              touched.establishedDate && errors.establishedDate
                            ) &&
                            String(
                              touched.establishedDate && errors.establishedDate
                            )
                          }
                        />
                      )}
                    />
                  </LocalizationProvider>
                </Box>
                <Box>
                  Select file to upload(png, jpg, jpeg, pdf)
                  <Typography component="span" color="error">
                    *
                  </Typography>
                  <FormControl
                    size="small"
                    sx={{ width: "100%", marginBottom: "40px" }}
                  >
                    <Select
                      id="demo-simple-select"
                      placeholder="select any file to upload"
                    >
                      {!corporateRegisterImage && (
                        <>
                          <MenuItem
                            onClick={() => {
                              corporateRegisterFile();
                            }}
                          >
                            Certified copy of corporate Register
                          </MenuItem>
                          <input
                            type="file"
                            id="corporateRegisterFile"
                            style={{ display: "none" }}
                            onChange={(e: any) =>
                              onChangeImageUpload(e, "corporateRegisterImage")
                            }
                            accept=".jpeg, .png, .jpg, .svg, .pdf"
                          ></input>
                        </>
                      )}
                      {!sealedCertificateImage && (
                        <>
                          <MenuItem
                            onClick={() => {
                              sealedCertificateFile();
                            }}
                          >
                            Corporate Seal Certificate
                          </MenuItem>
                          <input
                            type="file"
                            id="sealedCertificateFile"
                            style={{ display: "none" }}
                            onChange={(e: any) =>
                              onChangeImageUpload(e, "sealedCertificateImage")
                            }
                            accept=".jpeg, .png, .jpg, .svg, .pdf"
                          ></input>
                        </>
                      )}
                      {!taxPaymentImage && (
                        <>
                          <MenuItem
                            onClick={() => {
                              taxPaymentFile();
                            }}
                          >
                            Corporate Tax Payment Certificate
                          </MenuItem>
                          <input
                            type="file"
                            id="taxPaymentFile"
                            style={{ display: "none" }}
                            onChange={(e: any) =>
                              onChangeImageUpload(e, "taxPaymentImage")
                            }
                            accept=".jpeg, .png, .jpg, .svg, .pdf"
                          ></input>
                        </>
                      )}
                      {!otherImage && (
                        <>
                          <MenuItem
                            onClick={() => {
                              otherFile();
                            }}
                          >
                            Other File
                          </MenuItem>
                          <input
                            type="file"
                            id="otherFile"
                            style={{ display: "none" }}
                            onChange={(e: any) =>
                              onChangeImageUpload(e, "otherImage")
                            }
                            accept=".jpeg, .png, .jpg, .svg, .pdf"
                          ></input>
                        </>
                      )}
                      {corporateRegisterImage &&
                        sealedCertificateImage &&
                        taxPaymentImage &&
                        otherImage && <MenuItem>No Items</MenuItem>}
                    </Select>
                  </FormControl>
                  <div
                    style={{
                      marginTop: "-30px",
                      marginBottom: "40px",
                      display: "grid",
                      gridTemplateColumns: "auto auto",
                      gap: "5px",
                    }}
                    className="display-image-personal-kyc"
                  >
                    {corporateRegisterImage &&
                      displayUploadedImage(
                        corporateRegisterImage,
                        corporateRegisterFile,
                        onChangeImageUpload,
                        setCorporateRegisterImage,
                        "Certified copy of corporate Register",
                        "corporateRegisterImage",
                        "corporateRegisterFile"
                      )}
                    {sealedCertificateImage &&
                      displayUploadedImage(
                        sealedCertificateImage,
                        sealedCertificateFile,
                        onChangeImageUpload,
                        setSealedCertificateImage,
                        "Corporate Seal Certificate",
                        "sealedCertificateImage",
                        "sealedCertificateFile"
                      )}
                    {taxPaymentImage &&
                      displayUploadedImage(
                        taxPaymentImage,
                        taxPaymentFile,
                        onChangeImageUpload,
                        setTaxPaymentImage,
                        "Corporate Tax Payment Certificate",
                        "taxPaymentImage",
                        "taxPaymentFile"
                      )}
                    {otherImage &&
                      displayUploadedImage(
                        otherImage,
                        otherFile,
                        onChangeImageUpload,
                        setOtherImage,
                        "Other File",
                        "otherImage",
                        "otherFile"
                      )}
                  </div>
                </Box>
                <Stack direction="column">
                  {isLoading ? (
                    <Button
                      variant="contained"
                      startIcon={
                        <CircularProgress size="1.5rem" color="inherit" />
                      }
                    ></Button>
                  ) : (
                    <Button
                      disabled={isSubmitting}
                      type="submit"
                      variant="contained"
                      color="primary"
                    >
                      Submit
                    </Button>
                  )}
                </Stack>
              </Stack>
            </Form>
          </Stack>
          {openModal && (
            <ViewImage
              displayData={displayData}
              setOpenModal={setOpenModal}
              openModal={openModal}
            />
          )}
        </Box >
      </FormikProvider >
    </>
  );
};

export default BusinessKycForm;
