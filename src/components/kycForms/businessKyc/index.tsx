import { Box, Stack } from "@mui/material";
// import BusinessKycForm from "./formComponent";
import Image from "next/image";
import loginPic from "../../../../public/loginPic.png";
import BusinessKycForm from "./BusinessKycForm";

const BusinessKycFormCombined = () => {
  return (
    <>
      <Stack direction={{ sm: "column", lg: "row" }}>
        <Stack
          width={{ sm: "100%", lg: "70%" }}
          justifyContent="center"
          direction="column"
          alignContent={"center"}
          sx={{ margin: "auto" }}
        >
          <BusinessKycForm />
        </Stack>
      </Stack>
    </>
  );
};
export default BusinessKycFormCombined;
