import { Box } from "@mui/material";
import Image from "next/image";
import loginPic from "../../../../public/loginPic.png";
const ImageComponent = () => {
  return (
    <Box>
      <Image
        src={loginPic}
        alt="thumbnail"
      />
    </Box>
  );
};
export default ImageComponent;
