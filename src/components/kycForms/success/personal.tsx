import React from "react";
import { Button, Stack, Typography } from "@mui/material";
import { Box, Paper } from "@mui/material";
import styles from "../../Forms.module.css";
import Router, { useRouter } from "next/router";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import ReplyAllIcon from "@mui/icons-material/ReplyAll";
import moment from "moment";
import Link from "next/link";

function PersonalFormSuccess() {
  const router = useRouter();
  const currentRoute = router?.query?.successDisplay;
  const submittedDateTime = router.query.date;
  const stateMessage = router.query.message;

  const handlePending = () => {
    router.replace({
      pathname: "/kycforms/personalkyc",
      query: { status: "PENDING" },
    });
  };

  function handleClick() {
    if (currentRoute === "personal") {
      Router.push("/register");
    } else {
      localStorage.setItem("personal-kyc-open", "data");
      Router.push("/kycforms/personalkyc");
    }
  }

  return (
    <Stack
      justifyContent={"center"}
      alignItems={"center"}
      spacing={4}
      height={"100vh"}
    >
      <Box
        className={styles.forms}
        sx={{
          width: { lg: "45%", md: "60%", sm: "70%", xs: "95%" },
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          p: 2,
        }}
      >
        {currentRoute !== "PENDING" && (
          <>
            <Typography variant="h2" gutterBottom color={"primary"}>
              Thank You!
            </Typography>
            <DoneAllIcon color="success" />
          </>
        )}
        <Typography variant="h5" gutterBottom textAlign={"center"}>
          {currentRoute === "personal" &&
            "Your personal KYC has been submitted successfully."}
          {currentRoute === "personal-kyc" &&
            "Shopo User Account created successfully"}
          {currentRoute === "PENDING" &&
            "Your personal KYC is in pending state."}
        </Typography>
        {currentRoute === "personal" && (
          <p style={{ margin: "0", padding: "0" }}>
            <span style={{ fontWeight: "bold" }}>Submitted date : </span>
            {moment(submittedDateTime).format("YYYY/MM/DD")}
          </p>
        )}
        <Typography variant="h6" sx={{ textAlign: "center" }}>
          {currentRoute === "personal" &&
            "It may take 1-2 business days to verify your personal KYC. We will notify you by email after being completed. So please wait for a while until completion."}
          {currentRoute === "personal-kyc" &&
            "Please click here to fill your personal KYC"}
          {currentRoute === "PENDING" && `Message :  ${stateMessage}`}
        </Typography>
        <Typography>
          {currentRoute === "PENDING" && (
            <p
              onClick={handlePending}
              style={{ cursor: "pointer", color: "blue" }}
            >
              Click here to resubmit ro edit
            </p>
          )}
        </Typography>
      </Box>
      {currentRoute !== "PENDING" && (
        <Box>
          <Box>
            <Button
              variant="contained"
              onClick={handleClick}
              sx={{ input: { cursor: "pointer" } }}
            >
              {currentRoute === "personal" && "Back to Login"}
              {currentRoute === "personal-kyc" && "Personal KYC"}
            </Button>
          </Box>
        </Box>
      )}
    </Stack>
  );
}

export default PersonalFormSuccess;
