import React from "react";
import { Button, Stack, Typography } from "@mui/material";
import { Box, Paper } from "@mui/material";
import styles from "../../Forms.module.css";
import Router from "next/router";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import ReplyAllIcon from "@mui/icons-material/ReplyAll";

function BusinessFormSuccess() {
  return (
    <Stack
      justifyContent={"center"}
      alignItems={"center"}
      spacing={4}
      height={"100vh"}
    >
      <Box
        className={styles.forms}
        sx={{
          width: { lg: "45%", md: "60%", sm: "70%", xs: "95%" },
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          borderRadius: "20px",
          background: "#e8e8e8",
          p: 2,
        }}
      >
        <Typography variant='h2' gutterBottom color={"primary"}>
          Thank You!
        </Typography>
        <DoneAllIcon color='success' />
        <Typography variant='h5' gutterBottom textAlign={"center"}>
          Our Team will verify your Business KYC Form.
        </Typography>
        <Typography variant='h6'>Please Check back later!</Typography>
      </Box>
    </Stack>
  );
}

export default BusinessFormSuccess;
