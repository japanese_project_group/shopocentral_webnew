import DoneAllIcon from "@mui/icons-material/DoneAll";
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  FormControl,
  Modal,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { FormikProvider, useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import FormControlLabel from "@mui/material/FormControlLabel";
import MenuItem from "@mui/material/MenuItem";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import _ from "lodash";
import moment from "moment";
import NextImages from "next/image";
import Router, { useRouter } from "next/router";
import { Client, HydrationProvider } from "react-hydration-provider";
import "react-perfect-scrollbar/dist/css/styles.css";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { ValidationSchemaPersonalKyc } from "../../../common/validationSchema";

import {
  usePostPersonalKycRequestMutation,
  useUnverifiedDataUpdateMutation,
} from "../../../../redux/api/user/user";
import { setAuthState } from "../../../../redux/features/authSlice";
import { removeKycData } from "../../../../redux/features/kycDataSlice";
import { RootState } from "../../../../redux/store";
import { InputComponent } from "../../../common/Components";
import ErrorMessage from "../../../common/ErrorMessage";
import ModelBox from "../../../common/MUI/ModelBox";
import styles from "../../../components/Forms.module.css";
import { PersonalKycStatusMessage } from "../../customComponents/personalKycStatusMessage";

const FormS = () => {
  const dispatch = useDispatch();

  const router = useRouter();
  const kycDataStatus = router?.query?.status;
  const [ValueUpdate, { isSuccess, data, isLoading, error, isError }] =
    usePostPersonalKycRequestMutation();

  const [
    UnverifiedValueUpdate,
    { isSuccess: isSuccessUnverifiedUpdate, data: unverifiedData },
  ] = useUnverifiedDataUpdateMutation();

  const { userDetail } = useSelector((state: RootState) => state?.auth);
  const { kycData } = useSelector((state: RootState) => state?.kycData);
  const [citizenshipFrontImage, setCitizenshipFrontImage] = useState<any>();
  const [citizenshipBackImage, setCitizenshipBackImage] = useState<any>();
  const [passportImage, setPassportImage] = useState<any>();
  const [nriImage, setNriImage] = useState<any>();
  const [licenseImage, setLicenseImage] = useState<any>();
  const [firstSelectedImage, setFirstSelectedImage] = useState<any>();
  const [allCountryData, setAllCountryData] = useState<any>([]);
  const [previewData, setPreviewData] = useState<any>([]);
  const [previewClicked, setPreviewClicked] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [displayData, setDisplayData] = useState<any>("");
  ///index
  const [frontImageIndex, setFrontImageIndex] = useState<any>();
  const [passportImageIndex, setPassportImageIndex] = useState<any>();
  const [backImageIndex, setBackImageIndex] = useState<any>();
  const [nriImageIndex, setNriImageIndex] = useState<any>();
  const [licenseImageIndex, setLicenseImageIndex] = useState<any>();
  const [openSelect, setOpenSelect] = useState<boolean>();
  const [imageUploadField, setImageUploadCount] = useState<any>(0);
  const [anotherLanguageID, setAnotherLanguageID] =
    React.useState<any>("FALSE");
  const [addNumber, setAddNumber] = useState<any>([]);

  const pstatus = localStorage.getItem("pstatus");
  const handleAnotherLanguage = (event: any) => {
    setAnotherLanguageID(event.target.value);
  };
  const countryData = [...allCountryData].flat();
  useEffect(() => {
    if (isSuccess && data) {
      localStorage.removeItem("token");
      localStorage.removeItem("name");
      localStorage.removeItem("pstatus");
      localStorage.removeItem("userId");
      localStorage.removeItem("Id");
      localStorage.removeItem("email");
      localStorage.removeItem("locale");
      localStorage.removeItem("personal-kyc-open");
      localStorage.removeItem("phoneNumberId");
      toast("Personal KYC verification request", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      Router.push(`/kycforms/formsubmission/personal?date=${data?.createdAt}`);
    }
  }, [isSuccess, data]);

  const formik = useFormik({
    initialValues: {
      firstName: kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.firstName,
      middleName: kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.middleName,
      lastName: kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.lastName,
      occupation: kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.occupation,
      phoneType: "+81",
      alternativeName: "",
      country_en: "JAPAN",
      postalcode_en:
        kycData?.pstatus === "UNVERIFIED"
          ? ""
          : kycData?.address?.postalcode_en?.trim()?.slice(0, 4),
      postalcode_end:
        kycData?.pstatus === "UNVERIFIED"
          ? ""
          : kycData?.address?.postalcode_en?.slice(4, 8),
      prefecture_en:
        kycData?.pstatus === "UNVERIFIED"
          ? ""
          : kycData?.address?.prefecture_en,
      city_en:
        kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.address?.city_en,
      town_en:
        "" || kycData?.pstatus === "UNVERIFIED"
          ? ""
          : kycData?.address?.town_en,
      house_number:
        "" || kycData?.pstatus === "UNVERIFIED"
          ? ""
          : kycData?.address?.house_number,
      // --------------------------------------------------------------//
      documentType:
        kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.idProof?.documentType,
      idNumber:
        kycData?.pstatus === "UNVERIFIED" ? "" : kycData?.idProof?.idNumber,
      issueDate:
        kycData?.pstatus === "UNVERIFIED"
          ? ""
          : !!kycData?.idProof?.IssueDate
          ? kycData?.idProof?.IssueDate
          : "",
      validDate:
        "" || kycData?.pstatus === "UNVERIFIED"
          ? ""
          : !!kycData?.idProof?.validDate
          ? kycData?.idProof?.validDate
          : "",
      issueFrom: "",
      issuedBy: "",
      // --------------------------------------------------------------//
      frontImage: "",
      backImage: "",
      passportImage: "",
      nriImage: "",
      licenseImage: "",

      dob: "" || localStorage.getItem("dob") || kycData?.profile?.dob,
      gender: localStorage.getItem("gender") || kycData?.profile?.gender,
      nationality: kycData?.profile?.nationality || "",
      phoneNumber: localStorage.getItem("phoneNumber"),
      email: localStorage.getItem("email"),
    },
    enableReinitialize: true,
    validationSchema: ValidationSchemaPersonalKyc,
    onSubmit: async (values: any) => {
      const { postalcode_en, postalcode_end, ...rest } = values;
      const fullpostalCode = `${postalcode_en}${postalcode_end}`;

      const data = {
        postalcode_en: fullpostalCode,
        ...rest,
      };

      await submitValues(data);
    },
  });

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;

  useEffect(() => {
    if (values.documentType === "Residence Card") {
      setFieldValue("issuedBy", "Immigration Bureu Of Japan");
    } else if (values.documentType === "Driver License") {
      setFieldValue("issuedBy", "National Public Safety Commission");
    } else if (values.documentType === "Individual My Number Card") {
      setFieldValue("issuedBy", "Ministry Of Internal Affairs");
    } else if (values.documentType === "Japanese Passport") {
      setFieldValue("issuedBy", "Ministry Of Foreign Affairs");
    }
    setFieldValue("issueFrom", "Japan");
  }, [values.documentType]);

  async function submitValues(val: any) {
    if (!!citizenshipBackImage && !citizenshipFrontImage) {
      toast.error("Please upload Residence Card (front)");
    } else if (!!citizenshipFrontImage && !citizenshipBackImage) {
      toast.error("Please upload Residence Card (back)");
    } else if (
      !citizenshipBackImage &&
      !citizenshipFrontImage &&
      !nriImage &&
      !passportImage &&
      !licenseImage
    ) {
      toast.error("You must upload Documents");
    } else {
      const userId = localStorage.getItem("userId");
      const profileId = localStorage.getItem("profileId");
      const phoneNumberId = localStorage.getItem("phoneNumberId");
      const finalData: any = {
        ...val,
        userId: userId,
        frontImage: val.frontImage,
        backImage: val.backImage,
        profileId: profileId,
        phoneNumberId: phoneNumberId,
        // optionalNumber:addNumber,
      };
      setPreviewData(finalData);
      setPreviewClicked(true);
    }
  }
  const handleClose = () => setOpenModal(false);

  const onChangecertifiedRegistered = (e: any, index?: number) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (index === -1) {
      setFirstSelectedImage(e.target.files[0]);
    }
    if (file.type === "application/pdf") {
      setFieldValue("frontImage", e.target.files[0]);
      setCitizenshipFrontImage(e.target.files[0]);
      setFrontImageIndex(index);
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than or equal to 2mb", {
              type: "error",
            });
          } else {
            setFieldValue("frontImage", e.target.files[0]);
            setCitizenshipFrontImage(e.target.files[0]);
            setFrontImageIndex(index);
            setOpenSelect(false);
          }
        };
      };
    }
  };
  const onChangesealedCertificate = (e: any, index?: number) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (index === -1) {
      setFirstSelectedImage(e.target.files[0]);
    }
    if (file.type === "application/pdf") {
      setFieldValue("backImage", e.target.files[0]);
      setCitizenshipBackImage(e.target.files[0]);
      setBackImageIndex(index);
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than or equal to 2mb", {
              type: "error",
            });
          } else {
            setFieldValue("backImage", e.target.files[0]);
            setCitizenshipBackImage(e.target.files[0]);
            setBackImageIndex(index);
          }
        };
      };
    }
  };
  const onChangePassportImage = (e: any, index?: number) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (index === -1) {
      setFirstSelectedImage(e.target.files[0]);
    }
    if (file.type === "application/pdf") {
      setFieldValue("passportImage", e.target.files[0]);
      setPassportImage(e.target.files[0]);
      setPassportImageIndex(index);
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than or equal to 2mb", {
              type: "error",
            });
          } else {
            setFieldValue("passportImage", e.target.files[0]);
            setPassportImage(e.target.files[0]);
            setPassportImageIndex(index);
          }
        };
      };
    }
  };
  const onChangeNriImage = (e: any, index?: number) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (index === -1) {
      setFirstSelectedImage(e.target.files[0]);
    }
    if (file.type === "application/pdf") {
      setFieldValue("nriImage", e.target.files[0]);
      setNriImage(e.target.files[0]);
      setNriImageIndex(index);
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than or equal to 2mb", {
              type: "error",
            });
          } else {
            setFieldValue("nriImage", e.target.files[0]);
            setNriImage(e.target.files[0]);
            setNriImageIndex(index);
          }
        };
      };
    }
  };
  const onChangeLicenseImage = (e: any, index?: number) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (index === -1) {
      setFirstSelectedImage(e.target.files[0]);
    }
    if (file?.type === "application/pdf") {
      setFieldValue("licenseImage", e.target.files[0]);
      setLicenseImage(e.target.files[0]);
      setLicenseImageIndex(index);
    } else if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            toast("Please upload image with size less than or equal to 2mb", {
              type: "error",
            });
          } else {
            setFieldValue("licenseImage", e.target.files[0]);
            setLicenseImage(e.target.files[0]);
            setLicenseImageIndex(index);
          }
        };
      };
    }
  };

  // numbers are not allowed
  const handleNumber = (e: any) => {
    _.range(10).toString().includes(e.key) ||
      (["!@#$%^&*|';:?><{`}/[/\\/]=`_"].toString().includes(e.key) &&
        e.preventDefault());
  };

  function handlePreviewSubmit() {
    let fd = new FormData();
    Object.keys(previewData).map((item: any) => {
      fd.append(item, previewData[item] ? previewData[item] : null);
    });
    for (let i = 0; i < addNumber?.length; i++) {
      fd.append(`secondaryPhoneNumber[${[i]}]`, addNumber[i]?.addNumber);
    }
    let reqData = {
      data: fd,
    };
    if (kycDataStatus !== "UNVERIFIED") {
      ValueUpdate(reqData)
        .then((res: any) => {
          const finalData = {
            ...res,
            access_token: localStorage.getItem("token"),
          };
          dispatch(setAuthState(finalData));
        })
        .catch((error: any) => {
          toast.error(error);
        });
    } else {
      let body = {
        ...previewData,
        profileId: kycData?.profile?.id,
        idProofId: kycData?.idProof?.id,
        identityStatusId: kycData?.pIdentityStatusId,
      };
      let fd = new FormData();
      Object.keys(body).map((item: any) => {
        fd.append(item, body[item] ? body[item] : null);
      });
      let reqData = {
        data: fd,
      };
      UnverifiedValueUpdate({
        userId: kycData?.userId,
        data: reqData?.data,
      })
        .unwrap()
        .catch((error: any) => {
          toast.error(error);
        });
    }

    localStorage.removeItem("dob");
    localStorage.removeItem("gender");
    localStorage.removeItem("nationality");
    localStorage.removeItem("postalcodeValue");
  }

  const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    // width: { lg: "65%", sm: "70%", xs: "90%" },
    bgcolor: "background.paper",
    boxShadow: 24,
    // p: 4,
    borderRadius: 2,
  };

  function handlePreviewEdit() {
    setPreviewClicked(false);
  }
  const KYCdata: any = [
    {
      title: "Family Name",
      value: previewData?.lastName,
    },
    {
      title: "First Name",
      value: previewData?.firstName,
    },
    {
      title: "Middle Name",
      value: previewData?.middleName,
    },
    {
      title: "Native Name",
      value: previewData?.alternativeName,
    },
    {
      title: "Date Of Birth",
      value: moment(previewData?.dob).format("YYYY/MM/DD"),
    },

    {
      title: "Occupation",
      value: previewData?.occupation,
    },
    {
      title: "Nationality",
      value: previewData?.nationality,
    },
    {
      title: "Gender",
      value: previewData?.gender,
    },
    {
      title: "Mobile Number",
      value: "+81 " + previewData?.phoneNumber,
    },
    {
      title: "Optional Number",
      value: addNumber?.map((number: any, index: number) => (
        <div style={{ display: "flex", flexDirection: "column" }}>
          {addNumber?.length - 1 === index
            ? `+81 ${number.addNumber}`
            : `+81 ${number.addNumber}`}
        </div>
      )),
    },
    {
      title: "Email",
      value: previewData?.email,
    },
    {
      title: "Country",
      value: previewData?.country_en,
    },
    {
      title: "Postal Code",
      value:
        kycData?.pstatus === "UNVERIFIED" ? "" : previewData?.postalcode_en,
    },
    {
      title: "Prefecture:",
      value: previewData?.prefecture_en,
    },
    {
      title: " City/Village",
      value: previewData?.city_en,
    },
    {
      title: "Town/Ward",
      value: previewData?.town_en,
    },
    {
      title: "Building Name/Room Number",
      value: previewData?.house_number,
    },

    {
      title: "ID Document Type",
      value: previewData?.documentType,
    },
    {
      title: "ID Number",
      value: previewData?.idNumber,
    },
    {
      title: "Issue Date",
      value: moment(previewData?.issueDate).format("YYYY/MM/DD"),
    },
    {
      title: "Issue From ",
      value: previewData?.issueFrom,
    },
    {
      title: "Issued By ",
      value: previewData?.issuedBy,
    },
    {
      title: "Valid Date",
      value: moment(previewData?.validDate).format("YYYY/MM/DD"),
    },
  ];
  const frontFile = () => {
    document.getElementById("frontFile")?.click();
  };
  const backFile = () => {
    document.getElementById("backFile")?.click();
  };
  const nriFile = () => {
    document.getElementById("nriFile")?.click();
  };
  const passportFile = () => {
    document.getElementById("passportFile")?.click();
  };
  const licenseFile = () => {
    document.getElementById("licenseFile")?.click();
  };

  // handle preview pdf
  // const handlePreviewPdf = (imageItem: any) => {
  //   window.open(URL.createObjectURL(imageItem), "_blank");
  // };

  const imageMapFunc = (imageItem: any, name: string) => {
    return (
      <Box
        sx={{
          padding: 4,
          boxShadow:
            "rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px;",
          borderRadius: 2,
        }}
      >
        <Typography variant="h6" textAlign={"center"} fontWeight="bold" mb={2}>
          {name}
          {/* {console.log("imageItem", URL.createObjectURL(imageItem))} */}
        </Typography>
        {imageItem.type === "application/pdf" ? (
          <>
            <iframe
              src={URL.createObjectURL(imageItem)}
              height="350px"
              width="100%"
            />
            {/* <button onClick={()=>handlePreviewPdf(imageItem)}>Preview PDF</button> */}
          </>
        ) : (
          // <iframe
          //   src={`https://docs.google.com/viewer?url=${imageItem}&embedded=true`}
          //   height="350px"
          //   width="100%"
          //   style={{ minHeight: "400px" }}
          // />
          <NextImages
            src={
              !!imageItem?.length ? imageItem : URL.createObjectURL(imageItem)
            }
            height={350}
            width={550}
            alt={imageItem?.alt}
            className="image-view-class"
          />
        )}

        <Stack direction={"column"}>
          <ModelBox
            image={imageItem}
            title={name}
            alt={imageItem?.alt}
            status={kycDataStatus}
          />
        </Stack>
      </Box>
    );
  };

  const IdDocumentFunc = () => {
    return (
      <>
        ID Document Type:{" "}
        <Typography component="span" color="error" sx={{ marginLeft: "5px" }}>
          *
        </Typography>
        <FormControl size="small" sx={{ width: "100%" }}>
          <Select
            id="demo-simple-select"
            {...getFieldProps("documentType")}
            error={Boolean(touched.documentType && errors.documentType)}
          >
            <MenuItem value={"Residence Card"}>Residence Card</MenuItem>
            <MenuItem value={"Japanese Passport"}>Japanese Passport</MenuItem>
            <MenuItem value={"Driver License"}>Driver's License</MenuItem>
            <MenuItem value={"Individual My Number Card"}>
              Individual My Number Card
            </MenuItem>
          </Select>
          <ErrorMessage
            errors={touched.documentType && errors.documentType}
            type="min"
            name="number"
          />
        </FormControl>
      </>
    );
  };

  const uploadImage = (data?: any, index?: number) => {
    return (
      <>
        <div className="display-flex">
          <div style={{ width: data === "multiple" ? "95%" : "100%" }}>
            {/* {data === "multiple" && IdDocumentFunc()} */}
            Upload document (JPG, PNG and PDF)
            <Typography component="span" color="error" marginLeft={2}>
              *
            </Typography>
            <FormControl size="small" sx={{ width: "100%" }}>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                placeholder="select any file to upload"
                style={{ marginBottom: "15px" }}
                disabled={
                  index === passportImageIndex ||
                  (index === frontImageIndex && index === backImageIndex) ||
                  index === licenseImageIndex ||
                  index === nriImageIndex
                    ? true
                    : false
                }
              >
                {(index !== -1
                  ? !citizenshipFrontImage &&
                    values.documentType !== "Residence Card"
                  : !citizenshipFrontImage &&
                    values.documentType === "Residence Card") && (
                  <>
                    <MenuItem
                      onClick={() => {
                        frontFile();
                      }}
                    >
                      Front Image of Document
                    </MenuItem>
                    <input
                      type="file"
                      id="frontFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangecertifiedRegistered(e, index)}
                      accept=".jpeg, .png, .pdf, .jpg, .svg"
                    ></input>
                  </>
                )}
                {(index !== -1
                  ? !citizenshipBackImage &&
                    values.documentType !== "Residence Card"
                  : !citizenshipBackImage &&
                    values.documentType === "Residence Card") && (
                  <>
                    <MenuItem
                      onClick={() => {
                        backFile();
                      }}
                    >
                      Back Image of Document
                    </MenuItem>
                    <input
                      type="file"
                      id="backFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangesealedCertificate(e, index)}
                      accept=".jpeg, .png, .jpg, .svg, .pdf"
                    ></input>
                  </>
                )}
                {(index !== -1
                  ? !passportImage &&
                    values.documentType !== "Japanese Passport"
                  : !passportImage &&
                    values.documentType === "Japanese Passport") && (
                  <>
                    <MenuItem
                      onClick={() => {
                        passportFile();
                      }}
                    >
                      Japanese Passport
                    </MenuItem>
                    <input
                      type="file"
                      id="passportFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangePassportImage(e, index)}
                      accept=".jpeg, .png, .jpg, .svg, .pdf"
                    ></input>
                  </>
                )}
                {(index !== -1
                  ? !licenseImage && values.documentType !== "Driver License"
                  : !licenseImage &&
                    values.documentType === "Driver License") && (
                  <>
                    <MenuItem
                      onClick={() => {
                        licenseFile();
                      }}
                    >
                      Driver's License
                    </MenuItem>
                    <input
                      type="file"
                      id="licenseFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangeLicenseImage(e, index)}
                      accept=".jpeg, .png, .jpg, .svg, .pdf"
                    ></input>
                  </>
                )}
                {(index !== -1
                  ? !nriImage &&
                    values.documentType !== "Individual My Number Card"
                  : !nriImage &&
                    values.documentType === "Individual My Number Card") && (
                  <>
                    <MenuItem
                     onClick={() => {
                        nriFile();
                      }}
                    >
                      Individual My Number Card
                    </MenuItem>
                    <input
                      type="file"
                      id="nriFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangeNriImage(e, index)}
                      accept=".jpeg, .png, .jpg, .svg, .pdf"
                    ></input>
                  </>
                )} 
                {nriImage &&
                  citizenshipBackImage &&
                  citizenshipFrontImage &&
                  passportImage &&
                  licenseImage && <MenuItem>No Items</MenuItem>}
              </Select>
              <Box>
                <Typography sx={{ color: "#ff0000" }}>
                  {touched.frontImage && errors.frontImage && (
                    <>{errors.frontImage}</>
                  )}
                  {/* {touched.frontImage && errors.frontImage} */}
                </Typography>
              </Box>
            </FormControl>
          </div>
          {data === "multiple" && index === imageUploadField - 1 && (
            <div style={{ width: "2%", marginTop: "23px" }}>
              <Button
                variant="outlined"
                style={{ height: "40px" }}
                onClick={() => {
                  setImageUploadCount(imageUploadField - 1);
                }}
              >
                <DeleteIcon />
              </Button>
            </div>
          )}
        </div>
        <div
          style={{ marginBottom: "38px", gap: "40px" }}
          className="display-image-personal-kyc"
        >
          {citizenshipFrontImage && index === frontImageIndex && (
            <div style={{ width: "50%" }}>
              {(!!citizenshipFrontImage?.length &&
                citizenshipFrontImage?.includes(".pdf")) ||
              (!citizenshipFrontImage?.length &&
                citizenshipFrontImage?.name?.includes(".pdf")) ? (
                <iframe
                  src={
                    !!citizenshipFrontImage?.length
                      ? `http://docs.google.com/viewer?url=${citizenshipFrontImage}&embedded=true`
                      : URL.createObjectURL(citizenshipFrontImage)
                  }
                  height="284px"
                  width="100%"
                />
              ) : (
                <NextImages
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(citizenshipFrontImage);
                  }}
                  src={
                    !!citizenshipFrontImage?.length
                      ? citizenshipFrontImage
                      : URL.createObjectURL(citizenshipFrontImage)
                  }
                  height={320}
                  width={470}
                  style={{ borderRadius: "12px" }}
                  className="image-view-class"
                />
              )}

              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-start",
                }}
              >
                <div style={{ marginTop: "5px" }}>Front Image of Document</div>
                <div>
                  {!!citizenshipFrontImage?.length
                    ? !citizenshipFrontImage?.includes(".pdf")
                    : !citizenshipFrontImage?.name?.includes(".pdf") && (
                        <Button
                          variant="outlined"
                          style={{ height: "30px" }}
                          onClick={() => {
                            setOpenModal(true);
                            setDisplayData(citizenshipFrontImage);
                          }}
                        >
                          <RemoveRedEyeIcon />
                        </Button>
                      )}
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      frontFile();
                    }}
                  >
                    <EditIcon />
                    <input
                      type="file"
                      id="frontFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangecertifiedRegistered(e, index)}
                    ></input>
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      setFieldValue("frontImage", "");
                      setCitizenshipFrontImage(null);
                      setFrontImageIndex(null);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </div>
            </div>
          )}
          {citizenshipBackImage && index === backImageIndex && (
            <div style={{ width: "50%" }}>
              {(!!citizenshipBackImage?.length &&
                citizenshipBackImage?.includes(".pdf")) ||
              (!citizenshipBackImage?.length &&
                citizenshipBackImage?.name?.includes(".pdf")) ? (
                <iframe
                  src={
                    !!citizenshipBackImage?.length
                      ? `http://docs.google.com/viewer?url=${citizenshipBackImage}&embedded=true`
                      : URL.createObjectURL(citizenshipBackImage)
                  }
                  height="284px"
                  width="100%"
                />
              ) : (
                <NextImages
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(citizenshipBackImage);
                  }}
                  src={
                    !!citizenshipBackImage?.length
                      ? citizenshipBackImage
                      : URL.createObjectURL(citizenshipBackImage)
                  }
                  height={320}
                  width={470}
                  style={{ borderRadius: "12px" }}
                  className="image-view-class"
                />
              )}

              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-start",
                }}
              >
                <div style={{ marginTop: "5px" }}>Back Image of Document</div>
                <div>
                  {!!citizenshipBackImage?.length
                    ? !citizenshipBackImage?.includes(".pdf")
                    : !citizenshipFrontImage?.name?.includes(".pdf") && (
                        <Button
                          variant="outlined"
                          style={{ height: "30px" }}
                          onClick={() => {
                            setOpenModal(true);
                            setDisplayData(citizenshipFrontImage);
                          }}
                        >
                          <RemoveRedEyeIcon />
                        </Button>
                      )}
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      backFile();
                    }}
                  >
                    <EditIcon />
                    <input
                      type="file"
                      id="backFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangesealedCertificate(e, index)}
                    ></input>
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      setFieldValue("backImage", "");
                      setCitizenshipBackImage(null);
                      setBackImageIndex(null);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </div>
            </div>
          )}
          {passportImage && index === passportImageIndex && (
            <div style={{ width: "50%" }}>
              {(!!passportImage?.length && passportImage?.includes(".pdf")) ||
              (!passportImage?.length &&
                passportImage?.name?.includes(".pdf")) ? (
                <iframe
                  src={
                    !!passportImage?.length
                      ? `http://docs.google.com/viewer?url=${passportImage}&embedded=true`
                      : URL.createObjectURL(passportImage)
                  }
                  height="300px"
                  width="100%"
                />
              ) : (
                <NextImages
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(passportImage);
                  }}
                  src={
                    !!passportImage?.length
                      ? passportImage
                      : URL.createObjectURL(passportImage)
                  }
                  height={320}
                  width={470}
                  style={{ borderRadius: "12px" }}
                  className="image-view-class"
                />
              )}

              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-start",
                }}
              >
                <div style={{ marginTop: "5px" }}> Japanese Passport Image</div>
                <div>
                  {!!passportImage?.length
                    ? !passportImage?.includes(".pdf")
                    : !passportImage?.name?.includes(".pdf") && (
                        <Button
                          variant="outlined"
                          style={{ height: "30px" }}
                          onClick={() => {
                            setOpenModal(true);
                            setDisplayData(passportImage);
                          }}
                        >
                          <RemoveRedEyeIcon />
                        </Button>
                      )}
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      passportFile();
                    }}
                  >
                    <EditIcon />
                    <input
                      type="file"
                      id="passportFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangePassportImage(e, index)}
                    ></input>
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      setFieldValue("passportImage", "");
                      setPassportImage(null);
                      setPassportImageIndex(null);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </div>
            </div>
          )}
          {licenseImage && index === licenseImageIndex && (
            <div style={{ width: "50%" }}>
              {(!!licenseImage?.length && licenseImage?.includes(".pdf")) ||
              (!licenseImage?.length &&
                licenseImage?.name?.includes(".pdf")) ? (
                <iframe
                  src={
                    !!licenseImage?.length
                      ? `http://docs.google.com/viewer?url=${licenseImage}&embedded=true`
                      : URL.createObjectURL(licenseImage)
                  }
                  height="300px"
                  width="100%"
                />
              ) : (
                <NextImages
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(licenseImage);
                  }}
                  src={
                    !!licenseImage?.length
                      ? licenseImage
                      : URL.createObjectURL(licenseImage)
                  }
                  height={320}
                  width={470}
                  style={{ borderRadius: "12px" }}
                  className="image-view-class"
                />
              )}

              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-start",
                }}
              >
                <div style={{ marginTop: "5px" }}>Driver's License Image </div>
                <div>
                  {!!licenseImage?.length
                    ? !licenseImage?.includes(".pdf")
                    : !passportImage?.name?.includes(".pdf") && (
                        <Button
                          variant="outlined"
                          style={{ height: "30px" }}
                          onClick={() => {
                            setOpenModal(true);
                            setDisplayData(passportImage);
                          }}
                        >
                          <RemoveRedEyeIcon />
                        </Button>
                      )}
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      licenseFile();
                    }}
                  >
                    <EditIcon />
                    <input
                      type="file"
                      id="licenseFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangeLicenseImage(e, index)}
                    ></input>
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      setFieldValue("licenseImage", "");
                      setLicenseImage(null);
                      setLicenseImageIndex(null);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </div>
            </div>
          )}
          {nriImage && index === nriImageIndex && (
            <div style={{ width: "50%" }}>
              {(!!nriImage?.length && nriImage?.includes(".pdf")) ||
              (!nriImage?.length && nriImage?.name?.includes(".pdf")) ? (
                <iframe
                  src={
                    !!nriImage?.length
                      ? `http://docs.google.com/viewer?url=${nriImage}&embedded=true`
                      : URL.createObjectURL(nriImage)
                  }
                  height="300px"
                  width="100%"
                />
              ) : (
                <NextImages
                  onClick={() => {
                    setOpenModal(true);
                    setDisplayData(nriImage);
                  }}
                  src={
                    !!nriImage?.length
                      ? nriImage
                      : URL.createObjectURL(nriImage)
                  }
                  height={320}
                  width={470}
                  style={{ borderRadius: "12px" }}
                  className="image-view-class"
                />
              )}
              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-start",
                }}
              >
                <div style={{ marginTop: "5px" }}>
                  Individual My Number Image{" "}
                </div>
                <div>
                  {!!nriImage?.length
                    ? !nriImage?.includes(".pdf")
                    : !nriImage?.name?.includes(".pdf") && (
                        <Button
                          variant="outlined"
                          style={{ height: "30px" }}
                          onClick={() => {
                            setOpenModal(true);
                            setDisplayData(nriImage);
                          }}
                        >
                          <RemoveRedEyeIcon />
                        </Button>
                      )}
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      nriFile();
                    }}
                  >
                    <EditIcon />
                    <input
                      type="file"
                      id="nriFile"
                      style={{ display: "none" }}
                      onChange={(e) => onChangeNriImage(e, index)}
                    ></input>
                  </Button>
                  <Button
                    variant="outlined"
                    style={{ height: "30px" }}
                    onClick={() => {
                      setFieldValue("nriImage", "");
                      setNriImage(null);
                      setNriImageIndex(null);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </div>
            </div>
          )}
        </div>
      </>
    );
  };

  useEffect(() => {
    localStorage.removeItem("postalcodeValue");
    /*************/
    fetch(
      "https://valid.layercode.workers.dev/list/countries?format=select&flags=true&value=code"
    )
      .then((response) => response.json())
      .then((data) => {
        setAllCountryData(data.countries);
      });
    /***********/
    const personalKycOpen: any = localStorage.getItem("personal-kyc-open");
    if (personalKycOpen?.length <= 1 || !!!personalKycOpen) {
      Router.push("/login");
    }
  }, []);

  let imageIndex: number;
  useEffect(() => {
    if (!!kycData) {
      const imageLength = [
        kycData?.idProof?.frontImage,
        kycData?.idProof?.backImage,
        kycData?.idProof?.passportImage,
        kycData?.idProof?.licenseImage,
        kycData?.idProof?.nriImage,
      ];
      const validImage = imageLength?.filter((data: any) => data != null);
      setImageUploadCount(
        kycData?.pstatus !== "UNVERIFIED" && validImage?.length
          ? validImage?.length - 1
          : 0
      );
      imageIndex = validImage?.length;
      localStorage.setItem(
        "postalcodeValue",
        kycData?.address && kycData?.pstatus !== "UNVERIFIED"
          ? kycData?.address?.postalcode_en?.trim()?.slice(0, 3)
          : ""
      );
      localStorage.setItem("phoneNumberId", kycData?.phoneNumber?.id);
      setFirstSelectedImage(kycData?.idProof?.documentType);
      if (!!kycData?.idProof?.passportImage) {
        setPassportImage(
          kycData?.pstatus !== "UNVERIFIED" && kycData?.idProof?.passportImage
          // kycData?.idProof?.passportImage
        );
        setFieldValue("passportImage", kycData?.idProof?.passportImage);
        setPassportImageIndex(kycUnverfiedImageIndex("Japanese Passport"));
      }
      if (!!kycData?.idProof?.frontImage) {
        setCitizenshipFrontImage(
          kycData?.pstatus !== "UNVERIFIED" && kycData?.idProof?.frontImage
        );
        setFieldValue("frontImage", kycData?.idProof?.passportImage);
        setFrontImageIndex(kycUnverfiedImageIndex("Residence Card"));
      }
      if (!!kycData?.idProof?.backImage) {
        setCitizenshipBackImage(
          kycData?.pstatus !== "UNVERIFIED" && kycData?.idProof?.backImage
          // kycData?.idProof?.backImage
        );
        setFieldValue("backImage", kycData?.idProof?.backImage);
      }
      if (!!kycData?.idProof?.licenseImage) {
        setLicenseImage(
          kycData?.pstatus !== "UNVERIFIED" && kycData?.idProof?.licenseImage
          // kycData?.idProof?.licenseImage
        );
        setFieldValue("licenseImage", kycData?.idProof?.licenseImage);
        setLicenseImageIndex(kycUnverfiedImageIndex("Driver License"));
      }
      if (!!kycData?.idProof?.nriImage) {
        setNriImage(
          kycData?.pstatus !== "UNVERIFIED" && kycData?.idProof?.nriImage
          // kycData?.idProof?.nriImage
        );
        setFieldValue("nriImage", kycData?.idProof?.nriImage);
        setNriImageIndex(kycUnverfiedImageIndex("Individual My Number Card"));
      }
    }
  }, [kycData]);

  const kycUnverfiedImageIndex = (documentType: string) => {
    if (kycData?.pstatus !== "UNVERIFIED") {
      const imageLength = [
        kycData?.idProof?.frontImage,
        kycData?.idProof?.backImage,
        kycData?.idProof?.passportImage,
        kycData?.idProof?.licenseImage,
        kycData?.idProof?.nriImage,
      ];
      const validImage = imageLength?.filter((data: any) => data != null);
      setImageUploadCount(
        kycData?.idProof?.frontImage && kycData?.idProof?.backImage
          ? validImage?.length - 2
          : validImage?.length - 1
      );
      if (kycData?.idProof?.documentType === documentType) {
        return -1;
      } else {
        if (documentType === "Residence Card") {
          imageIndex = validImage?.length - imageIndex;
          setBackImageIndex(imageIndex);
          return imageIndex;
        } else if (documentType === "Japanese Passport") {
          imageIndex = validImage?.length - imageIndex;
          return imageIndex;
        } else if (documentType === "Driver License") {
          imageIndex = validImage?.length - imageIndex;
          return imageIndex;
        } else if (documentType === "Individual My Number Card") {
          imageIndex = validImage?.length - imageIndex;
          return imageIndex;
        }
      }
    }
  };

  useEffect(() => {
    if (!!unverifiedData) {
      toast.success("Successfully updated your data");
      dispatch(removeKycData());
      Router.replace("/login");
    }
  }, [unverifiedData]);
  return (
    <>
      {previewClicked === true ? (
        <>
          <HydrationProvider>
            <Client>
              <Box>
                <Box
                  sx={{
                    display: {
                      sx: "block",
                    },
                    width: {
                      xl: "70%",
                      lg: "85%",
                      md: "85%",
                      sm: "100%",
                      xs: "100%",
                    },
                    borderRadius: 4,
                    padding: 5,
                    boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
                    m: "auto",
                    mt: 12,
                    mb: 12,
                  }}
                >
                  <Typography
                    textAlign={"center"}
                    variant="h4"
                    color="primary"
                    my={5}
                  >
                    Please Confirm Your Details
                  </Typography>
                  <Divider />
                  <Box>
                    <Box
                      sx={{
                        display: {
                          xl: "flex",
                          lg: "block",
                        },
                        justifyContent: "space-between",
                      }}
                    >
                      <Box sx={{ width: "100%" }} mt={5}>
                        <Box sx={{ width: "100%" }}>
                          {Object.keys(KYCdata).map((item: any, i: number) => {
                            return (
                              <Box
                                sx={{
                                  display: "flex",
                                  width: "100%",
                                  marginBottom: "2%",
                                }}
                                key={item}
                              >
                                <Box sx={{ width: "60%" }}>
                                  <Typography variant="h6">
                                    {KYCdata[item].title} :
                                  </Typography>
                                </Box>
                                <Box sx={{ width: "70%" }}>
                                  <Typography variant="h6">
                                    {KYCdata[item].value}
                                  </Typography>
                                </Box>
                              </Box>
                            );
                          })}
                        </Box>
                      </Box>
                      <Stack
                        justifyContent={"start"}
                        alignItems="center"
                        width="100%"
                      >
                        <Stack
                          spacing={4}
                          width={{ xl: "80%", lg: "50%" }}
                          direction={{ xl: "column", lg: "column" }}
                          mt={5}
                        >
                          {kycData?.profile?.userImage &&
                            imageMapFunc(
                              kycData?.profile?.userImage,
                              "Profile Image"
                            )}
                          {!!previewData?.frontImage &&
                            imageMapFunc(
                              previewData?.frontImage,
                              "Residence card Image(front)"
                            )}
                          {!!previewData?.backImage &&
                            imageMapFunc(
                              previewData?.backImage,
                              "Residence card Image(back)"
                            )}
                          {!!previewData?.nriImage &&
                            imageMapFunc(
                              previewData?.nriImage,
                              "Individual Number Card Image"
                            )}
                          {!!previewData?.passportImage &&
                            imageMapFunc(
                              previewData?.passportImage,
                              "Passport Image"
                            )}
                          {!!previewData?.licenseImage &&
                            imageMapFunc(
                              previewData?.licenseImage,
                              "License Image"
                            )}
                        </Stack>
                      </Stack>
                    </Box>
                  </Box>
                  <Divider sx={{ marginTop: "5%" }} />
                  <Box
                    sx={{
                      width: "100%",
                      display: "flex",
                      aliginItems: "center",
                      justifyContent: "space-between",
                      marginTop: "4%",
                      marginBottom: "4%",
                    }}
                  >
                    {isLoading ? (
                      <Button
                        variant="contained"
                        color="primary"
                        sx={{ width: "100%" }}
                        startIcon={
                          <CircularProgress size="1.5rem" color="inherit" />
                        }
                      ></Button>
                    ) : (
                      <>
                        <Box></Box>
                        <Box
                          sx={{
                            width: {
                              lg: "50%",
                              md: "50%",
                              sm: "100%",
                              xs: "100%",
                            },
                            justifyContent: "space-between",
                          }}
                        >
                          <Button
                            onClick={handlePreviewEdit}
                            variant="contained"
                            color="primary"
                            sx={{ width: "45%", marginRight: "2%" }}
                          >
                            edit
                          </Button>
                          <Button
                            onClick={handlePreviewSubmit}
                            variant="contained"
                            color="primary"
                            sx={{ width: "45%", marginLeft: "2%" }}
                          >
                            submit
                          </Button>
                        </Box>
                      </>
                    )}
                  </Box>
                </Box>
              </Box>
            </Client>
          </HydrationProvider>
        </>
      ) : (
        <>
          <>
            <Box
              className={styles.personalKycBackground}
              sx={{
                p: 5,
              }}
            >
              {pstatus === "UNVERIFIED" && (
                <PersonalKycStatusMessage
                  status="UNVERIFIED"
                  message={kycData?.message}
                />
              )}

              {pstatus === "PENDING" && 
              <PersonalKycStatusMessage
              status="PENDING"
              
            />}

              <Stack
                direction="column"
                justifyContent="center"
                width={{ sm: "90%", lg: "60%" }}
                alignItems={"center"}
                sx={{ p: 5, m: "auto" }}
                className={styles.glassMorphism}
              >
                <Typography
                  variant="h4"
                  color="primary"
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginBottom: "4%",
                  }}
                >
                  Personal Identity Verification
                </Typography>
                <Typography
                  variant="h6"
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  Please fill in the form as per your identity verification
                  document.
                </Typography>

                <FormikProvider value={formik}>
                  <form onSubmit={handleSubmit} autoComplete="off">
                    <Box sx={{ border: "2", marginTop: "2%" }}>
                      <Box sx={{ padding: "3%" }}>
                        <Typography
                          variant="h5"
                          color="primary"
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            marginBottom: "1%",
                          }}
                        >
                          Personal Information:
                        </Typography>
                        Family Name:{" "}
                        <Typography component="span" color="error">
                          *
                        </Typography>
                        <TextField
                          {...getFieldProps("lastName")}
                          size="small"
                          fullWidth
                          error={Boolean(touched.lastName && errors.lastName)}
                          helperText={
                            Boolean(touched.lastName && errors.lastName) &&
                            String(touched.lastName && errors.lastName)
                          }
                          onKeyDown={handleNumber}
                        />
                        First Name:
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          size="small"
                          {...getFieldProps("firstName")}
                          fullWidth
                          error={Boolean(touched.firstName && errors.firstName)}
                          helperText={
                            Boolean(touched.firstName && errors.firstName) &&
                            String(touched.firstName && errors.firstName)
                          }
                          onKeyDown={handleNumber}
                        />
                        Middle Name:
                        <TextField
                          size="small"
                          {...getFieldProps("middleName")}
                          fullWidth
                          error={Boolean(
                            touched.middleName && errors.middleName
                          )}
                          onKeyDown={handleNumber}
                          helperText={
                            Boolean(touched.middleName && errors.middleName) &&
                            String(touched.middleName && errors.middleName)
                          }
                        />
                        <Box>
                          <Typography>
                            Is your name in any other language than english ?
                          </Typography>
                          <FormControl>
                            <RadioGroup
                              row
                              aria-labelledby="demo-controlled-radio-buttons-group"
                              name="controlled-radio-buttons-group"
                              value={anotherLanguageID}
                              onChange={handleAnotherLanguage}
                            >
                              <FormControlLabel
                                value="TRUE"
                                control={<Radio />}
                                label="Yes"
                              />
                              <FormControlLabel
                                value="FALSE"
                                control={<Radio />}
                                label="No"
                              />
                            </RadioGroup>
                          </FormControl>
                        </Box>
                        <Box>
                          {anotherLanguageID === "TRUE" ? (
                            <>
                              Enter Full Name in Native
                              <TextField
                                size="small"
                                {...getFieldProps("alternativeName")}
                                fullWidth
                              />
                            </>
                          ) : (
                            <></>
                          )}
                        </Box>
                        Occupation:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <FormControl size="small" sx={{ width: "100%" }}>
                          <Select
                            id="demo-simple-select"
                            {...getFieldProps("occupation")}
                            error={Boolean(
                              touched.occupation && errors.occupation
                            )}
                          >
                            <MenuItem value={"Student"}>Student</MenuItem>
                            <MenuItem value={"Part Time Employee"}>
                              Part Time Employee
                            </MenuItem>
                            <MenuItem value={"Full Time Employee"}>
                              Full Time Employee
                            </MenuItem>
                            <MenuItem value={"Business Owner"}>
                              Business Owner
                            </MenuItem>
                            <MenuItem value={"Self Employed"}>
                              Self Employed
                            </MenuItem>
                            <MenuItem value={"Dependent"}>Dependent</MenuItem>
                            <MenuItem value={"Other"}>Other</MenuItem>
                          </Select>
                          <ErrorMessage
                            errors={touched.occupation && errors.occupation}
                            type="min"
                            name="number"
                          />
                        </FormControl>
                        Date of Birth:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <LocalizationProvider dateAdapter={AdapterMoment}>
                          <DatePicker
                            maxDate={moment().format("YYYY-MM-DD")}
                            {...getFieldProps("dob")}
                            onChange={(newValue) => {
                              setFieldValue(
                                "dob",
                                moment(newValue).format("YYYY-MM-DD")
                              );
                            }}
                            inputFormat="YYYY/MM/DD"
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                type="date"
                                size="small"
                                fullWidth
                                error={Boolean(touched.dob && errors.dob)}
                                helperText={
                                  Boolean(touched.dob && errors.dob) &&
                                  String(touched.dob && errors.dob)
                                }
                              />
                            )}
                          />
                        </LocalizationProvider>
                        Nationality:{""}
                        <FormControl fullWidth>
                          <Autocomplete
                            options={allCountryData}
                            sx={{ width: 300 }}
                            style={{ width: "100%" }}
                            autoHighlight
                            clearIcon={false}
                            onChange={(event: any, value: any) => {
                              setFieldValue("nationality", value?.label);
                            }}
                            defaultValue={kycData?.profile?.nationality || ""}
                            renderInput={(params: any) => (
                              <>
                                {/* {console.log(kycData?.pstatus )} */}
                                {/* {console.log(getFieldProps("nationality"))} */}
                                <TextField
                                  {...params}
                                  {...getFieldProps("nationality")}
                                  error={Boolean(
                                    touched.nationality && errors.nationality
                                  )}
                                  helperText={
                                    Boolean(
                                      touched.nationality && errors.nationality
                                    ) &&
                                    String(
                                      touched.nationality && errors.nationality
                                    )
                                  }
                                  size="small"
                                />
                              </>
                            )}
                          />
                        </FormControl>
                        Gender:{""}
                        <FormControl fullWidth>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            error={Boolean(touched.gender && errors.gender)}
                            {...getFieldProps("gender")}
                            size="small"
                          >
                            <MenuItem value={"MALE"}>Male</MenuItem>
                            <MenuItem value={"FEMALE"}>Female</MenuItem>
                            <MenuItem value={"OTHER"}>Others</MenuItem>
                          </Select>
                          <ErrorMessage
                            errors={touched.gender && errors.gender}
                            type="min"
                            name="number"
                          />
                        </FormControl>
                        <InputComponent
                          type="number"
                          disabled={true}
                          label="Mobile Number"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="phoneNumber"
                          displayIcon={<DoneAllIcon />}
                        />
                        <InputComponent
                          type="secondaryPhoneNumber"
                          addNumber={addNumber}
                          setAddNumber={setAddNumber}
                        />
                        <InputComponent
                          type="text"
                          disabled={true}
                          label="Email"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="email"
                          displayIcon={<DoneAllIcon />}
                        />
                      </Box>
                      <Box sx={{ padding: "3%" }}>
                        <Typography
                          variant="h5"
                          color="primary"
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "1%",
                          }}
                        >
                          Address:
                        </Typography>
                        <InputComponent
                          type="text"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="country_en"
                          touched={touched.country_en}
                          errors={errors.country_en}
                          label="Country:"
                          disabled={true}
                        />
                        <InputComponent
                          label="Postal Code:"
                          type="postalCode"
                          touched1={touched.postalcode_en}
                          touched2={touched.postalcode_end}
                          errors1={errors.postalcode_en}
                          errors2={errors.postalcode_end}
                          getFieldProps={getFieldProps}
                          values={values}
                          setFieldValue={setFieldValue}
                        />

                        <InputComponent
                          type="text"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="prefecture_en"
                          touched={touched.prefecture_en}
                          errors={errors.prefecture_en}
                          label="Prefecture:"
                        />
                        <InputComponent
                          type="text"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="city_en"
                          touched={touched.city_en}
                          errors={errors.city_en}
                          label="City/Village:"
                        />

                        <InputComponent
                          type="text"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="town_en"
                          touched={touched.town_en}
                          errors={errors.town_en}
                          label="Town / Ward:"
                        />

                        <InputComponent
                          type="text"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="house_number"
                          touched={touched.house_number}
                          errors={errors.house_number}
                          label="Building Name/Room Number:"
                        />
                      </Box>

                      {/* ------------------------------------------------------------------------------------- */}
                      <Box sx={{ padding: "3%" }}>
                        <Typography
                          variant="h5"
                          color="primary"
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            margin: "1%",
                          }}
                        >
                          Identity Document
                        </Typography>
                        {IdDocumentFunc()}
                        ID Number:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <TextField
                          size="small"
                          type="string"
                          {...getFieldProps("idNumber")}
                          fullWidth
                          error={Boolean(touched.idNumber && errors.idNumber)}
                          helperText={
                            Boolean(touched.idNumber && errors.idNumber) &&
                            String(touched.idNumber && errors.idNumber)
                          }
                        />
                        Issue Date:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <Box>
                          <LocalizationProvider dateAdapter={AdapterMoment}>
                            <DatePicker
                              maxDate={moment().format("YYYY-MM-DD")}
                              {...getFieldProps("issueDate")}
                              onChange={(newValue) => {
                                setFieldValue(
                                  "issueDate",
                                  moment(newValue).format("YYYY-MM-DD")
                                );
                              }}
                              inputFormat="YYYY-MM-DD"
                              renderInput={(params) => (
                                <TextField
                                  size="small"
                                  {...params}
                                  fullWidth
                                  error={Boolean(
                                    touched.issueDate && errors.issueDate
                                  )}
                                  helperText={
                                    Boolean(
                                      touched.issueDate && errors.issueDate
                                    ) &&
                                    String(
                                      touched.issueDate && errors.issueDate
                                    )
                                  }
                                />
                              )}
                            />
                          </LocalizationProvider>
                        </Box>
                        Issue From:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <FormControl size="small" sx={{ width: "100%" }}>
                          <Select
                            id="demo-simple-select"
                            {...getFieldProps("issueFrom")}
                            error={Boolean(
                              touched.issueFrom && errors.issueFrom
                            )}
                          >
                            <MenuItem value="Japan">Japan</MenuItem>
                          </Select>
                          <ErrorMessage
                            errors={touched.issueFrom && errors.issueFrom}
                            type="min"
                            name="number"
                          />
                        </FormControl>
                        Issue By:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <FormControl size="small" sx={{ width: "100%" }}>
                          <Select
                            id="demo-simple-select"
                            {...getFieldProps("issuedBy")}
                            error={Boolean(touched.issuedBy && errors.issuedBy)}
                            disabled={true}
                          >
                            <MenuItem value={"Immigration Bureu Of Japan"}>
                              Immigration Bureau of Japan
                            </MenuItem>
                            <MenuItem
                              value={"National Public Safety Commission"}
                            >
                              National Public Safety Commission
                            </MenuItem>
                            <MenuItem value={"Ministry Of Internal Affairs"}>
                              Ministry of Internal Affairs and Communications
                            </MenuItem>
                            <MenuItem value={"Ministry Of Foreign Affairs"}>
                              Ministry of Foreign Affairs
                            </MenuItem>
                          </Select>
                          <ErrorMessage
                            errors={touched.issuedBy && errors.issuedBy}
                            type="min"
                            name="number"
                          />
                        </FormControl>
                        Valid Date:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <Box style={{ marginBottom: "5px" }}>
                          <LocalizationProvider dateAdapter={AdapterMoment}>
                            <DatePicker
                              minDate={moment().format("YYYY-MM-DD")}
                              {...getFieldProps("validDate")}
                              onChange={(newValue) => {
                                setFieldValue(
                                  "validDate",
                                  moment(newValue).format("YYYY-MM-DD")
                                );
                              }}
                              inputFormat="YYYY-MM-DD"
                              renderInput={(params) => (
                                <TextField
                                  size="small"
                                  {...params}
                                  fullWidth
                                  error={Boolean(
                                    touched.validDate && errors.validDate
                                  )}
                                  helperText={
                                    Boolean(
                                      touched.validDate && errors.validDate
                                    ) &&
                                    String(
                                      touched.validDate && errors.validDate
                                    )
                                  }
                                />
                              )}
                            />
                          </LocalizationProvider>
                        </Box>
                        {/* ------------------------------------------------------------------------------------- */}
                        {uploadImage("", -1)}
                        <div>
                          <div>
                            {[...Array(imageUploadField).keys()]?.map(
                              (count: number, index: number) => (
                                <div key={index}>
                                  {uploadImage("multiple", index)}
                                </div>
                              )
                            )}
                          </div>
                          {imageUploadField < 3 && (
                            <div className="display-flex-end">
                              <Button
                                variant="contained"
                                style={{ marginBottom: "20px" }}
                                onClick={() => {
                                  imageUploadField < 3 &&
                                    setImageUploadCount(imageUploadField + 1);
                                }}
                              >
                                Add More Files
                              </Button>
                            </div>
                          )}
                        </div>
                        <Box
                          sx={{
                            width: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <Button
                            disabled={isSubmitting}
                            type="submit"
                            variant="contained"
                            color="primary"
                            sx={{ width: "80%" }}
                          >
                            Submit
                          </Button>
                        </Box>
                        {openModal && (
                          <Modal
                            open={openModal}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                          >
                            <Box sx={style}>
                              <Stack
                                direction="column"
                                justifyContent={"center"}
                              >
                                <img
                                  src={
                                    !!displayData?.length
                                      ? displayData
                                      : URL.createObjectURL(displayData)
                                  }
                                  className="image-view"
                                />
                              </Stack>
                            </Box>
                          </Modal>
                        )}
                        {/* ------------------------------------------------------------------------------------- */}
                      </Box>
                    </Box>
                    {/* ------------------------------------------------------------------------------------- */}
                  </form>
                </FormikProvider>
              </Stack>
            </Box>
          </>
        </>
      )}
    </>
  );
};
export default FormS;
