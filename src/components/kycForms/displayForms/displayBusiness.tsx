import Router from "next/router";
import { Paper, Typography, Box, Button } from "@mui/material";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { RootState } from "../../../../redux/store";
const DisplayBusiness = () => {
  const kycData = useSelector(
    (state: RootState) => state?.profile?.profileData?.identityStatus?.[0]
  );

  const val = 3;
  const router = useRouter();
  const handleClick = () => {
    router.push({
      pathname: "/kycforms/displayforms/individual",
      query: { id: val },
    });
  };
  function handleAddBusinessKyc() {
    Router.push("/kycforms/businesskyc");
  }

  return (
    <>
      {kycData ? (
        <>
          <Box sx={{ width: "95%", margin: "2%", height: "100%" }}>
            <Paper
              sx={{
                padding: "2%",
                display: {
                  md: "flex",
                  sx: "block",
                },
                justifyContent: "space-between",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  margin: "1%",
                }}
              >
                Registered Trade Name: {kycData?.registerTradeName}
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  margin: "1%",
                }}
              >
                Verification Status: {kycData?.status}
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  margin: "1%",
                }}
              >
                <Button
                  variant='contained'
                  color='primary'
                  onClick={handleClick}
                >
                  View
                </Button>
              </Box>
            </Paper>
          </Box>
        </>
      ) : (
        <>
          <Paper
            sx={{
              padding: "2%",
              marginTop: "2%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {" "}
            <Typography> No Business Kyc Found!!! </Typography>
            <Button
              variant='contained'
              color='primary'
              onClick={handleAddBusinessKyc}
              sx={{ marginLeft: "5%" }}
            >
              Add Business Kyc
            </Button>
          </Paper>
        </>
      )}
    </>
  );
};
export default DisplayBusiness;
