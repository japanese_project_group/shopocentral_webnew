import { useGetAllDataQuery } from "../../../../redux/api/user/user";
import { useEffect, useState } from "react";
import { Box, Typography, Paper, Stack, Divider } from "@mui/material";
import Image from "next/image";
import { HydrationProvider, Client } from "react-hydration-provider";
import Router from "next/router";
import { useSelector } from "react-redux";
import { RootState } from "../../../../redux/store";
import Link from "next/link";
import ModelBox from "../../../common/MUI/ModelBox";
const DisplayPersonal = () => {
  const language = useSelector((state: RootState) => state?.lang?.lang);
  const [details, setDetails] = useState<any>();
  const id = useSelector((state: RootState) => state?.auth?.userDetail?.id);
  const {
    data: allData,
    isSuccess,
    data,
    error,
    isError,
    refetch,
  } = useGetAllDataQuery();
  useEffect(() => {
    if (allData) {
      setDetails(allData);
    }
  }, [isSuccess, allData]);
  useEffect(() => {
    refetch();
  }, []);

  const userDetail: any = [
    {
      title: "First Name",
      value: details?.data?.firstName,
    },
    {
      title: "Middle Name",
      value: details?.data?.middleName,
    },
    {
      title: "Last Name",
      value: details?.data?.lastName,
    },
    {
      title: "City",
      value: details?.data?.address?.city_en,
    },
    {
      title: "Town",
      value: details?.data?.address?.town_en,
    },
    {
      title: "District",
      value: details?.data?.address?.district_en,
    },
    {
      title: "Country",
      value: details?.data?.address?.country_en,
    },
    {
      title: "Postal Code",
      value: details?.data?.address?.postalcode_en,
    },
    {
      title: "Gender",
      value: details?.data?.profile?.gender,
    },
    {
      title: "Prefecture:",
      value: details?.data?.address?.prefecture_en,
    },

    {
      title: "Issue Form ",
      value: details?.data?.idProof?.issueForm,
    },
    {
      title: "Issue Date",
      value: new Date(details?.data?.idProof?.IssueDate).toDateString(),
    },
    {
      title: "Valid Date",
      value: new Date(details?.data?.idProof?.validDate).toDateString(),
    },
  ];

  const imageItem = [
    {
      name: "Front Image of Document:",
      image: details?.data?.idProof?.frontImage,
      alt: "Front Image of Document",
    },
    {
      name: "Back Image of Document:",
      image: details?.data?.idProof?.backImage,
      alt: "Back Image of Document",
    },
  ];

  return (
    <>
      <HydrationProvider>
        <Client>
          <Box>
            <Box
              sx={{
                display: {
                  sx: "block",
                },
                width: {
                  xl: "70%",
                  lg: "85%",
                  md: "95%",
                  sm: "100%",
                  xs: "100%",
                },
                borderRadius: 4,
                padding: 5,
                boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
                m: "auto",
                mt: 5,
              }}
            >
              <Typography
                textAlign={"center"}
                variant='h4'
                color='primary'
                my={5}
              >
                Personal Kyc Details
              </Typography>
              <Divider />
              <Box>
                <Box
                  sx={{
                    display: {
                      xl: "flex",
                      lg: "block",
                    },
                    justifyContent: "space-between",
                  }}
                >
                  <Box sx={{ width: "100%" }} mt={5}>
                    <Box sx={{ width: "100%" }}>
                      {/* map item */}
                      {userDetail.map((item: any, i: number) => {
                        return (
                          <Box
                            sx={{
                              display: "flex",
                              width: "100%",
                              marginBottom: "2%",
                            }}
                            key={i}
                          >
                            <Box sx={{ width: "50%" }}>
                              <Typography variant='h6' fontWeight={"bold"}>
                                {item.title}:
                              </Typography>
                            </Box>
                            <Box sx={{ width: "70%" }}>
                              <Typography variant='h6'>{item.value}</Typography>
                            </Box>
                          </Box>
                        );
                      })}

                    </Box>
                  </Box>
                  <Stack
                    justifyContent={"center"}
                    alignItems='center'
                    width='100%'
                  >
                    <Stack
                      spacing={4}
                      width={{ xl: "80%", lg: "90%" }}
                      direction={{ xl: "column", lg: "row" }}
                      mt={5}
                    >
                      {imageItem.map((item: any, i: number) => {
                        return (
                          <Box
                            sx={{
                              padding: 4,
                              boxShadow:
                                "rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px;",
                              // boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px;",
                              borderRadius: 2,
                            }}
                            key={i}
                          >
                            <Typography
                              variant='h6'
                              textAlign={"center"}
                              fontWeight='bold'
                              mb={2}
                            >
                              {item.name}
                            </Typography>

                            <Image
                              src={item.image}
                              height={350}
                              width={550}
                              alt={item.alt}
                            />
                            <Stack direction={"column"}>
                              <ModelBox
                                image={item.image}
                                title={item.name}
                                alt={item.alt}
                              />
                            </Stack>
                          </Box>
                        );
                      })}
                    </Stack>
                  </Stack>
                </Box>
              </Box>
            </Box>
          </Box>
        </Client>
      </HydrationProvider>
    </>
  );
};
export default DisplayPersonal;
