import React from "react";
import { Typography } from "@mui/material";
import { Box } from "@mui/material";
import styles from "../../Forms.module.css"
function NoPersonalKYC() {
    return (
        <Box sx={{ padding: "2%" }}>

            <Box
                className={styles.forms}
                sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                }}
            >

                <Typography variant="h4" color="primary" gutterBottom sx={{ marginBottom: "2%" }}>
                    NO PERSONAL KYC SUBMITTED
                </Typography>

                <Typography variant="h4" gutterBottom>
                    Please Submit Personal KYC From Our Mobile Application
                </Typography>
            </Box>
        </Box >
    );
}

export default NoPersonalKYC;
