// Build in library
import Link from "next/link";
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import {
  Box,
  Button,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";

// Third party library
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Form, FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

// Redux
import { useCheckmailmerchantMutation } from "../../../../redux/api/auth/auth";
import { setKycData } from "../../../../redux/features/kycDataSlice";
// style
import styles from "../../../../src/components/Forms.module.css";
// validation
import { validationSchemaaRegister } from "../../../common/validationSchema";

const RegisterPage = () => {
  const dispatch = useDispatch();
  const [submitValues, { isLoading, isSuccess, data: allData }] =
    useCheckmailmerchantMutation();
  const [showPassword, setShowPassword] = useState(false);

  console.log(allData);
  type valid = {
    email: string;
    password: string;
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchemaaRegister,
    onSubmit: async (values) => {
      localStorage.setItem("email", values?.email);
      await submitValues(values)
        .unwrap()
        .catch((error: any) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    },
  });

  const { values, touched, errors, handleChange, handleBlur, getFieldProps } =
    formik;
  useEffect(() => {
    if (isSuccess && allData) {
      localStorage.removeItem("verificationEmail");
      localStorage.removeItem("Id");
      localStorage.setItem("userId", allData?.userId);
      localStorage.setItem("phoneNumber", allData?.phonenumber?.phoneNumber);
      toast("Customer Account Identified", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      if (allData?.verified === false) {
        Router.replace("verifyotp");
      } else if (allData?.pstatus === "FINITIAL") {
        if (isEmpty(allData?.profile)) {
          Router.push("createaccountform");
        } else {
          localStorage.setItem("personal-kyc-open", "data");
          localStorage.setItem("dob", allData?.profile.dob);
          localStorage.setItem("gender", allData?.profile.gender);
          localStorage.setItem("nationality", allData?.profile?.nationality);
          localStorage.setItem("profileId", allData?.profile?.id);
          localStorage.setItem("phoneNumberId", allData?.phonenumber?.id);
          dispatch(setKycData(allData));
          Router.push("kycforms/personalkyc");
        }
      } else if (allData?.pstatus === "INITIAL") {
        Router.push(
          `kycforms/formsubmission/personal?date=${allData?.createdAt}`
        );
      } else if (allData?.pstatus === "PENDING") {
        localStorage.setItem("dob", allData?.profile.dob);
        localStorage.setItem("gender", allData?.profile.gender);
        localStorage.setItem("nationality", allData?.profile?.nationality);
        localStorage.setItem("profileId", allData?.profile?.id);
        localStorage.setItem("phoneNumberId", allData?.phonenumber?.id);
        localStorage.setItem("pstatus", allData?.pstatus);
        dispatch(setKycData(allData));

        Router.push(
          `kycforms/formsubmission/${allData?.pstatus}?message=${allData?.message}`
        );
      } else if (allData?.pstatus === "UNVERIFIED") {
        dispatch(setKycData(allData));
        console.log("here");
        localStorage.setItem("personal-kyc-open", "data");
        localStorage.setItem("pstatus", allData?.pstatus);
        // setKycData({profile:allData?.profile,phoneNumber:allData?.phonenumber,pstatus:allData?.pstatus,})
        Router.push(`kycforms/personalkyc?status=UNVERIFIED`);
      } else if (allData?.pstatus === "VERIFIED") {
        Router.push("register/businessaccount/");
      }
    }
  }, [isSuccess, allData]);

  return (
    <>
      <Box
        sx={{
          background: "#0072d8",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          // width: "50%",
          height: "100vh",
          display: "flex",
          padding: "5%",
        }}
      >
        <Box className={styles.forms} sx={{ padding: "2%" }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              margin: "2%",
            }}
          >
            <Box
              sx={{
                mb: 5,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h4"
                color="primary"
                component="div"
                align="center"
              >
                Login to your Shopo User account
              </Typography>
            </Box>
            <Box
              sx={{
                mt: 1,
                width: { sm: "450px", xs: "100%" },
              }}
            >
              <FormikProvider value={formik}>
                <Form>
                  <Box mb={4}>
                    <Typography sx={{ color: "black", fontWeight: "bold" }}>
                      Shopo User ID:
                    </Typography>
                    <TextField
                      fullWidth
                      variant="outlined"
                      {...getFieldProps("email")}
                      onBlur={handleBlur}
                      error={touched.email && Boolean(errors.email)}
                      helperText={touched.email ? errors.email : ""}
                      placeholder={"Please enter your email or mobile number"}
                      type="email"
                      sx={{ my: 1 }}
                    />
                  </Box>

                  <Box>
                    <Typography sx={{ color: "black", fontWeight: "bold" }}>
                      Password :
                    </Typography>
                    <OutlinedInput
                      id="outlined-adornment-password"
                      fullWidth
                      type={showPassword ? "text" : "password"}
                      {...getFieldProps("password")}
                      placeholder={"Please enter your password"}
                      onBlur={handleBlur}
                      error={touched.password && Boolean(errors.password)}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={() =>
                              setShowPassword((prev: boolean) => !prev)
                            }
                            edge="end"
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {touched.password && errors.password && (
                      <FormHelperText error id="accountId-error">
                        {errors.password}
                      </FormHelperText>
                    )}
                  </Box>

                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    style={{ margin: "2rem 0 1rem 0" }}
                  >
                    <Button
                      type="submit"
                      fullWidth
                      size="large"
                      variant="contained"
                      color="primary"
                    >
                      Submit
                    </Button>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: "5%",
                    }}
                  >
                    <Typography
                      textAlign={"center"}
                      sx={{ cursor: "pointer", marginRight: "2%" }}
                    >
                      Does Not Have Shopo User ID?
                    </Typography>
                    <Link href="/register/customer-account" passHref>
                      <Typography
                        textAlign={"center"}
                        color="blue"
                        sx={{ cursor: "pointer" }}
                      >
                        Create Now
                      </Typography>
                    </Link>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: "2%",
                    }}
                  >
                    <Typography
                      textAlign={"center"}
                      sx={{ cursor: "pointer", marginRight: "2%" }}
                    >
                      Already Have a Business Account?
                    </Typography>
                    <Link href="/login" passHref>
                      <Typography
                        textAlign={"center"}
                        color="blue"
                        sx={{ cursor: "pointer" }}
                      >
                        Click here to login
                      </Typography>
                    </Link>
                  </Box>

                  <Typography
                    sx={{ marginTop: "1.5rem", cursor: "pointer" }}
                    component="div"
                    align="center"
                  >
                    © 2023 Shopo Central. All rights reserved
                  </Typography>
                </Form>
              </FormikProvider>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default RegisterPage;
