// Build in library
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import {
  Box,
  Button,
  FormHelperText,
  TextField,
  Typography,
} from "@mui/material";

// third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Assets
import loginPic from "../../../../../public/loginPic.png";
// redux
import { useVerifyOTPMutation } from "../../../../../redux/api/auth/auth";
// Vaidation
import { validationSchemaOtp } from "../../../../common/validationSchema";

const VerifyCustomerOtp = () => {
  const router = useRouter();
  const [reqdata, setReqData] = useState();
  const [verifyOTP, { isLoading, isSuccess, data, isError }] =
    useVerifyOTPMutation();

  useEffect(() => { }, [reqdata]);

  useEffect(() => {
    if (data && isSuccess) {
      toast("OTP Verification Successfull", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      router.push({
        pathname: "/verifypersonalkycforms/details",
        query: {},
      });
      localStorage.removeItem("email");
    }
  }, [data, isSuccess]);

  useEffect(() => {
    if (isError) {
      toast("OTP Does Not Match", {
        autoClose: 2500,
        type: "error",
        pauseOnHover: true,
      });
    }
  }, [isError]);

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Box
          sx={{
            my: 10,
            mx: 4,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
          }}
        >
          <Box
            sx={{
              mb: 10,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h3"
              color="primary"
              component="div"
              align="center"
            >
              {" "}
              OTP Verification
            </Typography>
          </Box>
          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <Formik
              initialValues={{
                code: "",
              }}
              onSubmit={async (values: any) => {
                const email = localStorage.getItem("email");
                values = { ...values, email: email };
                setReqData(values);
                await verifyOTP(values);
              }}
              validationSchema={validationSchemaOtp}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Box sx={{ marginBottom: "20%", marginTop: "-12%" }}>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        We have sent a Verification code in your email!
                      </Typography>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          marginTop: "8%",
                        }}
                      >
                        Please check your email
                      </Typography>
                    </Box>
                    <Box mb={4}>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        Code
                      </Typography>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="code"
                        disabled={isLoading}
                        value={values.code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.code && Boolean(errors.code)}
                        placeholder={"Code"}
                      />
                      {touched.code && errors.code && (
                        <FormHelperText>{errors.code}</FormHelperText>
                      )}
                    </Box>
                    <Button
                      type="submit"
                      fullWidth
                      size="large"
                      variant="contained"
                      color="primary"
                      disabled={isLoading}
                    >
                      Verify Otp
                    </Button>
                  </form>
                );
              }}
            </Formik>
          </Box>
        </Box>
        <Box
          sx={{
            flex: 1,
            width: "50%",
            height: "100vh",
            display: {
              xs: "none",
              md: "flex",
            },
          }}
        >
          <Image src={loginPic} />
        </Box>
      </Box>
    </>
  );
};

export default VerifyCustomerOtp;
