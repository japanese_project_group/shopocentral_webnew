// Build inn library
import Router from "next/router";
import React, { useEffect, useState } from "react";
// Third party library
import { Form, Formik } from "formik";
import { toast } from "react-toastify";
// Redux
import {
  useCreatebusinessaccountMutation
} from "../../../../redux/api/auth/auth";

// MUI Components
import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
  Stack,
  Typography
} from "@mui/material";
// Types
import { companyType } from "../../../../src/common/commonData";
// style
import styles from "../../../../src/components/Forms.module.css";
// components
import { InputComponent } from "../../../common/Components";
import { validationSchemaBusiness } from "../../../common/validationSchema";

const BusinessAccountForm = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [adminYesValue, setAdminYesValue] = useState<boolean>(false);
  const [adminNoValue, setAdminNoValue] = useState<boolean>(false);
  const [addNumber, setAddNumber] = useState<any>([])

  const [submitValues, { isLoading, isSuccess, data, isError, error }] =
    useCreatebusinessaccountMutation();

  function handleChangeAdmin(e: any, value: any) {
    if (value === "yes") {
      setAdminYesValue(!adminYesValue);
      !adminYesValue && setAdminNoValue(false);
    } else {
      setAdminNoValue(!adminNoValue);
      !adminNoValue && setAdminYesValue(false);
    }
  }

  useEffect(() => {
    if (isSuccess && data) {
      toast("Business Account Created", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      localStorage.setItem("businessId", data?.data?.address?.businessId);
      Router.push("/login");
    }
  }, [isSuccess, data]);

  return (
    <Box className={styles.background}>
      <Box
        className={styles.glassMorphism}
        sx={{
          zIndex: "20",
          p: 2,
          width: { lg: "45%", md: "65%", sm: "85%", xs: "95%" },
          m: "auto",
          my: 4,
        }}
      >
        <Box>
          {/* <Scrollbars className='scrollBar' style={{ height: 900, width: 600 }}> */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginBottom: "4%",
            }}
          >
            <Typography
              variant="h4"
              color="primary"
              component="div"
              align="center"
              sx={{ marginBottom: "2%", mt: 2 }}
            >
              Create Your Business Account
            </Typography>
            <Typography variant="subtitle1" component="div" align="center">
              Please Enter Your Business Account Details
            </Typography>
          </Box>
          <Box sx={{ paddingLeft: "5%" }}>
            <Formik
              initialValues={{
                name_en: "",
                katagana_name: "",
                businessEmail: "",
                registerTradeName: "",
                // furigana: "",
                // englishname: "",
                type: "PERSONAL",
                password: "",
                city_en: "",
                password_confirmation: "",
                country_en: "Japan",
                postalcode_en: "",
                prefecture_en: "",
                district_en: "",
                town_en: "",
                state_en: "",
                phoneType: "+977",
                phoneNumber: "",
                phoneNumber2: "",
                postalcode_end: "",
                businessAccessId: "",
                house_number: "",
              }}
              onSubmit={async (values) => {
                const email: any = localStorage.getItem("verificationEmail");
                const userId: any = localStorage.getItem("userId");
                const {
                  password_confirmation,
                  postalcode_en,
                  postalcode_end,
                  ...rest
                } = values;
                const fullpostalCode = ` ${postalcode_en}${postalcode_end}`;
                const finalData = {
                  email: email,
                  postalcode_en: fullpostalCode,
                  userId: userId,
                  isCEO: adminYesValue ? true : false,
                  secondaryPhoneNumber: addNumber?.map(
                    (number: any) => number?.addNumber
                  ),
                  ...rest,
                };
                if (!adminYesValue && !adminNoValue) {
                  toast.error("Please check whether you are CEO or Not");
                  return;
                }
                await submitValues(finalData)
                  .unwrap()
                  .catch((error: any) => {
                    const ermsg = error?.data?.message;
                    toast.error(ermsg);
                  });
              }}
              validationSchema={validationSchemaBusiness}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  getFieldProps,
                  setFieldValue,
                } = props;

                const displayInfo = (
                  fieldName?: string,
                  fieldValue?: string,
                  editable?: boolean,
                  fieldsValueDisplay?: any,
                  type?: string,
                  options?: any,
                  selectValue?: any,
                  setSelectType?: any,
                  multiple?: boolean,
                  ref?: any
                ) => {
                  return (
                    <Box
                      sx={{
                        display: "flex",
                        width: "100%",
                        marginBottom: editable ? "1%" : "15px",
                      }}
                    >
                      <Box sx={{ width: "30%" }}>{fieldName}</Box>
                      {!editable ? (
                        multiple ? (
                          <div>
                            {fieldsValueDisplay?.map((data: any, index: number) => (
                              <Box key={index} sx={{ width: "70%" }}>
                                {"+81-" + data}
                              </Box>
                            ))}
                          </div>
                        ) : (
                          <Box sx={{ width: "70%" }}>{fieldsValueDisplay}</Box>
                        )
                      ) : (
                        <div style={{ marginTop: "-4px", width: "60%" }}>
                          <InputComponent
                            type={type ? type : "text"}
                            getFieldProps={getFieldProps}
                            getFieldPropsValue={fieldValue}
                            options={options}
                            selectValue={selectValue}
                            setFieldValue={setFieldValue}
                            label={type === "postalCode" ? "hh" : ""}
                            // showtitle={type === "postalCode" ? "hh" : ""}
                            values={values}
                          />
                        </div>
                      )}
                    </Box>
                  );
                };

                return (
                  <Form onSubmit={handleSubmit}>
                    <Typography variant="h6" color="primary">
                      Company Information:
                    </Typography>

                    <Box sx={{ padding: "4%" }}>
                      <Box mb={4} sx={{ border: "1px solid gray" }} p={10}>
                        <p
                          style={{
                            marginTop: "-50px",
                            width: "140px",
                            height: "40px",
                            backgroundColor: "white",
                            textAlign: "center",
                            fontWeight: "bold",
                          }}
                        >
                          Company Name
                        </p>
                        <Box mb={4}>
                          <InputComponent
                            type="text"
                            label="Registered Name "
                            getFieldProps={getFieldProps}
                            getFieldPropsValue="registerTradeName"
                            touched={touched.registerTradeName}
                            errors={errors.registerTradeName}
                            placeholder={"Please enter Registered name "}
                          />
                        </Box>
                        <Box mb={4}>
                          <InputComponent
                            type="text"
                            label="Name in katagana"
                            getFieldProps={getFieldProps}
                            getFieldPropsValue="katagana_name"
                            touched={touched.katagana_name}
                            errors={errors.katagana_name}
                            placeholder={"Please enter name in Katagana"}
                          />
                        </Box>
                        <InputComponent
                          type="text"
                          label="Please enter English name"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="name_en"
                          touched={touched.name_en}
                          errors={errors.name_en}
                          placeholder={"Please enter your name "}
                        />
                      </Box>

                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Business Email"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="businessEmail"
                          touched={touched.businessEmail}
                          errors={errors.businessEmail}
                          placeholder={"Please enter your Business Email"}
                        />
                      </Box>

                      <Box mb={4}>
                        <InputComponent
                          type="number"
                          label="Business Phone Number"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue={"phoneNumber"}
                          touched={touched.phoneNumber}
                          errors={errors.phoneNumber}
                          placeholder={
                            "Please enter your Business Phone Number"
                          }
                        />
                      </Box>
                      <Box mb={4}>
                        <InputComponent
                          type="secondaryPhoneNumber"
                          label="Optional Number"
                          placeholder={
                            "Please enter your Business Optional Number"
                          }
                          addNumber={addNumber}
                          setAddNumber={setAddNumber}
                        />
                      </Box>
                      {/* <Box mb={4}>
                        <InputComponent
                          type="number"
                          label=" Phone Number"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue={"phoneNumber2"}
                          placeholder={"Please enter your Phone Number "}
                          compulsary="false"
                        />
                      </Box> */}

                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Choose Shopo Central Business ID :"
                          touched={touched.businessAccessId}
                          errors={errors.businessAccessId}
                          getFieldProps={getFieldProps}
                          getFieldPropsValue={"businessAccessId"}
                          placeholder={
                            "Please choose your shopo central Business Access ID"
                          }
                        />
                      </Box>
                      <Box mb={4}>
                        <InputComponent
                          type="option"
                          label="Please select type of company:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="companyType"
                          errors={errors.companyType}
                          touched={touched.companyType}
                          placeholder={"Please select your company Type"}
                          options={companyType}
                        />
                      </Box>
                      <Box mb={4}>
                        Business Password:
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                        <OutlinedInput
                          id="outlined-adornment-password"
                          sx={{ width: "95%" }}
                          name="password"
                          size="small"
                          type={showPassword ? "text" : "password"}
                          value={values.password}
                          disabled={isLoading}
                          placeholder={"Please enter your Business Password"}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          error={touched.password && Boolean(errors.password)}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={() =>
                                  setShowPassword((prev: boolean) => !prev)
                                }
                                edge="end"
                              >
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {touched.password && errors.password && (
                          <FormHelperText error id="accountId-error">
                            {errors.password}
                          </FormHelperText>
                        )}
                      </Box>

                      <Box mb={4}>
                        <Typography>Confirm Password :</Typography>
                        <OutlinedInput
                          size="small"
                          id="outlined-adornment-password"
                          sx={{ width: "95%" }}
                          name="password_confirmation"
                          type={showConfirmPassword ? "text" : "password"}
                          value={values.password_confirmation}
                          disabled={isLoading}
                          onChange={handleChange}
                          placeholder={"Please enter your password"}
                          onBlur={handleBlur}
                          error={
                            touched.password_confirmation &&
                            Boolean(errors.password_confirmation)
                          }
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={() =>
                                  setShowConfirmPassword(!showConfirmPassword)
                                }
                                edge="end"
                              >
                                {showConfirmPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />

                        {touched.password_confirmation &&
                          errors.password_confirmation && (
                            <FormHelperText error id="accountId-error">
                              {errors.password_confirmation}
                            </FormHelperText>
                          )}
                      </Box>
                    </Box>
                    <Typography variant="h6" color="primary">
                      Address:
                    </Typography>

                    <Box sx={{ padding: "4%" }}>
                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Country:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="country_en"
                          errors={errors.country_en}
                          touched={touched.country_en}
                          placeholder={"Please enter your Country"}
                          disabled={true}
                        />
                      </Box>
                      <Box mb={4}>
                        {displayInfo(
                          "",
                          "postalcode_en",
                          true,
                          "",
                          "postalCode",
                          "",
                          "",
                          ""
                        )}
                        {/* <InputComponent
                          label="postal_code"
                          type="postalCode"
                          touched1={touched.postalcode_en}
                          touched2={touched.postalcode_end}
                          errors1={errors.postalcode_en}
                          errors2={errors.postalcode_end}
                          getFieldProps={getFieldProps}
                          values={values}
                          setFieldValue={setFieldValue}
                        /> */}
                      </Box>
                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Prefecture:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="prefecture_en"
                          errors={errors.prefecture_en}
                          touched={touched.prefecture_en}
                          placeholder={"Please enter your Prefecture"}
                        />
                      </Box>
                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="City/Village:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="city_en"
                          errors={errors.city_en}
                          touched={touched.city_en}
                          placeholder={"Please enter a City/Village"}
                        />
                      </Box>

                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Town/Ward:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="town_en"
                          errors={errors.town_en}
                          touched={touched.town_en}
                          placeholder={"Please enter a Town/Ward"}
                        />
                      </Box>
                      <Box mb={4}>
                        <InputComponent
                          type="text"
                          label="Building Name/Room Number:"
                          getFieldProps={getFieldProps}
                          getFieldPropsValue="house_number"
                          errors={errors.house_number}
                          touched={touched.house_number}
                          placeholder={"Please enter a Building Name/Room Number"}
                        />
                      </Box>
                      <Box>
                        Do You Want to be CEO?
                        <Stack
                          direction={"row"}
                          spacing={2}
                          justifyContent="start"
                        >
                          <FormControlLabel
                            style={{ display: "flex", justifyContent: "start" }}
                            control={
                              <Checkbox
                                checked={adminYesValue}
                                onChange={(e: any) =>
                                  handleChangeAdmin(e, "yes")
                                }
                                name="yes"
                              />
                            }
                            label="Yes"
                          />
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={adminNoValue}
                                onChange={(e: any) =>
                                  handleChangeAdmin(e, "no")
                                }
                                name="no"
                              />
                            }
                            label="NO"
                          />
                        </Stack>
                      </Box>
                      <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                        style={{ margin: "4rem 0 1rem 0" }}
                      >
                        <Button
                          type="submit"
                          sx={{ width: "95%" }}
                          size="large"
                          variant="contained"
                          color="primary"
                          disabled={isLoading}
                        >
                          Submit
                        </Button>
                      </Box>
                    </Box>
                    <Typography
                      sx={{
                        marginTop: "1.5rem",
                        cursor: "pointer",
                        marginBottom: "5%",
                      }}
                      component="div"
                      align="center"
                    >
                      © 2023 Shopo Central. All rights reserved
                    </Typography>
                  </Form>
                );
              }}
            </Formik>
          </Box>
          {/* </Scrollbars> */}
        </Box>
      </Box>
    </Box>
  );
};

export default BusinessAccountForm;
