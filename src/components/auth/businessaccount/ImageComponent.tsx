// Build in library
import Image from "next/image";
// MUI Components
import { Box } from "@mui/material";
// assets
import loginPic from "../../../../public/loginPic.png";

const ImageComponent = () => {
    return (
        <Box
            sx={{
                flex: 1,
                width: "100%",
                height: "100%",
            }}
        >
            <Image src={loginPic}></Image>
        </Box>
    );
};
export default ImageComponent;
