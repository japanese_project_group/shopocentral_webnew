// MUI Components
import { Box } from "@mui/material";
// Component
import BusinessAccountForm from "./FormComponent";
const BusinessAccount = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      <Box
        sx={{
          alignItems: "center",
          justifyContent: "center",
          display: "flex",
          flex: 1,
          width: "50%",
        }}
      >
        <BusinessAccountForm />
      </Box>

    </Box>
  );
};
export default BusinessAccount;
