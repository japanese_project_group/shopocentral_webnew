// Build in library
import Router from "next/router";
import React, { useEffect, useMemo, useState } from "react";
// MUI Components
import SendIcon from '@mui/icons-material/Send';
import { Stack, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
// Query
import { skipToken } from "@reduxjs/toolkit/dist/query";
import { useAdminoCeoMutation, useSearchCEOQuery } from "../../../../redux/api/auth/auth";
// Third party library
import { FormikProvider, useFormik } from "formik";
import { toast } from "react-toastify";
// constants & validation
import { businessId, userId } from '../../../common/utility/Constants';
import { validationSchemaAdmino } from "../../../common/validationSchema";

const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 2,
    borderRadius: 2,
};
const AdminOrCeo = () => {
    const [email, setEmail] = React.useState()
    const { data: allDataEmailResponse, isSuccess, error: errorSearchCEO } = useSearchCEOQuery(email ?? skipToken);
    const [adminoCeo, { isSuccess: adminoCEOSuccess, data: adminCeoData, error: adminCeoError }] = useAdminoCeoMutation()
    const [open, setOpen] = React.useState(true);
    const handleOpen = () => setOpen(true);
    const [checkEmail, setCheckEmail] = React.useState(false);
    const [emailAvailable, setEmailAvailable] = React.useState(false);
    const [alldata, setAlldata] = useState<boolean>(false)
    const [storeResponse, setStoreResponse] = useState<any>([]);

    const formik = useFormik({
        initialValues: {
            email: ""
        },
        enableReinitialize: true,
        validationSchema: validationSchemaAdmino,

        onSubmit: async (values: any) => {
            setEmail(values.email);
        }
    });
    const deleteIc = () => {
        setFieldValue("email", "")
        setAlldata(false)
    }
    const {
        errors,
        values,
        touched,
        isSubmitting,
        handleSubmit,
        getFieldProps,
        setFieldValue,
    } = formik;
    const handleClose = () => {
        Router.push("/login")
    };
    const submitAdminCeo = (type: string) => {
        if (type === "user") {
            let data = {
                isCeo: true,
                userId: userId,
                businessId: businessId
            }
            submitForm(data)
        } else if (type === "otherUser") {
            let data = {
                isCeo: true,
                userId: storeResponse?.getUser?.id,
                businessId: storeResponse?.getUser?.businessKyc?.[0]?.id
            }
            submitForm(data)
        }
    }
    const submitForm = (data: any) => {
        adminoCeo(data)
            .unwrap()
            .catch((error: { data: { message: any } }) => {
                const ermsg = error?.data?.message;
                toast.error(ermsg);
            });
    }
    const displayErrorToast = (value: any) => {
        toast.error(value?.data?.message)
    }

    useMemo(() => {
        if (!!allDataEmailResponse) {
            setAlldata(true);
            setStoreResponse(allDataEmailResponse)
            toast.success("User Verified as CEO")
        }
    }, [allDataEmailResponse])

    useEffect(() => {
        displayErrorToast(errorSearchCEO)
    }, [errorSearchCEO])

    useEffect(() => {
        if (!!adminoCEOSuccess) {
            Router.push("/login")
        }
    }, [adminoCEOSuccess])

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby='modal-modal-title'
                aria-describedby='modal-modal-description'
            >
                <Box sx={style}>
                    <FormikProvider value={formik}>
                        <form onSubmit={handleSubmit}>
                            {!checkEmail ? (
                                <Box>
                                    <Typography
                                        id='modal-modal-title'
                                        variant='h6'
                                        component='h2'
                                        textAlign={"center"}
                                    >
                                        Do You Want to be CEO
                                    </Typography>

                                    <Stack direction={"row"} spacing={2} justifyContent='center'>
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            onClick={() => submitAdminCeo("user")}
                                            size='small'
                                        >
                                            Yes
                                        </Button>
                                        <Button variant='contained' color='primary' size='small' onClick={() => Router.push("/login")}>
                                            No
                                        </Button>
                                    </Stack>
                                </Box>
                            ) : (
                                <Stack spacing={2}>
                                    <Typography
                                        id='modal-modal-title'
                                        variant='h5'
                                        component='h2'
                                        textAlign={"center"}
                                    >
                                        Check Email
                                    </Typography>
                                    <Stack direction={"column"} spacing={1}>
                                        <>
                                            <TextField placeholder='Enter Your Email' size='small' error={Boolean(touched.email && errors.email)}
                                                helperText={
                                                    Boolean(touched.email && errors.email) &&
                                                    String(touched.email && errors.email)
                                                } onChange={(e) => setFieldValue("email", e.target.value)}

                                            />
                                            <Button
                                                variant='contained'
                                                type="submit"
                                                size='small'
                                                onClick={() => handleSubmit}>
                                                verify email
                                            </Button>

                                            {alldata ? (
                                                <Stack direction="row" spacing={2} className="display-flex-sb">
                                                    <Button variant="outlined" onClick={deleteIc}>
                                                        Re Enter
                                                    </Button>
                                                    <Button variant="contained" onClick={() => submitAdminCeo("otherUser")} endIcon={<SendIcon />}>
                                                        Send
                                                    </Button>
                                                </Stack>
                                            ) : (<></>)}
                                        </>

                                    </Stack>


                                </Stack>

                            )}
                            {emailAvailable && (
                                <Box>
                                    <Typography>Name: Dummy Name</Typography>
                                    <Typography>Name: Dummy Name</Typography>
                                    <Typography>Name: Dummy Name</Typography>
                                    <Stack>
                                        <Button>Button</Button>
                                        <Button>Two</Button>
                                    </Stack>
                                </Box>
                            )}
                        </form>
                    </FormikProvider>
                </Box>
            </Modal>
        </div>
    );
}


export default AdminOrCeo