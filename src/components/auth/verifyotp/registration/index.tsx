// Build in library
import Link from "next/link";
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import {
  Box,
  Button,
  FormHelperText,
  TextField,
  Typography,
} from "@mui/material";
// Third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Redux
import {
  useLoginUserMutation,
  useResendOtpMutation,
  useVerifyOTPMutation,
} from "../../../../../redux/api/auth/auth";
// Validation
import { getDecryptedData } from "../../../../common/utility";
import { validationSchemaOtp } from "../../../../common/validationSchema";

const VerifyOtpComponent = () => {
  const [email, setEmail] = useState<string | null>("");
  const [password, setPassword] = useState<string | null>("");
  const [counter, setCounter] = useState(60);

  const [verifyOTP, { isLoading, isSuccess, data, isError }] =
    useVerifyOTPMutation();
  const [
    ResendOtp,
    {
      isLoading: ResendOtpLoading,
      isSuccess: ResendOtpSuccess,
      data: ResendOtpData,
    },
  ] = useResendOtpMutation();

  useEffect(() => {
    setEmail(localStorage.getItem("email"));
    setPassword(getDecryptedData(localStorage.getItem("password")));
  }, []);

  useEffect(() => {
    const timer: any =
      counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  const [
    loginUser,
    {
      isLoading: userIsLoading,
      isSuccess: userIsSuccess,
      data: userData,
      isError: userIsError,
    },
  ] = useLoginUserMutation();

  useEffect(() => {
    if (data && isSuccess) {
      localStorage.setItem("userId", data.userId);
      toast("OTP Verification Successfull", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      loginUser({ email, password });
    }
  }, [data, isSuccess]);

  useEffect(() => {
    if (userData && userIsSuccess) {
      localStorage.removeItem("password");
      localStorage.setItem("token", userData?.access_token);
      localStorage.setItem("personal-kyc-open", "data");
      Router.replace("/createaccountform");
    }
  }, [userData, userIsSuccess]);

  // useEffect(() => {
  //   if (isError) {
  //     toast.error("OTP Does Not Match", {
  //       autoClose: 2500,
  //       type: "error",
  //       pauseOnHover: true,
  //     });
  //   }
  // }, [isError]);

  useEffect(() => {
    if (ResendOtpSuccess && ResendOtpData) {
      toast(ResendOtpData.message, {
        type: "success",
      });
    }
  }, [ResendOtpSuccess, ResendOtpSuccess]);

  function handleResendOtp() {
    ResendOtp({ email: email })
      .unwrap()
      .catch((error: any) => {
        const ermsg = error?.data?.message;
        toast.error(ermsg);
      });
  }
  return (
    <>
      <Box
        sx={{
          background: "#0072d8",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          // width: "50%",
          height: "100vh",
          display: "flex",
          padding: "5%",
        }}
      >
        <Box className="forms-screen" sx={{ p: 6 }}>
          <Box
            sx={{
              mb: 15,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h3"
              color="primary"
              component="div"
              align="center"
            >
              {" "}
              OTP Verification
            </Typography>
          </Box>
          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <Formik
              initialValues={{
                code: "",
              }}
              onSubmit={async (values: any) => {
                values = { ...values, email: email };
                await verifyOTP(values).then((data: any) => {
                  toast(data?.error?.data?.message, {
                    autoClose: 2500,
                    type: "error",
                    pauseOnHover: true,
                  });
                });

                if (!localStorage.getItem("code")) {
                  localStorage.setItem("code", values.code);
                } else {
                  localStorage.clearItem("code");
                }
              }}
              validationSchema={validationSchemaOtp}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Box sx={{ marginBottom: "20%", marginTop: "-12%" }}>
                      <Typography
                        variant="h6"
                        sx={{
                          marginTop: "8%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        We have sent a verifcation code to
                      </Typography>
                      <Typography
                        variant="h6"
                        color="primary"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        {email}
                      </Typography>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          marginTop: "3%",
                        }}
                      >
                        <Typography variant="h6">
                          Entered wrong Email ? Click here to
                          <Link href="/changemail" passHref>
                            <Typography
                              textAlign={"center"}
                              color="blue"
                              sx={{ cursor: "pointer" }}
                            >
                              Change your Email
                            </Typography>
                          </Link>
                        </Typography>
                      </Box>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          marginTop: "8%",
                        }}
                      >
                        Please enter the code from the email
                      </Typography>
                    </Box>
                    <Box mb={4}>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        Code
                      </Typography>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="code"
                        disabled={isLoading}
                        value={values.code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.code && Boolean(errors.code)}
                        placeholder={"Code"}
                      />
                      {touched.code && errors.code && (
                        <FormHelperText>{errors.code}</FormHelperText>
                      )}
                    </Box>
                    <Button
                      type="submit"
                      fullWidth
                      size="large"
                      variant="contained"
                      color="primary"
                      disabled={isLoading}
                    >
                      Verify Otp
                    </Button>
                    <Box
                      sx={{
                        width: "100%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      {counter < 1 ? (
                        <Button
                          color="secondary"
                          disabled={isLoading}
                          sx={{ marginTop: "3%" }}
                          onClick={handleResendOtp}
                        >
                          Resend OTP
                        </Button>
                      ) : (
                        <Button color="secondary" sx={{ marginTop: "3%" }}>
                          00:{counter}
                        </Button>
                      )}
                    </Box>
                  </form>
                );
              }}
            </Formik>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default VerifyOtpComponent;
