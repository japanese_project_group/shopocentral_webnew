// Build in library
import Image from "next/image";
import Router from "next/router";
import { useEffect } from "react";
// MUI Components
import { Box, Button, TextField, Typography } from "@mui/material";
// Third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Redux
import { useVerifyOTPMutation } from "../../../../../redux/api/auth/auth";
// Assets
import loginPic from "../../../../public/loginPic.png";

const VerifyOtpComponent = () => {
  const [verifyOTP, { isLoading, isSuccess, data, isError }] =
    useVerifyOTPMutation();

  useEffect(() => {
    if (data && isSuccess) {
      localStorage.setItem("token", data?.token);
      toast("OTP Verification Successfull", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      localStorage.removeItem("email");
      Router.replace("/login");
    }
  }, [data, isSuccess]);

  useEffect(() => {
    if (isError) {
      toast("OTP Does Not Match", {
        autoClose: 2500,
        type: "error",
        pauseOnHover: true,
      });
    }
  }, [isError]);

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Box
          sx={{
            my: 10,
            mx: 4,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
          }}
        >
          <Box
            sx={{
              mb: 10,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h3"
              color="primary"
              component="div"
              align="center"
            ></Typography>

            <Typography
              variant="subtitle1"
              sx={{ marginTop: "1rem" }}
              component="div"
              align="center"
            ></Typography>
          </Box>
          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <Formik
              initialValues={{
                code: "",
              }}
              onSubmit={async (values: any) => {
                const email = localStorage.getItem("email");
                values = { ...values, email: email };
                await verifyOTP(values);
              }}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Box sx={{ marginBottom: "20%" }}>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        We have sent a verifcation code in your email!
                      </Typography>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        Please check your email
                      </Typography>
                    </Box>
                    <Box mb={4}>
                      <Typography
                        variant="h6"
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        Code
                      </Typography>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="code"
                        disabled={isLoading}
                        value={values.code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.code && Boolean(errors.code)}
                        placeholder={"Code"}
                      />
                    </Box>
                    <Button
                      type="submit"
                      fullWidth
                      size="large"
                      variant="contained"
                      color="primary"
                      disabled={isLoading}
                    >
                      Verify Otp
                    </Button>
                  </form>
                );
              }}
            </Formik>
          </Box>
        </Box>
        <Box
          sx={{
            flex: 1,
            width: "50%",
            height: "100vh",
            display: {
              xs: "none",
              md: "flex",
            },
          }}
        >
          <Image src={loginPic} />
        </Box>
      </Box>
    </>
  );
};

export default VerifyOtpComponent;
