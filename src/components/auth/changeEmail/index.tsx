// Build in library
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import {
    Box,
    Button,
    TextField,
    Typography,
} from "@mui/material";
// Third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Redux
import { useChangeEmailMutation } from "../../../../redux/api/auth/auth";
// Validation
import { validationSchemaChangeEmail } from "../../../common/validationSchema";

const ChangeEmailComponent = () => {
    const [email, setEmail] = useState<string | null>("");
    const [password, setPassword] = useState<string | null>("");
    const [showPassword, setShowPassword] = useState(false);

    const [ChangeEmail, { isLoading, isSuccess, data, isError }] =
        useChangeEmailMutation();

    useEffect(() => {
        setEmail(localStorage.getItem("email"))
    }, [])

    useEffect(() => {
        if (data && isSuccess) {
            localStorage.setItem("email", data.email)
            Router.push("/verifyotp")
            toast("OTP Send Successfully", {
                autoClose: 2500,
                type: "success",
                pauseOnHover: true,
            });
        }
    }, [data, isSuccess]);


    return (
        <>
            <Box
                sx={{
                    background: "#0072d8",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    flex: 1,
                    height: "100vh",
                    display: "flex",
                    padding: "5%",
                }}
            >
                <Box className="forms-screen" sx={{ p: 6 }}>
                    <Box
                        sx={{
                            mb: 10,
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Typography
                            variant='h3'
                            color='primary'
                            component='div'
                            align='center'
                        >
                            {" "}
                            Email Modification
                        </Typography>
                    </Box>
                    <Box
                        sx={{
                            mt: 1,
                            width: { sm: "450px", xs: "100%" },
                        }}
                    >
                        <Formik
                            initialValues={{
                                newEmail: "",
                            }}
                            onSubmit={async (values: any) => {
                                const finalData = {
                                    ...values,
                                    oldEmail: email
                                }
                                await ChangeEmail(finalData)
                                    .unwrap()
                                    .catch((error) => {
                                        toast.error(error?.data?.message)
                                    })
                            }}
                            validationSchema={validationSchemaChangeEmail}
                        >
                            {(props: any) => {
                                const {
                                    values,
                                    touched,
                                    errors,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                } = props;
                                return (
                                    <form onSubmit={handleSubmit}>
                                        <Box sx={{ marginBottom: "20%", marginTop: "-12%" }}>
                                            <Typography
                                                variant='h6'
                                                sx={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    marginTop: "6%"
                                                }}
                                            >
                                                Please enter new email below:
                                            </Typography>

                                        </Box>

                                        <Box mb={4}>
                                            <Typography
                                                variant='h6'
                                            >
                                                New Email
                                            </Typography>
                                            <TextField
                                                fullWidth
                                                variant='outlined'
                                                name='newEmail'
                                                disabled={isLoading}
                                                value={values.newEmail}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={touched.newEmail && Boolean(errors.newEmail)}
                                                placeholder={"Please enter new Email"}
                                                helperText={touched.newEmail ? errors.newEmail : ""}
                                            />
                                        </Box>
                                        <Button
                                            type='submit'
                                            fullWidth
                                            size='large'
                                            variant='contained'
                                            color='primary'
                                            disabled={isLoading}

                                            sx={{ marginTop: "7%" }}
                                        >
                                            Change Email
                                        </Button>


                                    </form>
                                );
                            }}
                        </Formik>
                    </Box>
                </Box>
            </Box>
        </>
    );
};

export default ChangeEmailComponent;
