// Build in library
import Link from "next/link";
import Router from "next/router";
import { useEffect, useState } from "react";

// MUI Components
import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Box,
  Button,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
// Third party library
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
// Route
import withAuth from "../../../../hoc/PublicRoute";
// Redux
import { useLoginMutation } from "../../../../redux/api/auth/auth";
import { setAuthState } from "../../../../redux/features/authSlice";
// style
import styles from "../../../../src/components/Forms.module.css";
// Cryptojs
import { getEncryptedData } from "../../../common/utility";

const LoginPage = () => {
  const [login, { isLoading, isSuccess, data: userData, isError, error }] =
    useLoginMutation();
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (userData && isSuccess) {
      if (userData?.access_token) {
        const token = userData?.access_token;
        const userId = userData?.data?.id;
        const businessId = userData?.businesssKycId;
        localStorage.setItem("token", getEncryptedData(token));
        localStorage.setItem("userIdNotification", getEncryptedData(userId));
        localStorage.setItem("businessId", getEncryptedData(businessId));
        dispatch(setAuthState(userData));
        Router.replace("/dashboard");
        toast("Welcome Back", {
          autoClose: 2500,
          type: "success",
          pauseOnHover: true,
        });
      }
    }
  }, [userData, isSuccess]);

  return (
    <>
      <Box
        sx={{
          background: "#0072d8",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          // width: "50%",
          height: "100vh",
          display: "flex",
          padding: "5%",
        }}
      >
        <Box className={styles.forms} sx={{ p: 6 }}>
          <Box
            sx={{
              mb: 5,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h5"
              color="primary"
              fontWeight="bold"
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Welcome to
            </Typography>
            <Typography
              variant="h5"
              color="primary"
              fontWeight="bold"
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Shopo Central Management System
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ marginTop: "1rem" }}
              component="div"
              align="center"
            >
              {" "}
              Log in
            </Typography>
          </Box>
          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <Formik
              initialValues={{
                email: "",
                businessAccessId: "",
                password: "",
                fcmToken: "randomfcmtoken",
              }}
              onSubmit={async (values) => {
                localStorage.setItem(
                  "businessAccessId",
                  getEncryptedData(values.businessAccessId)
                );
                await login(values)
                  .unwrap()
                  .catch(() => {
                    const ermsg = "Invalid credentials";
                    toast.error(ermsg);
                  });
              }}
            // validationSchema={validationSchemaLogin}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={4}>
                      <InputLabel sx={{ color: "black" }}>
                        Shopo User ID:
                      </InputLabel>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.email && Boolean(errors.email)}
                        placeholder={"Please enter your email or mobile number"}
                        helperText={touched.email ? errors.email : ""}
                      />
                    </Box>
                    <Box mb={4}>
                      <InputLabel sx={{ color: "black" }}>
                        Shopo Business ID:
                      </InputLabel>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="businessAccessId"
                        value={values.businessAccessId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={
                          touched.businessAccessId &&
                          Boolean(errors.businessAccessId)
                        }
                        placeholder={"Please enter your Business ID"}
                        helperText={
                          touched.businessAccessId
                            ? errors.businessAccessId
                            : ""
                        }
                      />
                    </Box>
                    <Box sx={{ mb: 1 }}>
                      <InputLabel sx={{ color: "black" }}>
                        Business Password :
                      </InputLabel>
                      <OutlinedInput
                        id="outlined-adornment-password"
                        fullWidth
                        name="password"
                        type={showPassword ? "text" : "password"}
                        value={values.password}
                        onChange={handleChange}
                        placeholder={"Please enter your password"}
                        onBlur={handleBlur}
                        error={touched.password && Boolean(errors.password)}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={() =>
                                setShowPassword((prev: boolean) => !prev)
                              }
                              edge="end"
                            >
                              {showPassword ? (
                                <Visibility />
                              ) : (
                                <VisibilityOff />
                              )}
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                      {touched.password && errors.password && (
                        <FormHelperText error id="accountId-error">
                          {errors.password}
                        </FormHelperText>
                      )}
                    </Box>
                    <Link href="/passwordreset/forgotpassword" passHref>
                      <Typography
                        sx={{ cursor: "pointer", mb: 2, color: "blue" }}
                      >
                        Forgot Password ?
                      </Typography>
                    </Link>

                    <Box>
                      <Button
                        type="submit"
                        fullWidth
                        size="large"
                        variant="contained"
                        color="primary"
                      >
                        login
                      </Button>
                    </Box>
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        marginTop: "3%",
                      }}
                    >
                      <Typography
                        textAlign={"center"}
                        sx={{ cursor: "pointer", marginRight: "2%" }}
                      >
                        Does Not Have Business ID?
                      </Typography>
                      <Link href="/register/" passHref>
                        <Typography
                          textAlign={"center"}
                          color="blue"
                          sx={{ cursor: "pointer" }}
                        >
                          Create Now
                        </Typography>
                      </Link>
                    </Box>

                    <Typography
                      sx={{ cursor: "pointer", marginTop: "5%" }}
                      component="div"
                      align="center"
                    >
                      © 2023 Shopo Central. All rights reserved
                    </Typography>
                  </form>
                );
              }}
            </Formik>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default withAuth(LoginPage);
