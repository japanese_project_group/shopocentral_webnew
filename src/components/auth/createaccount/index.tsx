import AddAPhotoIcon from "@mui/icons-material/AddAPhoto";
import {
  Autocomplete,
  Box,
  Button,
  FormControl,
  FormHelperText,
  IconButton,
  MenuItem,
  Select,
  SelectChangeEvent,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { Field, FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import moment from "moment";
import NextImage from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import user from "../../../../public/defaultPro.png";
import { useUsedCountriesQuery } from "../../../../redux/api/auth/auth";
import { useCreateAccountMutation } from "../../../../redux/api/user/user";
import styles from "../../../../src/components/Forms.module.css";
import ErrorMessage from "../../../common/ErrorMessage";
import { validationSchemaCreateAccount } from "../../../common/validationSchema";
import { setKycData } from "redux/features/kycDataSlice";

const CreateAccountForm = () => {
  const router = useRouter();
  const dispatch = useDispatch()
  const [selectedCountry, setSelectedCountry] = useState({});
  const [nationalityCode, setCountryCode] = React.useState<any>("Japanese");
  const [userImage, setCustomPicture] = useState<string>();
  const [userImageBlob, setUserImageBlob] = useState<any>();
  const [allCountryData, setAllCountryData] = useState<any>([]);
  const [loyaltyCountryData, setLoyaltyCountryData] = useState<any>([]);

  const [createAccount, { isSuccess, data,  isLoading }] =
    useCreateAccountMutation();
  const {
    data: countryDataLoyalty,
  } = useUsedCountriesQuery();


  useEffect(() => {
    if (isSuccess && data) {
      toast("Shopo User Account created successfully", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      localStorage.setItem("profileId", data?.data?.user?.profile?.id);
      localStorage.setItem("phoneNumberId", data?.data?.user?.phoneNumber?.id);
      dispatch(setKycData({...data?.data?.user,pstatus:data?.data?.user?.pstatus || "PENDING"}));
      router.push("/kycforms/formsubmission/personal-kyc");
    }
  }, [isSuccess, data]);


  const handleChangeCountryCode = (event: SelectChangeEvent) => {
    setCountryCode(event.target.value as string);
  };
  const [gender, setGender] = React.useState<any>("Male");

  const handleChangeGender = (event: SelectChangeEvent) => {
    setGender(event.target.value as string);
  };
  const userId = localStorage.getItem("userId");

  useEffect(() => {
    fetch(
      "https://valid.layercode.workers.dev/list/countries?format=select&flags=true&value=code"
    )
      .then((response) => response.json())
      .then((data) => {
        setAllCountryData(data.countries);
        setSelectedCountry(data.userSelectValue);
      });
  }, []);

  const formik = useFormik({
    initialValues: {
      dob: "",
      gender: "",
      nationality: "",
      userImage: "",
      termsAndConditions: false,
    },
    enableReinitialize: true,
    validationSchema: validationSchemaCreateAccount,
    onSubmit: async (values: any) => {
      const finalData = {
        ...values,
        userId: userId,
      };
      let fd = new FormData();
      Object.keys(finalData).map((item: any) => {
        fd.append(item, finalData[item] ? finalData[item] : null);
      });

      let reqData = {
        data: fd,
      };
      createAccount(reqData)
        .unwrap()
        .catch((error: any) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
      localStorage.setItem("dob", values?.dob);
      localStorage.setItem("nationality", values?.nationality);
      localStorage.setItem("gender", values?.gender);
    },
  });

  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
    handleBlur,
  } = formik;

  const onChangeCustomPicture = (e: any) => {
    setCustomPicture(e.target.files[0]?.name);
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          const imageWidth = parseInt(`${image.width}`);
          const imageHeight = parseInt(`${image.height}`);
          if (imageWidth > 8100 || imageHeight > 8100) {
            toast("Image height and width too big", {
              type: "error",
            });
          } else if (file.size > 2000000) {
            setUserImageBlob(null);
            toast("Please upload image with size less than 2 MB", {
              type: "error",
            });
            return;
          } else {
            setFieldValue("userImage", e.target.files[0]);
            setUserImageBlob(e.target?.files?.[0]);
          }
        };
      };
    }
  };

  useEffect(() => {
    if (!isEmpty(countryDataLoyalty)) {
      setLoyaltyCountryData(countryDataLoyalty?.data);
    }
  }, [countryDataLoyalty]);

  // const sortedLoyaltyCountryData = loyaltyCountryData?.sort(function (a: any, b: any) { return b._count.nationality - a._count.nationality })

  const filterByReferenceNotMatching = (arr1: any, arr2: any) => {
    let res = [];
    res = arr1.filter((el: any) => {
      return !arr2.find((element: any) => {
        return element.nationality === el.value;
      });
    });
    return res;
  };
  const filterByReferenceMatching = (arr1: any, arr2: any, data1: any) => {
    let res = [];
    res = arr1.filter((el: any) => {
      return arr2.find((element: any) => {
        return element.nationality === el.value;
      });
    });
    const arr3 = res.concat(data1);
    return arr3;
  };
  const data1 = filterByReferenceNotMatching(
    allCountryData,
    loyaltyCountryData
  );
  const data2 = filterByReferenceMatching(
    allCountryData,
    loyaltyCountryData,
    data1
  );

  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          width: "100vw",
          height: "100vh",
          backgroundColor: "#0072d8",
        }}
        className={styles.customerRegister}
      >
        <Box
          className={styles.glassMorphism}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            margin: "25px",
            padding: "25px",
          }}
        >
          <Typography variant="h3" color="primary" component="div">
            Welcome
          </Typography>

          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <FormikProvider value={formik}>
              <form onSubmit={handleSubmit}>
                <Box sx={{ border: "2", marginTop: "2%" }}>
                  <Stack spacing={2}>
                    <Box
                      sx={{
                        width: "100%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Box
                        sx={{
                          borderRadius: "50%",
                          height: "130px",
                          width: "130px",
                        }}
                      >
                        <IconButton
                          component="label"
                          sx={{
                            display: "flex",
                            borderRadius: "50%",
                            height: "130px",
                            width: "130px",
                            flexDirection: "column",
                          }}
                        >
                          <Box>
                            {!userImageBlob ? (
                              <>
                                <NextImage
                                  src={user}
                                  layout="fill"
                                  objectFit="contain"
                                />
                              </>
                            ) : (
                              <>
                                <NextImage
                                  src={URL?.createObjectURL(userImageBlob)}
                                  layout="fill"
                                  objectFit="contain"
                                  style={{ borderRadius: "50%" }}
                                />
                              </>
                            )}
                          </Box>
                          <Box
                            sx={{
                              zIndex: "10",
                              backgroundColor: "white",
                              borderRadius: "50%",
                              height: "30px",
                              width: "30px",
                              marginTop: "65%",
                              marginLeft: "65%",
                            }}
                          >
                            <AddAPhotoIcon
                              className="setIcon"
                              color="success"
                            />
                          </Box>

                          <input
                            type="file"
                            hidden
                            accept="image/png, image/jpg, image/jpeg"
                            onChange={onChangeCustomPicture}
                          />
                        </IconButton>
                      </Box>
                    </Box>

                    <Box>
                      <Typography>
                        Gender:
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                      </Typography>
                      <FormControl fullWidth>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          error={Boolean(touched.gender && errors.gender)}
                          {...getFieldProps("gender")}
                        >
                          <MenuItem value={"MALE"}>Male</MenuItem>
                          <MenuItem value={"FEMALE"}>Female</MenuItem>
                          <MenuItem value={"OTHER"}>Others</MenuItem>
                        </Select>
                        <ErrorMessage
                          errors={touched.gender && errors.gender}
                          type="min"
                          name="number"
                        />
                      </FormControl>
                    </Box>
                    <Box>
                      <Typography>
                        Date of Birth:{" "}
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                      </Typography>
                      <LocalizationProvider dateAdapter={AdapterMoment}>
                        <DatePicker
                          maxDate={moment().format("YYYY-MM-DD")}
                          {...getFieldProps("dob")}
                          onChange={(newValue) => {
                            setFieldValue(
                              "dob",
                              moment(newValue).format("YYYY-MM-DD")
                            );
                          }}
                          inputFormat="YYYY/MM/DD"
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              type="date"
                              fullWidth
                              error={Boolean(touched.dob && errors.dob)}
                              helperText={
                                Boolean(touched.dob && errors.dob) &&
                                String(touched.dob && errors.dob)
                              }
                            />
                          )}
                        />
                      </LocalizationProvider>
                    </Box>
                    <Box>
                      <Typography>
                        Nationality:
                        <Typography
                          component="span"
                          color="error"
                          sx={{ marginLeft: "5px" }}
                        >
                          *
                        </Typography>
                      </Typography>
                      <FormControl fullWidth>
                        <Autocomplete
                          options={data2}
                          sx={{ width: 300 }}
                          style={{ width: "100%" }}
                          autoHighlight
                          onChange={(event, value: any) => {
                            setFieldValue("nationality", value?.label);
                          }}
                          renderInput={(params) => (
                            <TextField
                              className="mynationality"
                              {...params}
                              {...getFieldProps("nationality")}
                              error={Boolean(
                                touched.nationality && errors.nationality
                              )}
                              helperText={
                                Boolean(
                                  touched.nationality && errors.nationality
                                ) &&
                                String(
                                  touched.nationality && errors.nationality
                                )
                              }
                            />
                          )}
                        />
                      </FormControl>
                    </Box>

                    <Box sx={{ display: "flex" }}>
                      <Field
                        type="checkbox"
                        name="termsAndConditions"
                        size="medium"
                      />
                      <Typography sx={{ marginLeft: "2%" }}>
                        {" "}
                        By tapping the ' Create Account ' button, you accept our
                        'Member's Terms and Conditions of Use.'
                      </Typography>
                    </Box>
                    <FormHelperText error id="accountId-error">
                      <FormHelperText error id="accountId-error">
                        {touched.termsAndConditions && errors.termsAndConditions && (
                          <>{errors.termsAndConditions}</>
                        )}
                      </FormHelperText>
                      {/* {termsAndConditionsError && (
        <FormHelperText error>{termsAndConditionsError}</FormHelperText>
      )} */}
                    </FormHelperText>
                  </Stack>

                  <Stack direction="column">
                    {isLoading ? (
                      <Button
                        variant="contained"
                        startIcon={
                          <CircularProgress size="1.5rem" color="inherit" />
                        }
                      ></Button>
                    ) : (
                      <Button
                        disabled={isSubmitting}
                        type="submit"
                        variant="contained"
                        color="primary"
                      >
                        Create Account
                      </Button>
                    )}
                  </Stack>
                </Box>
              </form>
            </FormikProvider>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default CreateAccountForm;
