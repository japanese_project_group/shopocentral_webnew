// Build in library
import Link from "next/link";
import Router from "next/router";
import { useEffect } from "react";
// MUI Components
import { Box, Button, InputLabel, TextField, Typography } from "@mui/material";
// Third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Redux
import { useForgotPasswordMutation } from "../../../../redux/api/auth/auth";
// validation
import { validationSchemaForgetPassword } from "../../../common/validationSchema";

const ForgotPassword = () => {
  const [submitValues, { isLoading, isSuccess, data, isError, error }] =
    useForgotPasswordMutation();

  useEffect(() => {
    if (isError || error) {
      let errorMessage: any = error;
      toast.error(errorMessage?.data?.message);
    }
  }, [isError, error]);
  useEffect(() => {
    if (isSuccess && data) {
      toast("Password Reset Link Sent To Email", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      Router.push("/passwordreset/verifypasswordotp");
    }
  }, [isSuccess, data]);

  return (
    <>
      <Box
        sx={{
          background: "#0072d8",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          height: "100vh",
          display: "flex",
          padding: "5%",
        }}
      >
        <Box className="forms-screen" sx={{ p: 6 }}>
          <Box
            sx={{
              mb: 5,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h3"
              color="primary"
              component="div"
              align="center"
            >
              Forgot Password
            </Typography>

            <Typography
              variant="subtitle1"
              sx={{ marginTop: "1rem" }}
              component="div"
              align="center"
            >
              {" "}
              Please enter your email address and Business ID <br></br>to reset
              your password.
            </Typography>
          </Box>
          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <Formik
              initialValues={{
                email: "",
                bId: "",
              }}
              onSubmit={async (values) => {
                localStorage.setItem("bId", values.bId);
                localStorage.setItem("email", values?.email);
                await submitValues(values);
              }}
              validationSchema={validationSchemaForgetPassword}
            >
              {(props: any) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={4}>
                      <InputLabel shrink htmlFor="bootstrap-input"></InputLabel>
                      Email :<span style={{ color: "red" }}>*</span>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="email"
                        disabled={isLoading}
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.email && Boolean(errors.email)}
                        placeholder={"Please enter your email or Number"}
                        helperText={touched.email ? errors.email : ""}
                        sx={{ my: 1 }}
                      />
                      <InputLabel shrink htmlFor="bootstrap-input"></InputLabel>
                      Business ID :<span style={{ color: "red" }}>*</span>
                      <TextField
                        fullWidth
                        variant="outlined"
                        name="bId"
                        disabled={isLoading}
                        value={values.bId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={touched.bId && Boolean(errors.bId)}
                        helperText={touched.bId ? errors.bId : ""}
                        placeholder={"Please enter your Business ID"}
                        sx={{ my: 1 }}
                      />
                    </Box>

                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                      style={{ margin: "2rem 0 1rem 0" }}
                    >
                      <Button
                        type="submit"
                        fullWidth
                        size="large"
                        variant="contained"
                        color="primary"
                        disabled={isLoading}
                      >
                        Submit
                      </Button>
                    </Box>
                    <div className="display-flex-jc-center">
                      <Typography
                        textAlign={"center"}
                        sx={{ cursor: "pointer", marginRight: "2%" }}
                      >
                        Go back to business login?
                      </Typography>
                      <Link href="/login" passHref>
                        <Typography
                          textAlign={"center"}
                          color="blue"
                          sx={{ cursor: "pointer" }}
                        >
                          Click here
                        </Typography>
                      </Link>
                    </div>
                    <Typography
                      sx={{ marginTop: "1.5rem", cursor: "pointer" }}
                      component="div"
                      align="center"
                    >
                      © 2023 Shopo Central. All rights reserved
                    </Typography>
                  </form>
                );
              }}
            </Formik>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              marginTop: "2%",
            }}
          ></Box>
        </Box>
      </Box>
    </>
  );
};

export default ForgotPassword;
