// Build in library
import Router from "next/router";
import { useEffect, useState } from "react";
// MUI Components
import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Box,
  Button,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
// Third party library
import { Formik } from "formik";
import { toast } from "react-toastify";
// Route
import withAuth from "../../../../../hoc/PublicRoute";
// Redux
import {
  useForgotPasswordMutation,
  useResetPasswordMutation
} from "../../../../../redux/api/auth/auth";
// Validation
import {
  validatePasswordChange
} from "../../../../common/validationSchema";
// style
import styles from "../../../../components/Forms.module.css";

const VerifyPasswordOtp = () => {
  const [email, setEmail] = useState<string | null>("");
  const [bid, setBid] = useState<string | null>("");
  const [count, setCount] = useState<any>(60);

  const [
    ResendOtp,
    {
      isLoading: ResendOtpLoading,
      isSuccess: ResendOtpSuccess,
      isError: ResendOtpFailed,
      data: ResendOtpData,
    },
  ] = useForgotPasswordMutation();

  const [ResetPassword, { isLoading, isSuccess, data, isError, error }] =
    useResetPasswordMutation();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  useEffect(() => {
    setEmail(localStorage.getItem("email"));
    setBid(localStorage.getItem("bid"));
  }, []);

  useEffect(() => {
    const timer: any =
      count > 0 && setInterval(() => setCount(count - 1), 1000);
    return () => clearInterval(timer);
  }, [count]);

  const resetOtp = async () => {
    setCount(60);
    ResendOtp({ email: email, bid: bid })
      .unwrap()
      .then((data: any) => {
        toast.success(data?.message);
        setCount(60);
      })
      .catch((error: any) => {
        const ermsg = error?.data?.message;
        toast.error(ermsg);
      });
  };

  useEffect(() => {
    if (data && isSuccess) {
      Router.replace("/login");
      toast("Password Reset Successful", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
    }
  }, [data, isSuccess]);
  useEffect(() => {
    if (isError || error) {
      let errorMessage: any = error;
      toast.error(errorMessage?.data?.message);
    }
  }, [isError, error]);

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Box
          sx={{
            background: "#0072d8",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            width: "50%",
            height: "100vh",
            display: "flex",
            padding: "5%",
          }}
        >
          <Box className={styles.forms} sx={{ p: 6 }}>
            <Box
              sx={{
                mb: 5,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h3"
                color="primary"
                component="div"
                align="center"
              >
                Welcome
              </Typography>

              <Typography
                variant="subtitle1"
                sx={{ marginTop: "1rem" }}
                component="div"
                align="center"
              >
                {" "}
                Welcome Back! Please enter your Credentials
              </Typography>
            </Box>
            <Box
              sx={{
                mt: 1,
                width: { sm: "450px", xs: "100%" },
              }}
            >
              <Formik
                initialValues={{
                  code: "",
                  password: "",
                  bId: "",
                  password_confirmation: "",
                }}
                validationSchema={validatePasswordChange}
                onSubmit={async (values) => {
                  const emailData = localStorage.getItem("email");
                  if (values?.password !== values?.password_confirmation) {
                    toast.error("password doesnt match");
                  }
                  const email = {
                    email: emailData,
                  };

                  let finalVal = {
                    ...values,
                    bId: localStorage.getItem("bId"),
                    ...email,
                  };
                  await ResetPassword(finalVal);
                }}
              >
                {(props: any) => {
                  const {
                    values,
                    touched,
                    errors,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                  } = props;
                  return (
                    <form onSubmit={handleSubmit}>
                      <Box mb={4}>
                        <InputLabel
                          shrink
                          htmlFor="bootstrap-input"
                        ></InputLabel>
                        Code
                        <TextField
                          fullWidth
                          variant="outlined"
                          name="code"
                          disabled={isLoading}
                          value={values.code}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          error={touched.code && Boolean(errors.code)}
                          helperText={touched.code ? errors.code : ""}
                          placeholder={"Please enter your code"}
                          inputProps={{ maxLength: 6 }}
                        />
                      </Box>
                      <Box>
                        <InputLabel shrink htmlFor="bootstrap-input">
                          <Typography variant="h6"></Typography>
                        </InputLabel>
                        Password
                        <OutlinedInput
                          id="outlined-adornment-password"
                          fullWidth
                          name="password"
                          type={showPassword ? "text" : "password"}
                          value={values.password}
                          disabled={isLoading}
                          onChange={handleChange}
                          placeholder={"Please enter your password"}
                          // onBlur={handleBlur}
                          error={touched.password && Boolean(errors.password)}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={() =>
                                  setShowPassword((prev: boolean) => !prev)
                                }
                                edge="end"
                              >
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {touched.password && errors.password && (
                          <FormHelperText error id="accountId-error">
                            {errors.password}
                          </FormHelperText>
                        )}
                      </Box>
                      <Box sx={{ mt: 3 }}>
                        <InputLabel shrink htmlFor="bootstrap-input">
                          <Typography variant="h6"></Typography>
                        </InputLabel>
                        Confirm Password
                        <OutlinedInput
                          id="outlined-adornment-password"
                          fullWidth
                          name="password_confirmation"
                          type={showConfirmPassword ? "text" : "password"}
                          value={values.password_confirmation}
                          disabled={isLoading}
                          onChange={handleChange}
                          placeholder={"Confirm Password"}
                          // onBlur={handleBlur}
                          error={
                            touched.password_confirmation &&
                            Boolean(errors.password_confirmation)
                          }
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={() =>
                                  setShowConfirmPassword(!showConfirmPassword)
                                }
                                edge="end"
                              >
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {touched.password_confirmation &&
                          errors.password_confirmation && (
                            <FormHelperText error id="accountId-error">
                              {errors.password_confirmation}
                            </FormHelperText>
                          )}
                      </Box>
                      <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                        style={{ margin: "2rem 0 1rem 0" }}
                      >
                        <Button
                          type="submit"
                          fullWidth
                          size="large"
                          variant="contained"
                          color="primary"
                          disabled={isLoading}
                        >
                          Submit
                        </Button>
                      </Box>

                      {count !== 0 ? (
                        <p
                          style={{
                            width: "100%",
                            textAlign: "center",
                            fontWeight: "bold",
                          }}
                        >
                          code expires in: {count}
                        </p>
                      ) : (
                        <Typography
                          style={{
                            width: "100%",
                            textAlign: "center",
                            color: "blue",
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            resetOtp();
                          }}
                        >
                          {ResendOtpLoading ? (
                            <p>OTP Sending....</p>
                          ) : (
                            <p>Click here to Resend Opt</p>
                          )}
                        </Typography>
                      )}

                      <Typography
                        sx={{ marginTop: "1.5rem", cursor: "pointer" }}
                        component="div"
                        align="center"
                      >
                        © 2023 Shopo Central. All rights reserved
                      </Typography>
                    </form>
                  );
                }}
              </Formik>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default withAuth(VerifyPasswordOtp);
