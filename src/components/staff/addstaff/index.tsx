import {
  Box,
  Button,
  TextField,
  Typography
} from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { FormikProvider, useFormik } from "formik";
import Router from "next/router";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
  usePostCheckForStaffMutation,
  usePostRoleForStaffMutation,
} from "../../../../redux/api/user/user";
const Staff = () => {
  const [
    checkStaff,
    {
      isLoading: Checkstaff,
      isSuccess: CheckStaffSuccess,
      data: CheckStaffData,
      isError: CheckStaffisError,
      error: CheckStaffError,
    },
  ] = usePostCheckForStaffMutation();
  const [AssignStaff, { isLoading, isSuccess, data, isError, error }] =
    usePostRoleForStaffMutation();
  const [ID, setID] = useState<any>();

  useEffect(() => {
    if (CheckStaffSuccess && CheckStaffData) {
      if (CheckStaffData?.status == "VERIFIED") {
        toast("Email Account Identified", {
          autoClose: 2500,
          type: "success",
          pauseOnHover: true,
        });
        setID(CheckStaffData?.id);
      }
    }
  }, [CheckStaffSuccess, CheckStaffData]);

  useEffect(() => {
    if (isSuccess && data) {
      toast("Successfully added staff", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      Router.push("/staff/viewstaff");
    }
  }, [isSuccess, data]);

  const handleStaffRole = (val: any) => {
    const data = {
      userId: ID,
    };
    AssignStaff(data);
  };
  const formik = useFormik({
    initialValues: {
      email: "",
    },
    enableReinitialize: true,
    onSubmit: async (values: any) => {
      await checkStaff(values);
    },
  });
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;
  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          width: "100%",
          marginTop: "5%",
        }}
      >
        <FormikProvider value={formik}>
          <form onSubmit={handleSubmit}>
            <Box
              sx={{
                border: "2",
                marginTop: "2%",
                marginBottom: "5%",
                width: "40vw",
              }}
            >
              <Typography>Email</Typography>
              <TextField
                {...getFieldProps("email")}
                placeholder="Enter Email"
                fullWidth
              />
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Button
                  disabled={isSubmitting}
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  sx={{ marginTop: "2%" }}
                >
                  Check Staff
                </Button>
              </Box>
            </Box>
            {ID ? (
              <>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Role</InputLabel>
                  <Select onChange={handleStaffRole}>
                    <MenuItem value="SADMIN"> Staff</MenuItem>
                    {/* <MenuItem value="SSTAFF">Shop Staff</MenuItem>
                                <MenuItem value="BADMIN">Branch Admin</MenuItem>
                                <MenuItem value="BSTAFF">Branch Staff</MenuItem> */}
                  </Select>
                </FormControl>
              </>
            ) : (
              <></>
            )}
          </form>
        </FormikProvider>
      </Box>
    </>
  );
};
export default Staff;
