import { useState } from "react"
import { StaffTableView } from "./staffTableView"

export const StaffMain = () => {
    const [openSearchAddStaff, setOpenSearchStaff] = useState<boolean>(false)

    return (
        <div>
            <div className="staff-view" >
                <div className="staff-view-tableview" >
                    <StaffTableView setOpenSearchStaff={setOpenSearchStaff} />
                </div>
            </div>
        </div>
    )
}