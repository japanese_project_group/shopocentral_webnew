import { Button, CircularProgress } from "@mui/material";
import { useRouter } from "next/router";

import { skipToken } from "@reduxjs/toolkit/dist/query";
import {
  useGetStaffStatusInvitationQuery,
  usePatchStaffInvitationActionMutation,
} from "../../../redux/api/user/user";
import { randomPasswordGenerator } from "../../common/commonFunc";
import { businessId } from "../../common/utility/Constants";

export const StaffInvitationForm = () => {
  const router = useRouter();
  // console.log("buisneid", businessKycId)
  // const roleId = router.query.roleId;
  const statusId = router.query.statusId;
  console.log("statusId", statusId);
  const businessKycId = businessId;
  const userId = router.query.userId;
  // console.log("userId", userId);

  const body = {
    businessKycId: businessKycId,
    //  roleId: roleId,
    statusId: statusId,
    userId: userId,
  };
  const { data: staffCheckdata, isLoading } = useGetStaffStatusInvitationQuery(
    body ?? skipToken
  );

  const [updateStatus, { data: updatedStaffStatus }] =
    usePatchStaffInvitationActionMutation();

  const handleAcceptReject = (value: string) => {
    const password = randomPasswordGenerator();
    let body = {
      id: staffCheckdata?.id,
      businessAccessId:
        staffCheckdata?.businessKyc?.userOnBusinessKyc?.[0]?.businessAccessId,
      businessKycId: businessId,
      accepted: true,
      password: password,
    };
    updateStatus(body)
      .unwrap()
      .catch((error?: any) => { });
  };
  console.log("Staff Check", staffCheckdata);
  return (
    <div>
      {!isLoading ? (
        <div className="display-flex-jc-center">
          <div className="staff-invitation-accept-reject-box">
            {staffCheckdata?.isAccepted === false && !!!updatedStaffStatus ? (
              <>
                <div style={{ margin: "10px 0px 10px 0px", fontSize: "18px" }}>
                  Hello {staffCheckdata?.user?.firstName},
                  <div>
                    {" "}
                    You have been invited to join as a staff by{" "}
                    {staffCheckdata?.businessKyc?.name_en}
                  </div>
                </div>
                <div
                  className="display-flex-sb"
                  style={{ marginBottom: "15px" }}
                >
                  <Button
                    variant="contained"
                    onClick={() => {
                      handleAcceptReject("VERIFIED");
                    }}
                  >
                    Accept
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => {
                      handleAcceptReject("REJECTED");
                    }}
                  >
                    Reject
                  </Button>
                </div>
              </>
            ) : (
              <div style={{ margin: "10px 0px 10px 0px", fontSize: "18px" }}>
                <div>
                  You have{" "}
                  {(updatedStaffStatus?.accepted || staffCheckdata?.isAccepted)
                    && "accepted"}
                  {" "}
                  our invitation
                </div>
              </div>
            )}
          </div>
        </div>
      ) : (
        <div className="display-flex-jc-center">
          <div className="staff-invitation-accept-reject-box">
            <div className="display-flex-jc-center">
              <CircularProgress />
            </div>
            <div>Checking your status...</div>
          </div>
        </div>
      )}
    </div>
  );
};
