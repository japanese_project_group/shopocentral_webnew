import AddCircleIcon from "@mui/icons-material/AddCircle";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import { useFormik } from "formik";
import { isEmpty } from "lodash";
import { toast } from "react-toastify";

import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import {
  useAddStaffInviteMutation,
  useGetPermissionDataQuery,
  useGetRoleForStaffQuery,
  useGetStaffProfileDataQuery,
  usePatchRoleForStaffMutation,
  usePostAddStaffMutation,
  usePostCheckForStaffMutation,
  usePostRoleForStaffMutation,
} from "../../../../redux/api/user/user";
import { enrollmentTypeData } from "../../../common/commonData";
import { removeDuplicates } from "../../../common/commonFunc";
import { validationSchemeSearchStaff } from "../../../common/validationSchema";

export const AddStaffRole = () => {
  const router = useRouter();
  console.log("router", router);
  const [openInviteComponent, setOpenInviteComponent] = useState<boolean>(true);
  const [openInvitePage, setOpenInvitePage] = useState<boolean>(true);
  const [profileData, setProfileData] = useState<any>([]);
  const [openAssignRoleBox, setOpenAssignRoleBox] = useState<boolean>(true);
  const [staffRoles, setStaffRoles] = useState<any>([]);

  const businessKycId = localStorage.getItem("businessId");

  const [
    checkStaff,
    {
      isLoading: Checkstaff,
      isSuccess: CheckStaffSuccess,
      data: CheckStaffData,
      isError: CheckStaffisError,
      error: CheckStaffError,
    },
  ] = usePostCheckForStaffMutation();

  const {
    data: staffProfileData,
    isLoading: isLoadingStaffProfileData,
    refetch,
  } = useGetStaffProfileDataQuery({
    staffId: router?.query?.details,
    businessKycId,
  });

  useEffect(() => {
    refetch();
  }, [router?.query?.details, refetch]);

  const { data: allRoleStaff, isLoading } = useGetRoleForStaffQuery();
  console.log("staffProfile fgsgfh", staffProfileData);
  console.log(" al Role ", allRoleStaff?.data);
  const [
    addStaffInvite,
    {
      isLoading: CheckstaffInvite,
      isSuccess: CheckStaffInviteSuccess,
      data: CheckStaffInviteData,
      isError: CheckStaffInviteisError,
      error: CheckStaffInviteError,
    },
  ] = useAddStaffInviteMutation();

  const formik = useFormik({
    initialValues: {
      email: "",
      phonenumber: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemeSearchStaff,
    onSubmit: async (values: any) => {
      await checkStaff(values)
        .unwrap()
        .then((data: any) => { })
        .catch((error: any) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    },
  });
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;
  useMemo(() => {
    if (!isEmpty(CheckStaffData)) {
      toast.success(CheckStaffData?.status);
      // if (CheckStaffData?.status === "VERIFIED") {
      //   setOpenInviteComponent(true);
      //   setProfileData(CheckStaffData);
      // }
      // setProfileData(CheckStaffData);
    }
  }, [CheckStaffData]);
  useEffect(() => {
    if (!isEmpty(allRoleStaff)) {
      setStaffRoles(allRoleStaff?.data);
    }
  }, [allRoleStaff]);

  useEffect(() => {
    // if (router?.query?.edit === "addrole") {
    //   setOpenInviteComponent(true);
    //   setProfileData(staffProfileData?.user);
    // }
    setProfileData(staffProfileData);
  }, [staffProfileData]);

  const viewImageStyle = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: { lg: "35%", sm: "50%", xs: "80%" },
    bgcolor: "background.paper",
    boxShadow: 24,
    // p: 4,
    borderRadius: 2,
    height: "90%",
    // overf
  };
  const handleClose = () => {
    setOpenAssignRoleBox(false);
  };

  const toBeSelectedRole =
    staffProfileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
      ?.MerchantStaffRole?.MerchantStaffRoleOnPermission;

  // const toBeSelectedRole =
  //   staffProfileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]?.MerchantStaffRole?.MerchantStaffRoleOnPermission.filter(
  //     (data: any) => !isEmpty(data?.role?.userAcceptRole)
  //   );

  // const toBeSelectedRole = staffProfileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
  // ?.MerchantStaffRole?.MerchantStaffRoleOnPermission;
  // console.log("Staff staffProfileData", staffProfileData);
  console.log("Staff role selected", toBeSelectedRole);
  // const accessGiven = toBeSelectedRole?.[0]?.role?.permission?.map(
  //   (type: any) => type?.permission?.permissionType?.name
  // );

  // console.log("access iven ", accessGiven)
  // const accessGivenTypes = removeDuplicates(accessGiven);
  // console.log("AccessGivenTypes",accessGivenTypes)

  const handleInviteStaff = () => {
    const businessKycId = localStorage.getItem("businessId");

    const data = {
      userId: profileData?.profile?.userId,
      businessKycId: businessKycId,
    };
    addStaffInvite(data)
      .unwrap()
      .catch((error: any) => {
        toast.error(error?.data?.message[0]);
      });
  };

  useEffect(() => {
    if (!!CheckStaffInviteData) {
      toast.success(CheckStaffInviteData?.messgae);
      router.push("/staff");
    }
  }, [CheckStaffInviteData]);

  console.log("profileDAtanskvnk", profileData);
  console.log("toBeSelectedRole", toBeSelectedRole);
  return (
    <div className="staff-view">
      <div>
        {/* <Modal
            open={openAssignRoleBox}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          > */}

        <AddRole
          staffRoles={staffRoles}
          setOpenInvitePage={setOpenInvitePage}
          profileData={profileData}
          setStaffRoles={setStaffRoles}
          userHasRoleId={staffProfileData?.userId}
          router={router}
          allData={toBeSelectedRole}
          setProfileData={setProfileData}
        />

        {/* {router?.query?.edit === "add" ? (
          <AddRole
            staffRoles={staffRoles}
            setOpenInvitePage={setOpenInvitePage}
            profileData={profileData}
            setStaffRoles={setStaffRoles}

          />
        ) : (
          <Box style={{ overflow: "auto" }}>
            <AssignRoleBox
              staffRoles={staffRoles}
              setOpenInvitePage={setOpenAssignRoleBox}
              profileData={profileData}
              allData={toBeSelectedRole?.[0]}
              updated={true}
              userHasRoleId={staffProfileData?.userHasRoleId}
            />
          </Box>
        )} */}
        {/* </Modal>   */}
      </div>
    </div>
  );
};

export const AddRole = ({
  staffRoles,
  setOpenInvitePage,
  profileData,
  allData,
  userHasRoleId,
  setProfileData,
  router,
}: any) => {
  const [staffValue, setStaffValue] = useState<string>("");
  const [addStaffRole, { data: staffAddData }] = usePostAddStaffMutation();
  const { data: allRoleStaff, isLoading, refetch } = useGetRoleForStaffQuery();
  console.log("Staff Profile", profileData);
  const handleAddStaff = () => {
    let body = { name: staffValue };
    addStaffRole(body)
      .unwrap()
      .catch((error: any) => { });
    setStaffValue("");
  };
  useEffect(() => {
    if (!!staffAddData) {
      toast.success(staffAddData?.message);
      refetch();
    }
  }, [staffAddData]);

  return (
    <div className="">
      <Grid container sx={{ marginTop: "40px" }}>
        <Grid xs={12} md={8}>
          {router?.query?.edit === "add" ? (
            <AssignRoleBox
              staffRoles={staffRoles}
              setOpenInvitePage={setOpenInvitePage}
              profileData={profileData}
            />
          ) : (
            <Box style={{ overflow: "auto" }}>
              <AssignRoleBox
                staffRoles={staffRoles}
                // setOpenInvitePage={setOpenInvitePage}
                profileData={profileData}
                allData={allData}
                updated={true}
                userHasRoleId={userHasRoleId}
                setProfileData={setProfileData}
              />
            </Box>
          )}
        </Grid>
        <Grid xs={12} md={4}>
          <Stack
            sx={{
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              minHeight: "700px",
              borderRadius: "16px",
              background: "white",
            }}
            m={2}
          >
            <Box mt={10} ml={5}>
              <Typography variant="h5">Create Role</Typography>
              <div style={{ display: "flex", alignItems: "center" }}>
                <TextField
                  id="outlined-basic"
                  label=""
                  size="small"
                  variant="outlined"
                  value={staffValue}
                  onChange={(e) => {
                    setStaffValue(e.target.value);
                  }}
                />
                <AddCircleIcon
                  sx={{
                    cursor: "pointer",
                    fontSize: "50px",
                    "&:hover": {
                      color: "gray",
                    },
                  }}
                  onClick={() => {
                    handleAddStaff();
                  }}
                />
              </div>
              {staffRoles?.map((data: any, index: number) => (
                <Typography mt={5} key={index}>
                  {data?.name.charAt(0).toUpperCase() +
                    data?.name?.substring(1)}
                </Typography>
              ))}
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
};
export const AssignRoleBox = ({
  staffRoles,
  setOpenInvitePage,
  profileData,
  allData,
  updated,
  userHasRoleId,
  setProfileData,
}: any) => {
  console.log("Profile Data assis", profileData);
  const router = useRouter();
  const [
    AssignStaff,
    { isLoading, isSuccess, data: addStaffData, isError, error },
  ] = usePostRoleForStaffMutation();
  const [UpdateStaff, { data: updateStaffData }] =
    usePatchRoleForStaffMutation();

  const { data: permissionData, isSuccess: isSuccessPermissionData } =
    useGetPermissionDataQuery();
  // console.log("permissionData", permissionData);

  const [selectRole, setSelectRole] = useState<any>(
    `${profileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]?.MerchantStaffRole?.id}`
  );
  console.log("select Role", selectRole);
  const [selectEnrollment, setSelectEnrollment] = useState<any>("");
  const [permissionId, setPermissionId] = useState<any>([]);
  const [checkedPermissionTyeId, setCheckedPermissionTyeId] = useState<any>([]);
  const [checkedData, setCheckedData] = useState<any>([]);
  const [updatedData, setUpdatedData] = useState<any>([]);

  console.log("checked permission", checkedPermissionTyeId);

  console.log(
    "slected role",
    profileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
      ?.MerchantStaffRole?.id
  );
  // console.log("permission id ", permissionId)
  // console.log("router id ", router?.query?.details)
  // console.log("user id", profileData?.userId)
  // console.log("userHasId", userHasRoleId)
  // console.log("profile Data called", allData);
  // console.log("Profile id", profileData?.userId)
  // console.log("AllData jn", allData);

  useEffect(() => {
    setSelectRole(
      `${profileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]?.MerchantStaffRole?.id}`
    );
  }, [profileData]);

  const inviteStaff = () => {
    if (updated) {
      const previosPermissoinId = allData?.map(
        (data: any) => data?.Permissions?.id
      );
      // const uniqueUserHasPermissionId = removeDuplicates(userHasPermissionId);
      const userHasRoleId = localStorage.getItem("userId");
      const businesskycId = localStorage.getItem("businessId");

      console.log("uniqueUserHasPermissionId", previosPermissoinId);
      const data = {
        // roleId: selectRole,
        userId: profileData?.merchantStaffId,
        merchantStaffId: profileData?.staff?.MerchantStaffBy[0]?.id,
        previousRoleId:
          profileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
            ?.MerchantStaffRole?.id,
        latestRoleId: selectRole,
        // userId: profileData?.id,

        // userHasRoleId: userHasRoleId,
        previousPermissionId: previosPermissoinId,
        latestPermissionId: permissionId,
        businessKycId: businesskycId,

        // permissionId: permissionId,
        // userAcceptId: profileData?.userAcceptId,
      };
      UpdateStaff(data)
        .unwrap()
        .catch((error: any) => {
          toast.error(error?.data?.message[0]);
        });
    } else {
      console.log("assign clle");
      const businesskycId = localStorage.getItem("businessId");
      const data = {
        roleId: selectRole,
        userId: profileData?.merchantStaffId,
        permissionId: permissionId,
        businessKycId: businesskycId,
        staffMerchantId: profileData?.staff?.MerchantStaffBy[0]?.id,
        // userAcceptId: profileData?.userAcceptId,
      };
      AssignStaff(data)
        .unwrap()
        .catch((error: any) => {
          toast.error(error?.data?.message);
        });
    }
  };
  console.log("Permission Id", permissionId);

  const handleCheckPermissionChange = (e: any, data: any, index?: number) => {
    console.log("checkPermission ee", e.target.checked);
    console.log("checked data", data);
    if (e.target.checked) {
      setCheckedPermissionTyeId((checked: any) => [
        ...(checked || []),
        data?.id,
      ]);
      if (data?.permission && Array.isArray(data.permission)) {
        data.permission.forEach((subData: any) => {
          setPermissionId((id: any) => [...(id || []), subData?.id]);
        });
      }

      setCheckedData(data);
    } else {
      const finalData = checkedPermissionTyeId?.filter(
        (datas: any) => datas !== data?.id
      );
      setCheckedPermissionTyeId(finalData);
      const removeItems = (array: any, itemToRemove: any) => {
        return array.filter((v: any) => {
          return !itemToRemove.includes(v);
        });
      };
      const tobeRemovedData = data?.permission?.map((data: any) => data?.id);
      const uncheckedData = removeItems(permissionId, tobeRemovedData);
      setPermissionId(uncheckedData);
      setCheckedData([]);
    }
  };
  const handleCheckSubPermissionChange = (e: any, data: any) => {
    if (e.target.checked) {
      setPermissionId((id: any) => [...id, data?.id]);
    } else {
      const checkedIds = permissionId?.filter(
        (datas: any) => datas != data?.id
      );
      setPermissionId(checkedIds);
    }
  };

  useEffect(() => {
    if (!!addStaffData) {
      toast.success(addStaffData?.messgae);
      // setCheckedData([]);
      // setUpdatedData([]);

      // window.location.reload();
      router.push("/staff");
    }
  }, [addStaffData]);
  useEffect(() => {
    if (updateStaffData) {
      toast.success("Successfully updated staff ");
      // setCheckedData([]);
      // setUpdatedData([]);
      // setPermissionId([]);
      // setCheckedPermissionTyeId([]);
      // setProfileData([]);

      // router.replace("/staff");
      // window.location.reload();
      // const currentUrl = window.location.href;
      // window.location.replace(currentUrl);

      location.replace("/staff");
    }
  }, [updateStaffData]);

  useEffect(() => {
    if (!!profileData) {
      // setSelectRole(allData?.Permissions?.id);
      const selectedPermissionTypeId = allData?.map(
        (value: any) => value?.Permissions?.permissionType?.id
      );

      // console.log("Selected per ", selectedPermissionTypeId)
      const uniqueSelectedPermissionTypeId = removeDuplicates(
        selectedPermissionTypeId
      );
      const selectedSubPermission = allData?.map(
        (value: any) => value?.Permissions?.id
      );
      console.log("selectedSubPermission", selectedSubPermission);
      const uniqueSelectedSubPermisssion = removeDuplicates(
        selectedSubPermission
      );
      setPermissionId(uniqueSelectedSubPermisssion);
      setCheckedPermissionTyeId(uniqueSelectedPermissionTypeId);
      const selectedPermissionNames = allData?.map(
        (value: any) => value?.Permissions?.name
      );
      const uniqueSelectedPermissionNames = removeDuplicates(
        selectedPermissionNames
      );

      // setUpdatedData([...uniqueSelectedPermissionNames, "delete_users"]);
      setUpdatedData(uniqueSelectedPermissionNames);
      console.log("uniqueSEle", uniqueSelectedSubPermisssion);
    }
  }, [profileData, updated]);

  console.log("uniqueSelectedSubPermisssion", checkedData);

  useEffect(() => {
    if (!!checkedData) {
      checkedData?.permission?.map((data: any) => {
        const checked: any = document.getElementsByClassName(
          `access-list${data?.name}`
        )[0];
        // console.log("checked new", checkedData);
        // checked.checked = true;

        if (!!checked) {
          checked.checked = true;
        }
      });
    }
  }, [checkedData]);

  useEffect(() => {
    if (updatedData?.length > 0) {
      updatedData?.map((names: any) => {
        const checked: any = document.getElementsByClassName(
          `access-list${names}`
        )[0];

        // checked.checked = true;

        if (!!checked) {
          checked.checked = true;
        }
        // checked.checked = true;
        // console.log("updated new", updatedData);
      });
    }
  }, [updatedData]);
  console.log("updated Data", updatedData);

  console.log(
    "profile id hbk",
    profileData?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
      ?.MerchantStaffRole?.id
  );

  return (
    <Stack
      sx={{
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: "16px",
        minHeight: "700px",
        background: "white",
      }}
      m={2}
    >
      <Box sx={{ marginLeft: "40px" }} mt={2}>
        <Typography variant="h5">Add Staff</Typography>
        <Typography variant="h6" fontWeight="bold" mt={2}>
          Name
        </Typography>
        <Typography sx={{ display: "flex" }}>
          {profileData?.staff?.firstName} {profileData?.staff?.middleName}{" "}
          {profileData?.staff?.lastName}
        </Typography>
      </Box>
      <Box mt={5} sx={{ marginLeft: "40px" }}>
        <Typography variant="h6" fontWeight="bold">
          Enrollment Type:
        </Typography>
        <Select
          value={selectEnrollment}
          style={{ width: 300 }}
          onChange={(e: any) => setSelectEnrollment(e.target.value)}
        >
          {enrollmentTypeData?.map((data: any, index: number) => (
            <MenuItem value={data?.value} key={index}>
              {data?.label}
            </MenuItem>
          ))}
        </Select>
        <Typography variant="h6" fontWeight="bold">
          Roles
        </Typography>
        <FormControl sx={{ width: 300 }}>
          <Select
            // name={selectRole}
            value={selectRole}
            onChange={(e: any) => {
              console.log("ejabsdkfjbsa", e.target.value);
              setSelectRole(e?.target?.value);
            }}
          >
            {staffRoles?.map((data: any, index: number) => (
              <MenuItem value={data?.id} key={data?.id}>
                {data?.name}
              </MenuItem>
            ))}
          </Select>

          <Typography variant="h6" fontWeight="bold" mt={10}>
            Access
          </Typography>
          <>
            {permissionData?.data?.map(
              (permissionType?: any, index?: number) => (
                <div>
                  <div
                    key={permissionType?.id}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: "20px",
                    }}
                  >
                    <Checkbox
                      onChange={(e: any) => {
                        handleCheckPermissionChange(e, permissionType, index);
                      }}
                      checked={
                        checkedPermissionTyeId?.includes(permissionType?.id)
                          ? true
                          : false
                      }
                    />
                    <Typography>{permissionType?.name}</Typography>
                    {/* <Typography>{permissionType?.id}</Typography> */}
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    {checkedPermissionTyeId?.includes(permissionType?.id) &&
                      permissionType?.permission?.map((permission: any) => (
                        <div
                          key={permission?.id}
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginLeft: "20px",
                          }}
                        >
                          <input
                            id={`access-list${permission?.name}`}
                            type="checkbox"
                            style={{
                              padding: "3px",
                              accentColor: "#5F9EA0",
                            }}
                            className={`access-list${permission?.name}`}
                            onChange={(e: any) => {
                              setUpdatedData([]);
                              handleCheckSubPermissionChange(e, permission);
                            }}
                          />
                          <p
                            style={{
                              color: "#1a76d2",
                              margin: "0px",
                              padding: "0px",
                            }}
                          >
                            {permission?.name}
                          </p>
                        </div>
                      ))}
                  </div>
                </div>
              )
            )}
          </>
        </FormControl>
      </Box>

      <Box sx={{ height: 200 }}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignSelf: "center",
            gap: "40px",
            marginTop: "150px",
          }}
        >
          <Button
            variant="outlined"
            onClick={() => {
              // setOpenInvitePage(false);
              router.push("/staff");
            }}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            onClick={() => {
              inviteStaff();
            }}
          >
            Invite
          </Button>
        </div>
      </Box>
    </Stack>
  );
};
