import AddCircleIcon from "@mui/icons-material/AddCircle";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import SearchIcon from "@mui/icons-material/Search";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import Modal from "@mui/material/Modal";
import { FormikProvider, useFormik } from "formik";
import { isEmpty } from "lodash";
import { toast } from "react-toastify";

import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import {
  useAddStaffInviteMutation,
  useGetPermissionDataQuery,
  useGetRoleForStaffQuery,
  useGetStaffProfileDataQuery,
  usePatchRoleForStaffMutation,
  usePostAddStaffMutation,
  usePostCheckForStaffMutation,
  usePostRoleForStaffMutation,
} from "../../../redux/api/user/user";
import {
  InputComponent,
  ProfileView
} from "../../common/Components";
import { enrollmentTypeData } from "../../common/commonData";
import { removeDuplicates } from "../../common/commonFunc";
import { businessId } from "../../common/utility/Constants";
import { validationSchemeSearchStaff } from "../../common/validationSchema";

export const SearchFormStaff = () => {
  const router = useRouter();
  console.log("router", router);
  const [openInviteComponent, setOpenInviteComponent] =
    useState<boolean>(false);
  const [staffRoles, setStaffRoles] = useState<any>([]);
  const [openInvitePage, setOpenInvitePage] = useState<boolean>(false);
  const [profileData, setProfileData] = useState<any>("");
  const [openAssignRoleBox, setOpenAssignRoleBox] = useState<boolean>(false);
  const [
    checkStaff,
    {
      isLoading: Checkstaff,
      isSuccess: CheckStaffSuccess,
      data: CheckStaffData,
      isError: CheckStaffisError,
      error: CheckStaffError,
    },
  ] = usePostCheckForStaffMutation();
  const { data: staffProfileData, isLoading: isLoadingStaffProfileData } =
    useGetStaffProfileDataQuery(businessId);
  const { data: allRoleStaff, isLoading } = useGetRoleForStaffQuery();

  console.log("check staff ndjaks", CheckStaffData);
  console.log("all role");

  const [
    addStaffInvite,
    {
      isLoading: CheckstaffInvite,
      isSuccess: CheckStaffInviteSuccess,
      data: CheckStaffInviteData,
      isError: CheckStaffInviteisError,
      error: CheckStaffInviteError,
    },
  ] = useAddStaffInviteMutation();

  const formik = useFormik({
    initialValues: {
      email: "",
      phonenumber: "",
    },
    enableReinitialize: true,
    validationSchema: validationSchemeSearchStaff,
    onSubmit: async (values: any) => {
      await checkStaff(values)
        .unwrap()
        .then((data: any) => { })
        .catch((error: any) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    },
  });
  const {
    errors,
    values,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setFieldValue,
  } = formik;
  useMemo(() => {
    if (!isEmpty(CheckStaffData)) {
      toast.success(CheckStaffData?.status);
      if (CheckStaffData?.status === "VERIFIED") {
        setOpenInviteComponent(true);
        setProfileData(CheckStaffData);
      }
    }
  }, [CheckStaffData]);
  useEffect(() => {
    if (!isEmpty(allRoleStaff)) {
      setStaffRoles(allRoleStaff?.data);
    }
  }, [allRoleStaff]);
  useEffect(() => {
    if (router?.query?.view === "view") {
      setOpenInviteComponent(true);
      setProfileData(staffProfileData?.user);
    }
  }, [staffProfileData]);

  const viewImageStyle = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: { lg: "35%", sm: "50%", xs: "80%" },
    bgcolor: "background.paper",
    boxShadow: 24,
    // p: 4,
    borderRadius: 2,
    height: "90%",
    // overf
  };
  const handleClose = () => {
    setOpenAssignRoleBox(false);
  };
  const toBeSelectedRole = staffProfileData?.user?.role?.filter(
    (data: any) => !isEmpty(data?.role?.userAcceptRole)
  );
  const accessGiven = toBeSelectedRole?.[0]?.role?.permission?.map(
    (type: any) => type?.permission?.permissionType?.name
  );
  const accessGivenTypes = removeDuplicates(accessGiven);

  const handleInviteStaff = () => {
    // const businessKycId = localStorage.getItem("businessId");

    const data = {
      userId: profileData?.profile?.userId,
      businessKycId: businessId,
    };
    addStaffInvite(data)
      .unwrap()
      .catch((error: any) => {
        toast.error(error?.data?.message[0]);
      });
  };

  console.log("Prpfile Data", profileData);

  useEffect(() => {
    if (!!CheckStaffInviteData) {
      toast.success(CheckStaffInviteData?.messgae);
      router.push("/staff");
    }
  }, [CheckStaffInviteData]);

  return (
    <div className="staff-view">
      {!openInviteComponent ? (
        <div className="staff-view-searchstaff">
          <div className="staff-view-title">
            <ArrowBackIcon
              style={{
                fontSize: "30px",
                marginBottom: "-5px",
                cursor: "pointer",
              }}
              onClick={() => {
                router.push("/staff");
              }}
            />
            Invite Staff
          </div>
          <div style={{ marginTop: "30px" }}>Search</div>
          <FormikProvider value={formik}>
            <form onSubmit={handleSubmit}>
              <div className="display-flex" style={{ marginTop: "30px" }}>
                <div style={{ width: "70%" }} className="display-flex">
                  <div style={{ width: "50%" }}>
                    <InputComponent
                      type="text"
                      placeholder="Enter email"
                      getFieldProps={getFieldProps}
                      getFieldPropsValue="email"
                      errors={errors.email}
                      touched={touched.email}
                    />
                  </div>
                  <div style={{ width: "50%", marginLeft: "10px" }}>
                    <InputComponent
                      type="text"
                      placeholder="Enter mobile number"
                      getFieldProps={getFieldProps}
                      getFieldPropsValue="phonenumber"
                      errors={errors.phonenumber}
                      touched={touched.phonenumber}
                      maxLength={11}
                    />
                  </div>
                </div>
                <Button
                  variant="contained"
                  style={{ marginLeft: "30px", height: "40px" }}
                  type="submit"
                >
                  <SearchIcon />
                  Search
                </Button>
              </div>
            </form>
          </FormikProvider>
        </div>
      ) : openInvitePage ? (
        <AddRole
          staffRoles={staffRoles}
          setOpenInvitePage={setOpenInvitePage}
          profileData={profileData}
          setStaffRoles={setStaffRoles}
        />
      ) : (
        <div>
          <ProfileView
            showButton={false}
            header={"Personal Details"}
            familyName={profileData?.familyName}
            firstName={profileData?.firstName}
            middleName={profileData?.middleName}
            occupation={profileData?.occupation}
            // country_region={profileData?.profile?.country_region}
            email={profileData?.email}
            phoneNumber={`${profileData?.phoneNumber?.phoneType}-${profileData?.phoneNumber?.phoneNumber}`}
            optionalNumber={profileData?.phoneNumber?.secondaryPhoneNumber}
            gender={profileData?.profile?.gender}
            dob={profileData?.profile?.dob}
            profileImage={profileData?.profile?.userImage}
          />

          <div style={{ marginTop: "30px" }}></div>
          <ProfileView
            header={"Address Details"}
            country={profileData?.address?.country_en}
            postalCode={profileData?.address?.postalcode_en}
            prefecture={profileData?.address?.prefecture_en}
            city={profileData?.address?.city_en}
            town={profileData?.address?.town_en}
          />
          {/* <ProfileView
            header={"KYC Documents"}
            frontImage={profileData?.idProof?.frontImage}
            backImage={profileData?.idProof?.backImage}
            nriImage={profileData?.idProof?.nriImage}
            passportImage={profileData?.idProof?.passportImage}
            licenseImage={profileData?.idProof?.licenseImage}
          /> */}
          {/* {router?.query?.view === "view" && (
            <>
              <div style={{ marginTop: "30px" }}></div>
              <ProfileView
                header={"Roles And Access"}
                assignedRole={toBeSelectedRole?.[0]?.role?.name}
                accessGivenTypes={accessGivenTypes}
                setOpenAssignRoleBox={setOpenAssignRoleBox}
              />
            </>
          )} */}
          {/* {router?.query?.view === "view" && (
            <>
              <div style={{ height: "100vh", background: "white" }}></div>
            </>
          )} */}
          {router?.query?.view !== "view" && (
            <>
              <div className="display-flex-end">
                <Button
                  variant="contained"
                  onClick={() => setOpenInviteComponent(false)}
                  style={{ marginRight: "20px" }}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  // onClick={() => setOpenInvitePage(true)}
                  onClick={handleInviteStaff}
                >
                  Invite
                </Button>
              </div>
            </>
          )}
          {openAssignRoleBox && (
            <Modal
              open={openAssignRoleBox}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={viewImageStyle} style={{ overflow: "auto" }}>
                <AssignRoleBox
                  staffRoles={staffRoles}
                  setOpenInvitePage={setOpenAssignRoleBox}
                  profileData={profileData}
                  allData={toBeSelectedRole?.[0]}
                  updated={true}
                  userHasRoleId={staffProfileData?.userHasRoleId}
                />
              </Box>
            </Modal>
          )}
        </div>
      )}
    </div>
  );
};

export const AddRole = ({
  staffRoles,
  setOpenInvitePage,
  profileData,
}: any) => {
  const [staffValue, setStaffValue] = useState<string>("");
  const [addStaffRole, { data: staffAddData }] = usePostAddStaffMutation();
  const { data: allRoleStaff, isLoading, refetch } = useGetRoleForStaffQuery();

  const handleAddStaff = () => {
    let body = { name: staffValue };
    addStaffRole(body)
      .unwrap()
      .catch((error: any) => { });
    setStaffValue("");
  };
  useEffect(() => {
    if (!!staffAddData) {
      toast.success(staffAddData?.message);
      refetch();
    }
  }, [staffAddData]);

  return (
    <div className="">
      <Grid container sx={{ marginTop: "40px" }}>
        <Grid xs={12} md={8}>
          <AssignRoleBox
            staffRoles={staffRoles}
            setOpenInvitePage={setOpenInvitePage}
            profileData={profileData}
          />
        </Grid>
        <Grid xs={12} md={4}>
          <Stack
            sx={{
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              minHeight: "700px",
              borderRadius: "16px",
              background: "white",
            }}
            m={2}
          >
            <Box mt={10} ml={5}>
              <Typography variant="h5">Create Role</Typography>
              <div style={{ display: "flex", alignItems: "center" }}>
                <TextField
                  id="outlined-basic"
                  label=""
                  size="small"
                  variant="outlined"
                  value={staffValue}
                  onChange={(e) => {
                    setStaffValue(e.target.value);
                  }}
                />
                <AddCircleIcon
                  sx={{
                    cursor: "pointer",
                    fontSize: "50px",
                    "&:hover": {
                      color: "gray",
                    },
                  }}
                  onClick={() => {
                    handleAddStaff();
                  }}
                />
              </div>
              {staffRoles?.map((data: any, index: number) => (
                <Typography mt={5} key={index}>
                  {data?.role?.name.charAt(0).toUpperCase() +
                    data?.role?.name?.substring(1)}
                </Typography>
              ))}
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
};
export const AssignRoleBox = ({
  staffRoles,
  setOpenInvitePage,
  profileData,
  allData,
  updated,
  userHasRoleId,
}: any) => {
  const router = useRouter();
  // console.log("AllData" ,allData)
  const [
    AssignStaff,
    { isLoading, isSuccess, data: addStaffData, isError, error },
  ] = usePostRoleForStaffMutation();
  const [UpdateStaff, { data: updateStaffData }] =
    usePatchRoleForStaffMutation();

  const { data: permissionData, isSuccess: isSuccessPermissionData } =
    useGetPermissionDataQuery();

  const [selectRole, setSelectRole] = useState<any>("");
  const [selectEnrollment, setSelectEnrollment] = useState<any>("");
  const [permissionId, setPermissionId] = useState<any>([]);
  const [checkedPermission, setCheckedPermission] = useState<any>([]);
  const [checkedData, setCheckedData] = useState<any>([]);
  const [updatedData, setUpdatedData] = useState<any>([]);

  // console.log("User Profile ", profileData )

  const inviteStaff = () => {
    if (updated) {
      const userHasPermissionId = allData?.role?.permission?.map(
        (data: any) => data?.id
      );
      const uniqueUserHasPermissionId = removeDuplicates(userHasPermissionId);

      const data = {
        roleId: selectRole,
        userId: allData?.userId,
        userHasRoleId: userHasRoleId,
        roleHasPermissionId: uniqueUserHasPermissionId,
        permissionId: permissionId,
        userAcceptId: profileData?.userAcceptId,
      };
      UpdateStaff(data)
        .unwrap()
        .catch((error: any) => {
          toast.error(error?.data?.message[0]);
        });
    } else {
      // const businesskycId = localStorage.getItem("businessId");
      const data = {
        roleId: selectRole,
        userId: profileData?.profile?.userId,
        permissionId: permissionId,
        businessKycId: businessId,
        userAcceptId: profileData?.userAcceptId,
      };
      AssignStaff(data)
        .unwrap()
        .catch((error: any) => {
          toast.error(error?.data?.message);
        });
    }
  };

  const handleCheckPermissionChange = (e: any, data: any, index?: number) => {
    if (e.target.checked) {
      setCheckedPermission((checked: any) => [...checked, data?.id]);
      data?.permission?.map((subData: any) => {
        setPermissionId((id: any) => [...id, subData?.id]);
      });
      setCheckedData(data);
    } else {
      const finalData = checkedPermission?.filter(
        (datas: any) => datas !== data?.id
      );
      setCheckedPermission(finalData);
      const removeItems = (array: any, itemToRemove: any) => {
        return array.filter((v: any) => {
          return !itemToRemove.includes(v);
        });
      };
      const tobeRemovedData = data?.permission?.map((data: any) => data?.id);
      const uncheckedData = removeItems(permissionId, tobeRemovedData);
      setPermissionId(uncheckedData);
      setCheckedData([]);
    }
  };
  const handleCheckSubPermissionChange = (e: any, data: any) => {
    if (e.target.checked) {
      setPermissionId((id: any) => [...id, data?.id]);
    } else {
      const checkedIds = permissionId?.filter(
        (datas: any) => datas != data?.id
      );
      setPermissionId(checkedIds);
    }
  };

  useEffect(() => {
    if (!!addStaffData) {
      toast.success(addStaffData?.messgae);
      router.push("/staff");
    }
  }, [addStaffData]);
  useEffect(() => {
    if (!!updateStaffData) {
      toast.success("Successfully updated staff ");
      router.push("/staff");
    }
  }, [updateStaffData]);

  console.log("AllData", allData);
  useEffect(() => {
    if (!!allData) {
      setSelectRole(allData?.roleId);
      const selectedPermission = allData?.role?.permission?.map(
        (value: any) => value?.permission?.permissionType?.id
      );
      const uniqueSelectedPermission = removeDuplicates(selectedPermission);
      const selectedSubPermission = allData?.role?.permission?.map(
        (value: any) => value?.permission?.id
      );
      const uniqueSelectedSubPermisssion = removeDuplicates(
        selectedSubPermission
      );
      setPermissionId(uniqueSelectedSubPermisssion);
      setCheckedPermission(uniqueSelectedPermission);
      const selectedPermissionNames = allData?.role?.permission?.map(
        (value: any) => value?.permission?.name
      );
      const uniqueSelectedPermissionNames = removeDuplicates(
        selectedPermissionNames
      );
      setUpdatedData(uniqueSelectedPermissionNames);
    }
  }, [allData]);

  useEffect(() => {
    if (!!checkedData) {
      checkedData?.permission?.map((data: any) => {
        const checked: any = document.getElementsByClassName(
          `access-list${data?.name}`
        )[0];
        checked.checked = true;
      });
    }
  }, [checkedData]);

  useEffect(() => {
    if (updatedData?.length > 0) {
      updatedData?.map((names: any) => {
        const checked: any = document.getElementsByClassName(
          `access-list${names}`
        )[0];
        if (checked) {
          checked.checked = true;
        }
      });
    }
  });

  console.log("Updated Data", updatedData)

  return (
    <Stack
      sx={{
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: "16px",
        minHeight: "700px",
        background: "white",
      }}
      m={2}
    >
      <Box sx={{ marginLeft: "40px" }} mt={2}>
        <Typography variant="h5">Add Staff</Typography>
        <Typography variant="h6" fontWeight="bold" mt={2}>
          Name
        </Typography>
        <Typography>{profileData?.name}</Typography>
      </Box>
      <Box mt={5} sx={{ marginLeft: "40px" }}>
        <Typography variant="h6" fontWeight="bold">
          Enrollment Type:
        </Typography>
        <Select
          value={selectEnrollment}
          style={{ width: 300 }}
          onChange={(e: any) => setSelectEnrollment(e.target.value)}
        >
          {enrollmentTypeData?.map((data: any, index: number) => (
            <MenuItem value={data?.value} key={index}>
              {data?.label}
            </MenuItem>
          ))}
        </Select>
        <Typography variant="h6" fontWeight="bold">
          Roles
        </Typography>
        <FormControl sx={{ width: 300 }}>
          <Select
            value={selectRole}
            onChange={(e: any) => setSelectRole(e.target.value)}
          >
            {staffRoles?.map((data: any, index: number) => (
              <MenuItem value={data?.role?.id} key={index}>
                {data?.role?.name}
              </MenuItem>
            ))}
          </Select>

          <Typography variant="h6" fontWeight="bold" mt={10}>
            Access
          </Typography>
          <>
            {permissionData?.data?.map(
              (permissionType?: any, index?: number) => (
                <div>
                  <div
                    key={permissionType?.id}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: "20px",
                    }}
                  >
                    <Checkbox
                      onChange={(e: any) => {
                        handleCheckPermissionChange(e, permissionType, index);
                      }}
                      checked={
                        checkedPermission?.includes(permissionType?.id)
                          ? true
                          : false
                      }
                    />
                    <Typography>{permissionType?.name}</Typography>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    {checkedPermission.includes(permissionType?.id) &&
                      permissionType.permission?.map((permission: any) => (
                        <div
                          key={permission?.id}
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginLeft: "20px",
                          }}
                        >
                          <input
                            type="checkbox"
                            style={{
                              padding: "3px",
                              accentColor: "#5F9EA0",
                            }}
                            className={`access-list${permission?.name}`}
                            onChange={(e: any) => {
                              setUpdatedData([]);
                              handleCheckSubPermissionChange(e, permission);
                            }}
                          />
                          <p
                            style={{
                              color: "#1a76d2",
                              margin: "0px",
                              padding: "0px",
                            }}
                          >
                            {permission?.name}
                          </p>
                        </div>
                      ))}
                  </div>
                </div>
              )
            )}
          </>
        </FormControl>
      </Box>

      <Box sx={{ height: 200 }}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignSelf: "center",
            gap: "40px",
            marginTop: "150px",
          }}
        >
          <Button
            variant="outlined"
            onClick={() => {
              setOpenInvitePage(false);
            }}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            onClick={() => {
              inviteStaff();
            }}
          >
            Invite
          </Button>
        </div>
      </Box>
    </Stack>
  );
};
