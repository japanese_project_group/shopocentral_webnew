import NoDataFound from "@/src/common/NoDataFound";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/Edit";
import SearchIcon from "@mui/icons-material/Search";
import {
  Button,
  CircularProgress,
  IconButton,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { isEmpty } from "lodash";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useGetStaffListQuery } from "../../../redux/api/user/user";
import { businessId } from "../../common/utility/Constants";

type staffTableViewType = {
  setOpenSearchStaff?: any;
};

export const StaffTableView = ({ setOpenSearchStaff }: staffTableViewType) => {
  const router = useRouter();
  const [staffData, setStaffData] = useState<any>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const { data: allStaffData, refetch, isSuccess, isLoading, isError } = useGetStaffListQuery({ businessId, page: currentPage, pageSize: currentPageSize });
  console.log("all Staff Data", allStaffData);

  useEffect(() => {
    refetch();
  }, [businessId, refetch]);

  useEffect(() => {
    refetch({ page: currentPage, pageSize: currentPageSize });
  }, [currentPage, currentPageSize, refetch]);

  function handleAddStaff() {
    router.push("staff/add");
  }
  const columns: GridColDef[] = [
    {
      field: "staff name",
      headerName: "Staff name",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <div>
          {params.row?.staff?.firstName} {params.row?.staff?.middleName}{" "}
          {params.row?.staff?.lastName}
        </div>
      ),
    },
    {
      field: "Enrollment Type",
      headerName: "Enrollment Type",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
    },
    {
      field: "firstNRollame",
      headerName: "Role",
      minWidth: 150,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <div>
          {
            params.row?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
              ?.MerchantStaffRole?.name
          }
        </div>
      ),
    },
    {
      field: "Access",
      headerName: "Access",
      minWidth: 150,
      flex: 1,
      headerClassName: "data-grid-header-classname",
    },
    {
      field: "Phone",
      headerName: "Phone Number",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <div>
          {params.row?.staff?.phoneNumber?.phoneType}-
          {params.row?.staff?.phoneNumber?.phoneNumber}
        </div>
      ),
    },
    {
      field: "accepted",
      headerName: "Account Status",
      minWidth: 180,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params: any) => (
        <div>{params.row?.staff?.identityStatus[0]?.status}</div>
      ),
    },
    {
      field: "Date",
      headerName: "Date Joined",
      minWidth: 200,
      headerClassName: "data-grid-header-classname",
      renderCell: (params) => (
        <div>{moment(params?.row?.createdAt).format("YYYY/MM/DD")}</div>
      ),
    },
    {
      field: "action",
      headerName: "Action",
      minWidth: 200,
      flex: 1,
      headerClassName: "data-grid-header-classname",
      renderCell: (params) => (
        <>
          {console.log("params", params)}
          {params.row?.staff?.MerchantStaffBy[0]?.MerchantStaffOnRole[0]
            ?.MerchantStaffRole?.name ? (
            <>
              <div>
                <Button
                  variant="contained"
                  onClick={() => {
                    handleEditStaffAccess(params?.id);
                  }}
                >
                  <EditIcon
                    sx={{ fontSize: "20px" }}
                    className="shop-body-add-icon"
                  />

                  <Typography
                    sx={{
                      color: "#fff",
                      fontSize: "12px",
                      m: 1,
                      ml: 3,
                      opacity: 0.75,
                    }}
                  >
                    Edit access role
                  </Typography>
                </Button>
              </div>
            </>
          ) : (
            <>
              <div>
                <Button
                  variant="contained"
                  onClick={() => {
                    handleAddStaffAccess(params?.id);
                  }}
                >
                  <EditIcon
                    sx={{ fontSize: "20px" }}
                    className="shop-body-add-icon"
                  />

                  <Typography
                    sx={{
                      color: "#fff",
                      fontSize: "12px",
                      m: 1,
                      ml: 3,
                      opacity: 0.75,
                    }}
                  >
                    Add access role
                  </Typography>
                </Button>
              </div>
            </>
          )}
        </>
      ),
    },
  ];
  useEffect(() => {
    if (!isEmpty(allStaffData)) {
      setStaffData(allStaffData);
    }
  }, [allStaffData]);
  // const handleOnCellClick = (e: any) => {
  //   router.push(`/staff/view?details=${e?.id}`);
  //   console.log(e?.id)
  // };

  const handleOnCellClick = () => { };

  const handleAddStaffAccess = (id: any) => {
    router.push(`/staff/access/add?details=${id}`);
    console.log(id);
  };
  const handleEditStaffAccess = (id: any) => {
    router.push(`/staff/access/edit?details=${id}`);
  };

  return (
    <div>
      <div className="staff-view-title">Staff</div>
      <div className="staff-view-search-add display-flex-sb">
        <div>
          <Paper
            component="form"
            sx={{
              p: "2px 4px",
              display: "flex",
              alignItems: "center",
              width: 400,
              height: "34px",
            }}
            className="staff-view-search"
          >
            <IconButton type="button" sx={{ p: "10px" }} aria-label="search">
              <SearchIcon />
            </IconButton>
            <TextField
              sx={{ ml: 1, flex: 1 }}
              placeholder="Search By staff name"
              inputProps={{ "aria-label": "search google maps" }}
            />
          </Paper>
        </div>
        <div>
          <Button
            variant="contained"
            onClick={() => {
              handleAddStaff();
            }}
          >
            <AddCircleOutlineIcon className="shop-body-add-icon" />
            Add Staff
          </Button>
        </div>
      </div>
      <div className="staff-view-table">
        {isLoading ? (
          <Box display="flex" justifyContent="center" alignItems="center" height="200px">
            <CircularProgress />
          </Box>
        ) : (
          <Box sx={{ height: 600, width: "100%" }}>
            {!isLoading && !isError && staffData?.length > 0 ? (
              <DataGrid
                rows={staffData?.data || []}
                columns={columns}
                rowCount={staffData?.totalCount}
                pageSize={currentPageSize}
                page={currentPage - 1}
                onPageSizeChange={(newPageSize) => {
                  setCurrentPageSize(newPageSize);
                  setCurrentPage(1);
                }}
                onPageChange={(newPage) => {
                  setCurrentPage(newPage + 1);
                }}
                rowsPerPageOptions={[5, 10, 15]}
                paginationMode="server"
                onCellClick={handleOnCellClick}
                loading={isSuccess ? false : true}
              />
            ) : (
              <NoDataFound />
              // <KYCPending />
            )}
          </Box>
        )}
      </div>
    </div>
  );
};
