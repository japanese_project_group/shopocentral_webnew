export interface IPermission {
  financeManagement: boolean;
  supportManagement: boolean;
  userManagement: boolean;
  ticketManagement: boolean;
  ruleManagement: boolean;
  settingManagement: boolean;
}

export interface ILoginReq {
  email: string;
  password: string;
}
export interface IRegisterRes {
  token: string;
}
export interface IRegisterReq {
  firstName_en: string;
  lastName_en: string;
  email: string;
  password: string;
}

export interface IForgotPasswordRes {
  data: {
    otp: {
      code: string;
      userId: string;
      _id: string;
    };
  };
  msg: string;
  success: boolean;
}

export interface IForgotPasswordReq {
  userName: string;
  type: string;
}

export interface IResendOtpRes {
  token: string;
}

export interface IResendOtpReq {
  email: string;
}
export interface IVerifyOtpRes {
  token: string;
}

export interface IResetPasswordRes {
  token: string;
}

export interface IResetPasswordReq {
  password: string;
  passwordConfirmation: string;
  otpId: any;
  userId: any;
}
export interface ISendKycForm {
  //   firstName_en *
  // string
  // middleName_en
  // string
  // lastName_en *
  // string
  // firstName_jpn *
  // string
  // middleName_jpn *
  // string
  // lastName_jpn *
  // string
  // firstName_np *
  // string
  // middleName_np *
  // string
  // lastName_np *
  // string
  // phone *
  // number
  // occupation *
  // string
  // dob *
  // string($date-time)
  // city_en
  // string
  // city_jpn
  // string
  // city_np
  // string
  // country_en
  // string
  // country_np
  // string
  // country_jpn
  // string
  // postalcode_en
  // string
  // postalcode_jpn
  // string
  // postalcode_np
  // string
  // prefecture_en
  // string
  // prefecture_jpn
  // string
  // prefecture_np
  // string
  // district_en *
  // string
  // district_jpn *
  // string
  // district_np *
  // string
  // town_en *
  // string
  // town_jpn *
  // string
  // town_np *
  // string
  // state_en *
  // string
  // state_np *
  // string
  // state_jpn *
  // string
  // houseNo *
  // string
  // issueForm *
  // string
  // issueDate *
  // string($date-time)
  // should be use for the activities date - ISO format
  // validDate *
  // string($date-time)
  // frontImage *
  // string
  // should be image of front citizenship
  // backImage *
  // string
}
export interface ImageItemTypes {
  alt: "string";
}
