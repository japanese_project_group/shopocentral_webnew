export interface IUserFormRes {
  token: string;
}
export interface userData {
  createdAt: string;
  email: string;
  familyName: string | null;
  fcmToken: string | null;
  firstName: string;
  id: string;
  lastName: string;
  middleName: string | null;
  name: string;
  occupation: string | null;
  password: string;
  passwordResetRequested: boolean;
  status: string;
  updatedAt: string;
  verified: boolean;
}
export interface profileData {
  data: string;
}
export interface IGetAllData {
  data: string;
}
export interface HeadCell {
  key: string;
  label: any;
}
export interface PersonalKYCData {
  postalcode_en: string;
  firstName: string;
  middleName: string;
  lastName: string;
  occupation: string;
  alternateName: string;
  country_en: string;
  prefecture_en: string;
  city_en: string;
  town_en: string;
  houseNo: number;
  documentType: string;
  idNumber: number;
  issueDate: string;
  validDate: string;
  issueFrom: string;
  issuedBy: string;
  frontImage: any;
  backImage: any;
  userId: string | null;
  email: string | null;
}


export interface BusinessAccountRegistrationData {

}

export interface propType {
  values: string,
  touched: boolean,
}
export interface propType {
  values: string;
  touched: boolean;
  handleChange: () => null;
  handleBlur: () => null;
  handleSubmit: () => null;
  errors: boolean;
}
