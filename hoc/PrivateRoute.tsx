import { useGetAllDataByIdQuery } from "@/redux/api/user/user";
import ConnectionLost from "@/src/common/ConnectionLost";
import ServerDown from "@/src/common/ServerDown";
import { businessAccessId } from "@/src/common/utility/Constants";
import { Box } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";

const withAuthProtected = (Component: any) => {
  const Auth = (props: any) => {
    const router = useRouter();

    const token = useSelector((state: RootState) => state?.auth?.token);
    const indexPage = "/login";
    const { data: allDataProfile, isSuccess: isSuccessGetProfile, isError, refetch, isLoading, status } =
      useGetAllDataByIdQuery(businessAccessId);
    const profileStatus = allDataProfile?.data?.identityStatus?.[0]?.status;
    // const { data: allProfileData, isSuccess, isError: error } = useGetAllDataQuery();
    const [isOnline, setIsOnline] = useState(true);
    const [loading, setLoading] = useState(true);

    console.log("profileStatus", profileStatus, isError, status);

    useEffect(() => {
      const updateOnlineStatus = () => {
        setIsOnline(navigator.onLine);
      };

      window.addEventListener("online", updateOnlineStatus);
      window.addEventListener("offline", updateOnlineStatus);

      // Check and set the initial online status
      updateOnlineStatus();

      return () => {
        window.removeEventListener("online", updateOnlineStatus);
        window.removeEventListener("offline", updateOnlineStatus);
      };
    }, []);

    useEffect(() => {
      const callback = () => {
        if (token) {
          setLoading(false);
        } else {
          return router.replace(indexPage);
        }

        if (isOnline) {
          setLoading(false);
        }
      };
      callback();
    }, [router, token, isOnline]);

    // useEffect(() => {
    //   if (isError && status === 'rejected') {
    //     localStorage.clear();
    //     router.replace(indexPage);
    //   }
    // }, [isError, profileStatus, router, indexPage]);

    if (loading) {
      return <div />;
    }

    if (!isOnline) {
      return (
        <Box height={'100vh'}>
          <ConnectionLost />
        </Box>
      );
    }

    if (isError && profileStatus === 'VERIFIED') {
      return (
        <Box height={'100vh'}>
          <ServerDown />
        </Box>
      );
    }

    return <Component {...props} />;
  };

  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default withAuthProtected;
