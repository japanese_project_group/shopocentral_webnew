// build in library
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// third party library
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";

const withAuth = (Component: any) => {
  const indexPage = "/dashboard";
  const Auth = (props: any) => {
    const router = useRouter();

    const { userDetail } = useSelector((state: RootState) => state?.auth);

    const [loading, setLoading] = useState(true);

    useEffect(() => {
      const callback = () => {
        const user = localStorage.getItem("token");
        if (!user || !userDetail) {
          setLoading(false);
        } else {
          return router.replace(indexPage);
        }
      };
      callback();
    }, []);

    useEffect(() => { }, []);
    if (loading) {
      return <div />;
    }

    return <Component {...props} />;
  };

  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default withAuth;
