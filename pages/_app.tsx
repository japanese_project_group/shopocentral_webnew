// Build in library
import Router from "next/router";

// MUI Components
import { ThemeProvider } from "@mui/material";

// Third party library
import ProgressBar from "@badrap/bar-of-progress";
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";

// redux
import { store } from "../redux/store";

// styles
import "react-toastify/dist/ReactToastify.css";
import "../styles/customs.scss";
import "../styles/globals.scss";
import theme from "../styles/theme/defaultTheme";
import { AppWrapper } from "src/layout/appWrapper";

const progess = new ProgressBar({
  size: 4,
  color: "white",
  className: "progessBar",
  delay: 100,
});

Router.events.on("routeChangeStart", progess.start);
Router.events.on("routeChangeComplete", progess.finish);
Router.events.on("routeChangeError", progess.finish);

let persistor = persistStore(store);
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppWrapper>
            <Component {...pageProps}> </Component>
          </AppWrapper>
          <ToastContainer
            position="top-right"
            autoClose={3000}
            hideProgressBar={false}
            newestOnTop={false}
            draggable={false}
            closeOnClick
            pauseOnHover
          />
        </PersistGate>
      </Provider>
    </ThemeProvider>
  );
}

export default MyApp;
