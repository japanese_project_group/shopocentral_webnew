// build in library
import { ReactNode } from "react";

// components
import VerifyOtpComponent from "../src/components/auth/verifyotp/registration/index";
import AuthLayout from "../src/layout/authlayout/index";

const VerifyOtp = () => {
  return <VerifyOtpComponent />;
};
VerifyOtp.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default VerifyOtp;


