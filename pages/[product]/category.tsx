// components
import { AddCategory } from "../../src/components/product/masterProduct/category";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Manufacture = () => {
    return <MainNav mainPage={<AddCategory />} />
}
export default Manufacture;