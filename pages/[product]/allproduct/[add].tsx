// components
import { AddProduct } from "../../../src/components/product/masterProduct/AllProduct/addProduct";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const addProduct = () => {
    return <MainNav mainPage={<AddProduct />} />
}

export default addProduct