// components
import { AllProduct } from "../../../src/components/product/masterProduct/AllProduct";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const allProduct = () => {
    return <MainNav mainPage={<AllProduct />} />
}
export default allProduct;