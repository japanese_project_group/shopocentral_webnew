// components
import FeaturedProduct from "@/src/components/product/featuredProduct";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const featuredProduct = () => {
    return <MainNav mainPage={<FeaturedProduct />} />
}
export default featuredProduct;