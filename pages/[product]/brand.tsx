
// components
import { AddBrand } from "../../src/components/product/masterProduct/brand";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Brand = () => {
    return <MainNav mainPage={<AddBrand />} />
}
export default Brand;