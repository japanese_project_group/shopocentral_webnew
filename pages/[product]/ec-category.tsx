// components
import { AddEcCategory } from "../../src/components/product/masterProduct/ecCategory";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Manufacture = () => {
    return <MainNav mainPage={<AddEcCategory />} />
}
export default Manufacture;