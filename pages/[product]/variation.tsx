// components
import { AddVariation } from "../../src/components/product/masterProduct/variation";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Manufacture = () => {
    return <MainNav mainPage={<AddVariation />} />
}
export default Manufacture;