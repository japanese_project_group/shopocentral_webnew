// components
import { AddManufacture } from "../../src/components/product/masterProduct/manufacture";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Manufacture = () => {
    return <MainNav mainPage={<AddManufacture />} />
}
export default Manufacture;