// components
import VerifyCustomerOtp from "../../../src/components/auth/register/verifyCustomerOtp/index";

const VerifyCustomerOtpPage = () => {
    return <VerifyCustomerOtp />;
};

export default VerifyCustomerOtpPage;
