// Build in library
import { ReactNode } from "react";
// components
import AdminOrCeo from "../../../src/components/auth/adminorceo/index";
import AuthLayout from "../../../src/layout/authlayout/index";

const AdminOrCeoComponent = () => {
    return <AdminOrCeo />;
};
AdminOrCeoComponent.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default AdminOrCeoComponent;
