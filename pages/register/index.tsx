// build in library
import { ReactNode } from "react";

// components
import RegisterPage from "../../src/components/auth/register/index";
import AuthLayout from "../../src/layout/authlayout/index";

const Register = () => {
  return <RegisterPage />;
};
Register.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default Register;
