// Build in library
import { ReactNode } from "react";
// // components
import BusinessAccount from "../../../src/components/auth/businessaccount/index";
import AuthLayout from "../../../src/layout/authlayout/index";
const Register = () => {
    return <BusinessAccount />;
};
Register.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default Register;
