// components
// import { NotificationComponent } from '../../../src/components/dashboard/notifications';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';
import { NotificationComponent } from '../../../src/layout/navbarcomponents/notificationList';

const Notifications = () => {
    // const { notificationData } = useSelector((state: RootState) => state.notifications);
    return <MainNav mainPage={<NotificationComponent />} />;
}
export default Notifications;