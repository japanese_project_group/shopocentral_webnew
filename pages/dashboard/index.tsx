// components
import DashBoardComponent from '../../src/components/dashboard/index';
import MainNav from '../../src/layout/dashboardLayout/dashboardLayout';

const index = () => {
  return <MainNav mainPage={<DashBoardComponent />} />;
};

export default index;
