// components
import BusinesskycCompletePage from "../src/constant/pages/BusinesskycCompletePage";
import MainNav from "../src/layout/dashboardLayout/dashboardLayout";

const businesskyccomplete = () => {
  return (
    <MainNav mainPage={BusinesskycCompletePage}></MainNav>
  );
};

export default businesskyccomplete;
