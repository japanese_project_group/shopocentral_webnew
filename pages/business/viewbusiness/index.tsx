// components
import ViewIndividualBusiness from '../../../src/components/business/viewbusiness/';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const ShopComponentPage = () => {
    return <MainNav mainPage={<ViewIndividualBusiness />} />;
};

export default ShopComponentPage;
