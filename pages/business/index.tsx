// components
import ViewBusiness from '../../src/components/business/index';
import MainNav from '../../src/layout/dashboardLayout/dashboardLayout';

const ShopComponentPage = () => {
    return <MainNav mainPage={<ViewBusiness />} />;
};

export default ShopComponentPage;
