// components
import AddBusiness from '../../../src/components/business/addbusiness/';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const ShopComponentPage = () => {
    return <MainNav mainPage={<AddBusiness />} />;
};

export default ShopComponentPage;
