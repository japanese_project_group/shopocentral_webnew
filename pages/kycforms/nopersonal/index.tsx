// components
import NoPersonalKYC from "../../../src/components/kycForms/nopersonalkyc/index";

const NoPersonalKycPage = () => {
    return (
        <>
            <NoPersonalKYC />
        </>
    );
};

export default NoPersonalKycPage;
