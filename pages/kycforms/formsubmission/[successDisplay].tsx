// components
import PersonalFormSuccess from "../../../src/components/kycForms/success/personal";

const PersonalFormSuccessPage = () => {
    return <PersonalFormSuccess />;
};

export default PersonalFormSuccessPage;
