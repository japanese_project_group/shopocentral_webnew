// components
import BusinessFormSuccess from "../../../src/components/kycForms/success/business";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const BusinessFormSuccessPage = () => {
    return <MainNav mainPage={<BusinessFormSuccess />} />;
};

export default BusinessFormSuccessPage;
