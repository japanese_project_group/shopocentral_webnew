// components
import BusinessKycFormCombined from "../../../src/components/kycForms/businessKyc/index";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const BusinessKyc = () => {
  return <MainNav mainPage={<BusinessKycFormCombined />} />;
};

export default BusinessKyc;
