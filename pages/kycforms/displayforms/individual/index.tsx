// components
import DisplayIndiviudalBusinessKyc from '../../../../src/components/kycForms/displayForms/displayIndividualBusinessKyc';
import MainNav from '../../../../src/layout/dashboardLayout/dashboardLayout';

const DisplayIndiviudalBusinessKycPage = () => {
    return <MainNav mainPage={<DisplayIndiviudalBusinessKyc />} />;
};

export default DisplayIndiviudalBusinessKycPage;
