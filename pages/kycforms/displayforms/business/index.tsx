// components
import DisplayBusiness from "../../../../src/components/kycForms/displayForms/displayBusiness";
import MainNav from '../../../../src/layout/dashboardLayout/dashboardLayout';

const index = () => {
    return <MainNav mainPage={<DisplayBusiness />} />;
};

export default index;
