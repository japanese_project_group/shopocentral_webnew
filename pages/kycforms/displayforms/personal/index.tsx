// components
import DisplayPersonal from "../../../../src/components/kycForms/displayForms/displayPersonal";
import MainNav from '../../../../src/layout/dashboardLayout/dashboardLayout';

const index = () => {
    return <MainNav mainPage={<DisplayPersonal />} />;
};

export default index;
