// components
import FormS from "../src/components/kycForms/personalKyc/Form";
import MainNav from "../src/layout/dashboardLayout/dashboardLayout";

const FormPage = () => {
  return <MainNav mainPage={<FormS />} />;
};

export default FormPage;
