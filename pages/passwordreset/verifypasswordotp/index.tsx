// components
import VerifyPasswordOtp from "../../../src/components/auth/forgotpassword/verifypasswordotp/index";

const VerifyPasswordOtpPage = () => {
  return <VerifyPasswordOtp />;
};

export default VerifyPasswordOtpPage;
