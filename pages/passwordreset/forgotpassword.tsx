// components
import ForgotPassword from "../../src/components/auth/forgotpassword/index";

const ForgotPasswordPage = () => {
  return <ForgotPassword />;
};

export default ForgotPasswordPage;
