// components
import { ShopRadiusComponent } from "../../src/components/shopradius";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const ShopRadiusPage = () => {
    return <MainNav mainPage={<ShopRadiusComponent />} />;
};
export default ShopRadiusPage;
