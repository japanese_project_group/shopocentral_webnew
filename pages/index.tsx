// components
import type { NextPage } from "next";
import LoginPage from "../src/components/auth/login/index";

const Home: NextPage = () => {
  return <LoginPage />;
};

export default Home;
