// components
import { AddSubscription } from "../../../src/components/subscription/addsubscription";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const addSubscription = () => {
  return <MainNav mainPage={<AddSubscription />} />;
};

export default addSubscription;
