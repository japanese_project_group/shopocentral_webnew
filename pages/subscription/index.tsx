// component
import SubscriptionComponent from "../../src/components/subscription";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const Subscription = () => {
  return <MainNav mainPage={<SubscriptionComponent />} />;
};
export default Subscription;
