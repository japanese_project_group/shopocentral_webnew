// components
import { ViewSubscription } from "../../../src/components/subscription/view";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const SubscriptionView = () => {
  return <MainNav mainPage={<ViewSubscription />} />;
};

export default SubscriptionView;
