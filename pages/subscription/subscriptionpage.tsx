// components
import SubscriptionPage from "../../src/components/subscription/addsubscription/subscriptionpage";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const subscriptionPage = () => {
  return <MainNav mainPage={<SubscriptionPage />} />;
};

export default subscriptionPage;
