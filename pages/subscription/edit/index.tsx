// components
import { EditSubscription } from "../../../src/components/subscription/editsubscription";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const SubscriptionEdit = () => {
  return <MainNav mainPage={<EditSubscription />} />;
};

export default SubscriptionEdit;
