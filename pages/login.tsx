// build in library
import { ReactNode } from "react";

// components
import LoginPage from "../src/components/auth/login/index";
import AuthLayout from "../src/layout/authlayout/index";

const Login = () => {
  return <LoginPage />;
};
Login.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;

export default Login;
