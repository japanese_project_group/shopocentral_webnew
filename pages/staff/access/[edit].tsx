import { AddStaffRole } from "../../../src/components/staff/access/AddStaffAccess";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";
const StaffComponent = () => {
  return <MainNav mainPage={<AddStaffRole />} />;
};

export default StaffComponent;
