// components
import { SearchFormStaff } from "../../../src/components/staff/searchFormStaff";
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const StaffComponent = () => {
    return <MainNav mainPage={<SearchFormStaff />} />;
};

export default StaffComponent;
