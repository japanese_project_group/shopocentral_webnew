// components
import { StaffMain } from "../../src/components/staff";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const StaffPage = () => {
    return <MainNav mainPage={<StaffMain />} />
}
export default StaffPage;