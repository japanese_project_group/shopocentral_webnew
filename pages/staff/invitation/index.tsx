// components
import { StaffInvitationForm } from "../../../src/components/staff/staffInvitationForm";

const StaffComponent = () => {
    return <StaffInvitationForm />;
};

export default StaffComponent;
