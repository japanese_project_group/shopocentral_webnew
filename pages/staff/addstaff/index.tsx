// components
import Staff from "../../../src/components/staff/addstaff";
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const StaffComponent = () => {
    return <MainNav mainPage={<Staff />} />;
};

export default StaffComponent;
