// Build in library
import { ReactNode } from "react";
// components
import ChangeEmailComponent from "../src/components/auth/changeEmail/index";
import AuthLayout from "../src/layout/authlayout/index";

const ChangeEmail = () => {
    return <ChangeEmailComponent />;
};
ChangeEmail.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default ChangeEmail;
