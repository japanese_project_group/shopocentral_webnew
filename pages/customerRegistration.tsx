// Build in library
import Link from "next/link";
import Router from "next/router";
import React, { useEffect, useState } from "react";

//MUI Components
import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Box,
  Button,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
  SelectChangeEvent,
  Stack,
  TextField,
  Typography
} from "@mui/material";

// third party library
import { Form, FormikProvider, useFormik } from "formik";
import _ from "lodash";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
// Redux
import { useCustomeresgistrationMutation } from "../redux/api/auth/auth";
// validation
import { registerCustomerValidation } from "../src/common/validationSchema";
// style
import { getEncryptedData } from "../src/common/utility";
import styles from "../src/components/Forms.module.css";

const CustomerRegistration = () => {
  const [login, { isLoading, isSuccess, data, isError, error }] =
    useCustomeresgistrationMutation();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      roleName: "USER",
      phoneType: "+81",
      phonenumber: "",
      fcmToken: "asdasdasd",
      password_confirmation: "",
    },
    validationSchema: registerCustomerValidation,
    onSubmit: async (values) => {
      localStorage.setItem("password", getEncryptedData(values.password));
      await login(values).unwrap()
        .catch((error: any) => {
          const ermsg = error?.data?.message;
          toast.error(ermsg);
        });
    },
  });

  const { values, touched, errors, getFieldProps, setFieldValue, handleChange, handleBlur, handleSubmit } =
    formik;

  useEffect(() => {
    if (isSuccess && data) {
      localStorage.setItem("email", data.data.email);
      localStorage.setItem("userId", data.data.id);
      localStorage.setItem("phoneNumber", data.data.phoneNumber?.phoneNumber)
      toast("Customer Account Created", {
        autoClose: 2500,
        type: "success",
        pauseOnHover: true,
      });
      Router.push("/verifyotp");

    }

  }, [data, isSuccess]);

  const [countryCode, setCountryCode] = React.useState("+977");

  const handleChangeCountryCode = (event: SelectChangeEvent) => {
    setCountryCode(event.target.value as string);
  };

  const takeNumber = (e: any) => {
    ['ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-+<>@#$%^&*!:"|±~?/+`"`,;()[]_ {/\\/}=+'].toString().includes(e.key) && e.preventDefault();
  }

  const nameValid = (e: any) => {
    _.range(10).toString().includes(e.key) && e.preventDefault();
  }
  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "100vh",
          width: "100vw",
        }}
        className={styles.customerRegister}
      >
        <Box
          className={styles.glassMorphism}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            margin: "25px",
            padding: "25px",
          }}
        >
          <Typography variant='h3' color='primary' component='div'>
            Welcome
          </Typography>
          <p style={{ fontWeight: "1000", fontSize: "1rem" }}>Sign up to create Shopo User Account</p>

          <Box
            sx={{
              mt: 1,
              width: { sm: "450px", xs: "100%" },
            }}
          >
            <FormikProvider value={formik}>
              <Form>
                <Stack spacing={2}>
                  <Box>
                    Full Name:
                    <Typography
                      component='span'
                      color='error'
                      sx={{ marginLeft: "5px" }}
                    >
                      *
                    </Typography>
                    <TextField
                      fullWidth
                      variant='outlined'
                      disabled={isLoading}
                      {...getFieldProps('name')}
                      placeholder={"Please enter your Name"}
                      error={Boolean(touched.name && errors.name)}
                      helperText={
                        Boolean(touched.name && errors.name) &&
                        String(touched.name && errors.name)
                      }
                      onKeyDown={nameValid}
                    />
                  </Box>
                  <Stack direction='column'>
                    <Typography>
                      Mobile Number:
                      <Typography
                        component='span'
                        color='error'
                        sx={{ marginLeft: "5px" }}
                      >
                        *
                      </Typography>
                    </Typography>
                    <Stack direction='row' spacing={1}>

                      <TextField sx={{ width: "30%" }} value={"+81"} disabled>
                      </TextField>
                      <TextField
                        sx={{ width: "70%" }}
                        variant='outlined'
                        {...getFieldProps('phonenumber')}
                        disabled={isLoading}
                        inputProps={{ maxLength: 11 }}
                        onKeyDown={takeNumber}
                        placeholder={"Please enter your mobile number"}
                        error={Boolean(
                          touched.phonenumber && errors.phonenumber
                        )}
                        helperText={
                          Boolean(touched.phonenumber && errors.phonenumber) &&
                          String(touched.phonenumber && errors.phonenumber)
                        }

                      />
                    </Stack>
                  </Stack>
                  <Box>
                    Email:
                    <Typography
                      component='span'
                      color='error'
                      sx={{ marginLeft: "5px" }}
                    >
                      *
                    </Typography>
                    <TextField
                      fullWidth
                      variant='outlined'
                      disabled={isLoading}
                      {...getFieldProps('email')}
                      placeholder={"Please enter your email"}
                      error={Boolean(touched.email && errors.email)}
                      helperText={
                        Boolean(touched.email && errors.email) &&
                        String(touched.email && errors.email)
                      }

                    />
                  </Box>
                  <Box>
                    Password :
                    <Typography
                      component='span'
                      color='error'
                      sx={{ marginLeft: "5px" }}
                    >
                      *
                    </Typography>
                    <OutlinedInput
                      id='outlined-adornment-password'
                      fullWidth
                      type={showPassword ? "text" : "password"}
                      disabled={isLoading}
                      {...getFieldProps('password')}
                      placeholder={"Please enter your password"}
                      onBlur={handleBlur}
                      error={touched.password && Boolean(errors.password)}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            aria-label='toggle password visibility'
                            onClick={() =>
                              setShowPassword((prev: boolean) => !prev)
                            }
                            edge='end'
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }


                    />
                    {touched.password && errors.password && (
                      <FormHelperText error id='accountId-error'>
                        {errors.password}
                      </FormHelperText>
                    )}

                  </Box>
                  <Box>
                    Confirm Password :
                    <Typography
                      component='span'
                      color='error'
                      sx={{ marginLeft: "5px" }}
                    >
                      *
                    </Typography>
                    <OutlinedInput
                      id='outlined-adornment-password'
                      fullWidth
                      {...getFieldProps('password_confirmation')}
                      type={showConfirmPassword ? "text" : "password"}
                      disabled={isLoading}
                      placeholder={"Please Confirm Your Password"}
                      onBlur={handleBlur}
                      error={
                        touched.password_confirmation &&
                        Boolean(errors.password_confirmation)
                      }
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            aria-label='toggle password visibility'
                            onClick={() =>
                              setShowConfirmPassword(!showConfirmPassword)
                            }
                            edge='end'
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {touched.password_confirmation &&
                      errors.password_confirmation && (
                        <FormHelperText error id='accountId-error'>
                          {errors.password_confirmation}
                        </FormHelperText>
                      )}
                  </Box>
                </Stack>

                <Box
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                  style={{ margin: "2rem 0 1rem 0" }}
                >
                  <Button
                    type='submit'
                    fullWidth
                    size='large'
                    variant='contained'
                    color='primary'
                  >
                    Register
                  </Button>
                </Box>
                <Stack
                  justifyContent={"center"}
                  direction='column'
                  alignItems={"center"}
                >
                  <Typography>Already have an account?</Typography>
                  <Link href='/register/' passHref>
                    <Typography>
                      Click Here to{" "}
                      <span
                        style={{
                          color: "blue",
                          cursor: "pointer",
                        }}
                      >
                        {" "}
                        Login
                      </span>
                    </Typography>
                  </Link>
                </Stack>

                <Typography
                  sx={{ marginTop: "1.5rem", cursor: "pointer" }}
                  component='div'
                  align='center'
                >
                  © 2023 Shopo Central. All rights reserved
                </Typography>
              </Form>
            </FormikProvider>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default CustomerRegistration;
