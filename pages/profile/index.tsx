
// components
import UserProfile from '../../src/components/profile';
import MainNav from '../../src/layout/dashboardLayout/dashboardLayout';

const index = () => {
    return <MainNav mainPage={<UserProfile />} />;
};


export default index;