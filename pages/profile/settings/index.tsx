// components
import { SettingPage } from '../../../src/components/profile/settings';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const VerifyBranchesPageComponent = () => {
    return <MainNav mainPage={<SettingPage />} />;
};

export default VerifyBranchesPageComponent;
