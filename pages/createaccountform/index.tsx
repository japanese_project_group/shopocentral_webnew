// build in library
import { ReactNode } from "react";
// components
import CreateAccountForm from "../../src/components/auth/createaccount/index";
import AuthLayout from "../../src/layout/authlayout/index";

const Register = () => {
    return <CreateAccountForm />;
};
Register.getLayout = (page: ReactNode) => <AuthLayout>{page}</AuthLayout>;
export default Register;
