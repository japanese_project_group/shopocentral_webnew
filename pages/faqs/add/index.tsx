// components
import { AddFaqsComponent } from "../../../src/components/faqs/add";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const AddFaqsPage = () => {
  return <MainNav mainPage={<AddFaqsComponent />} />;
};

export default AddFaqsPage;
