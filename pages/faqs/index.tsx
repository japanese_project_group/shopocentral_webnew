// components
import FAQSComponent from "../../src/components/faqs";
import MainNav from "../../src/layout/dashboardLayout/dashboardLayout";

const faqsPage = () => {
  return <MainNav mainPage={<FAQSComponent />} />;
};
export default faqsPage;
