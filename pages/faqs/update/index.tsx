// components
import { UpdateFaqs } from "../../../src/components/faqs/update";
import MainNav from "../../../src/layout/dashboardLayout/dashboardLayout";

const UpdateFaqsPage = () => {
  return <MainNav mainPage={<UpdateFaqs />} />;
};

export default UpdateFaqsPage;
