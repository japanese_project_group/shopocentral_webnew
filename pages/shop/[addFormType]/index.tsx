// components
import ShopDetailsPage from '../../../src/components/shop/addshopdetails/index';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const ShopDetails = () => {
  return <MainNav mainPage={<ShopDetailsPage />} />;
};

export default ShopDetails;
