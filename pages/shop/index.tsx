// components
import ShopComponent from '../../src/components/shop/index';
import MainNav from '../../src/layout/dashboardLayout/dashboardLayout';

const ShopComponentPage = () => {
  return <MainNav mainPage={<ShopComponent />} />;
};

export default ShopComponentPage;
