// components
import { ViewDetails } from '../../../src/components/shop/details/index';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const ShopDetails = () => {
    return <MainNav mainPage={<ViewDetails />} />;
};

export default ShopDetails;
