// components
import { AllOrder } from '@/src/components/shop/order';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const OrderPage = () => {
    return <MainNav mainPage={<AllOrder />} />;
};

export default OrderPage;
