// components
import EditOrder from '@/src/components/shop/order/Edit';
import MainNav from '../../../src/layout/dashboardLayout/dashboardLayout';

const EditOrderPage = () => {
    return <MainNav mainPage={<EditOrder />} />;
};

export default EditOrderPage;
