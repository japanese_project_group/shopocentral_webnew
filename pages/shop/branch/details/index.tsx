// components
import { BranchDetailsPage } from "../../../../src/components/shop/branchdetails";
import MainNav from "../../../../src/layout/dashboardLayout/dashboardLayout";

const BranchDetails = () => {
  return <MainNav mainPage={<BranchDetailsPage />} />;
};
export default BranchDetails;
