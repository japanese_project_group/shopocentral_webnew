// third party library
import { createApi } from "@reduxjs/toolkit/query/react";

// Redux
import { baseQueryWithReauth } from "../../index";

export const userApi: any = createApi({
  reducerPath: "user",
  baseQuery: baseQueryWithReauth,
  tagTypes: ["User"],
  endpoints: (builder) => ({
    GetAllData: builder.query<any, void>({
      query: () => ({
        url: "user/profile",
      }),
    }),
    GetAllDataById: builder.query<any, any>({
      query: (businessId: any) => ({
        url: `user/profile/${businessId}`,
      }),
    }),
    // ------------------------------- personal kyc  ---------------------------------//
    GetPersonalKycVerifyReqData: builder.query<any, void>({
      query: () => ({
        url: "admin/all-personal-kyc",
      }),
    }),
    GetPersonalKycIndividualData: builder.query<any, any>({
      query: (userID: any) => ({
        url: `/personal-kyc/${userID}`,
      }),
    }),
    PostPersonalKycStatus: builder.mutation<any, any>({
      query: ({ userID, status }) => ({
        url: `admin/status/${userID}`,
        method: "PATCH",
        body: status,
      }),
    }),
    PostPersonalKycRequest: builder.mutation<any, any>({
      query: ({ data }) => ({
        url: `user/personal-kyc/`,
        method: "POST",
        body: data,
      }),
    }),
    // ------------------------------- personal kyc  ---------------------------------//
    // ------------------------------- Business kyc  ---------------------------------//
    PostBusinessKycRequest: builder.mutation<any, any>({
      query: ({ data, id }: any) => ({
        url: `merchant/business-kyc/${id}`,
        method: "PATCH",
        body: data,
      }),
    }),
    GetBusinessKycVerifyReqData: builder.query<any, void>({
      query: () => ({
        url: "businesskyc",
      }),
    }),
    GetBusinessKycIndividualData: builder.query<any, any>({
      query: (userID: any) => ({
        url: `admin/businesskyc/${userID}`,
        method: "GET",
      }),
    }),
    PostBusinessKycStatus: builder.mutation<any, any>({
      query: ({ userID, status }) => ({
        url: `admin/business-kyc-status/${userID}`,
        method: "PATCH",
        body: status,
      }),
    }),

    // ------------------------------- Business kyc  ---------------------------------//
    GetShopData: builder.query<any, void>({
      query: () => ({
        url: `shop`,
      }),
    }),

    GetIndividualShopData: builder.query<any, void>({
      query: (data) => ({
        url: `shop/${data}`,
      }),
    }),

    GetBranchData: builder.query<any, void>({
      query: (id) => ({
        url: `shop/branch/${id}`,
      }),
    }),

    PostShopDetails: builder.mutation<any, any>({
      query: ({ data }) => ({
        url: `shop`,
        method: "POST",
        body: data,
      }),
    }),

    UpdateShopDetails: builder.mutation<any, any>({
      query: ([data, userID]) => ({
        url: `shop/${userID}`,
        method: "PATCH",
        body: data,
      }),
    }),
    AddShopDetails: builder.mutation<any, any>({
      query: (data) => ({
        url: `shop`,
        method: "PATCH",
        body: data,
      }),
    }),
    PostBranchDetails: builder.mutation<any, any>({
      query: ({ data }) => ({
        url: `shop/branch`,
        method: "POST",
        body: data,
      }),
    }),
    PostCheckForStaff: builder.mutation<any, any>({
      query: (data) => ({
        url: `staff/check-email`,
        method: "POST",
        body: data,
      }),
    }),
    PostRoleForStaff: builder.mutation<any, any>({
      query: (data) => ({
        url: `merchant/assign-staff`,
        method: "POST",
        body: data,
      }),
    }),
    PatchRoleForStaff: builder.mutation<any, any>({
      query: (data) => ({
        url: `merchant/assign-staff`,
        method: "PATCH",
        body: data,
      }),
    }),
    GetRoleForStaff: builder.query<any, void>({
      query: () => ({
        url: `merchant/role`,
      }),
    }),
    GetStaffData: builder.query<any, void>({
      query: (businessId) => ({
        url: `merchant/staffs/${businessId}`,
      }),
    }),

    GetIndividualBranchData: builder.query<any, void>({
      query: (id) => ({
        url: `shop/trex-branch-with/${id}`,
      }),
    }),

    CreateAccount: builder.mutation<any, any>({
      query: ({ data }) => ({
        url: `user/profile-detail`,
        method: "POST",
        body: data,
      }),
    }),
    GetDataFromPostalCode: builder.query<any, void>({
      query: (postalcode) => ({
        url: `address/csv?name_en=${postalcode}`,
      }),
    }),
    UnverifiedDataUpdate: builder.mutation<any, any>({
      query: ({ userId, data }) => ({
        url: `admin/unverified-personal-kyc/${userId}`,
        method: "PATCH",
        body: data,
      }),
    }),
    GetPermissionData: builder.query<any, void>({
      query: () => ({
        url: "/permission/type",
      }),
    }),
    GetCategory: builder.query<any, void>({
      query: () => ({
        url: `/category`,
        method: "GET",
      }),
    }),
    GetSubCategory: builder.query<any, void>({
      query: () => ({
        url: `/category/sub-category`,
        method: "GET",
      }),
    }),
    GetStaffStatusInvitation: builder.query<any, any>({
      query: (data) => ({
        url: `user/check-staff-invitation?businessKycId=${data?.businessKycId}&statusId=${data?.statusId}&userId=${data?.userId}`,
        method: "GET",
      }),
    }),

    GetStaffList: builder.query<any, any>({
      query: ({ businessId, page, pageSize }) => ({
        url: `/merchant/staffs/list/${businessId}?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),
    PatchStaffInvitationAction: builder.mutation<any, any>({
      query: (data) => ({
        url: `user/staff-action`,
        method: "PATCH",
        body: data,
      }),
    }),
    // AddStaffInvite: builder.mutation<any, any>({
    //   query: (data) => ({
    //     url: `merchant/invite-staff `,
    //     method: "POST",
    //     body: data,
    //   }),
    // }),

    UpdateProfile: builder.mutation<any, any>({
      query: ({ body }) => ({
        url: `user/change-profile-image`,
        method: "PATCH",
        body: body,
      }),
    }),
    ProfileUpdation: builder.mutation<any, any>({
      query: ({ userId, body }) => ({
        url: `admin/unverified-personal-kyc/${userId}`,
        // headers:{'Content-Type': 'multipart/form-data'},
        method: "PATCH",
        body: body,
      }),
    }),
    UpdateShopDetailsMerchant: builder.mutation<any, any>({
      query: ({ data, id }) => ({
        url: `merchant/shop/${id}`,
        method: "PATCH",
        body: data,
      }),
    }),
    GetStaffProfileData: builder.query<any, any>({
      query: ({ staffId, businessKycId }) => ({
        url: `merchant/staff/${staffId}/${businessKycId}`,
        method: "GET",
      }),
    }),
    PostAddStaff: builder.mutation<any, any>({
      query: (body) => ({
        url: "merchant/role",
        body: body,
        method: "POST",
      }),
    }),
    AddStaffInvite: builder.mutation<any, any>({
      query: (data) => ({
        url: `merchant/invite-staff `,
        method: "POST",
        body: data,
      }),
    }),

    PostChangePassword: builder.mutation<any, any>({
      query: (body) => ({
        url: "auth/change-business-password",
        body: body,
        method: "POST",
      }),
    }),

    GetIndividualBranch: builder.query<any, any>({
      query: ({ id }) => ({
        url: `shop/branch/${id}`,
        method: "GET",
      }),
    }),

    ///master product api
    PostAddBrand: builder.mutation<any, any>({
      query: (body) => ({
        url: "/merchant/brand",
        body: body,
        method: "POST",
      }),
    }),

    GetBrandData: builder.query<any, void>({
      query: (id: any) => ({
        url: `/brand/business/${id}`,
        method: "GET",
      }),
    }),

    GetBrandDataLimit: builder.query<any, any>({
      query: ({ id, page, pageSize }) => ({
        url: `brand/list/business/${id}?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),

    PatchUpdateBrand: builder.mutation<any, any>({
      query: ({ body, brandId }) => ({
        url: `/merchant/brand/${brandId}`,
        body: body,
        method: "PATCH",
      }),
    }),

    UpdateIndividualBranch: builder.mutation<any, any>({
      query: ({ id, body }) => ({
        url: `merchant/shop-branch/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),

    UpdateBusinessKyc: builder.mutation<any, any>({
      query: ({ userId, body }) => ({
        url: `admin/businesskyc/${userId}`,
        method: "PATCH",
        body: body,
      }),
    }),

    PostAddManufacture: builder.mutation<any, any>({
      query: (body) => ({
        url: "/merchant/manufacture",
        body: body,
        method: "POST",
      }),
    }),

    GetManufactureData: builder.query<any, void>({
      query: (id: any) => ({
        url: `/manufacture/business/${id}`,
        method: "GET",
      }),
    }),

    GetManufactureDataList: builder.query<any, any>({
      query: ({ id, page, pageSize }) => ({
        url: `/manufacture/list/business/${id}?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),

    PatchUpdateManufacture: builder.mutation<any, any>({
      query: ({ body, manufacturerId }) => ({
        url: `/merchant/manufacture/${manufacturerId}`,
        body: body,
        method: "PATCH",
      }),
    }),

    GetVariationData: builder.query<any, void>({
      query: (id: any) => ({
        url: `/variation/type/business/${id}`,
        method: "GET",
      }),
    }),

    GetVariationDataList: builder.query<any, any>({
      query: ({ id, page, pageSize }) => ({
        url: `/variation/type/business/${id}?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),

    PostAddVariation: builder.mutation<any, any>({
      query: (body) => ({
        url: "/variation/type",
        body: body,
        method: "POST",
      }),
    }),
    PatchUpdateVariation: builder.mutation<any, any>({
      query: ({ body, variationId }) => ({
        url: `/variation/type/${variationId}`,
        body: body,
        method: "PATCH",
      }),
    }),

    PostVariationTypeData: builder.mutation<any, any>({
      query: (body) => ({
        url: "/variation/item",
        body: body,
        method: "POST",
      }),
    }),

    GetVariationItem: builder.query<any, any>({
      query: (id) => ({
        url: `/variation/item/masterproduct/${id}`,
        method: "GET",
      }),
    }),

    PatchUpdateVariationWithProduct: builder.mutation<any, any>({
      query: (body) => ({
        url: `variation/with-product`,
        body: body,
        method: "PATCH",
      }),
    }),

    ///master prioduct category
    GetMasterCategoryData: builder.query<any, void>({
      query: () => ({
        url: `/master-product/category`,
        method: "GET",
      }),
    }),

    GetMasterCategoryDataLimit: builder.query<any, any>({
      query: ({ id, page, pageSize }) => ({
        url: `/master-product/category/list/business/${id}?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),

    PostAddMasterCategory: builder.mutation<any, any>({
      query: (body) => ({
        url: "/master-product/category",
        body: body,
        method: "POST",
      }),
    }),

    PostProductData: builder.mutation<any, any>({
      query: (body) => ({
        url: "/variation/with-product",
        body: body,
        method: "POST",
      }),
    }),

    GetMasterProductData: builder.query<any, any>({
      query: ({ page, pageSize, search }) => ({
        url: `/master-product/my-product?page=${page}&limit=${pageSize}&search=${search}`,
        method: "GET",
      }),
    }),

    GetAllMasterProductData: builder.query<any, any>({
      query: () => ({
        url: `/master-product/my-product`,
        method: "GET",
      }),
    }),

    PostMasterProductData: builder.mutation<any, any>({
      query: (body) => ({
        url: "/master-product",
        body: body,
        method: "POST",
      }),
    }),

    PostMasterProductImage: builder.mutation<any, any>({
      query: ({ body, ID }) => ({
        url: `master-product/${ID}/master-image`,
        body: body,
        method: "POST",
      }),
    }),

    UpdateAddonMasterProduct: builder.mutation<any, any>({
      query: ({ body, ID }) => ({
        url: `master-product/addon/${ID}`,
        body: body,
        method: "PATCH",
      }),
    }),

    UpdateMasterProduct: builder.mutation<any, any>({
      query: ({ body, ID }) => ({
        url: `master-product/update/${ID}`,
        body: body,
        method: "PATCH",
      }),
    }),

    //ec-category
    GetMasterECCategoryData: builder.query<any, void>({
      query: () => ({
        url: `/master-product/ec-category`,
        method: "GET",
      }),
    }),

    GetMasterECCategoryDataList: builder.query<any, any>({
      query: ({ page, pageSize }) => ({
        url: `master-product/ec-category/list?page=${page}&limit=${pageSize}`,
        method: "GET",
      }),
    }),

    GetSingleMasterProduct: builder.query<any, void>({
      query: (id) => ({
        url: `product/master-product/${id}`,
        method: "GET",
      }),
    }),

    PostAddMasterECCategory: builder.mutation<any, any>({
      query: (body) => ({
        url: "/master-product/ec-category",
        body: body,
        method: "POST",
      }),
    }),

    // subscription by Admin
    GetSubscriptionData: builder.query<any, void>({
      query: () => ({
        url: `/admin/subscription`,
        method: "GET",
      }),
    }),
    PostAddSubscriptionData: builder.mutation<any, any>({
      query: (body) => ({
        url: "/admin/subscription",
        body: body,
        method: "POST",
      }),
    }),

    GetIndividualSubscriptionData: builder.query<any, any>({
      query: (id: any) => ({
        url: `/admin/subscription/${id}`,
      }),
    }),

    UpdateSubscriptionData: builder.mutation<any, any>({
      query: ({ body, id }) => ({
        url: `admin/subscription/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),
    //Subscription For Merchant
    GetMerchantSubscriptionData: builder.query<any, void>({
      query: () => ({
        url: `/merchant/subscription`,
        method: "GET",
      }),
    }),
    PostAddMerchantSubscriptionData: builder.mutation<any, any>({
      query: (body) => ({
        url: "/merchant/subscription",
        body: body,
        method: "POST",
      }),
    }),

    GetMerchantIndividualSubscriptionData: builder.query<any, any>({
      query: (id: any) => ({
        url: `/merchant/subscription/${id}`,
      }),
    }),

    UpdateMerchantSubscriptionData: builder.mutation<any, any>({
      query: ({ body, id }) => ({
        url: `merchant/subscription/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),

    // Get Tax category Data
    GetTaxCategoryData: builder.query<any, void>({
      query: () => ({
        url: `/tax-category`,
        method: "GET",
      }),
    }),

    // faqs
    GetFaqsData: builder.query<any, void>({
      query: () => ({
        url: `faq`,
        method: "GET",
      }),
    }),
    PostAddFaqsData: builder.mutation<any, any>({
      query: (body) => ({
        url: "admin/faq",
        body: body,
        method: "POST",
      }),
    }),
    GetIndividualFaqsData: builder.query<any, any>({
      query: (id: any) => ({
        url: `/faq/${id}`,
      }),
    }),
    UpdateFaqsData: builder.mutation<any, any>({
      query: ({ body, id }) => ({
        url: `faq/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),

    GetSellingPriceCategory: builder.query<any, void>({
      query: () => ({
        url: `master-product/selling-price-category`,
        method: "GET",
      }),
    }),

    // review
    PostReview: builder.mutation<any, any>({
      query: (body) => ({
        url: "review",
        body: body,
        method: "POST",
      }),
    }),

    GetReviewData: builder.query<any, void>({
      query: () => ({
        url: `review`,
        method: "GET",
      }),
    }),

    GetProductReviewData: builder.query<any, void>({
      query: (id) => ({
        url: `review/product/${id}`,
        method: "GET",
      }),
    }),

    GetShopReviewData: builder.query<any, void>({
      query: (id) => ({
        url: `review/shop/${id}`,
        method: "GET",
      }),
    }),

    // notifications
    GetAllNotificationData: builder.query<any, void>({
      query: () => ({
        url: `push-notification/view-notification`,
        method: "GET",
      }),
    }),

    UpdateReadNotificationData: builder.mutation<any, any>({
      query: ({ body, id }) => ({
        url: `push-notification/read/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),

    // variation
    PostVariationWithProduct: builder.mutation<any, any>({
      query: (data) => ({
        url: `variation/with-product`,
        method: "POST",
        body: data,
      }),
    }),

    PostVariationImages: builder.mutation<any, any>({
      query: ({ data }) => ({
        url: `variation/image`,
        method: "POST",
        body: data,
      }),
    }),

    GetVariationImages: builder.query<any, any>({
      query: (ID) => ({
        url: `variation/image-name/${ID}`,
        method: "GET",
      }),
    }),

    UpdateVariationImages: builder.mutation<any, any>({
      query: ({ ID, data }) => ({
        url: `master-product/variant-image/${ID}`,
        method: "PATCH",
        body: data,
      }),
    }),

    PostVariationAddCategory: builder.mutation<any, any>({
      query: (data) => ({
        url: `variation/add/category`,
        method: "POST",
        body: data,
      }),
    }),

    // Product
    PostProduct: builder.mutation<any, any>({
      query: (data) => ({
        url: `/product`,
        method: "POST",
        body: data,
      }),
    }),

    GetIndividualShopProducts: builder.query<any, any>({
      query: (ID) => ({
        url: `product/shop/${ID}`,
        method: "GET",
      }),
    }),

    GetMyProducts: builder.query<any, any>({
      query: () => ({
        url: `product/my-products`,
        method: "GET",
      }),
    }),

    GetProductInventoryManagement: builder.query<any, any>({
      query: () => ({
        url: `product/inventory-management`,
        method: "GET",
      }),
    }),

    PostProductInventoryManagement: builder.mutation<any, any>({
      query: (body) => ({
        url: `product/inventory-management`,
        method: "POST",
        body: body,
      }),
    }),

    // Hot deal products
    PostHotDealProduct: builder.mutation<any, any>({
      query: (body) => ({
        url: `hot-deal`,
        method: "POST",
        body: body,
      }),
    }),
    UpdateHotDealProduct: builder.mutation<any, any>({
      query: ({ body, id }) => ({
        url: `hot-deal/${id}`,
        method: "PATCH",
        body: body,
      }),
    }),
    GetHotDealProduct: builder.query<any, any>({
      query: (id) => ({
        url: `hot-deal/${id}`,
        method: "GET",
      }),
    }),

    // Order
    PostOrderData: builder.mutation<any, any>({
      query: (body) => ({
        url: `order`,
        method: "POST",
        body: body,
      }),
    }),
    GetOrderById: builder.query<any, any>({
      query: (id) => ({
        url: `order/${id}`,
        method: "GET",
      }),
    }),

    GetMyOrder: builder.query<any, any>({
      query: () => ({
        url: `order/myorder`,
        method: "GET",
      }),
    }),
    UpdateOrder: builder.mutation<any, FormData>({
      query: (fd) => ({
        url: `order/${fd.get("id")}`,
        method: "PATCH",
        body: fd,
      }),
    }),
  }),
});

export const {
  useGetDataFromPostalCodeQuery,
  useCreateAccountMutation,
  useGetIualBranchDataQuery,
  useGetStaffDataQuery,
  usePostRoleForStaffMutation,
  usePostCheckForStaffMutation,
  useGetIndividualShopDataQuery,
  useAddShopDetailsMutation,
  useUpdateShopDetailsMutation,
  useGetBranchDataQuery,
  usePostBranchDetailsMutation,
  usePostShopDetailsMutation,
  useGetAllDataQuery,
  useGetAllDataByIdQuery,
  useGetPersonalKycVerifyReqDataQuery,
  useGetPersonalKycIndividualDataQuery,
  usePostPersonalKycStatusMutation,
  usePostPersonalKycRequestMutation,
  usePostBusinessKycRequestMutation,
  useGetBusinessKycVerifyReqDataQuery,
  useGetBusinessKycIndividualDataQuery,
  usePostBusinessKycStatusMutation,
  useGetShopDataQuery,
  useGetRoleForStaffQuery,
  useUnverifiedDataUpdateMutation,
  useGetPermissionDataQuery,
  useGetCategoryQuery,
  useGetSubCategoryQuery,
  useGetStaffStatusInvitationQuery,
  usePatchStaffInvitationActionMutation,
  useUpdateProfileMutation,
  useProfileUpdationMutation,
  useUpdateShopDetailsMerchantMutation,
  useGetIndividualBranchQuery,
  useGetStaffProfileDataQuery,
  usePostChangePasswordMutation,
  usePatchRoleForStaffMutation,
  usePostAddStaffMutation,

  ///master product
  usePostAddBrandMutation,
  useAddStaffInviteMutation,
  useGetStaffListQuery,
  useGetBrandDataQuery,
  useGetBrandDataLimitQuery,
  usePatchUpdateBrandMutation,
  usePostAddManufactureMutation,
  useGetManufactureDataQuery,
  useGetManufactureDataListQuery,
  usePatchUpdateManufactureMutation,
  useGetVariationDataQuery,
  useGetVariationDataListQuery,
  usePostAddVariationMutation,
  usePatchUpdateVariationMutation,
  usePostVariationTypeDataMutation,
  usePostProductDataMutation,
  useGetMasterProductDataQuery,
  useGetAllMasterProductDataQuery,
  usePostMasterProductDataMutation,
  usePostMasterProductImageMutation,
  useGetMasterECCategoryDataQuery,
  useGetMasterECCategoryDataListQuery,
  usePostAddMasterECCategoryMutation,
  usePostAddMasterCategoryMutation,
  useGetMasterCategoryDataQuery,
  useGetMasterCategoryDataLimitQuery,
  useUpdateIndividualBranchMutation,
  useUpdateBusinessKycMutation,
  useUpdateAddonMasterProductMutation,
  useUpdateMasterProductMutation,
  useGetSellingPriceCategoryQuery,
  useGetSingleMasterProductQuery,

  // Order
  usePostOrderDataMutation,
  useGetOrderByIdQuery,
  useGetMyOrderQuery,
  useUpdateOrderMutation,

  // Tax category
  useGetTaxCategoryDataQuery,

  // Hot Dal products
  usePostHotDealProductMutation,
  useUpdateHotDealProductMutation,
  useGetHotDealProductQuery,

  // variation
  usePostVariationWithProductMutation,
  usePostVariationImagesMutation,
  useGetVariationImagesQuery,
  useUpdateVariationImagesMutation,
  useGetVariationItemQuery,
  usePostVariationAddCategoryMutation,
  usePatchUpdateVariationWithProductMutation,

  // subscription
  // for Admin
  useGetSubscriptionDataQuery,
  usePostAddSubscriptionDataMutation,
  useGetIndividualSubscriptionDataQuery,
  useUpdateSubscriptionDataMutation,
  // for Merchant
  useGetMerchantSubscriptionDataQuery,
  usePostAddMerchantSubscriptionDataMutation,
  useGetMerchantIndividualSubscriptionDataQuery,
  useUpdateMerchantSubscriptionDataMutation,

  // Product
  usePostProductMutation,
  useGetIndividualShopProductsQuery,
  useGetMyProductsQuery,
  usePostProductInventoryManagementMutation,
  useGetProductInventoryManagementQuery,

  // faqs
  useGetFaqsDataQuery,
  useGetIndividualFaqsDataQuery,
  usePostAddFaqsDataMutation,
  useUpdateFaqsDataMutation,

  // review
  useGetReviewDataQuery,
  usePostReviewMutation,
  useGetProductReviewDataQuery,
  useGetShopReviewDataQuery,
  // notification
  useGetAllNotificationDataQuery,
  useUpdateReadNotificationDataMutation,
} = userApi;
