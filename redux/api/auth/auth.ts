// third party library
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const authApi = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.NEXT_PUBLIC_APP_BASE_URL,
  }),
  tagTypes: ["User"],
  endpoints: (builder) => ({
    //login for merchant
    login: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/merchant/login",
        method: "POST",
        body: data,
      }),
    }),
    //login for customer
    loginUser: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/user/login",
        method: "POST",
        body: data,
      }),
    }),

    forgotPassword: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/reset-password",
        method: "POST",
        body: data,
      }),
    }),
    resetPassword: builder.mutation<any, any>({
      query: (data) => ({
        url: `auth/reset-password`,
        method: "PATCH",
        body: {
          code: data.code,
          email: data.email,
          password: data.password,
          bId: data.bId,
        },
      }),
    }),

    verifyOTP: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/verification/",
        method: "POST",
        body: data,
      }),
    }),

    register: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/register",
        method: "POST",
        body: data,
      }),
    }),
    customeresgistration: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/register",
        method: "POST",
        body: data,
      }),
    }),
    checkmailmerchant: builder.mutation<any, any>({
      query: (data) => ({
        url: "merchant/check-email",
        method: "POST",
        body: data,
      }),
    }),
    createbusinessaccount: builder.mutation<any, any>({
      query: (data) => ({
        url: "merchant/register",
        method: "POST",
        body: data,
      }),
    }),
    verifyEmailandCode: builder.mutation<any, any>({
      query: (data) => ({
        url: "auth/verification",
        method: "POST",
        body: data,
      }),
    }),

    changeEmail: builder.mutation<any, any>({
      query: (data) => ({
        url: "user/change-email",
        method: "POST",
        body: data,
      }),
    }),

    resendOtp: builder.mutation<any, any>({
      query: (body) => ({
        url: "auth/reset-otp",
        method: "POST",
        body: body,
      }),
    }),

    SearchCEO: builder.query<any, any>({
      query: (email) => ({
        url: `user/search-ceo/${email}`,
        method: "GET",
      }),
    }),

    AdminoCeo: builder.mutation<any, any>({
      query: (data) => ({
        url: "merchant/is-ceo",
        method: "POST",
        body: data,
      }),
    }),

    UsedCountries: builder.query<any, void>({
      query: () => ({
        url: `user/top-nationality`,
      }),
    }),
  }),
});

export const {
  useSearchCEOQuery,
  useResendOtpMutation,
  useChangeEmailMutation,
  useVerifyEmailandCodeMutation,
  useCreatebusinessaccountMutation,
  useCheckmailmerchantMutation,
  useCustomeresgistrationMutation,
  useLoginMutation,
  useRegisterMutation,
  useForgotPasswordMutation,
  useVerifyOTPMutation,
  useResetPasswordMutation,
  useLoginUserMutation,
  useAdminoCeoMutation,
  useUsedCountriesQuery,
} = authApi;
