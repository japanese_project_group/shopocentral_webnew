// Build in library
import Router from "next/router";

// Third party library
import {
  BaseQueryFn,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from "@reduxjs/toolkit/query/react";

// crypto js
import { getDecryptedData } from "../src/common/utility";
// Redux
import { authSlice } from "./features/authSlice";
import { RootState } from "./store";


const baseQuery = fetchBaseQuery({
  baseUrl: process.env.NEXT_PUBLIC_APP_BASE_URL,
  prepareHeaders: (headers, { getState }) => {
    const token =
      (getState() as RootState).auth.token || getDecryptedData(localStorage.getItem("token"));

    if (token) {
      headers.set("Authorization", `Bearer ${token}`);
      headers.set("Access-Control-Allow-Headers", "*");
      headers.set("Access-Control-Allow-Credentials", "true");
    }
    return headers;
  },
});

export const baseQueryWithReauth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  let result: any = await baseQuery(args, api, extraOptions);
  if (result?.error && result?.error?.originalStatus === 401) {
    authSlice.actions.logoutUser();
    localStorage.removeItem("token");
    localStorage.removeItem("persist:root");
    Router.push("/login");
  }
  if (result.error) {
    //alert('token blank');
  }

  return result;
};
