// third party library
import { createSlice } from "@reduxjs/toolkit";

export interface profileDataState {
  profileData: any;
}
const initialState: profileDataState = {
  profileData: null,
};

export const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setProfile(state, action) {
      state.profileData = action?.payload;
    },
    removeProfile(state) {
      state.profileData = null;
    },
  },
});

export const { setProfile, removeProfile } = profileSlice.actions;
export default profileSlice.reducer;
