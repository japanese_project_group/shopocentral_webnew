// third party library
import { createSlice } from "@reduxjs/toolkit";

// types
import { PersonalKYCData } from "../../src/@types/user";

// Type for our state
export interface RegisterProcess {
  personalKycData: PersonalKYCData | null;
}
// Initial state
const initialState: RegisterProcess = {
  personalKycData: null,
};
// Actual Slice
export const registerProcessSlice = createSlice({
  name: "registerProcess",
  initialState,
  reducers: {
    setPersonalKycData(state, action) {
      state.personalKycData = action?.payload?.data;
    },
    removePersonalKycData(state) {
      state.personalKycData = null;
    },
  },
});

export const { setPersonalKycData, removePersonalKycData } =
  registerProcessSlice.actions;
export default registerProcessSlice.reducer;
