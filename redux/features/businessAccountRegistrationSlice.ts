import { createSlice } from "@reduxjs/toolkit";

import { BusinessAccountRegistrationData } from "../../src/@types/user";

// Type for our state
export interface BusinessAccountRegistration {
    businessAccountRegistrationData: BusinessAccountRegistrationData | null
}
// Initial state
const initialState: BusinessAccountRegistration = {
    businessAccountRegistrationData: null
};
// Actual Slice
export const businessAccountRegistrationSlice = createSlice({
    name: "businessAccountRegistration",
    initialState,
    reducers: {
        setBusinessAccountRegistrationData(state, action) {
            state.businessAccountRegistrationData = action?.payload;
        },  
        removeBusinessAccountRegistrationData(state) {
            state.businessAccountRegistrationData = null;
        },
    },
});

export const { setBusinessAccountRegistrationData, removeBusinessAccountRegistrationData } = businessAccountRegistrationSlice.actions;
export default businessAccountRegistrationSlice.reducer;
