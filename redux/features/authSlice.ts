// third party library
import { createSlice } from "@reduxjs/toolkit";

// types
import { userData } from "../../src/@types/user";

// Type for our state
export interface AuthState {
  token: string;
  userDetail: userData | null;
}

// Initial state
const initialState: AuthState = {
  token: "",
  userDetail: null,
};

// Actual Slice
export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    // Action to set the authentication status
    setAuthState(state, action) {
      state.userDetail = action?.payload?.data;
      state.token = action?.payload?.access_token;
    },
    logoutUser(state) {
      state.userDetail = null;
      state.token = "";
    },
  },
});

export const { setAuthState, logoutUser } = authSlice.actions;
export default authSlice.reducer;
