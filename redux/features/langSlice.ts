// third party library
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  lang: "",
};
// Actual Slice
export const langSlice = createSlice({
  name: "lang",
  initialState,
  reducers: {
    // Action to set lang
    setLang(state, action) {
      state.lang = action?.payload;
    },
  },
});

export const { setLang } = langSlice.actions;
export default langSlice.reducer;
