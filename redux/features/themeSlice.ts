// third party library
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  theme: "",
};
// Actual Slice
export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    // Action to set lang
    setTheme(state, action) {
      state.theme = action?.payload;
    },
  },
});

export const { setTheme } = themeSlice.actions;
export default themeSlice.reducer;
