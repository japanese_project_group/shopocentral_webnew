// third party library
import { createSlice } from "@reduxjs/toolkit";

interface kycDataState {
  kycData: any;
}
const initialState: kycDataState = {
  kycData: null,
};
export const KycDataSlice = createSlice({
  name: "kycData",
  initialState,
  reducers: {
    setKycData(state, action) {
      state.kycData = action?.payload;
    },
    removeKycData(state) {
      state.kycData = null;
    },
  },
});

export const { setKycData, removeKycData } = KycDataSlice.actions;
export default KycDataSlice.reducer;
