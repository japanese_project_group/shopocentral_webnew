// Third party library
// @ts-ignore
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  persistReducer,
} from "redux-persist";
import { encryptTransform } from "redux-persist-transform-encrypt";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";

// Redux
import { authApi } from "./api/auth/auth";
import { userApi } from "./api/user/user";
import { authSlice } from "./features/authSlice";
import { KycDataSlice } from "./features/kycDataSlice";
import { langSlice } from "./features/langSlice";
import { profileSlice } from "./features/profileSlice";
import { registerProcessSlice } from "./features/registerProcessSlice";
import { themeSlice } from "./features/themeSlice";

const EncryptTransform = encryptTransform({
  secretKey: "my-super-secret-key",
  onError: function (error) {
    // Handle the error.
  },
});

const createNoopStorage = () => {
  return {
    getItem(_key: any) {
      return Promise.resolve(null);
    },
    setItem(_key: any, value: any) {
      return Promise.resolve(value);
    },
    removeItem(_key: any) {
      return Promise.resolve();
    },
  };
};
const storage =
  typeof window !== "undefined"
    ? createWebStorage("local")
    : createNoopStorage();

const rootReducer = combineReducers({
  [authApi.reducerPath]: authApi.reducer,
  [authSlice.name]: authSlice.reducer,
  [langSlice.name]: langSlice.reducer,
  [themeSlice.name]: themeSlice.reducer,
  [profileSlice.name]: profileSlice.reducer,
  [KycDataSlice.name]: KycDataSlice.reducer,
  [registerProcessSlice.name]: registerProcessSlice.reducer,
  [userApi.reducerPath]: userApi.reducer,
});

const persistedReducer = persistReducer(
  {
    key: "root",
    storage: storage,
    whitelist: [
      "auth",
      "lang",
      "theme",
      "profile",
      "kycData",
      "registerProcess",
    ],
    transforms: [EncryptTransform],
  },
  rootReducer
);
export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(authApi.middleware, userApi.middleware),
});

// @ts-ignore
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
//TEST
