import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
  spacing: 4,
  palette: {
    // primary: {
    //   main: "#0B0B0B",
    // },
    // secondary: {
    //   main: "#E70505",
    // },
    background: {
      default: "#F5F5F5",
    },
    // success: {
    //   main: "#1a9d4e",
    // },
    // error: {
    //   main: "#FF5959",
    //   dark: "#D50000",
    //   light: "#FF84B7",
    // },
    // info: {
    //   main: "#458CF8",
    // },
  },
  typography: {
    fontWeightLight: 500,
    fontWeightRegular: 500,
    fontWeightBold: 500,
    fontFamily: 'Poppins',
    h3: {
      fontWeight: 500,

    },

  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: 14,
          fontFamily: ['Poppins', 'sans-serif'].join(','),
          fontWeight: '500',
        },
      },
    },
  }


});

export default theme;
