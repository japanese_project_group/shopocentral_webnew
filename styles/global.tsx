import GlobalStyles from '@mui/material/GlobalStyles';
import theme from './theme/defaultTheme';

const Styles = () => {

    return (
        <GlobalStyles
            styles={{
                "& .menu-dashboard": {
                    [theme.breakpoints.only('xs')]: {
                        "display": "block !important"
                    }
                }
            }}
        />
    );
};

export default Styles;